/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.throttle

import java.io.OutputStream

interface OutputStreamThrottleController {

    fun register(outputStream: OutputStream): OutputStream

    fun write(source: ThrottledOutputStream, buffer: ByteArray?, offset: Int, length: Int)

    fun limitWriteRate(bytesPerSecond: Int)
}