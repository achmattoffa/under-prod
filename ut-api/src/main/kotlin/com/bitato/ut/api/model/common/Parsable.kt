/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.common

interface Parsable {

    fun parse(): ByteArray
}