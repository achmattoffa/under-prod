/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks5

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.socks.SocksUtils
import java.nio.ByteBuffer
import java.nio.ByteOrder

class Socks5Request(cmd: Byte = 1,
                    addressType: Byte = 1,
                    address: ByteArray = ByteArray(4),
                    var port: Short) : Parsable {

    var cmd: Byte = 1
        set(value) {
            when (value) {
                CONNECT, BIND, UDP_ASSOCIATE -> field = value
            }
        }

    var addressType: Byte = 1
        set(addressType) {
            when (addressType) {
                IPV4_ADDR, DOMAIN_NAME, IPV6_ADDR -> field = addressType
            }
        }

    var address: ByteArray

    init {
        this.cmd = cmd
        this.addressType = addressType
        this.address = address
    }

    fun getAddressAsString(): String {
        var destAddress = when (addressType) {
            IPV4_ADDR -> {
                SocksUtils.byteArrayToIpV4AddressAsString(address)
            }
            DOMAIN_NAME -> {
                val bytes = address.copyOfRange(1, address.size)
                String(bytes, Charsets.ISO_8859_1)
            }
            else -> {
                //TODO
                ""
            }
        }

        return destAddress
    }

    override fun parse(): ByteArray {

        val buffer = ByteBuffer.allocate(1024)
          .order(ByteOrder.BIG_ENDIAN)
          .put(0x05)
          .put(cmd)
          .put(0x00)
          .put(addressType)

        if (addressType == DOMAIN_NAME) {
            buffer.put(address.size.toByte())
        }

        return buffer
          .put(address)
          .putShort(port)
          .array()
    }


    companion object {
        val BIND: Byte = 2
        val CONNECT: Byte = 1
        val DOMAIN_NAME: Byte = 3
        val IPV4_ADDR: Byte = 1
        val IPV6_ADDR: Byte = 4
        val UDP_ASSOCIATE: Byte = 3
    }
}
