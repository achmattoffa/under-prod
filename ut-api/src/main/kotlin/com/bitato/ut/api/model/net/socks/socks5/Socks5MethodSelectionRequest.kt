/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks5

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer
import java.nio.ByteOrder

class Socks5MethodSelectionRequest(var numMethods: Byte = 1, var methods: ByteArray) : Parsable {

    fun hasMethod(b: Byte): Boolean {
        return methods.contains(b)
    }

    override fun parse(): ByteArray {
        return ByteBuffer.allocate(2 + methods.size)
          .order(ByteOrder.BIG_ENDIAN)
          .put(0x05)
          .put(numMethods)
          .put(methods)
          .array()
    }

    object Methods {

        val NO_AUTHENTICATION_REQUIRED: Byte = 0X00
        val GSSAPI: Byte = 0X01
        val USERNAME_PASSWORD: Byte = 0X02
    }
}


