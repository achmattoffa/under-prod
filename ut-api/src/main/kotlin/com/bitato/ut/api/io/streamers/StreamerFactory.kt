/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.streamers

import java.io.InputStream
import java.io.OutputStream


interface StreamerFactory {

    enum class StreamerType {
        CHUNK_ENCODING, CONTENT_LENGTH, INDEFINITE
    }

    fun create(inputStream: InputStream, outputStream: OutputStream, streamerType: StreamerType): Streamer
}
