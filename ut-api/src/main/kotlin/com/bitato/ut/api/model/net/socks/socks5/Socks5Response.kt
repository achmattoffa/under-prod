/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks5

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.socks.SocksUtils
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

class Socks5Response(reply: Byte = 0, addressType: Byte = 1, address: ByteArray = ByteArray(4), port: Short = 0): Parsable {

    var reply: Byte = 0
        set(value) {
            if (STATUS_CODES.none { it == value })
                throw IllegalArgumentException("Illegal reply code, $value")

            field = value
        }

    var addressType: Byte = 1
        set(value) {
            when (value) {
                IPV4_ADDR, DOMAIN_NAME, IPV6_ADDR -> field = value
                else -> throw IllegalArgumentException("Illegal address type, $value")
            }
        }

    var address: ByteArray
    var port: Short

    init {
        this.reply = reply
        this.addressType = addressType
        this.address = address
        this.port = port
    }

    fun setBindAddress(address: String) {
        if (addressType == IPV4_ADDR) {
            this.address = SocksUtils.ipV4ToByteArray(address)
        } else if (addressType == DOMAIN_NAME) {
            val len = address.length

            this.address = ByteArray(len + 1)
            this.address[0] = len.toByte()

            for (i in 0..len - 1) {
                this.address[i + 1] = address[i].toByte()
            }
        } else {
            //TODO IPV6
        }
    }

    fun getBindAddressAsString(): String {
        val address = if (addressType == IPV4_ADDR) {
            SocksUtils.byteArrayToIpV4AddressAsString(address)
        } else if (addressType == DOMAIN_NAME) {
            val tmp = Arrays.copyOfRange(address, 1, address.size)
            String(tmp, Charsets.ISO_8859_1)
        } else {
            //todo IPV6
            ""
        }

        return address
    }

    override fun parse(): ByteArray {
        val buffer = ByteBuffer.allocate(6 + address.size)
          .order(ByteOrder.BIG_ENDIAN)
          .put(0x05)
          .put(reply)
          .put(0x00)
          .put(addressType)

        if (addressType == DOMAIN_NAME) {
            buffer.put(address.size.toByte())
        }

        return buffer
            .put(address)
            .putShort(port)
            .array()
    }

    companion object {
        val SUCCEEDED: Byte = 0
        val SERVER_FAILURE: Byte = 1
        val CONNECTION_NOT_ALLOWED: Byte = 2
        val NETWORK_UNREACHABLE: Byte = 3
        val HOST_UNREACHABLE: Byte = 4
        val CONNECTION_REFUSED: Byte = 5
        val TTL_EXPIRED: Byte = 6
        val CMD_NOT_SUPPORTED: Byte = 7
        val ADDRESS_TYPE_NOT_SUPPORTED: Byte = 8

        val STATUS_CODES: Array<Byte> = arrayOf(
                SUCCEEDED, SERVER_FAILURE, CONNECTION_NOT_ALLOWED,
                NETWORK_UNREACHABLE, HOST_UNREACHABLE, CONNECTION_REFUSED,
                TTL_EXPIRED, CMD_NOT_SUPPORTED, ADDRESS_TYPE_NOT_SUPPORTED)

        val IPV4_ADDR: Byte = 1
        val DOMAIN_NAME: Byte = 3
        val IPV6_ADDR: Byte = 4
    }
}
