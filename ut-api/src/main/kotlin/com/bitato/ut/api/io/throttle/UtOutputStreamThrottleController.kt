/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.throttle

import com.bitato.ut.api.model.common.AtomicCounter
import java.io.OutputStream
import java.util.concurrent.atomic.AtomicLong

class UtOutputStreamThrottleController(private var bytesPerSecond: Int = 102400000)
: OutputStreamThrottleController {

    companion object {
        const private val TIME_INTERVAL_MILLIS = 1000
    }

    private val counter = AtomicCounter(AtomicLong(0))
    private var timestamp = System.currentTimeMillis()

    override fun register(outputStream: OutputStream): OutputStream {
        return ThrottledOutputStream(outputStream, this)
    }

    override fun limitWriteRate(bytesPerSecond: Int) {
        this.bytesPerSecond = bytesPerSecond
    }

    @Synchronized
    override fun write(source: ThrottledOutputStream, buffer: ByteArray?, offset: Int, length: Int) {
        acquireLock()
        writeStream(buffer, offset, length, source)
    }

    private fun writeStream(buffer: ByteArray?, offset: Int, length: Int, source: ThrottledOutputStream) {
        val allowedWriteLength = bytesPerSecond - counter.value().toInt()

        val bytesLeft = if (length - offset >= allowedWriteLength) {
            source.reallyWrite(buffer, offset, allowedWriteLength)
            length - offset - allowedWriteLength
        } else {
            source.reallyWrite(buffer, offset, length)
            0
        }

        buffer?.let {
            if (length >= 0) {
                counter += length - bytesLeft
            }
        }

        while (bytesLeft > 0) {
            write(source, buffer, offset + allowedWriteLength, bytesLeft)
        }
    }

    private fun acquireLock() {
        if (counter.value() >= bytesPerSecond) {
            val now = System.currentTimeMillis()
            if (timestamp + TIME_INTERVAL_MILLIS >= now) {
                Thread.sleep(timestamp + TIME_INTERVAL_MILLIS - now)
            }

            timestamp = System.currentTimeMillis()
            counter.reset()
        }
    }
}