/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.streamers

interface Streamer {

    fun stream(length: Number = 0)
}
