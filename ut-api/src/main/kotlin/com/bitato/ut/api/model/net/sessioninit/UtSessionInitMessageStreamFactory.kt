/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sessioninit

import com.bitato.ut.api.io.readByte
import com.bitato.ut.api.io.readString
import com.bitato.ut.api.model.net.NetworkProtocol
import java.io.InputStream

object UtSessionInitMessageStreamFactory : SessionInitMessageStreamFactory {

    override fun createSessionInitRequest(iStream: InputStream): SessionInitRequest {
        val region = iStream.readString()
        val proto = NetworkProtocol.valueOf(iStream.readString())
        val port = iStream.readString().toInt()

        val username = iStream.readString()
        val password = iStream.readString()

        return SessionInitRequest(region, proto, port, username, password)
    }

    override fun InputStream.toSessionInitRequest(): SessionInitRequest {
        return createSessionInitRequest(this)
    }

    override fun createSessionInitResponse(iStream: InputStream): SessionInitResponse {

        val status = iStream.readByte()

        if (status == SessionInitResponse.ResponseCodes.ACCESS_GRANTED) {
            val uuid = iStream.readString()
            val addressType = iStream.readByte()
            val address = iStream.readString()

            val host = address.split(":")[0]
            val port = address.split(":")[1].toInt()
            //val port = 80

            return SessionInitResponse(status, addressType, host, port, uuid)
        } else {
            return SessionInitResponse(status)
        }
    }

    override fun InputStream.toSessionInitResponse(): SessionInitResponse {
        return createSessionInitResponse(this)
    }
}