import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.HttpResponse

/*
 * Copyright (c) 2016. Under Tunnel
 */

object HttpCommon {

    object Responses {
        val OKAY = HttpResponse("HTTP/1.1 200 Okay\r\nConnection: Keep-Alive\r\n\r\n")
    }

    object Requests {

        val DUMMY_CONNECT = HttpRequest("CONNECT mail.google.com:443 HTTP/1.1\r\nHost: mail.google.com:443\r\nConnection: Keep-Alive\r\n\r\n")
    }

    val EOL = "\r\n"
    val END_OF_MESSAGE = "\r\n\r\n"
}