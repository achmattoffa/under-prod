/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks5

import com.bitato.ut.api.io.readExactly
import java.io.DataInputStream
import java.io.IOException
import java.io.InputStream

object Socks5Factory {
    fun createRequestHeader(iStream: InputStream): Socks5Request {

        val dis = DataInputStream(iStream)

        val version = dis.readByte()
        val cmd = dis.readByte()
        val reserved = dis.readByte()
        val addressType = dis.readByte()

        dis.mark(1)
        val len = getAddressLength(dis, addressType)
        dis.reset()

        if (len == 0) {
            throw IOException(/*TODO*/)
        }

        val address = dis.readExactly(len)
        val port = dis.readShort()

        return Socks5Request(cmd, addressType, address, port)
    }

    fun createResponseHeader(iStream: InputStream): Socks5Response {

        val dis = DataInputStream(iStream)

        val version = dis.readByte()
        val reply = dis.readByte()
        val reserved = dis.readByte()
        val addressType = dis.readByte()

        val len = getAddressLength(dis, addressType)

        if (len == 0) {
            throw IOException(/*TODO*/)
        }

        val address = dis.readExactly(len)
        val port = dis.readShort()

        return Socks5Response(reply, addressType, address, port)
    }

    private fun getAddressLength(iStream: InputStream, addressType: Byte): Int {
        val length = when (addressType) {
            Socks5Request.IPV4_ADDR -> 4
            Socks5Request.DOMAIN_NAME -> iStream.read() + 1
            Socks5Request.IPV6_ADDR -> 16
            else -> 0
        }

        return length
    }

    fun createMethodSelectionRequest(inputStream: InputStream): Socks5MethodSelectionRequest {

        val dis = DataInputStream(inputStream)

        val version = dis.readByte()
        val numMethods = dis.readByte()
        val methods = dis.readExactly(numMethods.toInt())

        return Socks5MethodSelectionRequest(numMethods, methods)
    }

    fun createMethodSelectionResponse(inputStream: InputStream): Socks5MethodSelectionResponse {

        val dis = DataInputStream(inputStream)

        val version = dis.readByte()
        val selectedMethod = dis.readByte()
        return Socks5MethodSelectionResponse(selectedMethod)
    }

    fun createUsernamePasswordAuthenticationResponse(inputStream: InputStream): Socks5UsernamePasswordAuthenticationResponse {

        val dis = DataInputStream(inputStream)

        val version = dis.readByte()
        val responseCode = dis.readByte()

        return Socks5UsernamePasswordAuthenticationResponse(responseCode)
    }

    fun createUsernamePasswordAuthenticationRequest(inputStream: InputStream): Socks5UsernamePasswordAuthenticationRequest {

        val dis = DataInputStream(inputStream)

        val version = dis.readByte()

        val usernameLen = dis.readByte()
        val username = String(dis.readExactly(usernameLen.toInt()), Charsets.ISO_8859_1)

        val passwordLen = dis.readByte()
        val password = String(dis.readExactly(passwordLen.toInt()), Charsets.ISO_8859_1)

        return Socks5UsernamePasswordAuthenticationRequest(username, password)
    }
}