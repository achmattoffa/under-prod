/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sessioninit

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.NetworkProtocol
import java.nio.ByteBuffer

class SessionInitRequest(var region: String, var protocol: NetworkProtocol, var port: Int, var username: String, var password: String, var bitTorrentServer: Byte = 0) : Parsable {

    override fun parse(): ByteArray {

        val proto = protocol.name
        val portStr = port.toString()

        return ByteBuffer.allocate(6 + region.length + proto.length + portStr.length + username.length + password.length)
          .put(region.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(proto.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(portStr.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(username.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(password.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(bitTorrentServer)
          .array()
    }
}