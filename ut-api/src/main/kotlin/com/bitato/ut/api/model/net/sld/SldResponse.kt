/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sld

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer


class SldResponse(var status: Byte, var addressType: Byte = 0, var address: String = "", var port: Int = 80) : Parsable {

    object ResponseCodes {
        val SUCCESS: Byte = 0x01b
        val FAIL: Byte = 0x02b
        val NO_SERVERS_IN_REGION: Byte = 0x03b
        val UNAUTHORIZED: Byte = 0x04b
    }

    object AddressTypes {
        val HOSTNAME: Byte = 0x01b
        val IP_V4: Byte = 0x02b
        val IP_V6: Byte = 0x03b
    }

    override fun parse(): ByteArray {
        return ByteBuffer.allocate(3 + "$address:$port".length)
                .put(status)
                .put(addressType)
                .put("$address:$port".toByteArray(Charsets.ISO_8859_1))
                .put(Character.UNASSIGNED)
                .array()
    }
}