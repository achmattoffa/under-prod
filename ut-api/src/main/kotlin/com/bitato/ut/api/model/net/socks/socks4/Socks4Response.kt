/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks4

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.socks.SocksUtils
import java.nio.ByteBuffer

class Socks4Response(status: Byte = Socks4Response.REQUEST_REJECTED,
                     var port: ByteArray = ByteArray(2),
                     var ipAddress: ByteArray = ByteArray(4)): Parsable {

    private val nullByte: Byte = 0

    var status: Byte = REQUEST_REJECTED
        set(value) {
            if (isValidResponseCode(value))
                field = value
            else
                throw IllegalArgumentException("Invalid response code: $status")
        }

    init {
        this.status = status
    }

    constructor(bytes: ByteArray)
    : this() {
        setSocksResponse(bytes)
    }


    private fun isValidResponseCode(code: Byte): Boolean {
        return (code == REQUEST_GRANTED
                || code == REQUEST_REJECTED
                || code == CLIENT_UNREACHABLE
                || code == CANNOT_CONFIRM_USER_ID)
    }

    fun getAddress(): String {
        return SocksUtils.byteArrayToIpV4AddressAsString(ipAddress)
    }

    fun getPort(): Int {
        return SocksUtils.portArrayToInt(port)
    }

    fun setPort(port: Int) {
        this.port = SocksUtils.portToByteArray(port)
    }

    fun setIpAddress(ipAddr: String) {
        ipAddress = SocksUtils.ipV4ToByteArray(ipAddr)
    }

    override fun parse(): ByteArray {
        val buf = ByteBuffer.allocate(8)
        return buf.put(nullByte)
                .put(status)
                .put(port)
                .put(ipAddress)
                .array()
    }

    private fun setSocksResponse(response: ByteArray) {
        status = response[1]
        port = response.copyOfRange(2, 4)
        ipAddress = response.copyOfRange(4, 8)
    }

    companion object {
        val CANNOT_CONFIRM_USER_ID: Byte = 93
        val CLIENT_UNREACHABLE: Byte = 92
        val REQUEST_GRANTED: Byte = 90
        val REQUEST_REJECTED: Byte = 91
    }
}
