/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.events

import java.util.*

abstract class UtAbstractEventDispatcher<T> {

    private val eventListeners: MutableList<T>

    init {
        eventListeners = ArrayList()
    }

    @Synchronized
    open fun attachListener(listener: T) {
        if (!eventListeners.contains(listener)) {
            eventListeners.add(listener)
        }
    }

    @Synchronized
    open fun detachListener(listener: T) {
        if (eventListeners.contains(listener)) {
            eventListeners.remove(listener)
        }
    }

    @Synchronized
    protected fun <R> event(fn: (T) -> R) {
        if (eventListeners.isNotEmpty()) {
            eventListeners.toList().forEach {
                fn.invoke(it)
            }
        }
    }
}