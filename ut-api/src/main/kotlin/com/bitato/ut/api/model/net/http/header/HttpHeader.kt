/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http.header

import HttpCommon
import com.bitato.ut.api.util.empty


class HttpHeader(name: String = String.empty(), value: String = String.empty()) {

    var headerName: String = name
        set(value) {
            if (isHeaderNameValid(value)) {
                field = value
            } else {
                throw IllegalHeaderException("Invalid HTTP Header name: $value")
            }
        }

    var headerValue: String = value
        set(value) {
            if (isHeaderValueValid(value)) {
                field = value
            } else {
                throw IllegalHeaderException("Invalid HTTP Header value: $value")
            }
        }

    override fun toString(): String {
        return "$headerName: $headerValue${HttpCommon.EOL}"
    }

    override fun equals(other: Any?): Boolean {
        other?.let {
            if (it is HttpHeader) {
                return headerName.equals(it.headerName, true) && headerValue.equals(it.headerValue, true)
            }
        }

        return super.equals(other)
    }

    override fun hashCode(): Int{
        return 0
    }

    fun isHeaderNameValid(name: String): Boolean {
        return !name.matches("(.*(:|${HttpCommon.EOL}).*| +)".toRegex())
    }

    fun isHeaderValueValid(value: String): Boolean {
        //return !value.matches("(.*(${HttpCommon.EOL}).*| +)".toRegex())
        return true
    }
}
