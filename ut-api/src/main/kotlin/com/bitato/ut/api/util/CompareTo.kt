/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.util

import com.bitato.ut.api.io.copyTo
import com.bitato.ut.api.model.common.Parsable
import java.io.InputStream
import java.io.OutputStream

operator fun OutputStream.compareTo(str: String): Int {
    this.write(str.toByteArray(Charsets.ISO_8859_1))
    return 0
}

operator fun OutputStream.compareTo(bytes: ByteArray): Int {
    this.write(bytes)
    return 0
}

operator fun OutputStream.compareTo(b: Byte): Int {
    this.write(b.toInt())
    return 0
}

operator fun OutputStream.compareTo(inputStream: InputStream): Int {
    inputStream.copyTo(this)
    return 0
}

operator fun OutputStream.compareTo(int: Int): Int {
    this.write(int)
    return 0
}

operator fun OutputStream.compareTo(parsable: Parsable): Int {
    this.write(parsable.parse())
    return 0
}

operator fun ByteArray.compareTo(iStream: InputStream): Int {
    return iStream.read(this)
}

operator fun InputStream.compareTo(pair: Pair<OutputStream, Long>): Int {
    copyTo(pair.first, pair.second)
    return 0
}