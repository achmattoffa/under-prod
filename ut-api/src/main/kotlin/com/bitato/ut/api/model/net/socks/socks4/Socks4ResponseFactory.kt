/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks4

import com.bitato.ut.api.io.readExactly
import java.io.InputStream

object Socks4ResponseFactory {

    fun createResponseHeader(iStream: InputStream): Socks4Response {
        val bytes = iStream.readExactly(8)
        return Socks4Response(bytes)
    }
}