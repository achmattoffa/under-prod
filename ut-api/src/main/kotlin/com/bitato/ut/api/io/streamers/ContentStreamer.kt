/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.streamers

import com.bitato.ut.api.util.compareTo
import java.io.InputStream
import java.io.OutputStream

open class ContentStreamer(private val inputStream: InputStream, private val outputStream: OutputStream)
: Streamer {

    /**
     * Streams bytes from iStream to oStream. If the length provided is < 0, It copy bytes until the
     * stream returns -1. If the length parameter is greater than 0. up to length bytes will be
     * copied over
     *
     * @param length - The number of bytes to stream, or -1 for indefinite
     *
     * @throws java.io.IOException
     */
    override fun stream(length: Number) {
        if (length as Long >= 0) {
            inputStream < outputStream to length
        } else {
            outputStream < inputStream
        }
    }
}


