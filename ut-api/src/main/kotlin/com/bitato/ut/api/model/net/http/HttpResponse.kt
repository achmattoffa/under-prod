/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.http.header.HttpHeaderList

class HttpResponse() : HttpResponseLine(), Parsable {

    var headers: HttpHeaderList = HttpHeaderList()

    constructor(response: CharSequence) : this() {
        val split = response.toString().trim().split("\r\n".toRegex(), 2)
        setResponseLine(split[0])
        if (split.size == 2)
            headers = HttpHeaderList(split[1])
    }

    fun getContentLength(): Long {
        return if (hasContentLength()) {
            java.lang.Long.parseLong(headers.getValueOf("Content-Length").trim())
        } else {
            -1
        }
    }

    override fun parse(): ByteArray {
        return "${getResponseLine()}\r\n$headers\r\n".toByteArray(Charsets.ISO_8859_1)
    }

    fun hasChunkedBody(): Boolean {
        return headers.getValueOf("Transfer-Encoding")
          .toLowerCase()
          .matches(".*chunked.*".toRegex())
    }

    fun hasContentLength(): Boolean {
        return headers.contains("Content-Length")
    }

    fun hasMessageBody(): Boolean {
        return !code.matches("^(1..|204|304)".toRegex())
    }

    override fun toString(): String {
        return "${getResponseLine()}\r\n$headers\r\n"
    }

    fun getContentDeliveryMechanism(): HttpContentDeliveryMechanism {
        return if (hasContentLength()) {
            HttpContentDeliveryMechanism.CONTENT_LENGTH
        }
        else if (this.hasChunkedBody()) {
            HttpContentDeliveryMechanism.CHUNKED
        }
        else if (this.hasMessageBody()) {
            HttpContentDeliveryMechanism.INDEFINITE
        }
        else {
            HttpContentDeliveryMechanism.NONE
        }
    }

    operator fun get(header: String): String {
        return this.headers.getValueOf(header)
    }

    operator fun set(header: String, value: String) {
        return this.headers.set(header, value)
    }
}
