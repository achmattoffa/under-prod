/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.throttle

import java.io.InputStream

class ThrottledInputStream(private val original: InputStream, private val throttleController: InputStreamThrottleController)
: InputStream() {

    override fun read(): Int {

        val buffer = ByteArray(1)
        throttleController.read(this, buffer, 0, 1)
        return buffer[0].toInt()
    }

    override fun read(buffer: ByteArray?): Int {
        return if (buffer != null) {
            throttleController.read(this, buffer, 0, buffer.size)
        } else {
            0
        }
    }

    override fun read(buffer: ByteArray?, offset: Int, length: Int): Int {
        return throttleController.read(this, buffer, offset, length)
    }

    fun reallyRead(buffer: ByteArray?, offset: Int, length: Int): Int  {
        return  original.read(buffer, offset, length)
    }
}

fun InputStream.throttle(controller: InputStreamThrottleController): InputStream {
    return ThrottledInputStream(this, controller)
}