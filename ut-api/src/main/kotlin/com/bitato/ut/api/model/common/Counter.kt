/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.model.common

interface Counter {
    fun reset()
    /**
     * +=
     */
    operator fun plusAssign(value: Number)
    fun value(): Long
}