/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks4

import com.bitato.ut.api.model.net.socks.SocksUtils
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.nio.ByteBuffer

object Socks4RequestFactory {

    fun createRequestHeader(iStream: InputStream): Socks4Request {
        val buf = readSocks4Request(iStream)
        return makeSocksRequest(buf, iStream)
    }

    private fun readSocks4Request(iStream: InputStream): ByteArray {

        val buf = ByteArrayOutputStream()

        var byte = iStream.read()
        if (byte != 4)
            buf.write(4)

        buf.write(byte)

        byte = iStream.read()
        while (byte >= 0) {
            buf.write(byte)
            if (byte == 0 && buf.size() > 8) {
                break
            }

            byte = iStream.read()
        }

        return buf.toByteArray()
    }

    private fun makeSocksRequest(bytes: ByteArray, iStream: InputStream): Socks4Request {

        return if (isSocks4ARequest(bytes)) {
            createSocks4ARequest(iStream, bytes)
        } else {
            createSocks4Request(bytes)
        }
    }

    private fun createSocks4Request(requestBytes: ByteArray): Socks4Request {
        return Socks4Request(requestBytes)
    }

    private fun createSocks4ARequest(iStream: InputStream, request: ByteArray): Socks4ARequest {
        val host = iStream.readUntil("${0.toChar()}")
        val buf = ByteBuffer.allocate(request.size + 1)
                .put(request)
                .put(0)

        return Socks4ARequest(buf.array(), host)
    }

    private fun isSocks4ARequest(bytes: ByteArray): Boolean {
        val ip = SocksUtils.byteArrayToIpV4AddressAsString(bytes.copyOfRange(4, 8))
        return ip.matches(SOCKS_4A_IP_PATTERN.toRegex())
    }

    private fun InputStream.readUntil(delimiter: String): String {
        val buf = StringBuilder(128)

        var byte = read()
        while (byte >= 0) {
            buf.append(byte.toChar())
            if (buf.indexOf(delimiter) != -1)
                break

            byte = read()
        }

        return buf.substring(0, buf.length - 1)
    }

    private val SOCKS_4A_IP_PATTERN = "^0{0,3}\\.0{0,3}\\.0{0,3}\\..*$"
}
