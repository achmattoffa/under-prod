/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.concurrency

interface BackgroundWorker : Runnable {

    fun start()

    fun stop()
}