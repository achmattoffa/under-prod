/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.counter

import com.bitato.ut.api.model.common.Counter
import com.bitato.ut.api.util.compareTo
import java.io.OutputStream

open class OutputStreamCounter(private val outputStream: OutputStream, private val counter: Counter)
: OutputStream() {

    override fun toString(): String {
        return outputStream.toString()
    }

    override fun write(b: Int) {
        outputStream < b
        counter += 1
    }

    override fun write(buffer: ByteArray?, offset: Int, length: Int) {
        buffer?.let {
            outputStream < it.copyOfRange(offset, length)
            counter += length
        }
    }

    override fun hashCode(): Int {
        return outputStream.hashCode()
    }

    override fun write(buffer: ByteArray?) {
        buffer?.let {
            outputStream < it
            counter += it.size
        }
    }

    override fun equals(other: Any?): Boolean {
        return outputStream.equals(other)
    }

    override fun flush() {
        outputStream.flush()
    }

    override fun close() {
        outputStream.close()
    }
}

fun OutputStream.counter(counter: Counter): OutputStream {
    return OutputStreamCounter(this, counter)
}
