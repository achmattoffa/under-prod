/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.throttle

import com.bitato.ut.api.model.common.AtomicCounter
import java.io.InputStream
import java.util.concurrent.atomic.AtomicLong

class UtInputStreamThrottleController(private var bytesPerSecond: Int = 102400000)
: InputStreamThrottleController {

    companion object {
        const private val TIME_INTERVAL_MILLIS = 1000
    }

    private val counter = AtomicCounter(AtomicLong(0))
    private var timestamp = System.currentTimeMillis()

    override fun register(inputStream: InputStream): InputStream {
        return ThrottledInputStream(inputStream, this)
    }

    override fun limitReadRate(bytesPerSecond: Int) {
        this.bytesPerSecond = bytesPerSecond
    }

    @Synchronized
    override fun read(source: ThrottledInputStream, buffer: ByteArray?, offset: Int, length: Int): Int {
        acquireLock()
        return readStream(buffer, length, offset, source)
    }

    private fun readStream(buffer: ByteArray?, length: Int, offset: Int, source: ThrottledInputStream): Int {

        val bytesRead = if (length + counter.value() > bytesPerSecond) {
            source.reallyRead(buffer, offset, bytesPerSecond - counter.value().toInt())
        } else {
            source.reallyRead(buffer, offset, length)
        }

        buffer?.let {
            if (bytesRead >= 0) {
                counter += bytesRead
            }
        }

        return bytesRead
    }

    private fun acquireLock() {

        if (counter.value() >= bytesPerSecond) {
            val now = System.currentTimeMillis()
            if (timestamp + TIME_INTERVAL_MILLIS >= now) {
                Thread.sleep(timestamp + TIME_INTERVAL_MILLIS - now)
            }

            timestamp = System.currentTimeMillis()
            counter.reset()
        }
    }
}

