/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

object HttpStatusCodes {
    val CONTINUE: Int = 100
    val SWITCHING_PROTOCOLS: Int = 101
    val OK: Int = 200
    val CREATED: Int = 201
    val ACCEPTED: Int = 202
    val NON_AUTHORITATIVE_INFORMATION: Int = 203
    val NO_CONTENT: Int = 204
    val RESET_CONTENT: Int = 205
    val PARTIAL_CONTENT: Int = 206
    val MULTIPLE_CHOICES: Int = 300
    val MOVED_PERMANENTLY: Int = 301
    val FOUND: Int = 302
    val SEE_OTHER: Int = 303
    val NOT_MODIFIED: Int = 304
    val USE_PROXY: Int = 305
    val TEMPORARY_REDIRECT: Int = 307
    val BAD_REQUEST: Int = 400
    val UNAUTHORIZED: Int = 401
    val PAYMENT_REQUIRED: Int = 402
    val FORBIDDEN: Int = 403
    val NOT_FOUND: Int = 404
    val METHOD_NOT_ALLOWED: Int = 405
    val NOT_ACCEPTABLE: Int = 406
    val PROXY_AUTHENTICATION_REQUIRED: Int = 407
    val REQUEST_TIMEOUT: Int = 408
    val CONFLICT: Int = 409
    val GONE: Int = 410
    val LENGTH_REQUIRED: Int = 411
    val PRECONDITION_FAILED: Int = 412
    val PAYLOAD_TOO_LARGE: Int = 413
    val URI_TOO_LONG: Int = 414
    val UNSUPPORTED_MEDIA_TYPE: Int = 415
    val RANGE_NOT_SATISFIABLE: Int = 416
    val EXPECTATION_FAILED: Int = 417
    val UPGRADE_REQUIRED: Int = 416
    val INTERNAL_SERVER_ERROR: Int = 500
    val NOT_IMPLEMENTED: Int = 501
    val BAD_GATEWAY: Int = 502
    val SERVICE_UNAVAILABLE: Int = 503
    val GATEWAY_TIMEOUT: Int = 504
    val HTTP_VERSION_NOT_SUPPORTED: Int = 505
}
