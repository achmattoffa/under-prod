/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks5

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer
import java.nio.ByteOrder

class Socks5UsernamePasswordAuthenticationResponse(var responseCode: Byte = 0) : Parsable {

    override fun parse(): ByteArray {
        return ByteBuffer.allocate(2)
          .order(ByteOrder.BIG_ENDIAN)
          .put(0x05)
          .put(responseCode)
          .array()
    }
}


