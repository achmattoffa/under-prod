/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.throttle

import java.io.InputStream

interface InputStreamThrottleController {

    fun register(inputStream: InputStream): InputStream

    fun read(source: ThrottledInputStream, buffer: ByteArray?, offset: Int, length: Int): Int

    fun limitReadRate(bytesPerSecond: Int)
}