/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer

class InternalSessionResponse(var status: Byte) : Parsable {

    override fun parse(): ByteArray {
        val buffer = ByteBuffer.allocate(1)

        return buffer.put(status)
          .array()
    }

    object SessionStatusCodes {
        const val KEEP_ALIVE: Byte = 0x00
        const val CAP_REACHED: Byte = 0x01
        const val PACKAGE_EXPIRED: Byte = 0x02
        const val UPGRADE_REQUESTED: Byte = 0x03
        const val ACCOUNT_BANNED: Byte = 0x04
        const val SERVER_ERROR: Byte = 0x05
        const val TERMINATE: Byte = 0x06
    }

    object MessageTypes {
        const val SESSION_STATUS = 0x01
        const val SESSION_USAGE = 0x02
        const val INTERNAL = 0x03
        const val ACCOUNT_INFO = 0x04
    }
}