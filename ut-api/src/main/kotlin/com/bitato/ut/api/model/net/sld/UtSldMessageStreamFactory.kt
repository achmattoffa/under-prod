/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sld

import com.bitato.ut.api.io.readByte
import com.bitato.ut.api.io.readString
import com.bitato.ut.api.model.net.NetworkProtocol
import java.io.InputStream

object UtSldMessageStreamFactory : SldMessageStreamFactory {

    override fun createSldRequest(iStream: InputStream): SldRequest {
        val region = iStream.readString()
        val proto = NetworkProtocol.valueOf(iStream.readString())
        val port = iStream.readString()
        val authToken = iStream.readString()

        return SldRequest(region, proto, port.toInt(), authToken)
    }

    override fun InputStream.toSldRequest(): SldRequest {
        return createSldRequest(this)
    }

    override fun createSldResponse(iStream: InputStream): SldResponse {
        val status = iStream.readByte()

        if (status == SldResponse.ResponseCodes.SUCCESS) {
            val addressType = iStream.readByte()

            val address = iStream.readString()
            val host = address.split(":")[0]
            val port = address.split(":")[1].toInt()
            return SldResponse(status, addressType, host, port)
        } else {
            return SldResponse(status)
        }
    }

    override fun InputStream.toSldResponse(): SldResponse {
        return createSldResponse(this)
    }
}