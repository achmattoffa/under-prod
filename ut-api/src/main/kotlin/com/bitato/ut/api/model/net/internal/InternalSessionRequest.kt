/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer

class InternalSessionRequest(var sessionKey: String, var code: Byte): Parsable {

    override fun parse(): ByteArray {
        val buffer = ByteBuffer.allocate(sessionKey.length + 2)

        return buffer.put(sessionKey.toByteArray(Charsets.ISO_8859_1))
                .put(Character.UNASSIGNED)
                .put(code)
                .array()


    }

    object RequestCodes {
        const val CONNECT: Byte = 0x00
        const val DISCONNECT: Byte = 0x01
        const val STATUS: Byte = 0x02
    }
}


