/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import com.bitato.ut.api.io.readByte
import com.bitato.ut.api.io.readString
import java.io.InputStream


object UtInternalMessageStreamFactory : InternalMessageStreamFactory {

    override fun createInternalSessionRequest(iStream: InputStream): InternalSessionRequest {
        val sessionUuid = iStream.readString()
        val code = iStream.readByte()
        return InternalSessionRequest(sessionUuid, code)
    }

    override fun InputStream.toInternalSessionRequest() : InternalSessionRequest {
        return createInternalSessionRequest(this)
    }

    override fun createInternalSessionResponse(iStream: InputStream): InternalSessionResponse {
        val status = iStream.readByte()
        return InternalSessionResponse(status)
    }

    override fun InputStream.toInternalSessionResponse() : InternalSessionResponse {
        return createInternalSessionResponse(this)
    }

    override fun createInternalConnectRequest(iStream: InputStream): InternalConnectRequest {
        val command = iStream.readByte()
        val sessionUuid = iStream.readString()

        return InternalConnectRequest(command, sessionUuid)
    }

    override fun InputStream.toInternalConnectRequest() : InternalConnectRequest {
        return createInternalConnectRequest(this)
    }

    override fun createInternalConnectResponse(iStream: InputStream): InternalConnectResponse {
        val responseCode = iStream.readByte()
        val description = iStream.readByte()

        return InternalConnectResponse(responseCode, description)
    }

    override fun InputStream.toInternalConnectResponse() : InternalConnectResponse {
        return createInternalConnectResponse(this)
    }
}
