/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http.header

import HttpCommon
import com.bitato.ut.api.util.empty
import java.io.IOException

class HttpHeaderList(headers: CharSequence = String.empty()) {

    private val headersList: MutableList<HttpHeader>

    init {
        if (!headers.isNotEmpty()) {
            headersList = mutableListOf()
        } else {
            headersList = createHttpHeadersFromSequence(headers)
        }
    }

    private fun createHttpHeadersFromSequence(headers: CharSequence): MutableList<HttpHeader> {
        return headers.trim()
          .split(HttpCommon.EOL.toRegex())
          .map {
              val keyValue = it.split(NAME_VALUE_SEPARATOR.toRegex(), 2)

              try {
                  HttpHeader(keyValue[0], keyValue.subList(1, keyValue.size).joinToString())
              } catch (ex: IllegalHeaderException) {
                  throw IOException("Unable to create HTTP Header List", ex)
              }
          }
          .toMutableList()
    }

    fun add(header: HttpHeader): Boolean {
        return headersList.add(header)
    }

    fun add(name: String, value: ByteArray): Boolean {
        return headersList.add(HttpHeader(name, String(value, Charsets.ISO_8859_1)))
    }

    fun contains(headerName: String): Boolean {
        return headersList.any { it.headerName.endsWith(headerName, true) }
    }

    fun getValueOf(name: String): String {
        val header = headersList.firstOrNull { it.headerName.equals(name, true) }
        return header?.headerValue ?: String.empty()
    }

    fun size(): Int {
        return headersList.size
    }

    fun get(i: Int): HttpHeader {
        return headersList[i]
    }

    private fun set(index: Int, element: HttpHeader): HttpHeader {
        return headersList.set(index, element)
    }

    fun set(name: String, value: String) {
        if (contains(name)) {
            edit(name, value)
        } else {
            add(HttpHeader(name, value))
        }
    }

    override fun toString(): String {
        return headersList.joinToString("", transform = HttpHeader::toString)
    }

    private fun edit(name: String, value: String) {
        val pos = headersList.indexOfFirst { it.headerName.equals(name, true) }
        if (pos != -1) {
            set(pos, HttpHeader(name, value))
        }
    }

    companion object {
        private val NAME_VALUE_SEPARATOR = ": "
    }
}
