/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.counter

import com.bitato.ut.api.model.common.Counter
import java.io.InputStream


open class InputStreamCounter(private val inputStream: InputStream, private val counter: Counter)
: InputStream() {

    override fun read(): Int {
        val read = inputStream.read()
        counter += 1
        return read
    }

    override fun read(buffer: ByteArray?): Int {
        val bytesRead = inputStream.read(buffer)
        counter += bytesRead
        return bytesRead
    }

    override fun read(buffer: ByteArray?, offset: Int, length: Int): Int {
        val bytesRead = inputStream.read(buffer, offset, length)
        counter += bytesRead
        return bytesRead
    }

    override fun skip(bytes: Long): Long {
        return inputStream.skip(bytes)
    }

    override fun close() {
        inputStream.close()
    }

    override fun available(): Int {
        return inputStream.available()
    }

    override fun hashCode(): Int {
        return inputStream.hashCode()
    }

    override fun markSupported(): Boolean {
        return inputStream.markSupported()
    }

    override fun reset() {
        inputStream.reset()
    }

    override fun equals(other: Any?): Boolean {
        return inputStream.equals(other)
    }

    override fun toString(): String {
        return inputStream.toString()
    }

    override fun mark(readLimit: Int) {
        inputStream.mark(readLimit)
    }
}

fun InputStream.counter(counter: Counter): InputStream {
    return InputStreamCounter(this, counter)
}