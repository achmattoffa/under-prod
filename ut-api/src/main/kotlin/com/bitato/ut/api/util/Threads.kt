/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.util

inline fun waitUntil(f: () -> Boolean) {
    while (!f.invoke()) Thread.sleep(300)
}

inline fun waitWhile(f: () -> Boolean) {
    while (f.invoke()) Thread.sleep(300)
}

fun thread(f: () -> Unit): Thread {
    val worker = Thread { f.invoke() }
    worker.start()
    return worker
}