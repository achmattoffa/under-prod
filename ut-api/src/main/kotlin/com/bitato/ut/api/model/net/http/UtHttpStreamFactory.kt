/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

import java.io.IOException
import java.io.InputStream

object UtHttpStreamFactory : HttpStreamFactory {

    override fun createHttpRequest(inputStream: InputStream): HttpRequest {

        StringBuilder().apply {

            var byte = inputStream.read()
            while (byte >= 0) {
                this.append(byte.toChar())
                if (this.indexOf("\r\n\r\n") != -1) {
                    return HttpRequest(this)
                }

                byte = inputStream.read()
            }
        }

        throw IOException("Unexpected end of input stream")
    }

    override fun InputStream.toHttpRequest(): HttpRequest {
        return createHttpRequest(this)
    }

    override fun createHttpResponse(inputStream: InputStream): HttpResponse {
        StringBuilder().apply {

            var byte = inputStream.read()

            while (byte >= 0) {
                this.append(byte.toChar())
                if (this.indexOf("\r\n\r\n") != -1) {
                    return HttpResponse(this)
                }
                byte = inputStream.read()
            }
        }
        throw IOException("Unexpected end of input stream")
    }

    override fun InputStream.toHttpResponse(): HttpResponse {
        return createHttpResponse(this)
    }

}