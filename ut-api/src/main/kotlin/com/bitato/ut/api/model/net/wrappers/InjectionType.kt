/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.wrappers

enum class InjectionType {
    NONE, PRE_REQUEST, POST_REQUEST, HTTP_HEADER
}