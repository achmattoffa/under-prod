/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sessioninit

import java.io.InputStream

interface SessionInitMessageStreamFactory {
    fun createSessionInitRequest(iStream: InputStream): SessionInitRequest
    fun createSessionInitResponse(iStream: InputStream): SessionInitResponse
    fun InputStream.toSessionInitRequest(): SessionInitRequest
    fun InputStream.toSessionInitResponse(): SessionInitResponse
}