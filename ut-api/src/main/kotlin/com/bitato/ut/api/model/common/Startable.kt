/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.model.common

interface Startable {
    fun start()
}
