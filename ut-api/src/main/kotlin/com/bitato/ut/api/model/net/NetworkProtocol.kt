/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net

enum class NetworkProtocol {
    TCP,
    UDP,
    ANY
}
