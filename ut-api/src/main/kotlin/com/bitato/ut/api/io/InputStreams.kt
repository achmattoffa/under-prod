/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io

import java.io.IOException
import java.io.InputStream
import java.io.OutputStream


fun InputStream.copyTo(oStream: OutputStream, length: Long, bufSize: Int = 64 * 1024) {
    var bytesLeft = length
    var bytesRead: Int

    while (bytesLeft > 0) {

        val buffer: ByteArray
        if (bytesLeft < bufSize) {
            buffer = ByteArray(bytesLeft.toInt())
        } else {
            buffer = ByteArray(bufSize)
        }

        bytesRead = read(buffer)

        if (bytesRead == -1 && bytesLeft > 0) {
            throw IOException("InputStream closed unexpectedly")
        }

        oStream.write(buffer, 0, bytesRead)

        bytesLeft -= bytesRead
    }
}

fun InputStream.readByte(): Byte {
    return read().toByte()
}

fun InputStream.readExactly(len: Int): ByteArray {
    val temp = ByteArray(len)
    read(temp)

    return temp
}

fun InputStream.readUntil(delimiter: String): String {

    val buffer = StringBuilder()

    var b = read()
    while (b != -1) {

        buffer.append(b.toChar())
        if (buffer.indexOf(delimiter) != -1) {
            break
        }

        b = read()
    }

    return buffer.toString()
}

fun InputStream.readUntil(delimiter: Byte): ByteArray {

    val buffer = StringBuilder()

    var b = read()
    while (b != -1) {

        buffer.append(b.toChar())
        if (buffer.indexOf(delimiter.toChar()) != -1) {
            break
        }

        b = read()
    }

    return buffer.toString().toByteArray(Charsets.ISO_8859_1)
}

fun InputStream.readString(): String {

    val buffer = StringBuilder()

    var b = read()
    while (b != -1) {

        buffer.append(b.toChar())
        if (buffer.indexOf(Character.UNASSIGNED.toChar()) != -1) {
            break
        }

        b = read()
    }
    if (buffer.isNotEmpty())
        return  buffer.substring(0, buffer.length - 1)
    else
        return ""
}