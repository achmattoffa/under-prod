/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.util

import java.io.Closeable
import java.io.IOException

inline fun <R> protect(f: () -> R) {

    try {
        f.invoke()
    } catch (ex: Exception) {
        //TODO
        //ex.printStackTrace()
    }
}

inline fun <R> io(f: () -> R) {
    try {
        f.invoke()
    } catch (ex: IOException) {
        //TODO
        //ex.printStackTrace()
    }
}

inline fun <T : Closeable?, R> T.using(block: (T) -> R) {
    var closed = false
    try {
        block(this)
    } catch (e: Exception) {
        closed = true
        try {
            this?.close()
        } catch (closeException: Exception) {
        }

        //TODO
        e.printStackTrace()
    } finally {
        if (!closed) {
            this?.close()
        }
    }
}