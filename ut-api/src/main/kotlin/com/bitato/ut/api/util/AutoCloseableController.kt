/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.util

interface AutoCloseableController<T: AutoCloseable> : AutoCloseable {

    fun add(t: T)

    fun remove(t: T)
}