/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.model.net.wrappers

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer
import java.util.*

class UtRequestWrapper(var uuid: String, var requestType: ClientRequestType, var payload: ByteArray = ByteArray(0)) : Parsable {

    override fun parse(): ByteArray {

        val uuidBytes = uuid.toByteArray(Charsets.ISO_8859_1)
        //TODO
        val byteBuffer = ByteBuffer.allocate(2 + uuidBytes.size)
          .put(uuidBytes)
          .put(Character.UNASSIGNED)
          .put(requestType.ordinal.toByte())

        return Base64.getEncoder()
          .encode(byteBuffer.array())
    }
}