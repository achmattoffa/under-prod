/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks4

class Socks4ProtocolException(s: String = "") : InstantiationException(s)