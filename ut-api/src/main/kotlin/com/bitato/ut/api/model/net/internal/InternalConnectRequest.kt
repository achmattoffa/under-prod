/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer

class InternalConnectRequest(var command: Byte, var uuid: String) : Parsable {

    override fun parse(): ByteArray {
        val buffer = ByteBuffer.allocate(2 + uuid.length)
        return buffer
          .put(command)
          .put(uuid.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .array()
    }

    object RequestCodes {
        val CONNECT: Byte = 0x01
        val RECONNECT: Byte = 0x02
    }
}
