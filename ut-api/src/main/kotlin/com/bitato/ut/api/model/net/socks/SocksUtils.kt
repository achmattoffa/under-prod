/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks

import java.nio.ByteBuffer
import java.nio.ByteOrder

object SocksUtils {

    fun ipV4ToByteArray(addr: String): ByteArray {

        if (!addr.matches(BAD_IP_ADDRESS.toRegex())) {
            throw IllegalArgumentException("Bad IP address.")
        }

        val parts = addr.split("\\.".toRegex())
        val intParts = Array(4, { parts[it].toInt() })

        if (intParts.any({ it < 0 || it > 255 })) {
            throw IllegalArgumentException("Bad IP address.")
        }

        return Array(4, { intParts[it].toByte() }).toByteArray()
    }

    fun byteArrayToIpV4AddressAsString(ipAddress: ByteArray): String {
        if (ipAddress.size != 4) {
            throw IllegalArgumentException("Bad IP address size.")
        }

        var builder = StringBuilder().apply {
            ipAddress.forEach {
                append("${(it.toInt() shl 32 and 255)}.")
            }
        }

        return builder.substring(0, builder.length - 1)
    }

    fun portToByteArray(port: Int): ByteArray {

        if (port !in 1..65535) {
            throw IllegalArgumentException("Invalid port, $port")
        }

        return ByteBuffer.allocate(2)
          .order(ByteOrder.BIG_ENDIAN)
          .putShort(port.toShort())
          .array()
    }

    fun portArrayToInt(port: ByteArray): Int {

        return ByteBuffer.wrap(port)
          .order(ByteOrder.BIG_ENDIAN)
          .getShort(0).toInt()
    }

    private val BAD_IP_ADDRESS = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}.\\d{1,3}$"
}
