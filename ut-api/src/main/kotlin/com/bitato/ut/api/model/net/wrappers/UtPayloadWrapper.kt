/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.model.net.wrappers

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer
import java.util.*

class UtPayloadWrapper(val index: Int, val payload: ByteArray) : Parsable {

    override fun parse(): ByteArray {

        val indexString = index.toString()

        val buffer = ByteBuffer.allocate(1 + indexString.length)
          .put(indexString.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)

        return Base64.getEncoder()
          .encode(buffer.array())
    }
}