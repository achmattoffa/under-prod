/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sld

import java.io.InputStream

interface SldMessageStreamFactory {
    fun createSldRequest(iStream: InputStream): SldRequest
    fun createSldResponse(iStream: InputStream): SldResponse
    fun InputStream.toSldRequest(): SldRequest
    fun InputStream.toSldResponse(): SldResponse
}