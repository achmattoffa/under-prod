/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.wrappers

import java.io.InputStream

interface WrapperProvider {

    fun provideRequestWrapper(iStream: InputStream): UtRequestWrapper
    fun providePayloadWrapper(iStream: InputStream): UtPayloadWrapper
}
