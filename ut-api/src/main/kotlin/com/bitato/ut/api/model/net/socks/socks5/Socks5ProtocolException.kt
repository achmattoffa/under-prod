/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks5

class Socks5ProtocolException(s: String = "")
: InstantiationException(s)