import com.bitato.ut.api.util.compareTo
import java.io.ByteArrayOutputStream

/*
 * Copyright (c) 2017. Under Tunnel
 */

object ProcessHelper {

    fun getProcessList(): List<String> {

        val process = ProcessBuilder("tasklist.exe", "/FO", "CSV", "/v")
          .start()

        try {
            val buffer = ByteArrayOutputStream()
            buffer < process.inputStream

            return String(buffer.toByteArray(), Charsets.ISO_8859_1).lines()
        } finally {
            process.destroy()
        }
    }

    fun isProcessRunning(processGrep: String): Boolean {
        return getProcessList().any {
            it.toLowerCase().matches(processGrep.toRegex())
        }
    }
}