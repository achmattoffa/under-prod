/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net

import java.io.IOException

class UtInvalidRequestException : IOException {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
}