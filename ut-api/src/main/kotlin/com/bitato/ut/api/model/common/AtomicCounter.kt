/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.model.common

import java.util.concurrent.atomic.AtomicLong

class AtomicCounter(private val internal: AtomicLong) : Counter {

    override operator fun plusAssign(value: Number) {
        internal.addAndGet(value.toLong())
    }

    override fun value(): Long {
        return internal.get()
    }

    override fun reset() {
        internal.set(0)
    }
}