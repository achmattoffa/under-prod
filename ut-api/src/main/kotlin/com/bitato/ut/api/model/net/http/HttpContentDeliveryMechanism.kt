/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

enum class HttpContentDeliveryMechanism {

    NONE,
    CONTENT_LENGTH,
    CHUNKED,
    INDEFINITE
}