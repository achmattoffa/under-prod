/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

enum class UriType {
    ABSOLUTE,
    RELATIVE,
    NONE
}
