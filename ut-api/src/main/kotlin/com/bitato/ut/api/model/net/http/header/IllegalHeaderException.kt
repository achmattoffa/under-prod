/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http.header

import com.bitato.ut.api.util.empty

class IllegalHeaderException(msg: String = String.empty()): IllegalArgumentException(msg)
