/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.net

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.api.util.compareTo
import java.io.InputStream
import java.net.InetSocketAddress
import java.net.Socket
import java.util.*

class ServiceInteractionBuilder<T : Parsable> {

    private lateinit var targetHost: String
    private var targetPort: Int = 0
    private lateinit var ioRequestHeader: ByteArray
    private lateinit var ioResponseTransformation: (InputStream) -> T
    private var preRequestTransformation: ((Socket) -> Unit)? = null
    private lateinit var protocol: NetworkProtocol


    fun endpoint(host: String, port: Int, protocol: NetworkProtocol = NetworkProtocol.TCP) = apply {
        this.targetHost = host
        this.targetPort = port
        this.protocol = protocol
    }

    fun request(request: ByteArray) = apply {
        this.ioRequestHeader = request
    }

    fun request(request: Parsable) = apply {
        this.request(request.parse())
    }

    fun responseTransformer(fn: (InputStream) -> T) = apply {
        this.ioResponseTransformation = fn
    }

    fun preRequest(fn: (Socket) -> Unit) = apply {
        this.preRequestTransformation = fn
    }

    fun invoke(): Optional<T> {

        var response: T? = null

        val target = Socket()
        target.connect(InetSocketAddress(targetHost, targetPort), 10000)
        target.soTimeout = 60000

        if (preRequestTransformation != null) {
            preRequestTransformation!!.invoke(target)
        }

        val isSuccess = target.use {
            it.outputStream < ioRequestHeader
            response = ioResponseTransformation.invoke(it.inputStream)
            true
        }

        return if (isSuccess) Optional.of(response!!) else Optional.empty()
    }
}