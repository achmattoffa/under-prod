/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sessioninit

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer


class SessionInitResponse(var status: Byte, var addressType: Byte = 0, var address: String = "", var port: Int = 80, var uuid: String = "") : Parsable {

    object ResponseCodes {
        const val ACCESS_GRANTED: Byte = 0x01
        const val ERR_INVALID_CREDENTIALS: Byte = 0x02
        const val ERR_BANNED: Byte = 0x03
        const val ERR_DISABLED: Byte = 0x04
        const val ERR_SESSION_LIMIT: Byte = 0x05
        const val ERR_SERVER_ACCESS_DENIED: Byte = 0x06
        const val ERR_UNKNOWN: Byte = 0x07
        const val ERR_SLD_FAILED: Byte = 0x08
        const val ERR_INVALID_REQUEST: Byte = 0x09
    }

    object AddressTypes {
        const val HOSTNAME: Byte = 0x01b
        const val IP_V4: Byte = 0x02b
        const val IP_V6: Byte = 0x03b
    }

    override fun parse(): ByteArray {
        return ByteBuffer.allocate(4 + "$address:$port".length + uuid.length)
                .put(status)
                .put(uuid.toByteArray(Charsets.ISO_8859_1))
                .put(Character.UNASSIGNED)
                .put(addressType)
                .put(("$address:$port").toByteArray(Charsets.ISO_8859_1))
                .put(Character.UNASSIGNED)
                .array()
    }
}