/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.streamers

import com.bitato.ut.api.io.streamers.StreamerFactory.StreamerType
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.empty
import com.bitato.ut.api.util.io
import java.io.InputStream
import java.io.OutputStream

class ChunkStreamer(private val inputStream: InputStream, private val outputStream: OutputStream)
: Streamer {

    private val internalStreamer: Streamer = UtStreamerFactory().create(inputStream, outputStream, StreamerType.CONTENT_LENGTH)

    override fun stream(length: Number) {
        generateSequence {
            readChunkSize()
        }.takeWhile { chunk ->
            chunk != NO_MORE_CHUNKS
        }.forEach { chunkSize ->
            internalStreamer.stream(chunkSize.toLong())
        }
    }

    private fun readChunkSize(): Int {
        io {
            val chunkLine: String = readLine()
            outputStream < chunkLine

            val chunkSize = chunkLine.getSizeFromChunkLine()
            if (chunkSize.isNotEmpty()) {
                val sizeInt = Integer.parseInt(chunkSize, 16)
                //Take into consideration end of line delimiter
                if (sizeInt != 0) {
                    return sizeInt + EOL_DELIMITER_SIZE
                }
            }
        }

        return 0
    }

    private fun readLine(): String {
        val line = StringBuilder().apply {
            val byte = ByteArray(1)
            byte < inputStream

            while (byte[0] >= 0) {
                this.append(byte[0].toChar())
                if (this.containsEol()) {
                    break
                }

                byte < inputStream
            }
        }

        return line.toString()
    }

    private fun String.getSizeFromChunkLine(): String {
        return trim().replaceFirst(";.*".toRegex(), String.empty())
    }

    private fun StringBuilder.containsEol(): Boolean {
        return indexOf(EOL_DELIMITER) != -1
    }

    private companion object {
        private val NO_MORE_CHUNKS = 0
        private val EOL_DELIMITER = "\r\n"
        private val EOL_DELIMITER_SIZE = 2
    }
}