/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sessioninit

object AccountAccessVerificationCode {
    const val ACCESS_GRANTED: Byte = 0x01
    const val ERR_INVALID_CREDENTIALS: Byte = 0x02
    const val ERR_BANNED: Byte = 0x03
    const val ERR_DISABLED: Byte = 0x04
    const val ERR_SESSION_LIMIT: Byte = 0x05
    const val ERR_SERVER_ACCESS_DENIED: Byte = 0x06
}