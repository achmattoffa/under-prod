/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.io.monitors

import java.io.InputStream
import java.util.*

class WorkInhibitingInputStream(private val inputStream: InputStream, private val work: (ByteArray) -> Unit) : InputStream() {

    override fun skip(n: Long): Long {
        return inputStream.skip(n)
    }

    override fun available(): Int {
        return inputStream.available()
    }

    override fun reset() {
        inputStream.reset()
    }

    override fun close() {
        inputStream.close()
    }

    override fun mark(readlimit: Int) {
        inputStream.mark(readlimit)
    }

    override fun markSupported(): Boolean {
        return inputStream.markSupported()
    }

    override fun read(): Int {
        val byteRead = inputStream.read()
        work.invoke(byteArrayOf(byteRead.toByte()))

        return byteRead
    }

    override fun read(b: ByteArray?): Int {
        val read = inputStream.read(b)
        if (b != null)
            work.invoke(b)

        return read
    }

    override fun read(b: ByteArray?, off: Int, len: Int): Int {
        val read = inputStream.read(b, off, len)

        if (b != null)
            work.invoke(Arrays.copyOfRange(b, off, len))

        return read
    }
}

fun InputStream.applyWork(work: (ByteArray) -> Unit): InputStream {
    return WorkInhibitingInputStream(this, work)
}
