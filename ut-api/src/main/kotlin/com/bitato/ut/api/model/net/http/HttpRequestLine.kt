/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http


open class HttpRequestLine(var method: String = "GET", uri: String = "/", var version: String = "HTTP/1.1") {

    var requestUri: String = uri
        set(value) {
            field = value
            setUriType()
        }

    var uriType: UriType = UriType.NONE

    fun getRequestLine(): String {
        return "$method $requestUri $version"
    }

    override fun toString(): String {
        return getRequestLine()
    }

    protected fun setRequestLine(line: String) {
        if (!isRequestLineValid(line)) {
            throw IllegalArgumentException("Invalid HTTP Request Line: $line")
        }

        line.split(' ')
          .apply {
              method = first()
              requestUri = subList(1, size - 1).joinToString()
              version = last()
          }
    }

    protected fun setUriType() {
        uriType = when {
            requestUri.isEmpty() -> UriType.NONE
            requestUri.startsWith("/") -> UriType.RELATIVE
            else -> UriType.ABSOLUTE
        }
    }

    private fun isRequestLineValid(requestLine: String): Boolean {
        return requestLine.matches("^ *[^ \r\n]+ +([^ \r\n]+ )+ *HTTP/.+".toRegex())
    }
}