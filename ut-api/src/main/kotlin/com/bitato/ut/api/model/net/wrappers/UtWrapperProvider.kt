/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.wrappers

import com.bitato.ut.api.io.readString
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import java.io.InputStream
import java.util.*

class UtWrapperProvider : WrapperProvider {

    override fun provideRequestWrapper(iStream: InputStream): UtRequestWrapper {

        val httpRequest = UtHttpStreamFactory.createHttpRequest(iStream)

        val clientRequest64 = httpRequest["x-req"]
        val clientRequest = Base64.getDecoder().decode(clientRequest64)

        val clientRequestStream = clientRequest.inputStream()
        val clientUuid = clientRequestStream.readString()
        val clientRequestType = ClientRequestType.values()[clientRequestStream.read()]

        val clientPayload64 = httpRequest["x-dat"]
        val clientPayload = Base64.getDecoder().decode(clientPayload64)

        return UtRequestWrapper(clientUuid, clientRequestType, clientPayload)
    }

    override fun providePayloadWrapper(iStream: InputStream): UtPayloadWrapper {

        val httpRequest = UtHttpStreamFactory.createHttpRequest(iStream)

        val clientRequest64 = httpRequest["x-req"]
        val clientRequest = Base64.getDecoder().decode(clientRequest64)

        val clientRequestStream = clientRequest.inputStream()
        val payloadIndex = clientRequestStream.readString().toInt()

        val clientPayload64 = httpRequest["x-dat"]
        val clientPayload = Base64.getDecoder().decode(clientPayload64)

        return UtPayloadWrapper(payloadIndex, clientPayload)
    }
}