/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks4

import com.bitato.ut.api.model.net.socks.SocksUtils
import java.nio.ByteBuffer


class Socks4ARequest(cmd: Byte, port: ByteArray, val host: String, userId: String = "")
: Socks4Request(cmd, port, DUMMY_ADDRESS, userId) {

    constructor(bytes: ByteArray, host: String)
    : this(bytes[1], bytes.copyOfRange(2, 4), host, getUserIdFromArray(bytes))

    override fun getAddress(): String = host

    override fun parse(): ByteArray {
        val socks4Request = super.parse()

        val len = socks4Request.size + host.length + 1
        val buf = ByteBuffer.allocate(len)

        return buf.put(socks4Request)
                .put(host.toByteArray(Charsets.ISO_8859_1))
                .put(NULL)
                .array()
    }

    companion object {
        private fun getUserIdFromArray(array: ByteArray): String {
            return String(array.copyOfRange(8, array.indexOfSecondLast { it == 0.toByte() }), Charsets.ISO_8859_1)
        }

        private fun ByteArray.indexOfSecondLast(predicate: (Byte) -> Boolean): Int {
            val bytes = copyOfRange(0, indexOfLast(predicate))
            return bytes.indexOfLast(predicate)
        }
        private val DUMMY_ADDRESS = SocksUtils.ipV4ToByteArray("0.0.0.255")
    }
}


