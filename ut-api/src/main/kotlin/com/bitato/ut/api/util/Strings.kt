/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.util

enum class StripPosition {
    FIRST, NORMAL
}

fun String.strip(regex: String, pos: StripPosition = StripPosition.FIRST): String {
    return when (pos) {
        StripPosition.FIRST -> replaceFirst(regex.toRegex(), "")
        else -> replace(regex.toRegex(), "")
    }
}

fun String.Companion.empty(): String {
    return ""
}