/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks4

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.socks.SocksUtils
import java.nio.ByteBuffer

open class Socks4Request(val cmd: Byte,val port: ByteArray, val ipAddress: ByteArray, val userId: String = ""): Parsable {

    private val protocolVersion: Byte = VERSION

    constructor(bytes: ByteArray)
    : this(bytes[1], bytes.copyOfRange (2, 4), bytes.copyOfRange(4, 8), getUserIdFromArray(bytes))

    open fun getAddress(): String {
        return SocksUtils.byteArrayToIpV4AddressAsString(ipAddress)
    }

    fun getPort(): Int = SocksUtils.portArrayToInt(port)

    fun getUserId(): ByteArray = userId.toByteArray(Charsets.ISO_8859_1)

    override fun parse(): ByteArray {
        val len = 9 + userId.length
        val buf = ByteBuffer.allocate(len)

        return buf.put(protocolVersion)
                .put(cmd)
                .put(port)
                .put(ipAddress)
                .put(userId.toByteArray(Charsets.ISO_8859_1))
                .put(NULL).array()
    }

    companion object {
        private fun getUserIdFromArray(bytes: ByteArray): String {
            return String(bytes.copyOfRange(8, bytes.size - 1), Charsets.ISO_8859_1)
        }

        val NULL: Byte = 0
        val CONNECT: Byte = 1
        val BIND: Byte = 2
        val VERSION: Byte = 4
    }
}
