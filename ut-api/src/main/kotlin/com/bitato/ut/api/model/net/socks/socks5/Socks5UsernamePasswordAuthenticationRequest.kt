/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.model.net.socks.socks5

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer
import java.nio.ByteOrder

class Socks5UsernamePasswordAuthenticationRequest(val userName: String, val password: String) : Parsable {

    override fun parse(): ByteArray {
        return ByteBuffer.allocate(3 + userName.length + password.length)
          .order(ByteOrder.BIG_ENDIAN)
          .put(0x01)
          .put(userName.length.toByte())
          .put(userName.toByteArray(Charsets.ISO_8859_1))
          .put(password.length.toByte())
          .put(password.toByteArray(Charsets.ISO_8859_1))
          .array()
    }
}