/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.io.monitors

import java.io.OutputStream
import java.util.*

class WorkInhibitingOutputStream(private val outputStream: OutputStream, private val work: (ByteArray) -> Unit): OutputStream() {

    override fun write(b: Int) {
        work.invoke(byteArrayOf(b.toByte()))
        outputStream.write(b)
    }

    override fun write(b: ByteArray?) {
        if (b != null)
            work.invoke(b)
        outputStream.write(b)
    }

    override fun write(b: ByteArray?, off: Int, len: Int) {
        work.invoke(Arrays.copyOfRange(b, off, len))
        outputStream.write(b, off, len)
    }

    override fun flush() {
        outputStream.flush()
    }

    override fun close() {
        outputStream.close()
    }
}

fun OutputStream.applyWork(work: (ByteArray) -> Unit): OutputStream {
    return WorkInhibitingOutputStream(this, work)
}