/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import java.io.InputStream

interface InternalMessageStreamFactory {
    fun createInternalConnectRequest(iStream: InputStream): InternalConnectRequest
    fun createInternalConnectResponse(iStream: InputStream): InternalConnectResponse
    fun createInternalSessionRequest(iStream: InputStream): InternalSessionRequest
    fun createInternalSessionResponse(iStream: InputStream): InternalSessionResponse
    fun InputStream.toInternalSessionRequest(): InternalSessionRequest
    fun InputStream.toInternalSessionResponse(): InternalSessionResponse
    fun InputStream.toInternalConnectRequest(): InternalConnectRequest
    fun InputStream.toInternalConnectResponse(): InternalConnectResponse
}