/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.streamers

import com.bitato.ut.api.io.streamers.StreamerFactory.StreamerType
import java.io.InputStream
import java.io.OutputStream


class UtStreamerFactory : StreamerFactory {

    override fun create(inputStream: InputStream, outputStream: OutputStream, streamerType: StreamerType): Streamer {
        return when (streamerType) {
            StreamerType.CHUNK_ENCODING -> ChunkStreamer(inputStream, outputStream)
            StreamerType.CONTENT_LENGTH -> ContentStreamer(inputStream, outputStream)
            StreamerType.INDEFINITE -> IndefiniteStreamer(inputStream, outputStream)
        }
    }
}