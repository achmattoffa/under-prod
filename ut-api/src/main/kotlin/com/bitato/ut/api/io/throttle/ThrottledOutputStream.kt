/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.throttle

import java.io.OutputStream

class ThrottledOutputStream(private val original: OutputStream, private val throttleController: OutputStreamThrottleController)
: OutputStream() {

    override fun write(b: Int) {
        val buffer = ByteArray(1)
        buffer[0] = b.toByte()
        throttleController.write(this, buffer, 0, buffer.size)
    }

    override fun write(buffer: ByteArray?) {
        throttleController.write(this, buffer, 0, buffer!!.size)
    }

    override fun write(buffer: ByteArray?, offset: Int, length: Int) {
        throttleController.write(this, buffer, offset, length)
    }

    fun reallyWrite(buffer: ByteArray?, offset: Int, length: Int): Unit = original.write(buffer, offset, length)
}

fun OutputStream.throttle(controller: OutputStreamThrottleController): OutputStream {
    return ThrottledOutputStream(this, controller)
}