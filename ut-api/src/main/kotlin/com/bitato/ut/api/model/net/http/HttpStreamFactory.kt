/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

import java.io.InputStream

interface HttpStreamFactory {

    fun createHttpRequest(inputStream: InputStream): HttpRequest
    fun InputStream.toHttpRequest(): HttpRequest

    fun createHttpResponse(inputStream: InputStream): HttpResponse
    fun InputStream.toHttpResponse(): HttpResponse
}