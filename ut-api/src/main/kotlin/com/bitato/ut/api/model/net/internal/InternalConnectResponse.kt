/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import com.bitato.ut.api.model.common.Parsable
import java.nio.ByteBuffer


class InternalConnectResponse(var replyCode: Byte, var replyDetail: Byte = InternalConnectResponse.ResponseCodeDetail.NO_DETAIL) : Parsable {

    override fun parse(): ByteArray {
        val buffer = ByteBuffer.allocate(2)
        return buffer.put(replyCode)
          .put(replyDetail)
          .array()
    }

    object ResponseCodes {
        val SUCCESS: Byte = 0x00
        val FAIL: Byte = 0x01
    }

    object ResponseCodeDetail {
        val NO_DETAIL: Byte = 0x00
        val INVALID_AUTHENTICATION_CREDENTIALS: Byte = 0x01
    }
}
