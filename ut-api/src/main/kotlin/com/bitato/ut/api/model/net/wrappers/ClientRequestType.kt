/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.wrappers

enum class ClientRequestType {
    INTERNAL_REQUEST,
    HTTP_REQUEST,
    SOCKS4_REQUEST,
    SOCKS5_REQUEST,
    SESSION_INIT_REQUEST,
    EXPLOIT_DETECTION_REQUEST
}