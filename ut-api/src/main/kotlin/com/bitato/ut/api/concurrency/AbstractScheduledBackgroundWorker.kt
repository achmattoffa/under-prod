/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.concurrency

import com.bitato.ut.api.util.io
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit



abstract class AbstractScheduledBackgroundWorker(val sleepPeriod: Long, val initialDelay: Long = sleepPeriod)
: BackgroundWorker {

    val executor: ScheduledExecutorService = Executors.newScheduledThreadPool(2)

    override fun start() {
        executor.scheduleWithFixedDelay(this, initialDelay, sleepPeriod, TimeUnit.SECONDS)
    }

    override fun stop() {
        io {
            executor.shutdown()
        }
    }
}