/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.http.header.HttpHeaderList
import com.bitato.ut.api.util.empty
import com.bitato.ut.api.util.strip

class HttpRequest() : HttpRequestLine(), Parsable {

    var headers: HttpHeaderList = HttpHeaderList()

    var port: Int = 0
        set(value) {
            if (port == value) {
                return
            }

            field = value
            adjustHost()
        }

    constructor(request: CharSequence) : this() {
        setHttpRequest("${request.toString().trim()}\r\n\r\n")
    }

    constructor(requestLine: HttpRequestLine) : this() {
        setHttpRequest("${requestLine.getRequestLine()}\r\n\r\n")
    }

    private fun adjustHost() {
        val oldHost = getHost()
        //strip protocol if present
        var newHost = oldHost.strip(":.+")

        val appendPortToHost: (Int) -> Unit = {
            newHost += ":$it"
            setRequestUriType(UriType.ABSOLUTE)
        }

        if ((method == "CONNECT" && port != 443) || (port != 80)) {
            appendPortToHost.invoke(port)
        }

        headers.set("Host", newHost)

        //uri
        if (uriType == UriType.ABSOLUTE) {
            var noProto = requestUri.strip("(https?|ftp)://")
            noProto = noProto.split("(/|\\?|#)".toRegex())[0]
            requestUri = requestUri.replaceFirst(noProto.toRegex(), newHost)
        }
    }

    fun getContentLength(): Long {

        if (headers.contains("Content-Length")) {
            return headers.getValueOf("Content-Length").toLong()
        } else {
            return -1
        }
    }

    override fun parse(): ByteArray {
        return toString().toByteArray(Charsets.ISO_8859_1)
    }

    private fun setHttpRequest(request: String) {

        val parts = request.trim().split("\\r\\n".toRegex(), 2)
        setRequestLine(parts[0])
        if (parts.size == 2) {
            headers = HttpHeaderList(parts[1].trim())
        }

        initPort()
    }

    fun getHost(): String {
        var host = headers.getValueOf("Host")

        if (host.isEmpty()) {
            host = getHostname()
        }

        //discard port if present
        return host.strip(":.*")
    }

    fun getProtocol(): String {
        return if (method.equals("CONNECT")) {
            "https"
        } else if (uriType == UriType.ABSOLUTE) {
            requestUri.replaceFirst("://.*$".toRegex(), "")
        } else {
            "http"
        }
    }

    fun setRequestUriType(uType: UriType) {
        if (hasHostHeader() && !method.equals("CONNECT") && uriType != uType) {
            var uri = requestUri
            var host = headers.getValueOf("Host")

            if (uType == UriType.RELATIVE) {
                host = formatHost(headers.getValueOf("Host"), uri)
                uri = uri.split(host.toRegex())[1]
            } else {
                uri = "${getProtocol()}://$host$requestUri"
            }

            uriType = uType
            requestUri = uri
        }
    }

    private fun formatHost(host: String, uri: String): String {
        //todo
        return if (host.matches(".+:\\d+".toRegex()) && !uri.contains(host)) {
            host.strip(":.*")
        } else if (uri.matches(".*$host:\\d+.*".toRegex())) {
            "$host:\\d+"
        } else {
            host
        }
    }

    fun getTransferEncoding(): String {
        return headers.getValueOf("Transfer-Encoding")
    }

    fun hasHostHeader(): Boolean {
        return (headers.getValueOf("Host").isNotEmpty())
    }

    fun hasMessageBody(): Boolean {
        return getContentLength() > 0 || getTransferEncoding().isNotEmpty()
    }

    override fun toString(): String {
        return "${getRequestLine()}\r\n$headers\r\n"
    }

    private fun getHostname(): String {
        if (uriType != UriType.ABSOLUTE) {
            return String.empty()
        }

        var uri = requestUri

        //strip protocol
        uri = uri.strip("(https?|ftp)://")

        //strip location or queries (whichever comes first)
        uri = uri.strip("(/|\\?|#).*")

        return uri
    }

    protected fun initPort() {
        val host = getHostname()

        if (host.contains(":[0-8]+$".toRegex())) {
            port = host.replace("^.*:".toRegex(), "").toInt()
        } else if (getProtocol() == "http") {
            port = 80
        } else if (getProtocol() == "https") {
            port = 443
        }
    }

    fun isTunnelRequest(): Boolean {
        return this.method == "CONNECT"
    }

    operator fun get(header: String): String {

        if (!this.headers.contains(header)) {
            throw Exception("header $header does not exist")
        }

        return this.headers.getValueOf(header)
    }

    operator fun set(header: String, value: String) {
        return this.headers.set(header, value)
    }
}