/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.util

import java.net.Socket

fun <T>Socket.timeout(timeout: Int, work: Socket.() -> T): T {
    try {
        this.soTimeout = timeout
        return work.invoke(this)
    } finally {
        this.soTimeout = 0
    }
}