/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.sld

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.NetworkProtocol
import java.nio.ByteBuffer

class SldRequest(var region: String, var protocol: NetworkProtocol, var port: Int, var authToken: String) : Parsable {

    override fun parse(): ByteArray {

        val proto = protocol.name
        val portStr = port.toString()

        return ByteBuffer.allocate(4 + authToken.length + proto.length + portStr.length + region.length)
          .put(region.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(proto.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(portStr.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .put(authToken.toByteArray(Charsets.ISO_8859_1))
          .put(Character.UNASSIGNED)
          .array()
    }
}