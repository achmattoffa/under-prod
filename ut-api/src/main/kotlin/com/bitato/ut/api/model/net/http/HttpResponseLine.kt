/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.http

import com.bitato.ut.api.util.empty

open class HttpResponseLine {

    var version: String = String.empty()
    var code: String = String.empty()
    var text: String = String.empty()

    constructor() {
    }

    constructor(responseLine: String) {
        setResponseLine(responseLine)
    }

    constructor(protoVersion: String, code: String, message: String = String.empty()) {
        this.version = protoVersion
        this.code = code
        this.text = message
    }

    constructor(protoVersion: String, code: Int, message: String) {
        this.version = protoVersion
        this.code = code.toString()
        this.text = message
    }

    fun getResponseLine(): String {
        return "$version $code $text"
    }

    protected fun setResponseLine(line: String) {
        if (line.matches("^HTTP/[^ ]+ \\d{3} .+$".toRegex())) {
            val parts = line.split(" ".toRegex(), 3)
            version = parts[0]
            code = parts[1]
            text = parts[2]
        } else {
            throw IllegalArgumentException("Illegal Response line: \n$line")
        }
    }

    override fun toString(): String {
        return "$version $code $text"
    }
}
