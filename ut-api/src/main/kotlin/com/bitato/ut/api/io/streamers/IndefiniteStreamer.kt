/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.streamers

import com.bitato.ut.api.util.compareTo
import java.io.InputStream
import java.io.OutputStream

class IndefiniteStreamer(private val inputStream: InputStream, private val outputStream: OutputStream)
: Streamer {

    override fun stream(length: Number) {
        outputStream < inputStream
    }
}
