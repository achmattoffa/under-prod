/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class InternalConnectRequestTest {

    lateinit var request: InternalConnectRequest
    val uuid = "7432792825fd4b37a16800be419ae8af"

    @Before
    fun setUp() {
        request = InternalConnectRequest(InternalConnectRequest.RequestCodes.CONNECT, uuid)
    }

    @Test
    fun parse() {
        val bytes = request.parse()
        assertEquals(2 + uuid.length, bytes.size)
    }

    @Test
    fun getCommand() {
        assertEquals(InternalConnectRequest.RequestCodes.CONNECT, request.command)
    }

    @Test
    fun getSessionUuid() {
        assertEquals(uuid, request.uuid)
    }

}