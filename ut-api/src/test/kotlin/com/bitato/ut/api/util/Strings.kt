/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.util

import org.junit.Assert
import org.junit.Test

class Strings {

    @Test
    fun test_empty_string_companion() {
        Assert.assertEquals("", String.empty())
    }

    @Test
    fun test_strip_string_stripPositionFirst() {
        val text = "Afterlife is the only joy, the only guarantee of control."
        val result = text.strip("the ", StripPosition.FIRST)

        Assert.assertEquals("Afterlife is only joy, the only guarantee of control.", result)
    }

    @Test
    fun test_strip_string_stripPositionNormal() {
        val text = "Afterlife is the only joy, the only guarantee of control."
        val result = text.strip("the ", StripPosition.NORMAL)

        Assert.assertEquals("Afterlife is only joy, only guarantee of control.", result)
    }
}