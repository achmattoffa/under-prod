/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.util

import org.junit.Assert
import org.junit.Test

class Threads {

    @Test
    fun test_thread() {
        val work = thread {
            val counter = 0
            (0..10).fold(counter, { a, b -> a + b })
        }

        Assert.assertTrue(work.isAlive)
    }

}