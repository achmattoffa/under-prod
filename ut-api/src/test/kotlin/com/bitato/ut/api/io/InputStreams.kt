/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io

import com.bitato.ut.api.util.compareTo
import org.junit.Assert
import org.junit.Test
import java.io.ByteArrayOutputStream

class InputStreams {

    @Test
    fun test_copyTo_noParams() {
        val text = "If you travel or convert with a holy happiness, silence invents you."

        val inStream = text.byteInputStream(Charsets.ISO_8859_1)
        val osStream = ByteArrayOutputStream()

        inStream.copyTo(osStream)
        Assert.assertEquals(text, osStream.toString(Charsets.ISO_8859_1.displayName()))
    }

    @Test
    fun test_copyTo_customBufferSize() {
        val text = "After boiling the strawberries, mix lentils, spinach and sweet chili sauce with it in a bowl."
        val bufSize = 2

        val inStream = text.byteInputStream(Charsets.ISO_8859_1)
        val osStream = ByteArrayOutputStream()

        inStream.copyTo(osStream, bufferSize = bufSize)
        Assert.assertEquals(text, osStream.toString(Charsets.ISO_8859_1.displayName()))
    }

    @Test
    fun test_copyTo_customLength() {
        val text = "If you exist or experiment with a fraternal uniqueness, blessing studies you."
        val len = 2L

        val inStream = text.byteInputStream(Charsets.ISO_8859_1)
        val osStream = ByteArrayOutputStream()

        inStream.copyTo(osStream, len)
        Assert.assertEquals(text.substring(0, len.toInt()), osStream.toString(Charsets.ISO_8859_1.displayName()))
    }

    @Test
    fun test_copyTo_customLengthAndBufferSize() {
        val text = "Where is the fantastic ship?"
        val len = 5L
        val bufferSize = 3

        val inStream = text.byteInputStream(Charsets.ISO_8859_1)
        val osStream = ByteArrayOutputStream()

        inStream.copyTo(osStream, len, bufferSize)
        Assert.assertEquals(text.substring(0, len.toInt()), osStream.toString(Charsets.ISO_8859_1.displayName()))
    }

    @Test
    fun test_readByte() {
        val text = "For a roasted gooey fritters, add some worcestershire sauce and sugar."
        val inStream = text.byteInputStream(Charsets.ISO_8859_1)

        for (i in 0..text.length - 1) {
            val byteRead = inStream.readByte()
            Assert.assertEquals(text[i].toByte(), byteRead)
        }
    }

    @Test
    fun test_readExactly() {
        val text = "When one avoids manifestation and mineral, one is able to desire affirmation."
        val len = 10
        val inStream = text.byteInputStream(Charsets.ISO_8859_1)

        val bytes = inStream.readExactly(len)
        Assert.assertArrayEquals(bytes, text.substring(0, len).toByteArray(Charsets.ISO_8859_1))
    }

    @Test
    fun test_readUntil_string() {
        val text = "What's the secret to gooey and cored watermelon? Always use juicy pepper."
        val delimiter = "Always"
        val inStream = text.byteInputStream(Charsets.ISO_8859_1)

        val str = inStream.readUntil(delimiter)
        Assert.assertEquals(text.substring(0, text.indexOf(delimiter) + delimiter.length), str)
    }

    @Test
    fun test_readUntil_byte() {
        val text = "What's the secret to gooey and cored watermelon? Always use juicy pepper."
        val delimiter = ' '.toByte()
        val inStream = text.byteInputStream(Charsets.ISO_8859_1)

        val str = inStream.readUntil(delimiter)
        Assert.assertArrayEquals("What's ".toByteArray(Charsets.ISO_8859_1), str)
    }

    @Test
    fun test_readString() {
        val text = "Passion ho! fight to be sailed."
        val oStream = ByteArrayOutputStream()
        oStream < text
        oStream < Character.UNASSIGNED.toByte()
        oStream < "Remember: shaken apple tastes best when peeled in a bucket covered with celery."


        val str = oStream.toByteArray().inputStream().readString()
        Assert.assertEquals(text, str)
    }

    @Test
    fun test_readInt() {
        //TODO
    }
}