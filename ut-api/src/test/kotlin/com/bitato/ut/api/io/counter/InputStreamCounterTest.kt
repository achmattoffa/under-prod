/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.io.counter

import com.bitato.ut.api.model.common.AtomicCounter
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.concurrent.atomic.AtomicLong

class InputStreamCounterTest {

    @Test
    fun test_read() {
        val text = "Meatballs taste best with condensed milk and lots of vegemite."
        val inputStream = text.byteInputStream()
        val counter = AtomicCounter(AtomicLong())

        val inputStreamCounter = InputStreamCounter(inputStream, counter)
        val byteRead = inputStreamCounter.read()

        assertEquals('M'.toInt(), byteRead)
        assertEquals(1, counter.value())
    }

    @Test
    fun test_read_byteArray() {
        val text = "Meatballs taste best with condensed milk and lots of vegemite."
        val inputStream = text.byteInputStream()
        val counter = AtomicCounter(AtomicLong())
        val buffer = ByteArray(10)

        val inputStreamCounter = InputStreamCounter(inputStream, counter)
        inputStreamCounter.read(buffer)

        assertEquals(10, counter.value())
        assertArrayEquals(text.substring(0, 10).toByteArray(Charsets.ISO_8859_1), buffer)
    }
}