/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.util

import org.junit.Assert
import org.junit.Test
import java.io.ByteArrayOutputStream


class CompareTo {

    @Test
    fun test_compareToOutputStream_string() {
        val text = "Experimentum superbe ducunt ad flavum mortem."
        val baos = ByteArrayOutputStream()
        baos < text
        Assert.assertEquals(text, baos.toString())
    }

    @Test
    fun test_compareToOutputStream_byteArray() {
        val bytes = "Pol, a bene hilotae, planeta!".toByteArray(Charsets.ISO_8859_1)
        val baos = ByteArrayOutputStream()
        baos < bytes
        Assert.assertArrayEquals(bytes, baos.toByteArray())
    }

    @Test
    fun test_compareToOutputStream_inputStream() {
        val bytes = "Heu, acipenser!".toByteArray(Charsets.ISO_8859_1)
        val inputStream = bytes.inputStream()

        val baos = ByteArrayOutputStream()
        baos < inputStream
        Assert.assertArrayEquals(bytes, baos.toByteArray())
    }

    @Test
    fun test_compareToOutputStream_int() {
        //TODO
    }

    @Test
    fun test_compareToOutputStream_ioParsable() {
        //TODO
    }

    @Test
    fun test_compareToByteArray_inputStream() {
        val bytes = "Eheu, abaculus!".toByteArray(Charsets.ISO_8859_1)
        val inputStream = bytes.inputStream()


        val copyBuffer = ByteArray(bytes.size)
        copyBuffer < inputStream

        Assert.assertArrayEquals(bytes, copyBuffer)
    }

    @Test
    fun test_compareToInputStream_pairOfOutputStreamAndLong() {
        val bytes = "Devatios nocere in fatalis revalia!".toByteArray(Charsets.ISO_8859_1)
        val inputStream = bytes.inputStream()


        val baos = ByteArrayOutputStream()

        inputStream > baos to 8L


        Assert.assertEquals("Devatios", baos.toString())
    }

}