/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api.model.net.internal

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class InternalConnectResponseTest {

    lateinit var response: InternalConnectResponse

    @Before
    fun setUp() {
        response = InternalConnectResponse(InternalConnectResponse.ResponseCodes.SUCCESS, InternalConnectResponse.ResponseCodeDetail.NO_DETAIL)
    }

    @Test
    fun getReplyCode() {
        assertEquals(InternalConnectResponse.ResponseCodes.SUCCESS, response.replyCode)
    }

    @Test
    fun getReplyDesc() {
        assertEquals(InternalConnectResponse.ResponseCodeDetail.NO_DETAIL, response.replyDetail)
    }

}