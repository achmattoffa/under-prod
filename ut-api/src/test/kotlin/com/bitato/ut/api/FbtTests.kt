/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.api

import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.util.compareTo
import org.junit.Assert.assertFalse
import org.junit.Assert.fail
import org.junit.Test
import java.net.Socket


class FbtTests
{

    fun baseTest(target: String, port: Int, payload: String) {

        try {

            Socket("nofunds.mtn.co.za", 80)
            val sock = Socket(target, port)
            sock.outputStream < payload

            val response = UtHttpStreamFactory.createHttpResponse(sock.inputStream)

            if (response.headers.contains("location")) {
                assertFalse(response.headers.getValueOf("location").contains("nofunds"))
            }

            println(response)

        } catch (ex: Exception) {
            ex.printStackTrace()
            fail()
        }
    }

    @Test
    fun testHostInjection() {
        //baseTest("1app.mtn.co.za", 80, "GET http://1app.mtn.co.za/ HTTP/1.1\r\nHost: 1app.mtn.co.za\r\n\r\n")
        baseTest("kproxy.com", 80, "GET http://1app.mtn.co.za/ HTTP/1.1\r\nHost: 1app.mtn.co.za\r\n\r\n")
    }

}