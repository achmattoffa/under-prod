/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services

import com.bitato.ut.persist.utility.MemoryTable

interface UserPackageService {

    fun getActiveUserPackages(userId: Int): MemoryTable
}