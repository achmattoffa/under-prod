/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.persistence

import com.bitato.ut.persist.utility.MemoryRow

interface UserSessionDb {

    /**
     * Gets an existing active user session, or creates a new one if no active one is present. A user
     * session is required to create user tunnel sessions and acts as the parent to the relationship.
     * Any account can only have one active account session at any given type, but may be connected
     * with more than one device on the same or different tunnel servers.
     */
    fun getOrCreateSessionByUsername(username: String): MemoryRow
}