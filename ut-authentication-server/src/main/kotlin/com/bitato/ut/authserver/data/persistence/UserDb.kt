/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.persistence

import com.bitato.ut.persist.utility.MemoryRow

interface UserDb {

    /**
     * Get the salt stored for this user
     */
    fun getSalt(userId: Int): MemoryRow

    /**
     * Verifies a username and password against the database. If invalid credentials are provided,
     * an @UtAccountAuthenticationException is thrown
     */
    fun verifyLoginCredentials(username: String, password: String)

    /**
     * Gets a user account by a username. If no account is found for the specified username, an empty
     * List is returned
     */
    fun getAccountByUsername(username: String): MemoryRow
}