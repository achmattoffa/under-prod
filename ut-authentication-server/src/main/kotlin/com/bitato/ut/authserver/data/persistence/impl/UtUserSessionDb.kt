/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.persistence.impl

import com.bitato.ut.authserver.data.persistence.UserSessionDb
import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.MemoryRow
import com.bitato.ut.persist.utility.PersistenceBase

class UtUserSessionDb : UserSessionDb, PersistenceBase() {

    /**
     * Gets an existing active user session, or creates a new one if no active one is present. A user
     * session is required to create user tunnel sessions and acts as the parent to the relationship.
     * Any account can only have one active account session at any given type, but may be connected
     * with more than one device on the same or different tunnel servers.
     */
    override fun getOrCreateSessionByUsername(username: String): MemoryRow {
        return procQuery {
            procedure("usp_ut_user_session_create_by_username")
            parameters(username to DbParameterType.IN)
        }
    }

}