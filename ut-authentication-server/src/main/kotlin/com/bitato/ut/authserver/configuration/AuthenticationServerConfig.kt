/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.configuration

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.sbase.configuration.NetworkProtocolConverter
import com.bitato.ut.sbase.configuration.ServerBaseConfig
import org.aeonbits.owner.Config
import org.aeonbits.owner.Config.*


@Sources(
  "file:server.conf",
  "classpath:server.conf")
@LoadPolicy(LoadType.MERGE)
interface AuthenticationServerConfig : Config, ServerBaseConfig {

    @Key("server.bindTo")
    @DefaultValue("localhost")
    override fun bindTo(): String

    @Key("server.port")
    @DefaultValue("443")
    override fun port(): Int

    @Key("server.protocol")
    @DefaultValue("tcp")
    @Config.ConverterClass(NetworkProtocolConverter::class)
    override fun protocol(): NetworkProtocol
}

