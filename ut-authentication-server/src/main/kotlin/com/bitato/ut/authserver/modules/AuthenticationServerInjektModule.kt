/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.modules

import com.bitato.ut.api.model.net.sessioninit.SessionInitMessageStreamFactory
import com.bitato.ut.api.model.net.sessioninit.UtSessionInitMessageStreamFactory
import com.bitato.ut.api.model.net.wrappers.UtWrapperProvider
import com.bitato.ut.api.model.net.wrappers.WrapperProvider
import com.bitato.ut.authserver.data.persistence.UserDb
import com.bitato.ut.authserver.data.persistence.UserPackageDb
import com.bitato.ut.authserver.data.persistence.UserSessionDb
import com.bitato.ut.authserver.data.persistence.impl.UtUserDb
import com.bitato.ut.authserver.data.persistence.impl.UtUserPackageDb
import com.bitato.ut.authserver.data.persistence.impl.UtUserSessionDb
import com.bitato.ut.authserver.data.services.AccountAccessVerificationService
import com.bitato.ut.authserver.data.services.AccountService
import com.bitato.ut.authserver.data.services.UserPackageService
import com.bitato.ut.authserver.data.services.UserSessionService
import com.bitato.ut.authserver.data.services.impl.UtAccountAccessVerificationService
import com.bitato.ut.authserver.data.services.impl.UtAccountService
import com.bitato.ut.authserver.data.services.impl.UtUserPackageService
import com.bitato.ut.authserver.data.services.impl.UtUserSessionService
import com.bitato.ut.authserver.net.UtAuthRequestMapper
import com.bitato.ut.authserver.net.UtAuthServer
import com.bitato.ut.sbase.configuration.ServerBaseConfig
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import uy.kohesive.injekt.api.addPerKeyFactory

object AuthenticationServerInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<AccountAccessVerificationService> { UtAccountAccessVerificationService() }
        addFactory<AccountService> { UtAccountService() }
        addFactory<UserPackageService> { UtUserPackageService() }
        addFactory<UserSessionService> { UtUserSessionService() }
        addFactory<UserDb> { UtUserDb() }
        addFactory<UserPackageDb> { UtUserPackageDb() }
        addFactory<UserSessionDb> { UtUserSessionDb() }
        addPerKeyFactory<AbstractServer, ServerBaseConfig> { args: ServerBaseConfig -> UtAuthServer(args.protocol(), args.port()) }
        addFactory<RequestMapper> { UtAuthRequestMapper() }
        addFactory<WrapperProvider> { UtWrapperProvider() }
        addFactory<SessionInitMessageStreamFactory> { UtSessionInitMessageStreamFactory }
    }

}