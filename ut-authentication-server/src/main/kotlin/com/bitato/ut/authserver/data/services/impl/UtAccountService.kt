/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services.impl

import com.bitato.ut.authserver.data.persistence.UserDb
import com.bitato.ut.authserver.data.services.AccountService
import com.bitato.ut.log.util.LoggerBase
import com.bitato.ut.persist.utility.MemoryRow
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtAccountService : AccountService, LoggerBase() {

    private val userDb: UserDb = Injekt.get()

    override fun getSalt(userId: Int): String {
        return userDb.getSalt(userId)["sz_salt"] as? String ?: ""
    }

    override fun getUserByUsername(username: String): MemoryRow {
        return userDb.getAccountByUsername(username)
    }

    override fun verifyCredentials(username: String, password: String) {
        return userDb.verifyLoginCredentials(username, password)
    }

}