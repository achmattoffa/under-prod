/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.persistence.impl

import com.bitato.ut.authserver.data.persistence.UserPackageDb
import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.MemoryTable
import com.bitato.ut.persist.utility.PersistenceBase

class UtUserPackageDb : UserPackageDb, PersistenceBase() {

    /**
     * Gets all active tunnel packages assigned to a user. If the specified user has no active tunnel
     * packages, or an invalid user ID is specified, an empty list is returned
     */
    override fun getActiveUserPackagesByUserId(userId: Int): MemoryTable {
        return procQuery {
            procedure("usp_ut_user_package_get_active_packages_by_user_id")
            parameters(userId to DbParameterType.IN)
        }
    }

}