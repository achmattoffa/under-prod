/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services

import com.bitato.ut.persist.utility.MemoryRow

interface AccountService {

    fun getSalt(userId: Int): String

    fun verifyCredentials(username: String, password: String)

    fun getUserByUsername(username: String): MemoryRow
}