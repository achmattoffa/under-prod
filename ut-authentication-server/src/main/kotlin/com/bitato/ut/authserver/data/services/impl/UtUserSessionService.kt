/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services.impl

import com.bitato.ut.authserver.data.persistence.UserSessionDb
import com.bitato.ut.authserver.data.services.UserSessionService
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtUserSessionService : UserSessionService {

    private val db: UserSessionDb = Injekt.get()

    override fun getOrCreateSession(username: String): String {

        val userSession = db.getOrCreateSessionByUsername(username)

        return if (userSession.isNotEmpty()) {
            userSession["sz_user_session_uuid"] as String
        } else {
            ""
        }
    }
}