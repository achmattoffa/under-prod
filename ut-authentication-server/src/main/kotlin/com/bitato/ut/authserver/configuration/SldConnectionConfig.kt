/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.configuration

import org.aeonbits.owner.Config

@Config.Sources(
  "file:server.conf",
  "classpath:server.conf")
@Config.LoadPolicy(Config.LoadType.MERGE)
interface SldConnectionConfig : Config {

    @Config.Key("sld.address")
    fun remoteAddress(): String

    @Config.Key("sld.port")
    fun remotePort(): Int
}