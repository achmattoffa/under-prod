/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services

interface AccountAccessVerificationService {

    /**
     * Verifies that the current username and password combination is valid and that the associated
     * account has permission to access this service. Ie, the account is not banned, the account is
     * active, and the account has at least on active package.
     *
     * @param username  - The username associated with this Account
     * @param password - The password associated with this Account
     * @param isRawPassword - True if the password provided is in plain text, false if it has already
     * been encrypted
     *
     * @return - A byte value representing an AccountAccessVerificationCode
     */
    fun verifyAccountAccess(username: String, password: String, isRawPassword: Boolean): Byte
}