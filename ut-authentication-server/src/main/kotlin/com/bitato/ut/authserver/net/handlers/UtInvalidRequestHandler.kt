/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.authserver.net.handlers

import com.bitato.ut.api.model.net.sessioninit.SessionInitResponse
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.using
import com.bitato.ut.sbase.net.handler.AbstractInjectableRequestHandler
import com.github.salomonbrys.kotson.jsonObject
import java.net.Socket

class UtInvalidRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper)
    : AbstractInjectableRequestHandler(incoming, requestWrapper) {

    override fun run() {
        client.using {
            clientOut < wrapServerPayload(
              jsonObject(
                "success" to false,
                "code" to SessionInitResponse.ResponseCodes.ERR_INVALID_REQUEST
              ).toString().toByteArray(Charsets.ISO_8859_1)
            )
        }
    }
}