/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.persistence

import com.bitato.ut.persist.utility.MemoryTable


interface UserPackageDb {

    /**
     * Gets all active tunnel packages assigned to a user. If the specified user has no active tunnel
     * packages, or an invalid user ID is specified, an empty list is returned
     */
    fun getActiveUserPackagesByUserId(userId: Int): MemoryTable
}