/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.authserver.net.handlers

import com.bitato.ut.api.model.net.sessioninit.AccountAccessVerificationCode
import com.bitato.ut.api.model.net.sessioninit.SessionInitMessageStreamFactory
import com.bitato.ut.api.model.net.sessioninit.SessionInitRequest
import com.bitato.ut.api.model.net.sessioninit.SessionInitResponse
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.UtJsonHelper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.using
import com.bitato.ut.authserver.configuration.SldConnectionConfig
import com.bitato.ut.authserver.data.services.AccountAccessVerificationService
import com.bitato.ut.authserver.data.services.UserSessionService
import com.bitato.ut.sbase.net.handler.AbstractInjectableRequestHandler
import com.github.salomonbrys.kotson.jsonObject
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

class UtSessionInitRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper)
    : AbstractInjectableRequestHandler(incoming, requestWrapper) {

    private val verificationService: AccountAccessVerificationService = Injekt.get()
    private val sessionService: UserSessionService = Injekt.get()
    private val sldConfig: SldConnectionConfig = ConfigFactory.create(SldConnectionConfig::class.java)

    override fun run() {

        client.using {
            val messageFactory = Injekt.get<SessionInitMessageStreamFactory>()

            val sessionRequest = messageFactory.createSessionInitRequest(requestWrapper.payload.inputStream())
            log info "Session init request received for user ${sessionRequest.username}"

            val authenticationStatusCode = authenticateUser(sessionRequest)
            when (authenticationStatusCode) {
                AccountAccessVerificationCode.ACCESS_GRANTED -> lookupTunnelServer(sessionRequest)
                else -> {
                    clientOut < wrapServerPayload(
                      jsonObject(
                        "success" to false,
                        "code" to authenticationStatusCode
                      ).toString().toByteArray(Charsets.ISO_8859_1)
                    )
                }
            }
        }
    }

    private fun lookupTunnelServer(request: SessionInitRequest) {

        val tunnelSpecification = buildTunnelSpec(request)
        val url = "http://${sldConfig.remoteAddress()}:${sldConfig.remotePort()}/servers/lookup?$tunnelSpecification"
        val jsonResponse = UtJsonHelper.readFromUrlString(url)

        val tunnelServers = jsonResponse["servers"].asJsonArray
        if (tunnelServers.size() == 0) {
            clientOut < wrapServerPayload(
              jsonObject(
                "success" to false,
                "code" to SessionInitResponse.ResponseCodes.ERR_SLD_FAILED
              ).toString().toByteArray(Charsets.ISO_8859_1)
            )
        } else {
            val uuid = sessionService.getOrCreateSession(request.username)

            if (uuid.isEmpty()) {
                clientOut < wrapServerPayload(
                  jsonObject(
                    "success" to false,
                    "code" to SessionInitResponse.ResponseCodes.ERR_UNKNOWN
                  ).toString().toByteArray(Charsets.ISO_8859_1)
                )
            } else {
                clientOut < wrapServerPayload(
                  jsonObject(
                    "success" to true,
                    "code" to SessionInitResponse.ResponseCodes.ACCESS_GRANTED,
                    "uuid" to uuid,
                    "servers" to tunnelServers
                  ).toString().toByteArray(Charsets.ISO_8859_1)
                )
            }
        }
    }

    private fun buildTunnelSpec(request: SessionInitRequest): StringBuilder {
        return StringBuilder().apply {
            append("region=${request.region}&")
            append("protocol=${request.protocol}&")
            append("port=${request.port}")
        }
    }

    private fun authenticateUser(request: SessionInitRequest): Byte {
        return if (request.username.isNotEmpty()) {
            verificationService.verifyAccountAccess(request.username, request.password, true)
        } else {
            AccountAccessVerificationCode.ERR_INVALID_CREDENTIALS
        }
    }
}