/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.net

import com.bitato.ut.api.model.net.UtInvalidRequestException
import com.bitato.ut.api.model.net.sessioninit.SessionInitResponse
import com.bitato.ut.api.model.net.wrappers.ClientRequestType
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.model.net.wrappers.WrapperProvider
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.authserver.net.handlers.UtExploitDetectionRequestHandler
import com.bitato.ut.authserver.net.handlers.UtInvalidRequestHandler
import com.bitato.ut.authserver.net.handlers.UtSessionInitRequestHandler
import com.bitato.ut.sbase.net.handler.RequestHandler
import com.bitato.ut.sbase.net.mapper.AbstractRequestMapper
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket
import java.net.SocketTimeoutException

class UtAuthRequestMapper() : AbstractRequestMapper() {

    override fun mapRequest(incoming: Socket): RequestHandler {

        try {
            val wrapperProvider = Injekt.get<WrapperProvider>()

            incoming.soTimeout = 10000
            val requestWrapper = wrapperProvider.provideRequestWrapper(incoming.inputStream)
            incoming.soTimeout = 0

            return getRequestHandler(incoming, requestWrapper)
        }
        catch (ex: SocketTimeoutException) {
            incoming.getOutputStream() < SessionInitResponse(SessionInitResponse.ResponseCodes.ERR_INVALID_REQUEST)
            throw UtInvalidRequestException("Unable to determine request type. Socket has timed out")
        }
    }

    private fun getRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper): RequestHandler {

        return when (requestWrapper.requestType) {
            ClientRequestType.EXPLOIT_DETECTION_REQUEST -> UtExploitDetectionRequestHandler(incoming, requestWrapper)
            ClientRequestType.SESSION_INIT_REQUEST -> UtSessionInitRequestHandler(incoming, requestWrapper)
            else -> UtInvalidRequestHandler(incoming, requestWrapper)
        }
    }
}