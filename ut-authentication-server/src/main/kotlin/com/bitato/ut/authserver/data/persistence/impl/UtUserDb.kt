/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.persistence.impl

import com.bitato.ut.authserver.data.exceptions.UtAccountAuthenticationException
import com.bitato.ut.authserver.data.persistence.UserDb
import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.MemoryRow
import com.bitato.ut.persist.utility.PersistenceBase

class UtUserDb : UserDb, PersistenceBase() {

    /**
     * Get the salt stored for this user
     */
    override fun getSalt(userId: Int): MemoryRow {
        return procQuery {
            procedure("usp_gn_account_get_user_salt")
            parameters(userId to DbParameterType.IN)
        }
    }

    /**
     * Verifies a username and password against the database. If invalid credentials are provided,
     * an @UtAccountAuthenticationException is thrown
     */
    override fun verifyLoginCredentials(username: String, password: String) {
        val accountData = lazy<MemoryRow> {
            procQuery {
                procedure("usp_gn_account_get_by_username_and_password")
                parameters(username to DbParameterType.IN, password to DbParameterType.IN)
            }
        }

        if (accountData.value.isEmpty()) {
            throw UtAccountAuthenticationException("Unable to verify account login credentials")
        }
    }

    /**
     * Gets a user account by a username. If no account is found for the specified username, an empty
     * List is returned
     */
    override fun getAccountByUsername(username: String): MemoryRow {
        return procQuery {
            procedure("usp_gn_account_get_user_by_username")
            parameters(username to DbParameterType.IN)
        }
    }
}