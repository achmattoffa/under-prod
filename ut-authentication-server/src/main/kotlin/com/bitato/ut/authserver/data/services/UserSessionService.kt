/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services

interface UserSessionService {

    fun getOrCreateSession(username: String): String
}