/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.net

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket


class UtAuthServer(protocol: NetworkProtocol, port: Int) : AbstractServer(protocol, port) {

    val requestMapper: RequestMapper = Injekt.get()

    override fun handleClientConnection(client: Socket) {
        val requestHandler = requestMapper.mapRequest(client)
        threadPool.submit(requestHandler)
    }
}