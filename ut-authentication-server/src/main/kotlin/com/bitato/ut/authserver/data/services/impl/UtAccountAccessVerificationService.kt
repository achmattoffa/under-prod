/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services.impl

import com.bitato.ut.api.model.net.sessioninit.AccountAccessVerificationCode
import com.bitato.ut.authserver.data.exceptions.UtAccountAuthenticationException
import com.bitato.ut.authserver.data.services.AccountAccessVerificationService
import com.bitato.ut.authserver.data.services.AccountService
import com.bitato.ut.authserver.data.services.UserPackageService
import com.bitato.ut.log.util.ServiceDiagnostic
import com.bitato.ut.security.services.PasswordEncryptionService
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtAccountAccessVerificationService : AccountAccessVerificationService {

    private val accountService: AccountService = Injekt.get()
    private val passwordEncryptionService: PasswordEncryptionService = Injekt.get()
    private val userPackageService: UserPackageService = Injekt.get()

    /**
     * Verifies that the current username and password combination is valid and that the associated
     * account has permission to access this service. Ie, the account is not banned, the account is
     * active, and the account has at least on active package.
     *
     * @param username  - The username associated with this Account
     * @param password - The password associated with this Account
     * @param isRawPassword - True if the password provided is in plain text, false if it has already
     * been encrypted
     *
     * @return - A byte value representing an AccountAccessVerificationCode
     */
    override fun verifyAccountAccess(username: String, password: String, isRawPassword: Boolean): Byte {

        val account = accountService.getUserByUsername(username)
        if (account.isEmpty()) {
            return AccountAccessVerificationCode.ERR_INVALID_CREDENTIALS
        }

        var pwd = password
        if (isRawPassword) {
            //This is a raw password (unencrypted). Get the salt for this account and encrypt this
            //raw password so that we can compare the encrypted passwords in the data store.
            val salt = account["sz_salt"] as String
            pwd = passwordEncryptionService.encryptPassword(password, salt)
        }

        try {
            accountService.verifyCredentials(username, pwd)
        } catch (ex: UtAccountAuthenticationException) {
            ServiceDiagnostic.logException(ex)
            return AccountAccessVerificationCode.ERR_INVALID_CREDENTIALS
        }

        if (account["bt_banned"] as Boolean) {
            return AccountAccessVerificationCode.ERR_BANNED
        }

        if (!(account["bt_active"] as Boolean)) {
            return AccountAccessVerificationCode.ERR_DISABLED
        }

        val activePackages = userPackageService.getActiveUserPackages(account["pki_account_id"] as Int)

        if (activePackages.isEmpty()) {
            return AccountAccessVerificationCode.ERR_SESSION_LIMIT
        }

        return AccountAccessVerificationCode.ACCESS_GRANTED
    }
}