/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver.data.services.impl

import com.bitato.ut.authserver.data.persistence.UserPackageDb
import com.bitato.ut.authserver.data.services.UserPackageService
import com.bitato.ut.persist.utility.MemoryTable
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtUserPackageService : UserPackageService {

    private val userPackageDb: UserPackageDb = Injekt.get()

    override fun getActiveUserPackages(userId: Int): MemoryTable {
        return userPackageDb.getActiveUserPackagesByUserId(userId)
    }

}