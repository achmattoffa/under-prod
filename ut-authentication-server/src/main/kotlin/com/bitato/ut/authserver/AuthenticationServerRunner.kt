/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.authserver

import com.bitato.ut.authserver.configuration.AuthenticationServerConfig
import com.bitato.ut.authserver.modules.AuthenticationServerInjektModule
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.persist.module.PersistenceBaseInjektModule
import com.bitato.ut.sbase.ServerBaseMain
import com.bitato.ut.sbase.module.ServerBaseInjektModule
import com.bitato.ut.security.modules.SecurityUtilityInjektModule
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.api.InjektRegistrar

class AuthenticationServerRunner : Runnable {

    override fun run() {

        val configuration = ConfigFactory.create(AuthenticationServerConfig::class.java)
        startServer(configuration)
    }

    companion object AuthServerMain : ServerBaseMain() {

        override fun InjektRegistrar.registerInjectables() {

            importModule(AuthenticationServerInjektModule)
            importModule(PersistenceBaseInjektModule)
            importModule(ServerBaseInjektModule)
            importModule(SecurityUtilityInjektModule)
            importModule(LogBaseInjektModule)
        }

        @JvmStatic fun main(args: Array<String>) {
            AuthenticationServerRunner().run()
        }
    }
}