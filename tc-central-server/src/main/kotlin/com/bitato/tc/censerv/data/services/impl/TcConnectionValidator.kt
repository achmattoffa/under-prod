/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.censerv.data.services.impl

import com.bitato.tc.censerv.data.persistance.ConnectionValidatorDb
import com.bitato.tc.censerv.data.services.ConnectionValidator
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class TcConnectionValidator: ConnectionValidator {

    private val connectionValidatorDb: ConnectionValidatorDb = Injekt.get()

    override fun validateConnectRequest(accessKey: String, user: String, ip: String): Boolean {
        val result = connectionValidatorDb.validateConnectRequest(accessKey, user, ip)
        return result["sz_status"] == "SUCCESS"

    }

    override fun validateServer(accessKey: String): Boolean {
        val result = connectionValidatorDb.validateInit(accessKey)
        return result["sz_status"] == "SUCCESS"
    }

}