/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.censerv.modules

import com.bitato.tc.censerv.data.persistance.ConnectionValidatorDb
import com.bitato.tc.censerv.data.persistance.impl.TcConnectionValidatorDb
import com.bitato.tc.censerv.data.services.ConnectionValidator
import com.bitato.tc.censerv.data.services.impl.TcConnectionValidator
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory

object CentralServerInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<ConnectionValidatorDb> { TcConnectionValidatorDb() }
        addFactory<ConnectionValidator> { TcConnectionValidator() }
    }
}