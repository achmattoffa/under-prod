/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.censerv.data.persistance.impl

import com.bitato.tc.censerv.data.persistance.ConnectionValidatorDb
import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.MemoryRow
import com.bitato.ut.persist.utility.PersistenceBase

class TcConnectionValidatorDb : ConnectionValidatorDb, PersistenceBase() {
    override fun validateConnectRequest(accessKey: String, username: String, ip: String): MemoryRow {

        return procQuery {
            procedure("usp_tn_license_user_validate_connection_request")
            parameters(accessKey to DbParameterType.IN, username to DbParameterType.IN, ip to DbParameterType.IN)
        }
    }

    override fun validateInit(accessKey: String): MemoryRow {

        return procQuery {
            procedure("usp_tn_license_validate_init")
            parameters(accessKey to DbParameterType.IN)
        }
    }
}