/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.censerv.configuration

import org.aeonbits.owner.Config
import org.aeonbits.owner.Config.*

@Sources("file:app.conf")
interface CentralServerConfig : Config {

    @Key("server.interface")
    @DefaultValue("127.0.0.1")
    fun bindAddress(): String

    @Key("server.port")
    @DefaultValue("80")
    fun bindPort(): Int

    @Key("server.maxThreads")
    @DefaultValue("16")
    fun maxActiveThreads(): Int
}

