/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.censerv

import com.bitato.tc.censerv.configuration.CentralServerConfig
import com.bitato.tc.censerv.data.services.ConnectionValidator
import com.bitato.tc.censerv.modules.CentralServerInjektModule
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.persist.module.PersistenceBaseInjektModule
import com.bitato.ut.security.modules.SecurityUtilityInjektModule
import org.aeonbits.owner.ConfigFactory
import spark.Spark.*
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.InjektMain
import uy.kohesive.injekt.api.InjektRegistrar


class CentralServer : Runnable {

    private fun configureHttpServer() {

        val config = ConfigFactory.create(CentralServerConfig::class.java)
        port(config.bindPort())
        ipAddress(config.bindAddress())
        threadPool(config.maxActiveThreads())
    }


    private fun applyBindings() {

        val validator = Injekt.getInstance<ConnectionValidator>(ConnectionValidator::class.java)

        path("/:accessKey") {
            get("/validate") { request, response ->

                if (validator.validateServer(request.params("accessKey").trim())) {
                    response.status(200)
                } else {
                    response.status(400)
                }
            }
            get("/connect/:user") { request, response ->

                val ipAddress = request.queryParams("ip")

                if (validator.validateConnectRequest(request.params("accessKey"), request.params("user").trim(), ipAddress)) {
                    response.status(200)
                } else {
                    response.status(400)
                }
            }
        }
    }

    override fun run() {
        configureHttpServer()
        applyBindings()
    }

    companion object CentralServer : InjektMain() {

        override fun InjektRegistrar.registerInjectables() {

            importModule(CentralServerInjektModule)
            importModule(PersistenceBaseInjektModule)
            importModule(SecurityUtilityInjektModule)
            importModule(LogBaseInjektModule)

        }

        @JvmStatic fun main(args: Array<String>) {
            CentralServer().run()
        }
    }
}

