/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.censerv.data.persistance

import com.bitato.ut.persist.utility.MemoryRow

interface ConnectionValidatorDb {

    fun validateConnectRequest(accessKey: String, username: String, ip: String): MemoryRow

    fun validateInit(accessKey: String): MemoryRow

}