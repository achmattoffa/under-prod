/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.censerv.data.services

interface ConnectionValidator {

    fun validateConnectRequest(accessKey: String, user: String, ip: String): Boolean

    fun validateServer(accessKey: String): Boolean

}

