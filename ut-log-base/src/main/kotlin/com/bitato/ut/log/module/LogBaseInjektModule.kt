/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.log.module

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory

object LogBaseInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {
        addFactory<Logger> { LoggerFactory.getLogger("com.bitato.ut") }
    }
}