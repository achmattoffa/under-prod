/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.log.util

object ServiceDiagnostic : LoggerBase() {

    fun logException(e: Any) {

        if (e is Exception )
            log error e
        else
            log info e.toString()
    }
}