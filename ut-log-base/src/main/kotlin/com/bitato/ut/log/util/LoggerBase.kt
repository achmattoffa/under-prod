/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.log.util

import org.slf4j.Logger
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

/**
 * Base class to be extended for generic logging using the slf4j logging framework
 */
open class LoggerBase {
    protected val log: UtLogger = UtLogger()

    protected infix fun UtLogger.info(text: Any) {
        this.logger.info(text.toString())
    }

    protected infix fun UtLogger.warn(text: Any) {
        this.logger.warn(text.toString())
    }

    protected infix fun UtLogger.error(text: Any) {
        this.logger.error(text.toString())
    }

    protected infix fun UtLogger.debug(text: Any) {
        this.logger.debug(text.toString())
    }

    protected infix fun UtLogger.trace(text: Any) {
        this.logger.trace(text.toString())
    }
}

infix fun UtLogger.info(text: Any) {
    this.logger.info(text.toString())
}

infix fun UtLogger.warn(text: Any) {
    this.logger.warn(text.toString())
}

infix fun UtLogger.error(text: Any) {
    this.logger.error(text.toString())
}

infix fun UtLogger.debug(text: Any) {
    this.logger.debug(text.toString())
}

infix fun UtLogger.trace(text: Any) {
    this.logger.trace(text.toString())
}

class UtLogger {
    internal val logger: Logger = Injekt.get()
}

