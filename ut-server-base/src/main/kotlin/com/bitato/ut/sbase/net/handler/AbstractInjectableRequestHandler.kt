/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.sbase.net.handler

import com.bitato.ut.api.model.common.Parsable
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import java.io.InputStream
import java.net.Socket

abstract class AbstractInjectableRequestHandler(incoming: Socket, protected val requestWrapper: UtRequestWrapper)
: AbstractRequestHandler(incoming) {

    protected fun <T: Parsable> readRequest(fn: (InputStream) -> T): T {
        return fn.invoke(requestWrapper.payload.inputStream())
    }

    protected fun wrapServerPayload(response: ByteArray): ByteArray {
        return ResponsePayloadWrapper.wrapServerPayload(response)
    }
}