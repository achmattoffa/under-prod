/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.sbase.net.mapper

import com.bitato.ut.log.util.LoggerBase

abstract class AbstractRequestMapper: RequestMapper, LoggerBase()