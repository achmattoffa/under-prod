/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.sbase.net.mapper

import com.bitato.ut.sbase.net.handler.RequestHandler
import java.net.Socket

interface RequestMapper {
    fun mapRequest(incoming: Socket): RequestHandler
}