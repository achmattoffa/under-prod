/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.sbase.net.handler

import com.bitato.ut.api.io.streamers.StreamerFactory
import com.bitato.ut.log.util.LoggerBase
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket


abstract class AbstractRequestHandler(protected val client: Socket) : RequestHandler, LoggerBase() {

    protected val streamerFactory: StreamerFactory = Injekt.get()

    protected var clientIn: InputStream = client.inputStream
    protected var clientOut: OutputStream = client.outputStream

}