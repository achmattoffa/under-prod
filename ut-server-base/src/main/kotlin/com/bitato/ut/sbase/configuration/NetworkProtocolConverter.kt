/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.sbase.configuration

import com.bitato.ut.api.model.net.NetworkProtocol
import org.aeonbits.owner.Converter
import java.lang.reflect.Method

class NetworkProtocolConverter : Converter<NetworkProtocol> {
    override fun convert(p0: Method?, p1: String?) = NetworkProtocol.valueOf(p1!!.toUpperCase())
}