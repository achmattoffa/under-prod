/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.sbase

import com.bitato.ut.api.util.protect
import com.bitato.ut.sbase.configuration.ServerBaseConfig
import com.bitato.ut.sbase.net.server.AbstractServer
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.InjektMain
import uy.kohesive.injekt.api.get


abstract class ServerBaseMain : InjektMain() {

    protected fun startServer(config: ServerBaseConfig) {

        protect {
            val server = Injekt.get<AbstractServer>(config)
            server.start()
        }
    }
}
