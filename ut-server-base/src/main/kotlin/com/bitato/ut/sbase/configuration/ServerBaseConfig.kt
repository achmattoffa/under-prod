/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.sbase.configuration

import com.bitato.ut.api.model.net.NetworkProtocol

interface ServerBaseConfig {

    fun bindTo(): String

    fun port(): Int

    fun protocol(): NetworkProtocol
}