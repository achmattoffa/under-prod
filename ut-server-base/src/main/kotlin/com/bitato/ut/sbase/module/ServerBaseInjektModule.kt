/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.sbase.module

import com.bitato.ut.api.io.streamers.StreamerFactory
import com.bitato.ut.api.io.streamers.UtStreamerFactory
import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.sbase.net.server.ServerSocketBindingService
import com.bitato.ut.sbase.net.server.ServerSocketFactory
import com.bitato.ut.sbase.net.server.UtServerSocketBindingService
import com.bitato.ut.sbase.net.server.UtServerSocketFactory
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import uy.kohesive.injekt.api.addPerKeyFactory
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object ServerBaseInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<StreamerFactory> { UtStreamerFactory() }

        addPerKeyFactory { protocol: NetworkProtocol -> UtServerSocketFactory.create(protocol) }

        //TODO adjust accordingly
        addFactory<ExecutorService> {
            Executors.newFixedThreadPool(100)
        }

        addFactory<ServerSocketBindingService> { UtServerSocketBindingService }
        addFactory<ServerSocketFactory> { UtServerSocketFactory }
    }
}