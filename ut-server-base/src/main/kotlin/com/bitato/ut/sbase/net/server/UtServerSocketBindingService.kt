/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.sbase.net.server

import java.net.InetSocketAddress
import java.net.ServerSocket

object UtServerSocketBindingService : ServerSocketBindingService {

    override fun bind(server: ServerSocket, port: Int) {

        if (server.isBound) {
            return
        }

        server.bind(InetSocketAddress(port))
    }

    override fun unbind(server: ServerSocket) {

        if (!server.isBound && server.isClosed) {
            return
        }

        server.close()
    }
}