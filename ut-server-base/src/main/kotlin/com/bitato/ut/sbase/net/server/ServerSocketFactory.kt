/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.sbase.net.server

import com.bitato.ut.api.model.net.NetworkProtocol
import java.net.ServerSocket


interface ServerSocketFactory {

    fun create(protocol: NetworkProtocol): ServerSocket
}