/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.sbase.net.server

import com.bitato.ut.api.io.monitors.applyWork
import java.io.InputStream
import java.io.OutputStream
import java.net.InetAddress
import java.net.Socket
import java.net.SocketAddress
import java.nio.channels.SocketChannel

class WorkInhibitingSocket(private val socket: Socket, val work: (ByteArray) -> Unit): Socket() {

    override fun setReuseAddress(on: Boolean) {
        socket.reuseAddress = on
    }

    override fun isInputShutdown(): Boolean {
        return socket.isInputShutdown
    }

    override fun connect(endpoint: SocketAddress?) {
        socket.connect(endpoint)
    }

    override fun connect(endpoint: SocketAddress?, timeout: Int) {
        socket.connect(endpoint, timeout)
    }

    override fun setKeepAlive(on: Boolean) {
        socket.keepAlive = on
    }

    override fun getPort(): Int {
        return socket.port
    }

    override fun setSoLinger(on: Boolean, linger: Int) {
        socket.setSoLinger(on, linger)
    }

    override fun getSoLinger(): Int {
        return socket.soLinger
    }

    override fun setTrafficClass(tc: Int) {
        socket.trafficClass = tc
    }

    override fun shutdownInput() {
        socket.shutdownInput()
    }

    override fun close() {
        socket.close()
    }

    override fun isClosed(): Boolean {
        return socket.isClosed
    }

    override fun setSoTimeout(timeout: Int) {
        socket.soTimeout = timeout
    }

    override fun toString(): String {
        return socket.toString()
    }

    override fun getSendBufferSize(): Int {
        return socket.sendBufferSize
    }

    override fun isBound(): Boolean {
        return socket.isBound
    }

    override fun getLocalSocketAddress(): SocketAddress {
        return socket.localSocketAddress
    }

    override fun getReceiveBufferSize(): Int {
        return socket.receiveBufferSize
    }

    override fun sendUrgentData(data: Int) {
        socket.sendUrgentData(data)
    }

    override fun getKeepAlive(): Boolean {
        return socket.keepAlive
    }

    override fun getTcpNoDelay(): Boolean {
        return socket.tcpNoDelay
    }

    override fun getLocalPort(): Int {
        return socket.localPort
    }

    override fun setSendBufferSize(size: Int) {
        socket.sendBufferSize = size
    }

    override fun getRemoteSocketAddress(): SocketAddress {
        return socket.remoteSocketAddress
    }

    override fun setReceiveBufferSize(size: Int) {
        socket.receiveBufferSize = size
    }

    override fun getTrafficClass(): Int {
        return socket.trafficClass
    }

    override fun getSoTimeout(): Int {
        return socket.soTimeout
    }

    override fun isConnected(): Boolean {
        return socket.isConnected
    }

    override fun setOOBInline(on: Boolean) {
        socket.oobInline = on
    }

    override fun setTcpNoDelay(on: Boolean) {
        socket.tcpNoDelay = on
    }

    override fun getOOBInline(): Boolean {
        return socket.oobInline
    }

    override fun getInputStream(): InputStream {
        return socket.getInputStream().applyWork(work)
    }

    override fun getReuseAddress(): Boolean {
        return socket.reuseAddress
    }

    override fun getLocalAddress(): InetAddress {
        return socket.localAddress
    }

    override fun isOutputShutdown(): Boolean {
        return socket.isOutputShutdown
    }

    override fun getChannel(): SocketChannel {
        return socket.channel
    }

    override fun setPerformancePreferences(connectionTime: Int, latency: Int, bandwidth: Int) {
        socket.setPerformancePreferences(connectionTime, latency, bandwidth)
    }

    override fun getInetAddress(): InetAddress {
        return socket.inetAddress
    }

    override fun bind(bindpoint: SocketAddress?) {
        socket.bind(bindpoint)
    }

    override fun getOutputStream(): OutputStream {
        return socket.getOutputStream().applyWork(work)
    }

    override fun shutdownOutput() {
        socket.shutdownOutput()
    }
}