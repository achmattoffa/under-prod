/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.sbase.net.server

import java.net.ServerSocket


interface ServerSocketBindingService {

    fun bind(server: ServerSocket, port: Int)

    fun unbind(server: ServerSocket)
}

