/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.sbase.net.server

import com.bitato.ut.api.model.common.Startable
import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.api.util.protect
import com.bitato.ut.api.util.thread
import com.bitato.ut.log.util.LoggerBase
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.ServerSocket
import java.net.Socket
import java.util.concurrent.ExecutorService

abstract class AbstractServer(val protocol: NetworkProtocol, val port: Int) : Startable, AutoCloseable, LoggerBase() {

    protected val server: ServerSocket
    protected val threadPool: ExecutorService

    init {
        log info "Creating unbound $protocol server"
        server = ServerSocket()
        threadPool = Injekt.get()
    }

    override fun start() {
        log info "Binding server"
        UtServerSocketBindingService.bind(server, port)

        if (server.isBound) {
            log info "Server started on port $port"

            log info "Starting listener worker thread"
            val th = thread {
                listen()
            }

            th.name = "Local Server"
        } else {
            log info "Unable to bind server on port $port"
        }
    }

    override fun close() {

        protect {
            log info "Stopping listener worker thread"
            threadPool.shutdown()
        }

        //waitUntil { threadPool.isTerminated }

        log info "Stopping proxy server"
        UtServerSocketBindingService.unbind(server)

        if (!server.isBound && server.isClosed) {
            log info "Proxy server successfully closed"
        } else {
            log info "Error closing proxy server"
        }
    }

    /**
     * Listen for incoming connections and handle them
     */
    private fun listen() {

        log info "Invoking listener process"
        log info "Waiting for incoming connections"

        while (server.isBound && !server.isClosed) {

            try {

                val client = WorkInhibitingSocket(server.accept(), {
                    log trace String(it, Charsets.ISO_8859_1)
                })

                log info "Incoming connection accepted"
                log info "Client IP = ${client.localAddress.hostName}"
                log info "Client Port = ${client.localPort}"

                handleClientConnection(client)
            } catch (ex: Exception) {
                if (ex.message != null) {
                    log error ex.message!!
                }
            }
        }
    }

    protected abstract fun handleClientConnection(client: Socket)
}