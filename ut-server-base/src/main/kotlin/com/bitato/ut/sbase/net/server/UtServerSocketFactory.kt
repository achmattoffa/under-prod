/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.sbase.net.server

import com.bitato.ut.api.model.net.NetworkProtocol
import java.net.ServerSocket

object UtServerSocketFactory : ServerSocketFactory {

    private const val JVM_7_EXCLUSIVE_BIND_PROPERTY = "sun.net.useExclusiveBind"
    private var isExclusiveBindPropertySet = false

    override fun create(protocol: NetworkProtocol): ServerSocket {

        if (!isExclusiveBindPropertySet) {
            if (System.getProperty(JVM_7_EXCLUSIVE_BIND_PROPERTY, "true") == "true") {
                System.setProperty(JVM_7_EXCLUSIVE_BIND_PROPERTY, "false")
            }

            isExclusiveBindPropertySet = true
        }

        val server: ServerSocket = ServerSocket()

        return server.apply {
            reuseAddress = false
        }
    }
}