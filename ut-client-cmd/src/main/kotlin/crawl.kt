
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.protect
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentSkipListSet

/*
 * Copyright (c) 2017. Under Tunnel
 */

val IGNORE_MASK = "^.*\\.(jpg|jpeg|css|png|gif|pdf|mp3|exe|svg|js|pdf|mp4|mkv|avi|rm|zip|rar|7z|json|ico|cnxmlplus|webp|xlsx|xls|doc|docx)$"
val IGNORE_HOSTS = "^.*(facebook|fb).*$"
val MAX_HOST_PAGES = 10000

interface Accumulator<T> {
    fun accumulate(): List<T>
}

class UtWebAddressAccumulator(private val url: String)
    : Accumulator<String> {

    var useProxy = false
    var proxyHost = ""
    var proxyPort = 0

    override fun accumulate(): List<String> {

        protect {
            val connection = Jsoup.connect(url)

              if (useProxy) {
                  connection.proxy(proxyHost, proxyPort)
              }

            connection.followRedirects(true)
            connection.timeout(5000)

            val document = connection.get()
            return document.urls().toList()
        }

        return emptyList()
    }

    /**
     * Strips all urls present in a Jsoup Document and returns those urls in a List
     * @return List<String> containing all urls present in this Document
     */
    private fun Document.urls(): Collection<String> {

        val links = HashSet<String>()

        select("[src]").map {
            it.attr("abs:src")
        }.forEach {
            var stripped = it

            if (stripped.contains("#")) {
                stripped = stripped.substring(0, stripped.indexOf('#'))
            }

            if (stripped.contains("?")) {
                stripped = stripped.substring(0, stripped.indexOf('?'))
            }
            links.add(stripped)
        }

        select("[href]").map {
            it.attr("abs:href")
        }.filter {
            !it.startsWith("mailto:", true)
        }.forEach {
            var stripped = it

            if (stripped.contains("#")) {
                stripped = stripped.substring(0, stripped.indexOf('#'))
            }

            if (stripped.contains("?")) {
                stripped = stripped.substring(0, stripped.indexOf('?'))
            }
            links.add(stripped)
        }

        return links
    }
}

class Crawler(private var url: String) : Runnable {

    var useProxy = false
    var proxyHost = ""
    var proxyPort = 0

    companion object {
        val urlHistory = ConcurrentSkipListSet<String>()
        val hostHistory = ConcurrentHashMap<String, Int>()
    }

    override fun run() {

        try {
            perform()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    private fun perform() {

        url = url.trim()

        if (url.isEmpty()) {
            return
        }

        if (url.matches(IGNORE_MASK.toRegex()) || url.matches(IGNORE_HOSTS.toRegex()) || url.contains("%25")) {
            return
        }

        val h = getHost(url)
        if (urlHistory.contains(url) || (hostHistory.containsKey(h) && hostHistory[h]!! >= MAX_HOST_PAGES - 1) || url.length > 450) {
//        if (urlHistory.contains(url)) {
            return
        } else {
            if (!hostHistory.containsKey(h)) {
                hostHistory.put(h, 0)
                writeHost(h)
                println(h)
            } else {
                hostHistory[h] = hostHistory[h]!! + 1
            }
        }

        //println(url)
        urlHistory.add(url)
        writeUrl(h, url)

        protect {
            val accumulator = UtWebAddressAccumulator(url)
//
//            if (useProxy) {
//                accumulator.useProxy = true
//                accumulator.proxyHost = proxyHost
//                accumulator.proxyPort = proxyPort
//            }

            val urls = accumulator.accumulate()

            urls.filter {
                !it.toLowerCase().matches(IGNORE_MASK.toRegex()) &&
                !urlHistory.contains(it)
                //it.isNotEmpty()
            }

            .forEach {
                Thread(Crawler(it)).run()

                while (Thread.activeCount() > 100) {
                    Thread.sleep(300)
                }
            }

        }
    }

    fun getHost(url: String): String {

        var host = url

        if (host.startsWith("https://")) {
            host = host.replaceFirst("https://", "")
        }

        if (host.startsWith("http://"))
            host = host.replaceFirst("http://", "")

        if (host.contains("/"))
            host = host.substring(0, host.indexOf("/"))

        return host
    }
}

@Synchronized fun writeUrl(host: String, url: String) {

    val fos = FileOutputStream("output\\$host.txt", true)
    fos < "$url\r\n"
    fos.close()
}

@Synchronized fun writeHost(host: String) {

    val fos = FileOutputStream("output\\hosts.txt", true)
    fos < "$host\r\n"
    fos.close()
}

fun main(args: Array<String>) {

    val hosts = arrayListOf(
      "www.vodacombusiness.co.za",
      "staging-online-games.vodacom.co.za",
    "staging-gateway.mondiamedia.com",
    "staging-d.mondiamedia.com",
    "staging-vodacom-lcm.mondiamedia.com",
    "staging-mondia-lcm.vodacom.co.za",
    "d.wap.de",
    "delivery.vip.arvm.de",
    "vlive.mobi",
    "mondia-lcm.vodacom.co.za",
    "vodacom-castor.mondiamedia.com",
    "vodacom-ftmd.mondiamedia.com",
    "www.vodacom.co.za",
    "smart.vodacom.co.za",
    "retailweb.vodacom.co.za",
    "help.vodacom.co.za",
    "games.vodacom.co.za",
    "myvodacom.secure.vodacom.co.za",
    "www.vodacom4me.co.za",
    "staging-games.vodacom.co.za",
    "staging-music.vodacom.co.za",
    "live.vodacom.co.za",
    "staging-placebo.mondiamedia.com",
    "i.mondiamedia.com",
    "p.mondiamedia.com",
    "d.mondiamedia.com",
    "dev-games.vodacom.co.za",
    "dev-music.vodacom.co.za",
    "placebo.mondiamedia.com",
    "netperform.vodafone.com",
    "vgrop67lb-vip.dc-ratingen.de",
    "preprod.vfglogin.vodafone.com",
    "pre.start.vodafone.com",
    "start.vodafone.com",
    "hap-pre.sp.vodafone.com",
    "hap.sp.vodafone.com",
    "metric.vodacom.co.za",
    "siteintercept.qualtrics.com",
    "lz2.vodafone.com",
    "lz1.vodafone.com",
    "live.vodafone.com"

//      "wap.mtn.co.za",
//      "heliosuat.mtn.co.za",
//      "usd6.mtn.co.za",
//      "im.mtn.co.za",
//      "brc.mtn.co.za",
//      "www.mtndirect.co.za",
//      "wapportal.mtn.co.za",
//      "ppm.mtn.co.za",
//      "symbox.mtn.co.za",
//      "mtninternet.co.za",
//      "nofunds.mtn.co.za",
//      "mag.mtn.co.za",
//      "mtn.mobi",
//      "mtnsp.co.za",
//      "mymtnza.mtn.co.za",
//      "1app.mtn.co.za",
//      "ptldynamic-gamerplus.mtn.co.za",
//      "m-gamerplus.mtn.co.za",
//      "gamerplus.mtn.co.za",
//      "stage-1app.mtn.co.za",
//      "mms.mtn.co.za",
//      "www.mtnplay.mobi",
//      "www.mtnloaded.co.za",
//      "mtnloaded.mtn.co.za",
//      "mtnplay.mobi",
//      "apps.mtnplay.mobi",
//      "www.mtnplay.co.za",
//      "m.appshop.sa.imimobile.net",
//      "mtnplay.co.za",
//      "choconilla.mobi",
//      "speedtest.mtnbusiness.co.za",
//      "41-208-63-41.mtnns.net",
//      "oma.sabreweries.com",
//      "online.sabreweries.com",
//      "onlinedev.sabreweries.com",
//      "www.mtnplay.rw",
//      "mail.eaur.ac.rw",
//      "mtnplay.rw",
//      "mail.ufinance.co.rw",
//      "mail.mtnonline.rw",
//      "lsam.co.za",
//      "svn.mtnonline.rw",
//      "mtnbulkmail-1.discovery.co.za",
//      "www.mtnonline.rw",
//      "mtnbulkmail-2.discovery.co.za",
//      "api.wasac.rw",
//      "mail.autotrak.co.za",
//      "mail.autotraklive.com",
//      "mail.palabora.co.za",
//      "jh-trafweb-1.mtnns.net",
//      "mail.palabora.com",
//      "es-cr-1.mtnonline.rw",
//      "hq-ha-1.mtnonline.rw",
//      "m10.miranetworks.net",
//      "rusizi-bras-1.mtnonline.rw",
//      "m11.miranetworks.net",
//      "es-bras-2.mtnonline.rw",
//      "es-pr-2.mtnonline.rw",
//      "hq-ha-2.mtnonline.rw",
//      "m12.miranetworks.net",
//      "hq-bras-1.mtnonline.rw",
//      "es-bras-1.mtnonline.rw",
//      "m13.miranetworks.net",
//      "www.nydavms.org.za",
//      "es-csw-2.mtnonline.rw",
//      "eoh-pap-jpfw-1.mtnns.net",
//      "hq-csw-1.mtnonline.rw",
//      "es-csw-1.mtnonline.rw",
//      "ironport.danmargroup.co.za",
//      "41-208-48-176.mtn.co.za",
//      "mtnfrontrow.com",
//      "www.danmargroup.co.za",
//      "mail1.soras.co.rw",
//      "eaco.int",
//      "mtnfrontrow.discoverdigital.co.za"
//
//      "www.cellc.co.za",
//      "m.cellc.co.za",
//      "cellc.mobi",
//      "www.cellc.mobi",
//      "wallpapers.cellc.mobi",
//      "mms.cmobile.co.za",
//      "staging.cellc.mobi",
//      "dirtybabes.mobi",
//      "preview.justsayyes.co.za",
//      "econetplay.mobi",
//      "fairdaily.com"

    )

    hosts.forEach {

//        val crawl1 = Thread(Crawler("http://$it/").apply {
//            useProxy = true
//            proxyHost = "196.6.128.12"
//            proxyPort = 80
//        })

        val crawl2 = Thread(Crawler("http://$it/").apply {
//            useProxy = true
//            proxyHost = "196.6.128.12"
//            proxyPort = 8080
        })


        crawl2.start()
    }

    while (Thread.activeCount() > 1) {
        Thread.sleep(300)
    }
}