import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.protect
import com.bitato.ut.api.util.thread
import com.bitato.ut.client.data.services.monitors.BandwidthStreamWrapper
import com.bitato.ut.client.module.ClientCoreInjektModule
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.sbase.ServerBaseMain
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import java.io.*
import java.net.Socket
import java.nio.charset.Charset

/*
 * Copyright (c) 2017. Under Tunnel
 */


val vodacomProxies = listOf(
  "196.6.128.12" to 80,
  "196.6.128.12" to 8080
)

val mtnProxies = listOf(
  "196.11.240.241" to 8080,
  "196.11.240.251" to 8080,
  "utny1.duckdns.org" to 80
)

val cellcProxies = listOf(
  "196.31.116.250" to 8080,
  "196.31.116.251" to 8080,
  "utny1.duckdns.org" to 80
)

val proxies = vodacomProxies
val baseDir = "C:\\Users\\Achmat\\Desktop\\New folder (2)\\"
val testsDir = "$baseDir\\vvv"

class ClientCmd : Runnable {

    override fun run() {
        val loadedHosts = loadHosts(baseDir)
          .distinct()
        val injections = loadInjections("injections")

        loadedHosts
          .filter {
              it.matches(".*(vodafone|vodacom|mondiamedia).*".toRegex(RegexOption.IGNORE_CASE))
          }
          .forEach { loadedHost ->
            print(".")
            thread {
                injections.forEach { injection ->
                    protect {
                        testInjection(loadedHost, injection)
                    }
                }

            }
            while (Thread.activeCount() >= 30) {
                Thread.sleep(100)
            }
        }

//        val strm = FileInputStream("C:\\Users\\Achmat\\Downloads\\all-deleted.csv\\all-deleted.csv")
//        val voda = FileOutputStream("cellc", false).bufferedWriter()
//
//        strm.reader()
//          .buffered()
//          .lines()
//          .map {
//              it.split(",")[0]
//          }.filter {
//            it.matches(".*(cellc|mxit|cmobile|blackberry|whatsapp|facebook).*".toRegex(RegexOption.IGNORE_CASE))
//          }
//          .forEach {
//            print(it + ",")
//            voda.appendln(it)
//        }
//        strm.close()
//        voda.close()
    }

    private fun testInjection(host: String, injectionLine: String) {

        val testTarget80 = "kproxy.com"
        val testTarget443 = "netbeans.org"

        io {

            val useProxy = injectionLine.split(";", limit = 2)[0].toBoolean()
            val injection = injectionLine.split(";", limit = 2)[1]
              .replace("{{TARGET_HOST}}", host)
              .replace("{{TEST_TARGET_80}}", testTarget80)
              .replace("{{TEST_TARGET_443}}", testTarget443)
              .replace("\\r", "\r")
              .replace("\\n", "\n")


            if (useProxy) {

                for ((a, b) in proxies) {
                    runTest(a, b, injection, host)
                }

            } else {
                runTest(testTarget80, 80, injection, host)
            }
        }
    }

    private fun runTest(a: String, b: Int, injection: String, host: String) {

        val target = Socket(a, b)
        target.soTimeout = 5000
        target.getOutputStream() < injection
        val resp = UtHttpStreamFactory.createHttpResponse(target.getInputStream())


        val fos = FileOutputStream("$testsDir\\$host.txt", true)
        fos < target.inetAddress.toString() + "\r\n"
        fos < target.port.toString() + "\r\n\r\n"
        fos < injection
        fos < resp
        fos < "=========================================================================================================\r\n\r\n"

        if (!resp.toString().contains("free.facebook", ignoreCase = true)) {
            println("SUCCESS With $host ===================================================================================")
        }

        io { target.close() }
        io { fos.close() }
    }

    private fun loadInjections(path: String): List<String> {

        return javaClass.getResourceAsStream(path).bufferedReader()
          .readLines()
          .filter(String::isNotBlank)
    }

    private fun loadHosts(jsonDirPath: String): List<String> {

        val folder = File(jsonDirPath)
        val hosts = ArrayList<String>()

        val jsonParser = JSONParser()

        folder.listFiles().forEach {

            if (!it.isDirectory) {

                if (it.extension.toLowerCase() == "txt") {
                    FileInputStream(it).bufferedReader(Charset.defaultCharset())
                      .lines()
                      .forEach {
                          hosts.add(it)
                      }
                } else {
                    val obj = jsonParser.parse(FileReader(it))

                    val js = obj as JSONObject

                    if (js.containsKey("Domain")) {
                        hosts.add(js.get("Domain") as String)
                    }


                    val domains = if (js.containsKey("Subdomains"))
                        js.get("Subdomains") as JSONArray
                    else if (js.containsKey("Domains"))
                        js.get("Domains") as JSONArray
                    else
                        throw Exception()

                    domains.forEach {
                        val jsonObject = it as JSONObject
                        if (jsonObject.containsKey("domain")) {
                            hosts.add((jsonObject.get("domain") as String).replace("*.", ""))
                        }
                    }
                }
            }
        }

        return hosts.distinct().filter {
            !it.toLowerCase().contains("no record")
        }
    }

    companion object ClientCmd : ServerBaseMain() {

        override fun InjektRegistrar.registerInjectables() {
            importModule(ClientCoreInjektModule)
            importModule(LogBaseInjektModule)

            addFactory<BandwidthStreamWrapper> {
                object : BandwidthStreamWrapper {
                    override fun wrap(inputStream: InputStream): InputStream {
                        return inputStream
                    }

                    override fun wrap(outputStream: OutputStream): OutputStream {
                        return outputStream
                    }
                }
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            ClientCmd().run()
        }
    }
}

