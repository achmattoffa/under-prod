/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.uw.web

import com.bitato.ut.api.JsonApiBase
import com.bitato.ut.api.modules.JsonApiInjektModule
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.persist.module.PersistenceBaseInjektModule
import com.bitato.ut.security.modules.SecurityUtilityInjektModule
import com.bitato.uw.web.api.WebApi
import com.bitato.uw.web.modules.WebApiInjektModule
import uy.kohesive.injekt.InjektMain
import uy.kohesive.injekt.api.InjektRegistrar

class WebRunner : Runnable, JsonApiBase() {

    override fun run() {
        val api = WebApi()
        api.initServer()
    }

    companion object WebApiMain : InjektMain() {

        override fun InjektRegistrar.registerInjectables() {
            importModule(WebApiInjektModule)
            importModule(PersistenceBaseInjektModule)
            importModule(SecurityUtilityInjektModule)
            importModule(LogBaseInjektModule)
            importModule(JsonApiInjektModule)
        }

        @JvmStatic fun main(args: Array<String>) {
            WebRunner().run()
        }
    }
}