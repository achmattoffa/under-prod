/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.uw.web.api.endpoints

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.api.JsonApiBase
import spark.Spark.get
import spark.Spark.path

class Servers : ApiEndpoint, JsonApiBase() {

    override fun configure() {

        path("/servers") {

            get("/listall") { request, response ->
                "TODO"
            }

            get("/coordinates") { request, response ->
                "TODO"
            }
        }
    }
}