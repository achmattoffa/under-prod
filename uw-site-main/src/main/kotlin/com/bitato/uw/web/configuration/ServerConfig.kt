/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.uw.web.configuration

import org.aeonbits.owner.Config
import org.aeonbits.owner.Config.DefaultValue
import org.aeonbits.owner.Config.Sources

@Sources(
  "file:server.conf",
  "classpath:server.conf")
@Config.LoadPolicy(Config.LoadType.MERGE)
interface ServerConfig: Config {

    @DefaultValue("80")
    fun bindPort(): Int

    @DefaultValue("128")
    fun maxConnections(): Int

    @DefaultValue("/web")
    fun staticFilesLocation(): String

    @DefaultValue("/api")
    fun apiPath(): String
}