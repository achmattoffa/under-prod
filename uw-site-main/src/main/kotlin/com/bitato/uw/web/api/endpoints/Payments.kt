/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.uw.web.api.endpoints

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.api.JsonApiBase
import spark.Spark.*


class Payments : ApiEndpoint, JsonApiBase() {

    override fun configure() {

        path("/payments") {

            get("gateways") { request, response ->
                "TODO"
            }
            post("validate") { request, response ->
                "TODO"
            }
        }
    }

}