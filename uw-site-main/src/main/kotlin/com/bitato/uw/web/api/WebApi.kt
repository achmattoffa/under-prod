/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.uw.web.api

import com.bitato.ut.api.ApiEndpoint
import com.bitato.uw.web.configuration.ServerConfig
import org.aeonbits.owner.ConfigFactory
import spark.Spark.*
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get


class WebApi {

    private val serverBindings: List<ApiEndpoint> = Injekt.get()
    private val serverConfiguration = ConfigFactory.create(ServerConfig::class.java)

    fun initServer() {


        port(serverConfiguration.bindPort())
        threadPool(serverConfiguration.maxConnections())

            val projectDir = System.getProperty("user.dir")
            val staticDir = """\uw-site-main\src\main\resources${serverConfiguration.staticFilesLocation()}"""

        staticFiles.externalLocation(projectDir + staticDir)
        //TODO
//      staticFiles.location(serverConfig.staticFilesLocation())

        applyApiBindings()
        init()
    }

    private fun applyApiBindings() {

        path(serverConfiguration.apiPath()) {
            serverBindings.forEach {
                it.configure()
            }
        }
    }
}