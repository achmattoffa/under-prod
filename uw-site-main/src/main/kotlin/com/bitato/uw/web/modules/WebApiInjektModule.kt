/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.uw.web.modules

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.api.model.net.sld.SldMessageStreamFactory
import com.bitato.ut.api.model.net.sld.UtSldMessageStreamFactory
import com.bitato.uw.web.api.endpoints.Accounts
import com.bitato.uw.web.api.endpoints.Payments
import com.bitato.uw.web.api.endpoints.Pricing
import com.bitato.uw.web.api.endpoints.Servers
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory

object WebApiInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<SldMessageStreamFactory> { UtSldMessageStreamFactory }
        addFactory<List<ApiEndpoint>> {
            listOf(
              Accounts(),
              Payments(),
              Pricing(),
              Servers()
            )
        }
    }
}

