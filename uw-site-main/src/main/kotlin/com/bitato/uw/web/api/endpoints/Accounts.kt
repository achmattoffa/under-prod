/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.uw.web.api.endpoints

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.api.JsonApiBase
import spark.Spark.*

class Accounts : ApiEndpoint, JsonApiBase() {

    override fun configure() {

        path("/accounts") {

            post("/signin") { request, response ->
                "TODO"
            }

            post("/signup") { request, response ->
                "TODO"
            }

            post("/resetpassword") { reguest, response ->
                "TODO"
            }

            get("/information") { request, response ->
                "TODO"
            }

            post("/information") { request, response ->
                "TODO"
            }
        }
    }

}