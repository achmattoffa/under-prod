/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.net

import com.bitato.tc.client.Settings
import com.bitato.tc.client.configuration.InjectionProvider
import com.bitato.ut.api.io.streamers.StreamerFactory
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.thread
import com.bitato.ut.api.util.waitWhile
import com.bitato.ut.sbase.net.handler.RequestHandler
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket
import java.util.*


abstract class TunnelRequestHandlerBase(protected val clientSock: Socket) : RequestHandler {

    protected var clientIn: InputStream = clientSock.inputStream
    protected var clientOut: OutputStream = clientSock.outputStream
    protected val streamerFactory: StreamerFactory = Injekt.get()

    protected fun connectToServer(): Optional<Socket> {
        io {
            val socket = InjectionProvider.connectToTarget(Settings.remoteHost, Settings.remotePort)
            return Optional.of(socket)
        }

        return Optional.empty()
    }

    /**
     * Handles the relaying of data in cases where there is no certainty as to which way the data
     * will be travelling. Data can be sent from any end-point. The stream ends when one of the two
     * end-points closes their underlying socket.
     *
     * @param remoteIn      The <code>InputStream</code> associated with the remote server
     * @param remoteOut     The <code>OutputStream</code> associated with the remote server
     *
     * @throws java.io.IOException
     * @throws InterruptedException
     */
    protected fun streamIndefinitely(remoteIn: InputStream, remoteOut: OutputStream) {

        val clientToRemote = streamerFactory.create(clientIn, remoteOut, StreamerFactory.StreamerType.INDEFINITE)
        val remoteToClient = streamerFactory.create(remoteIn, clientOut, StreamerFactory.StreamerType.INDEFINITE)

        val threads = listOf(
          thread {
              try {
                  clientToRemote.stream()
              } catch (ex: IOException) {
              }
          },
          thread {
              try {
                  remoteToClient.stream()
              } catch (ex: IOException) {
              }
          })

        waitWhile { threads.all { th -> th.isAlive } }
    }
}