/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.data.services.servers

import com.bitato.ut.api.model.common.Startable
import com.bitato.ut.api.util.AutoCloseableController
import com.bitato.ut.sbase.net.server.AbstractServer

interface ServerManager : AutoCloseableController<AbstractServer>, Startable