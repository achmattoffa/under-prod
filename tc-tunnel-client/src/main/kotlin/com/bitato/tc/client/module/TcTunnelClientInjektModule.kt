/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.module

import com.bitato.tc.client.concurrency.PacketSnifferDetector
import com.bitato.tc.client.data.services.servers.ServerManager
import com.bitato.tc.client.data.services.servers.UtServerManager
import com.bitato.tc.client.net.util.SocketManager
import com.bitato.tc.client.net.util.UtSocketManager
import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.model.net.internal.InternalMessageStreamFactory
import com.bitato.ut.api.model.net.internal.UtInternalMessageStreamFactory
import com.bitato.ut.api.model.net.sessioninit.SessionInitMessageStreamFactory
import com.bitato.ut.api.model.net.sessioninit.UtSessionInitMessageStreamFactory
import com.bitato.ut.api.model.net.socks.socks5.Socks5Factory
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory

object TcTunnelClientInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {
        addFactory<HttpStreamFactory> { UtHttpStreamFactory }
        addFactory<Socks5Factory> { Socks5Factory }
        addFactory<ServerManager> { UtServerManager }
        addFactory<SocketManager> { UtSocketManager }
        addFactory<SessionInitMessageStreamFactory> { UtSessionInitMessageStreamFactory }
        addFactory<InternalMessageStreamFactory> { UtInternalMessageStreamFactory }
        addFactory<List<AbstractScheduledBackgroundWorker>> { listOf(PacketSnifferDetector()) }
    }
}