/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.net

import com.bitato.tc.client.net.http.UtHttpRequestMapper
import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import java.net.Socket

abstract class UtProxyServerBase(port: Int) : AbstractServer(NetworkProtocol.TCP, port) {

    protected val requestMapper: RequestMapper = UtHttpRequestMapper()

    override fun handleClientConnection(client: Socket) {

        log info "Mapping incoming request to request handler"
        val requestHandler = requestMapper.mapRequest(client)

        log info "Submitting request handler to thread pool"
        threadPool.submit(requestHandler)
    }

}