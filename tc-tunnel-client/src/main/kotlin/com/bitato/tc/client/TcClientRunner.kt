/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client

import com.bitato.tc.client.configuration.InjectionProvider
import com.bitato.tc.client.data.services.servers.ServerManager
import com.bitato.tc.client.module.TcTunnelClientInjektModule
import com.bitato.tc.client.net.http.UtHttpProxyServer
import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.sbase.ServerBaseMain
import com.bitato.ut.sbase.module.ServerBaseInjektModule
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.get

class TcClientRunner: Runnable {


    override fun run() {

        val serverManager = Injekt.get<ServerManager>()
        serverManager.add(UtHttpProxyServer(Settings.bindPort))

        startBackgroundProcesses()
        serverManager.start()
    }

    fun startBackgroundProcesses() {
        val services = Injekt.get<List<AbstractScheduledBackgroundWorker>>()
        services.forEach(AbstractScheduledBackgroundWorker::start)
    }

    companion object : ServerBaseMain() {

        override fun InjektRegistrar.registerInjectables() {
            importModule(TcTunnelClientInjektModule)
            importModule(ServerBaseInjektModule)
            importModule(LogBaseInjektModule)
        }

        @JvmStatic fun main(args: Array<String>) {
            Settings.init(args)
            InjectionProvider.injectionString = InjectionProvider.getInjectionString("injection")

            TcClientRunner().run()
        }
    }
}