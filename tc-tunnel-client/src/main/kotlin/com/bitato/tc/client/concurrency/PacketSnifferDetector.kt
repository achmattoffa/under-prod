/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.concurrency

import ProcessHelper
import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker

class PacketSnifferDetector : AbstractScheduledBackgroundWorker(5, 1) {

    override fun run() {

        if (ProcessHelper.isProcessRunning(".*(wireshark|tcpdump|netcat|ettercap|smartsniff|sysdig|ethereal|cloudshark|skapy|netmon|dumpcap).*")) {
            System.err.println("This client cannot run while packet sniffers are open. Please close all packet sniffers and then try again")
            System.exit(-1)
        }
    }
}