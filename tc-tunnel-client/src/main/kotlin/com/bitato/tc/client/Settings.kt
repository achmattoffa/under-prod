/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client

object Settings {

    var bindPort: Int = 0
    var remotePort: Int = 0
    var remoteHost: String = ""
    var userName: String = ""

    fun init(args: Array<String>) {

        listOf<String>("bindPort", "remotePort", "remoteHost", "userName").forEach { parameter ->
            if (args.all { argument -> !argument.matches("^$parameter=.+".toRegex()) }) {
                println("The argument '$parameter' was not specified")
                System.exit(-1)
            }
        }

        val params = args.associate { argument ->
            val parts = argument.split("=")
            parts[0].trim() to parts[1].trim()
        }

        bindPort = params["bindPort"]!!.toInt()
        remotePort = params["remotePort"]!!.toInt()
        remoteHost = params["remoteHost"]!!
        userName = params["userName"]!!.toLowerCase()
    }
}
