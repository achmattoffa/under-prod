/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.net.util

import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

object UtSocketManager : SocketManager {

    private val sockets: MutableList<Socket>

    init {
        sockets = arrayListOf()
    }

    @Synchronized
    override fun add(t: Socket) {
        sockets.add(t)
    }

    @Synchronized
    override fun remove(t: Socket): Unit {
        t.use {
            sockets.remove(it)
        }
    }

    @Synchronized
    override fun close(): Unit {
        sockets.forEach(Socket::close)
        sockets.clear()
    }

    override fun count() = sockets.count()
}

fun Socket.registerHandle() = Injekt.get<SocketManager>().add(this)