/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.configuration

import com.bitato.ut.api.io.readUntil
import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.model.net.wrappers.ClientRequestType
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import java.io.FileInputStream
import java.io.InputStream
import java.net.InetSocketAddress
import java.net.Socket
import java.util.*

object InjectionProvider {

    lateinit var injectionString: String

    fun replaceVariables(text: String, remoteHost: String, remotePort: Int): String {

        return text.replace("@EOL@", "\r\n")
          .replace("@RHOST@", remoteHost)
          .replace("@RPORT@", remotePort.toString())
    }

    fun connectToTarget(remoteHost: String, remotePort: Int): Socket {
        val openSockMatches = Regex("^OPEN_SOCK (.+) (.+)$").matchEntire(replaceVariables(injectionString.lines()[0], remoteHost, remotePort))!!
        val socketHost = openSockMatches.groups[1]!!.value
        val socketPort = openSockMatches.groups[2]!!.value.toInt()

        val initRegex = Regex(".*INIT_START(.*)INIT_END.*", RegexOption.DOT_MATCHES_ALL)
        val sockActions = ArrayList<(Socket) -> Any>()

        sockActions.add {
            it.connect(InetSocketAddress(socketHost, socketPort), 5000)
        }

        if (initRegex.matches(injectionString)) {
            sockActions.addAll(
              initRegex.find(injectionString)!!
                .groups[1]!!
                .value
                .trim()
                .lines()
                .filterNot { it.isEmpty() }
                .map {
                    val textParts = it.split(" +".toRegex(), 2)

                    val currentCommand = textParts[0]
                    val commandText = replaceVariables(textParts[1], remoteHost, remotePort)

                    when (currentCommand) {
                        "SWRITE" -> {
                            { target: Socket ->
                                target.outputStream < commandText
                            }
                        }
                        "SREADTO" -> {
                            { target: Socket ->
                                target.inputStream.readUntil(commandText)

                            }
                        }
                        "SFLUSH" -> {
                            { target: Socket ->
                                target.outputStream.flush()
                            }
                        }
                        else -> {
                            {
                                throw Exception()
                            }
                        }
                    }
                })
        }

        return Socket().apply {
            sockActions.forEach {
                it.invoke(this)
            }
        }
    }

    fun getConfigMap(remoteHost: String, remotePort: Int = 0): Map<String, String> {

        val configRegex = Regex(".*CONFIG_START(.*)CONFIG_END.*", RegexOption.DOT_MATCHES_ALL)
        val configMap = if (configRegex.matches(injectionString)) {

            configRegex.find(injectionString)!!
              .groups[1]!!
              .value
              .trim()
              .lines()
              .filterNot { it.isEmpty() }
              .map {
                  val textParts = it.split(" +".toRegex(), 2)
                  textParts[0] to replaceVariables(textParts[1], remoteHost, remotePort)
              }
              .toMap()
        } else {
            emptyMap()
        }

        return configMap
    }


    fun injectRequestPayload(requestPayload: ByteArray, requestType: ClientRequestType, remoteHost: String, remotePort: Int, userName: String): ByteArray {

        val config = getConfigMap(remoteHost, remotePort)
        val injection = config["INJECT_STRING"] ?: "GET / HTTP/1.1\r\nHost: foo.net\r\nConnection: Keep-Alive\r\n\r\n"

        val requestWrapper = UtRequestWrapper(userName.trim().toLowerCase(), requestType, requestPayload)

        val http = HttpRequest(injection).apply {
            headers.add("x-req", requestWrapper.parse())
            headers.add("x-dat", Base64.getEncoder().encode(requestPayload))
        }

        return http.parse()
    }

    fun <T> getResponsePayload(inputStream: InputStream, transformer: (ByteArray) -> T): T {
        val http = UtHttpStreamFactory.createHttpResponse(inputStream)
        val bytes = Base64.getDecoder().decode(http.headers.getValueOf("x-dat"))

        return transformer.invoke(bytes)
    }

    fun getInjectionString(path: String): String  {
        return if (FileInputStream(path).bufferedReader(Charsets.ISO_8859_1).readText().startsWith("<3") && FileInputStream(path).bufferedReader(Charsets.ISO_8859_1).readText().endsWith("2JZ")) {
            val str = FileInputStream(path).bufferedReader(Charsets.ISO_8859_1).readText().replaceFirst("<3", "").replace("2JZ", "")
            String(Base64.getDecoder().decode(str.reversed()))
        } else {
            FileInputStream(path)
              .bufferedReader()
              .lines()
              .toArray()
              .joinToString("\r\n")
        }
    }

}