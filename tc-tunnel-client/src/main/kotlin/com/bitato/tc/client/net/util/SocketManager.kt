/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.net.util

import com.bitato.ut.api.util.AutoCloseableController
import java.net.Socket


interface SocketManager : AutoCloseableController<Socket> {
    fun count(): Int
}

