/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.client.data.events

object EventBus {

    object EventIDs {

        const val REMOTE_TUNNEL_STARTED = "tunnel.started"
        const val REMOTE_TUNNEL_STOPPING = "tunnel.stopping"
        const val REMOTE_TUNNEL_STOPPED = "tunnel.stopped"
        const val REMOTE_TUNNEL_FAILED = "tunnel.failed"

        //CLIENT PROXY
        const val PROXY_REMOTE_CONNECTION_ESTABLISHED = "proxy.remote_connected"
        const val PROXY_REMOTE_CONNECTION_FAILED = "proxy.remote_failed"
        const val PROXY_REMOTE_REQUEST_SENT = "proxy.client_request_sent"
        const val PROXY_REMOTE_RESPONSE_RECEIVED = "proxy.remote_response_read"
        const val PROXY_REMOTE_CLIENT_PAYLOAD_SENT = "proxy.client_content_sent"
        const val PROXY_REMOTE_CONTENT_STREAMED = "proxy.remote_content_streamed"
        const val PROXY_CLIENT_RESPONSE_SENT = "proxy.client_response_sent"
        const val PROXY_STREAM_COMPLETED = "proxy.stream_completed"
        const val PROXY_ERROR_BEFORE_REMOTE_RESPONDED = "proxy.error_before_client_response"
        const val PROXY_ERROR_AFTER_REMOTE_RESPONDED = "proxy.error_after_client_response"
    }


    private val subscribers: HashMap<String, ArrayList<(Any) -> Unit>> = HashMap()

    @Synchronized
    fun subscribe(id: String, fn: (Any) -> Unit) {

        if (subscribers.containsKey(id)) {
            subscribers[id]!!.clear()
            subscribers[id]!!.add(fn)
        } else {
            subscribers.put(id, arrayListOf(fn))
        }
    }

    @Synchronized
    fun notify(id: String, data: Any = Any()) {
        if (subscribers.containsKey(id)) {
            subscribers[id]!!.forEach {
                it.invoke(data)
            }
        }
    }
}

