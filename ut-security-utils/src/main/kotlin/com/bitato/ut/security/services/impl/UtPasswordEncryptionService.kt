/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services.impl

import com.bitato.ut.security.configuration.EncryptionConfig
import com.bitato.ut.security.services.PasswordEncryptionService
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import javax.crypto.Mac
import javax.crypto.SecretKey

class UtPasswordEncryptionService(): PasswordEncryptionService {

    private val keySpec: SecretKey = Injekt.get()
    private val mac: Mac = Injekt.get()
    private val encryptionConfig: EncryptionConfig = Injekt.get()

    override fun encryptPassword(rawPassword: String, salt: String): String {

        mac.init(keySpec)

        var lastHash = salt + rawPassword

        // Apply encryption procedure for n iterations
        for (i in 0 until encryptionConfig.encryptionIterations()) {

            val hash = mac.doFinal(lastHash.toByteArray(Charsets.ISO_8859_1))

            val hashBuilder = StringBuilder().apply {
                hash.forEach {
                    val hex = Integer.toHexString(0xFF and it.toInt())
                    if (hex.length == 1) {
                        this@apply.append('0')
                    } else {
                        this@apply.append(hex)
                    }
                }
            }

            lastHash = salt + hashBuilder.toString()
        }

        return lastHash
    }
}