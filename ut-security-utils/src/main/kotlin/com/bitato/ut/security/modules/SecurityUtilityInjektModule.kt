/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.modules

import com.bitato.ut.security.configuration.EncryptionConfig
import com.bitato.ut.security.services.PasswordEncryptionService
import com.bitato.ut.security.services.SaltGenerator
import com.bitato.ut.security.services.SessionKeyGenerator
import com.bitato.ut.security.services.impl.UtPasswordEncryptionService
import com.bitato.ut.security.services.impl.UtSaltGenerator
import com.bitato.ut.security.services.impl.UtSessionKeyGenerator
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import java.security.MessageDigest
import java.security.SecureRandom
import java.util.*
import javax.crypto.Mac
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec


object SecurityUtilityInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        val config = ConfigFactory.create(EncryptionConfig::class.java)

        addFactory<EncryptionConfig> { config }

        addFactory<SecretKey> { SecretKeySpec((config.keySpec()).toByteArray(Charsets.ISO_8859_1), config.hashingAlgorithm()) }

        addFactory<Mac> { Mac.getInstance(config.hashingAlgorithm()) }

        addFactory<Random> { SecureRandom() }

        addFactory<MessageDigest> { MessageDigest.getInstance(config.hashFunction()) }

        addFactory<PasswordEncryptionService> { UtPasswordEncryptionService() }

        addFactory<SaltGenerator> { UtSaltGenerator() }

        addFactory<SessionKeyGenerator> { UtSessionKeyGenerator() }
    }
}
