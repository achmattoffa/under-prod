/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services

interface SessionKeyGenerator {

    fun generateKey(): String
}
