/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services.impl

import com.bitato.ut.security.services.SessionKeyGenerator
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.util.*
import javax.crypto.Mac
import javax.crypto.SecretKey

class UtSessionKeyGenerator(): SessionKeyGenerator {

    private val keySpec: SecretKey = Injekt.get()
    private val mac: Mac = Injekt.get()

    override fun generateKey(): String {

        mac.init(keySpec)
        val rand = Random()


        var lastHash = "${rand.nextInt(97836122)}"

        // Apply encryption procedure for n iterations
        for (i in 0 until 0x0F) {

            val hash = mac.doFinal(lastHash.toByteArray(Charsets.ISO_8859_1))

            val hashBuilder = StringBuilder().apply {
                hash.forEach {
                    val hex = Integer.toHexString(0xFF and it.toInt())
                    if (hex.length == 1) {
                        this@apply.append('0')
                    } else {
                        this@apply.append(hex)
                    }
                }
            }

            lastHash = hashBuilder.toString()
        }

        return lastHash
    }
}