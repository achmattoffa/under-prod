/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services

interface SaltGenerator {

    /**
     * Generate Salt to use with password encryption
     */
    fun generateSalt(): String
}

