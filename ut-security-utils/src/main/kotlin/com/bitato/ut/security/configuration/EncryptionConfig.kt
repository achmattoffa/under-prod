/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.configuration

import org.aeonbits.owner.Config

interface EncryptionConfig: Config {

    @Config.DefaultValue("200")
    fun encryptionIterations(): Int

    @Config.DefaultValue("HmacSHA1")
    fun hashingAlgorithm(): String

    @Config.DefaultValue("SHA1")
    fun hashFunction(): String

    @Config.DefaultValue("86122123")
    fun keySpec(): String
}