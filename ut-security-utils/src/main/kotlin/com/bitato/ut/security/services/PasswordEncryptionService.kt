/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services

interface PasswordEncryptionService {

    /**
     * Encrypt a given password using a set algo and a generated salt
     */
    fun encryptPassword(rawPassword: String, salt: String): String
}