/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services.impl

import com.bitato.ut.security.services.SaltGenerator
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.security.MessageDigest
import java.util.*

class UtSaltGenerator(): SaltGenerator {

    private val secureRandom: Random = Injekt.get()
    private val messageDigest: MessageDigest = Injekt.get()

    override fun generateSalt(): String {

        val random = Integer(secureRandom.nextInt()).toString()
        return hexify(messageDigest.digest(random.toByteArray(Charsets.ISO_8859_1))).toString()
    }

    private fun hexify(bytes: ByteArray): StringBuilder {

        val hexTable = ArrayList<Char>()
        hexTable.addAll('0'..'9')
        hexTable.addAll('a'..'f')

        return StringBuilder().apply {
            bytes.forEach { b ->
                append(hexTable[(b and 0xF0) shr 4].toChar())
                append(hexTable[(b and 0x0F)].toChar())
            }
        }
    }

    private infix fun Byte.and(i: Int): Int {
        return this.toInt() and i
    }
}