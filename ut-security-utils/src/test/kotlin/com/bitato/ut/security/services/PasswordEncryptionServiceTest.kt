/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services

import com.bitato.ut.security.SecurityUtilityUnitTestBase
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class PasswordEncryptionServiceTest : SecurityUtilityUnitTestBase() {

    private val passwordEncryptionService: PasswordEncryptionService
    private val saltGenerator: SaltGenerator

    init {
        passwordEncryptionService = Injekt.get()
        saltGenerator = Injekt.get()
    }

    @Test
    fun encryptedStringsShouldBeTheSameIfPasswordAndSaltIsTheSame() {

        val maxTries = 100
        val rawPassword = "testPassword"
        val salt = saltGenerator.generateSalt()

        val encryptedPassword = passwordEncryptionService.encryptPassword(rawPassword, salt)
        for (i in 0..maxTries) {
            val newEncryptedPassword = passwordEncryptionService.encryptPassword(rawPassword, salt)
            assertEquals(encryptedPassword, newEncryptedPassword)
        }
    }

    @Test
    fun encryptedStringsShouldBeDifferentIfPasswordIsTheSameAndSaltIsDifferent() {

        val maxTries = 100
        val rawPassword = "testPassword"
        val salt = saltGenerator.generateSalt()

        val encryptedPassword = passwordEncryptionService.encryptPassword(rawPassword, salt)

        for (i in 0..maxTries) {
            val newSalt = saltGenerator.generateSalt()
            val newEncryptedPassword = passwordEncryptionService.encryptPassword(rawPassword, newSalt)

            assertNotEquals(encryptedPassword, newEncryptedPassword)
        }
    }
}