/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security

import com.bitato.ut.security.modules.SecurityUtilityInjektModule
import uy.kohesive.injekt.InjektMain
import uy.kohesive.injekt.api.InjektRegistrar

abstract class SecurityUtilityUnitTestBase : InjektMain() {

    override fun InjektRegistrar.registerInjectables() {
        importModule(SecurityUtilityInjektModule)
    }
}