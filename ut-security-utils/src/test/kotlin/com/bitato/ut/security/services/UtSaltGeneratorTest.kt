/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services

import com.bitato.ut.security.SecurityUtilityUnitTestBase
import org.junit.Assert.assertFalse
import org.junit.Test
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtSaltGeneratorTest: SecurityUtilityUnitTestBase() {

    private val saltGenerator: SaltGenerator

    init {
        saltGenerator = Injekt.get()
    }

    @Test
    fun noTwoGeneratedSaltsShouldBeTheSame() {

        val maxRuns = 500
        val saltList = arrayListOf(saltGenerator.generateSalt())

        for (i in 0..maxRuns) {
            val salt = saltGenerator.generateSalt()
            assertFalse(saltList.contains(salt))
            saltList.add(salt)
        }
    }
}