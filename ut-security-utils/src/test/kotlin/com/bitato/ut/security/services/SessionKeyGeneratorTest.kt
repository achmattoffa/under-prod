/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.security.services

import com.bitato.ut.security.SecurityUtilityUnitTestBase
import org.junit.Assert.assertFalse
import org.junit.Test
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class SessionKeyGeneratorTest : SecurityUtilityUnitTestBase() {

    private val sessionKeyGen: SessionKeyGenerator

    init {
        sessionKeyGen = Injekt.get()
    }

    @Test
    fun noTwoGenerateKeysShouldBeTheSame() {

        val maxRuns = 1000
        val saltList = arrayListOf(sessionKeyGen.generateKey())

        for (i in 0..maxRuns) {
            val salt = sessionKeyGen.generateKey()
            assertFalse(saltList.contains(salt))
            saltList.add(salt)
        }
    }

}