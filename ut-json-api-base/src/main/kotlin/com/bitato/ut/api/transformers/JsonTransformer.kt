/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.transformers

import com.google.gson.Gson
import spark.ResponseTransformer

class JsonResponseTransformer : ResponseTransformer {

    override fun render(model: Any?): String {
        return Gson().toJson(model)
    }
}