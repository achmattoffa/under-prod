/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.modules

import com.bitato.ut.api.transformers.JsonResponseTransformer
import com.google.gson.Gson
import spark.ResponseTransformer
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory

object JsonApiInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<Gson>{
            Gson()
        }

        addFactory<ResponseTransformer> {
            JsonResponseTransformer()
        }
    }
}

