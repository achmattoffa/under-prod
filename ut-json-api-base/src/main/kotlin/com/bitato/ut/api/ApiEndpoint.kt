/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api

interface ApiEndpoint {
    fun configure()
}