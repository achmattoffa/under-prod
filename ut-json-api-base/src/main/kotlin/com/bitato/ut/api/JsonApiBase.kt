/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api

import com.google.gson.Gson
import spark.ResponseTransformer
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

abstract class JsonApiBase {

    protected val gson = Injekt.get<Gson>()
    protected val jsonResponseTransformer = Injekt.get<ResponseTransformer>()
}