/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.api.util

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.stream.JsonReader
import java.io.FileInputStream
import java.io.InputStream
import java.net.URL
import javax.print.DocFlavor

object UtJsonHelper {

    fun readFromUrlString(url: String): JsonObject {
        return JsonParser()
          .parse(JsonReader(URL(url)
            .openStream()
            .reader()))
          .asJsonObject
    }


    fun readFromStream(inputStream: InputStream): JsonObject {
        return JsonParser()
          .parse(JsonReader(inputStream.reader()))
          .asJsonObject
    }

    fun readFromFile(file: String): JsonObject {
        return JsonParser()
          .parse(JsonReader(FileInputStream(file).reader()))
          .asJsonObject
    }

    fun InputStream.toJsonObject(): JsonObject = readFromStream(this)

}