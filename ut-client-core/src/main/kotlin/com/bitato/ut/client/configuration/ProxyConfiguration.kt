/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.configuration

data class ProxyConfiguration(var enabled: Boolean, var type: String = "", var host: String = "", var port: Int = 0, var username: String = "", var password: String = "")