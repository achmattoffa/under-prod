/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.net.socks5

import com.bitato.ut.api.model.net.socks.socks5.Socks5Request
import com.bitato.ut.api.model.net.socks.socks5.Socks5Response
import com.bitato.ut.api.model.net.wrappers.ClientRequestType
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.client.configuration.UtNetworkConfiguration
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.data.services.monitors.monitor
import com.bitato.ut.client.data.services.session.SessionCache
import com.bitato.ut.client.net.util.registerHandle
import java.io.IOException
import java.net.Socket

class UtSocks5ConnectRequestHandler(request: Socks5Request, client: Socket) : UtSocks5RequestHandlerBase(request, client) {

    override fun run() {

        clientSock.use {
            val optRemote = connectToServer()

            if (!optRemote.isPresent) {
                EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_CONNECTION_FAILED)
            } else {

                val remoteSock = optRemote.get()
                remoteSock.use {
                    it.registerHandle()
                    EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_CONNECTION_ESTABLISHED)

                    handleRequest(it, request.getAddressAsString(), request.port.toInt())
                }
            }
        }
    }

    fun handleRequest(remoteSock: Socket, targetHost: String, targetPort: Int) {

        try {
            val remoteIn = remoteSock.inputStream.monitor()
            val remoteOut = remoteSock.outputStream.monitor()

            remoteOut < UtNetworkConfiguration.injectRequestPayload(request.parse(), ClientRequestType.SOCKS5_REQUEST, targetHost, targetPort, SessionCache.getSession().username)
            EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_REQUEST_SENT)

            val socksResponse = socksResponseFactory.createResponseHeader(remoteIn)
            EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_RESPONSE_RECEIVED)

            clientOut < socksResponse
            EventBus.notify(EventBus.EventIDs.PROXY_CLIENT_RESPONSE_SENT)

            if (socksResponse.reply == Socks5Response.SUCCEEDED) {
                streamIndefinitely(remoteIn, remoteOut)
                EventBus.notify(EventBus.EventIDs.PROXY_STREAM_COMPLETED)
            }

        } catch (ex: IOException) {
            if (!wasClientResponseSent) {
                EventBus.notify(EventBus.EventIDs.PROXY_ERROR_BEFORE_REMOTE_RESPONDED)
            } else {
                EventBus.notify(EventBus.EventIDs.PROXY_ERROR_AFTER_REMOTE_RESPONDED)
            }
        }
    }
}