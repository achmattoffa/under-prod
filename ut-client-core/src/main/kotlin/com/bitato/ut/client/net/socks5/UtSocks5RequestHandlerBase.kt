/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.net.socks5

import com.bitato.ut.api.model.net.socks.socks5.Socks5Factory
import com.bitato.ut.api.model.net.socks.socks5.Socks5Request
import com.bitato.ut.api.model.net.socks.socks5.Socks5Response
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.net.TunnelRequestHandlerBase
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

abstract class UtSocks5RequestHandlerBase(protected val request: Socks5Request, client: Socket)
    : TunnelRequestHandlerBase(client) {

    protected var wasClientResponseSent: Boolean
    protected val socksResponseFactory: Socks5Factory = Injekt.get()

    init {

        wasClientResponseSent = false

        EventBus.subscribe(EventBus.EventIDs.PROXY_REMOTE_CONNECTION_FAILED, {
            val resp = Socks5Response(Socks5Response.HOST_UNREACHABLE)

            io {
                clientOut < resp
            }
        })

        EventBus.subscribe(EventBus.EventIDs.PROXY_ERROR_BEFORE_REMOTE_RESPONDED, {
            val resp = Socks5Response(Socks5Response.SERVER_FAILURE)
            io {
                clientOut < resp
            }
        })

        EventBus.subscribe(EventBus.EventIDs.PROXY_CLIENT_RESPONSE_SENT, {
            wasClientResponseSent = true
        })
    }
}