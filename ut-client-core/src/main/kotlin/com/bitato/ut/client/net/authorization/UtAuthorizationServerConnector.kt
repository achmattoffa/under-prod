/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.authorization

import com.bitato.ut.api.model.net.sessioninit.SessionInitRequest
import com.bitato.ut.api.model.net.sessioninit.SessionInitResponse
import com.bitato.ut.api.model.net.wrappers.ClientRequestType
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.timeout
import com.bitato.ut.client.configuration.AuthServerConfig
import com.bitato.ut.client.configuration.UtNetworkConfiguration
import com.bitato.ut.client.data.ApplicationState
import com.bitato.ut.client.data.events.EventBus
import com.github.salomonbrys.kotson.*
import com.google.gson.JsonParser
import org.aeonbits.owner.ConfigFactory
import java.io.IOException
import java.io.InputStream
import java.net.*

class UtAuthorizationServerConnector : AuthorizationServerConnector {

    private val authServerConfig: AuthServerConfig = ConfigFactory.create(AuthServerConfig::class.java)
    private val injector = UtNetworkConfiguration

    override fun initializeSession(request: SessionInitRequest): SessionInitResponse {

        EventBus.notify(EventBus.EventIDs.AUTHENTICATION_STARTED)

        val remoteHost = authServerConfig.getAuthServerHost()
        val remotePort = authServerConfig.getAuthServerPort()

        val response = try {

            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                throw InterruptedException()
            }

            EventBus.notify(EventBus.EventIDs.AUTHENTICATION_CONNECTING)
            val targetServer = injector.connectToTarget(remoteHost, remotePort)
            EventBus.notify(EventBus.EventIDs.AUTHENTICATION_CONNECTED)

            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                throw InterruptedException()
            }

            targetServer.timeout(10000, {

                val injectRequestPayload = injector.injectRequestPayload(
                  request.parse(),
                  ClientRequestType.SESSION_INIT_REQUEST,
                  remoteHost,
                  remotePort,
                  request.username
                )

                getOutputStream() < injectRequestPayload

                getResponse(getInputStream())
            })

        } catch (ex: ConnectException) {
            EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Unable to connect to the authentication service")
            SessionInitResponse(SessionInitResponse.ResponseCodes.ERR_UNKNOWN)
        } catch (ex: NoRouteToHostException) {
            EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Unable to connect to the authentication service")
            SessionInitResponse(SessionInitResponse.ResponseCodes.ERR_UNKNOWN)
        } catch (ex: UnknownHostException) {
            EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Unable to connect to the authentication service")
            SessionInitResponse(SessionInitResponse.ResponseCodes.ERR_UNKNOWN)
        } catch (ex: SocketTimeoutException) {
            EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Authentication process has timed out")
            SessionInitResponse(SessionInitResponse.ResponseCodes.ERR_UNKNOWN)
        } catch (ex: SocketException) {
            EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "A Socket Exception has occurred")
            SessionInitResponse(SessionInitResponse.ResponseCodes.ERR_UNKNOWN)
        }

        return inspectResponse(response)
    }

    private fun inspectResponse(response: SessionInitResponse): SessionInitResponse {

        val responseCode = response.status

        when (responseCode) {
            SessionInitResponse.ResponseCodes.ACCESS_GRANTED -> {
                EventBus.notify(EventBus.EventIDs.AUTHENTICATION_SUCCEEDED)
            }
            SessionInitResponse.ResponseCodes.ERR_BANNED -> {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_FAILED, "Unable to connect to the authentication service")
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
            }
            SessionInitResponse.ResponseCodes.ERR_SLD_FAILED -> {
                EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Unable to locate any tunneling servers. Please try again later")
            }
            SessionInitResponse.ResponseCodes.ERR_DISABLED -> {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_FAILED, "Your account has been disabled")
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
            }
            SessionInitResponse.ResponseCodes.ERR_INVALID_CREDENTIALS -> {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_FAILED, "Invalid account credentials")
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
            }
            SessionInitResponse.ResponseCodes.ERR_INVALID_REQUEST -> {
                EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Invalid authentication request")
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
            }
            SessionInitResponse.ResponseCodes.ERR_SERVER_ACCESS_DENIED -> {
                EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Server access denied")
            }
            SessionInitResponse.ResponseCodes.ERR_SESSION_LIMIT -> {
                EventBus.notify(EventBus.EventIDs.AUTHENTICATION_FAILED, "Your package limit has been reached")
            }
        }

        return response
    }

    private fun getResponse(inputStream: InputStream): SessionInitResponse {

        try {

            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                throw InterruptedException()
            }

            val js = injector.getResponsePayload(inputStream, {
                val jsonParser = JsonParser().parse(it.inputStream().reader(Charsets.ISO_8859_1))
                if (jsonParser.isJsonObject) {
                    jsonParser.asJsonObject
                } else {
                    throw IOException("Invalid response from client")
                }
            })

            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                throw InterruptedException()
            }

            if (js["success"].bool) {

                val code = js["code"].byte
                val uuid = js["uuid"].string

                val servers = js["servers"].asJsonArray
                val selectedServer = servers.minBy { jsonServer ->
                    jsonServer["load"].int
                }

                val host = (selectedServer!!["address"].asJsonObject)["host"].string
                val port = ((selectedServer["ports"].asJsonArray).first().asJsonObject)["port"].int

                return SessionInitResponse(code, SessionInitResponse.AddressTypes.HOSTNAME, host, port, uuid)
            } else {
                val code = js["code"].byte
                return SessionInitResponse(code)
            }
        } catch (ex: IOException) {
            return SessionInitResponse(SessionInitResponse.ResponseCodes.ERR_UNKNOWN)
        }
    }
}

