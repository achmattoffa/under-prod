/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.events

object EventBus {

    object EventIDs {

        //SESSION STATUS
        const val SESSION_ACCOUNT_BANNED = "auth.response.account_banned"
        const val SESSION_CAP_REACHED = "auth.response.cap_reached"
        const val SESSION_PACKAGE_EXPIRED = "auth.response.package_expired"
        const val SESSION_REMOTE_ERROR = "auth.response.tunnel_server_error"
        const val SESSION_UPGRADE_REQUESTED = "auth.response.upgrade_requested"

        const val SESSION_STATUS_UPDATE = "session.status_update"
        const val ACCOUNT_INFORMATION = "account.information"


        //INTERNAL
        const val INTERNAL_COMMAND = "session.status_update"


        //CLIENT AUTHENTICATION
        const val AUTHENTICATION_STARTED = "auth.started"
        const val AUTHENTICATION_CONNECTING = "auth.connecting"
        const val AUTHENTICATION_CONNECTED = "auth.connected"
        const val AUTHENTICATION_FAILED = "auth.failed"
        const val AUTHENTICATION_SUCCEEDED = "auth.success"

        //REMOTE TUNNEL
        const val REMOTE_TUNNEL_STARTED = "tunnel.started"
        const val REMOTE_TUNNEL_CONNECTING = "tunnel.connecting"
        const val REMOTE_TUNNEL_CONNECTED = "tunnel.connected"
        const val REMOTE_TUNNEL_RECONNECTING = "tunnel.reconnecting"
        const val REMOTE_TUNNEL_RECONNECTED = "tunnel.reconnected"
        const val REMOTE_TUNNEL_DISCONNECTED = "tunnel.disconnected"
        const val REMOTE_TUNNEL_STOPPING = "tunnel.stopping"
        const val REMOTE_TUNNEL_STOPPED = "tunnel.stopped"
        const val REMOTE_TUNNEL_FAILED = "tunnel.failed"

        //CLIENT PROXY
        const val PROXY_REMOTE_CONNECTION_ESTABLISHED = "proxy.remote_connected"
        const val PROXY_REMOTE_CONNECTION_FAILED = "proxy.remote_failed"
        const val PROXY_REMOTE_REQUEST_SENT = "proxy.client_request_sent"
        const val PROXY_REMOTE_RESPONSE_RECEIVED = "proxy.remote_response_read"
        const val PROXY_REMOTE_CONNECTION_CLOSED = "proxy.remote_closed"
        const val PROXY_REMOTE_CLIENT_PAYLOAD_SENT = "proxy.client_content_sent"
        const val PROXY_REMOTE_CONTENT_STREAMED = "proxy.remote_content_streamed"
        const val PROXY_CLIENT_RESPONSE_SENT = "proxy.client_response_sent"
        const val PROXY_STREAM_COMPLETED = "proxy.stream_completed"
        const val PROXY_CLIENT_CONNECTION_CLOSED = "proxy.client_closed"
        const val PROXY_ERROR_BEFORE_REMOTE_RESPONDED = "proxy.error_before_client_response"
        const val PROXY_ERROR_AFTER_REMOTE_RESPONDED = "proxy.error_after_client_response"

        //VPN SERVICE
        const val VPN_SERVICE_STARTED = "vpn_service.started"
        const val VPN_SERVICE_CONNECTED = "vpn_service.connected"
        const val VPN_SERVICE_RECONNECTING = "vpn_service.reconnecting"
        const val VPN_SERVICE_RECONNECTED = "vpn_service.reconnected"
        const val VPN_SERVICE_RECONNECT_FAILED = "vpn_service.reconnect_failed"
        const val VPN_SERVICE_DISCONNECTING = "vpn_service.disconnecting"
        const val VPN_SERVICE_STOPPED = "vpn_service.stopped"
        const val VPN_SERVICE_FAILED = "vpn_service.failed"
        const val VPN_SERVICE_TAP_DRIVERS_INSTALL = "vpn_service.installing_drivers"


        //SERVICE INITIALIZATION
        const val SERVICE_TOGGLE_STATUS_UPDATE = "service_init.status_changed"
        const val SERVICE_TOGGLE_PROGRESS_UPDATE = "service_init.progress_changed"
        const val SERVICE_TOGGLE_STARTED = "service_init.started"
        const val SERVICE_TOGGLE_PAUSED = "service_init.pause"
        const val SERVICE_TOGGLE_FAILED = "service_init.failed"
        const val SERVICE_TOGGLE_CANCELLED = "service_init.cancelled"
        const val SERVICE_TOGGLE_DONE = "service_init.done"
        const val SERVICE_TOGGLE_SUCCESS = "service_init.success"

        //SERVICE
        const val SERVICE_STARTED = "service.started"
        const val SERVICE_FAILED = "service.failed"
        const val SERVICE_STOPPED = "service.stopped"
        const val GLOBAL_BLOCK = "service.blocked"
        const val SERVICE_RECONNECT_REQUESTED = "service.reconnect_requested"

    }


    private val subscribers: HashMap<String, ArrayList<(Any) -> Unit>> = HashMap()

    @Synchronized
    fun subscribe(id: String, fn: (Any) -> Unit) {

        if (subscribers.containsKey(id)) {
            subscribers[id]!!.clear()
            subscribers[id]!!.add(fn)
        } else {
            subscribers.put(id, arrayListOf(fn))
        }
    }

    @Synchronized
    fun unsubscribe(id: String) {

        if (subscribers.containsKey(id)) {
            subscribers[id]?.clear()
            subscribers.remove(id)
        }
    }

    @Synchronized
    fun notify(id: String, data: Any = Any()) {
        if (subscribers.containsKey(id)) {
            subscribers[id]!!.forEach {
                it.invoke(data)
            }
        }
    }
}

