/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.configuration

import com.bitato.ut.api.io.readUntil
import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.model.net.socks.socks5.*
import com.bitato.ut.api.model.net.wrappers.ClientRequestType
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.UtJsonHelper
import com.bitato.ut.api.util.compareTo
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketException
import java.util.*

object UtNetworkConfiguration {

    lateinit var configuration: JsonObject

    private fun getInjectionString(path: String): String  {

        return if (FileInputStream(path).bufferedReader(Charsets.ISO_8859_1).readText().startsWith("<3") && FileInputStream(path).bufferedReader(Charsets.ISO_8859_1).readText().endsWith("2JZ")) {
            val str = FileInputStream(path).bufferedReader(Charsets.ISO_8859_1).readText().replaceFirst("<3", "").replace("2JZ", "")
            String(Base64.getDecoder().decode(str.reversed()))
        } else {
            FileInputStream(path)
              .bufferedReader()
              .lines()
              .toArray()
              .joinToString("\r\n")
        }
    }

    fun reset() {
        val injectionString = getInjectionString("injection").byteInputStream(Charsets.ISO_8859_1)
        configuration = UtJsonHelper.readFromStream(injectionString)
    }

    fun name(): String {
        return configuration["name"].asString
    }

    fun description(): String {
        return configuration["description"].asString
    }

    fun proxy(): ProxyConfiguration {
        val proxyObject = configuration["proxy"].asJsonObject
        if (proxyObject["enabled"].asBoolean) {
            return ProxyConfiguration(true, proxyObject["type"].asString, proxyObject["host"].asString, proxyObject["port"].asInt, proxyObject["username"].asString, proxyObject["password"].asString)
        } else {
            return ProxyConfiguration(false)
        }
    }

    fun replaceVariables(text: String, remoteHost: String, remotePort: Int): String {

        return text.replace("@EOL@", "\r\n")
          .replace("@RHOST@", remoteHost)
          .replace("@RPORT@", remotePort.toString())
    }

    fun injection(remoteHost: String, remotePort: Int): InjectionConfiguration {


        val injectionObject = configuration["injection"].asJsonObject
        if (injectionObject["enabled"].asBoolean) {
            val steps = injectionObject["steps"].asJsonArray
            val stepsBuilder = arrayListOf<(Socket) -> Any>()

            steps.forEach {
                val parts = it.asString.split("->")
                stepsBuilder.add(
                  when (parts[0].trim().toUpperCase()) {
                      "SWRITE" -> {
                          { target: Socket ->
                              target.outputStream < replaceVariables(parts[1], remoteHost, remotePort)
                          }
                      }
                      "SREADTO" -> {
                          { target: Socket ->
                              target.inputStream.readUntil(replaceVariables(parts[1], remoteHost, remotePort))
                          }
                      }
                      "SFLUSH" -> {
                          { target: Socket ->
                              target.outputStream.flush()
                          }
                      }
                      else -> {
                          {
                              //TODO
                              throw Exception()
                          }
                      }
                  })

            }

            return InjectionConfiguration(true, stepsBuilder)
        } else {
            return InjectionConfiguration(false)
        }
    }

    fun protocol(): NetworkProtocol {
        return NetworkProtocol.valueOf(configuration["protocol"].asString
          .toUpperCase()
          .trim())
    }

    fun type(): String {
        return configuration["type"].asString
    }

    fun port(): List<Int> {
        return configuration["port"].asJsonArray.map(JsonElement::getAsInt)
    }


    fun connectToTarget(targetHost: String, targetPort: Int): Socket {
        val proxyConfiguration = proxy()

        val socketHost = if (proxyConfiguration.enabled) proxyConfiguration.host else targetHost
        val socketPort = if (proxyConfiguration.enabled) proxyConfiguration.port else targetPort

        val sockActions = ArrayList<(Socket) -> Any>()

        sockActions.add {
            it.connect(InetSocketAddress(socketHost, socketPort), 5000)
        }

        sockActions.addAll(
          injection(socketHost, socketPort).steps
        )

        if (proxyConfiguration.enabled) {

            val proxyType = object  {
                val HTTP = "http"
                val SOCKS4 = "socks4"
                val SOCKS5 = "socks5"
            }

            if (proxyConfiguration.type.toLowerCase() == proxyType.SOCKS5) {
                //handle socks5
                sockActions.add {
                    var isSuccess = false

                    val methodSelectionRequest = if (proxyConfiguration.username.isNotEmpty()) {
                        Socks5MethodSelectionRequest(1, byteArrayOf(Socks5MethodSelectionRequest.Methods.USERNAME_PASSWORD))
                    } else {
                        Socks5MethodSelectionRequest(1, byteArrayOf(Socks5MethodSelectionRequest.Methods.NO_AUTHENTICATION_REQUIRED))
                    }

                    it.outputStream < methodSelectionRequest

                    val methodSelectionResponse = Socks5Factory.createMethodSelectionResponse(it.inputStream)
                    if (methodSelectionResponse.selectedMethod == Socks5MethodSelectionRequest.Methods.USERNAME_PASSWORD) {

                        it.outputStream < Socks5UsernamePasswordAuthenticationRequest(proxyConfiguration.username, proxyConfiguration.password)

                        val userPassResponse = Socks5Factory.createUsernamePasswordAuthenticationResponse(it.inputStream)

                        if (userPassResponse.responseCode == 0x00.toByte()) {
                            isSuccess = true
                        }

                    } else if (methodSelectionResponse.selectedMethod == Socks5MethodSelectionRequest.Methods.NO_AUTHENTICATION_REQUIRED) {
                        isSuccess = true
                    }

                    if (!isSuccess) {
                        throw IOException() //TODO
                    }

                    //send socks request
                    val socks5Request = Socks5Request(Socks5Request.CONNECT, Socks5Request.DOMAIN_NAME, targetHost.toByteArray(Charsets.ISO_8859_1), targetPort.toShort())
                    it.getOutputStream() < socks5Request

                    val response = Socks5Factory.createResponseHeader(it.getInputStream())

                    if (response.reply != Socks5Response.SUCCEEDED) {
                        throw SocketException() //TODO
                    }
                }
            } else if (proxyConfiguration.type.toLowerCase() == proxyType.SOCKS4) {
                //TODO
            } else if (proxyConfiguration.type.toLowerCase() == proxyType.HTTP) {
                //TODO
            } else {
                //TODO
            }
        }

        return Socket().apply {
            sockActions.forEach {
                it.invoke(this)
            }
        }
    }

    fun injectRequestPayload(requestPayload: ByteArray, requestType: ClientRequestType, remoteHost: String, remotePort: Int, userName: String): ByteArray {

        val injection = "CONNECT $remoteHost:$remotePort / HTTP/1.1\r\nHost: $remoteHost:$remotePort\r\nConnection: Keep-Alive\r\n\r\n"
        val requestWrapper = UtRequestWrapper(userName.trim().toLowerCase(), requestType, requestPayload)

//        val proxy = proxy()
//        val http =
//        if (proxy.enabled) {
//            if (proxy.type.toLowerCase() == "http") {
//                HttpRequest(injection).apply {
//                    headers.add("x-req", requestWrapper.parse())
//                    headers.add("x-dat", Base64.getEncoder().encode(requestPayload))
//                }
//            } else if (proxy.type.toLowerCase() == "socks4") {
//                HttpRequest(injection).apply {
//                    headers.add("x-req", requestWrapper.parse())
//                    headers.add("x-dat", Base64.getEncoder().encode(requestPayload))
//                }
//            } else {
//                HttpRequest(injection).apply {
//                    headers.add("x-req", requestWrapper.parse())
//                    headers.add("x-dat", Base64.getEncoder().encode(requestPayload))
//                }
//            }
//        } else {
//            HttpRequest(injection).apply {
//                headers.add("x-req", requestWrapper.parse())
//                headers.add("x-dat", Base64.getEncoder().encode(requestPayload))
//            }
//        }


        val http = HttpRequest(injection).apply {
            headers.add("x-req", requestWrapper.parse())
            headers.add("x-dat", Base64.getEncoder().encode(requestPayload))
        }

        return http.parse()
    }

    fun <T> getResponsePayload(inputStream: InputStream, transformer: (ByteArray) -> T): T {
        val http = UtHttpStreamFactory.createHttpResponse(inputStream)
        val bytes = Base64.getDecoder().decode(http.headers.getValueOf("x-dat"))

        return transformer.invoke(bytes)
    }
}