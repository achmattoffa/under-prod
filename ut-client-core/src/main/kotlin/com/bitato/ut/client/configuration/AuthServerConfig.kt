/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.configuration

import org.aeonbits.owner.Config

@Config.Sources(
  "file:server.conf",
  "classpath:server.conf")
@Config.LoadPolicy(Config.LoadType.MERGE)
interface AuthServerConfig: Config {

    @Config.DefaultValue("sld1.duckdns.org")
    @Config.Key("authserv.address")
    fun getAuthServerHost(): String

    @Config.DefaultValue("443")
    @Config.Key("authserv.port")
    fun getAuthServerPort(): Int
}