/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.net.socks5

import com.bitato.ut.api.model.net.socks.socks5.Socks5Factory
import com.bitato.ut.api.model.net.socks.socks5.Socks5MethodSelectionResponse
import com.bitato.ut.api.model.net.socks.socks5.Socks5Request
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.sbase.net.handler.RequestHandler
import com.bitato.ut.sbase.net.mapper.AbstractRequestMapper
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import java.net.Socket

class UtSocks5RequestMapper : AbstractRequestMapper() {

    private val socks5RequestFactory = Socks5Factory

    override fun mapRequest(incoming: Socket): RequestHandler {

        socks5RequestFactory.createMethodSelectionRequest(incoming.getInputStream())
        incoming.getOutputStream() < Socks5MethodSelectionResponse(0)

        val request = socks5RequestFactory.createRequestHeader(incoming.inputStream)

        return when (request.cmd) {
            Socks5Request.CONNECT -> UtSocks5ConnectRequestHandler(request, incoming)
            else -> throw NotImplementedException() as Throwable//TODO
        }
    }

}