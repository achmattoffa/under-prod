/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.configuration

import java.net.Socket

data class InjectionConfiguration(var enabled: Boolean, var steps: List<(Socket) -> Any> = emptyList())