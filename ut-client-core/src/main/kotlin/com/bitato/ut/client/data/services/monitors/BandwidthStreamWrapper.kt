/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.data.services.monitors

import java.io.InputStream
import java.io.OutputStream

interface BandwidthStreamWrapper {
    fun wrap(inputStream: InputStream): InputStream
    fun wrap(outputStream: OutputStream): OutputStream
}

