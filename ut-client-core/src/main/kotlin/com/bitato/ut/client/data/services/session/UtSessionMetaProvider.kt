/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.services.session

class UtSessionMetaProvider: SessionMetaProvider {

    private val session : UserSession = SessionCache.getSession()

    override fun getMeta(): SessionMeta {
        return object : SessionMeta {

            override fun isActive(): Boolean {
                return session.isActive
            }

            override fun getUuid(): String {
                return session.serverSessionUuid
            }
        }
    }

}