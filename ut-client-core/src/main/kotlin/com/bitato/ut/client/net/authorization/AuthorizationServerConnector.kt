/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.authorization

import com.bitato.ut.api.model.net.sessioninit.SessionInitRequest
import com.bitato.ut.api.model.net.sessioninit.SessionInitResponse

interface AuthorizationServerConnector {
    fun initializeSession(request: SessionInitRequest): SessionInitResponse
}