/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.services.session

import java.net.Socket

class UserSession(
  var isActive: Boolean,
  val username: String,
  val serverSessionUuid: String,
  throttle: Int,
  val remoteRegion: String,
  val remoteAddress: String,
  val remotePort: Int,
  var internalSock: Socket,
  var updatedTime: Long = System.currentTimeMillis()
) {

    fun isReusable(): Boolean {
        return updatedTime > System.currentTimeMillis() - 15000
    }
}