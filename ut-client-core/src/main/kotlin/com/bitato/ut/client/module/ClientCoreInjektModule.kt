/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.module

import com.bitato.ut.api.io.throttle.InputStreamThrottleController
import com.bitato.ut.api.io.throttle.OutputStreamThrottleController
import com.bitato.ut.api.io.throttle.UtInputStreamThrottleController
import com.bitato.ut.api.io.throttle.UtOutputStreamThrottleController
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.model.net.internal.InternalMessageStreamFactory
import com.bitato.ut.api.model.net.internal.UtInternalMessageStreamFactory
import com.bitato.ut.api.model.net.sessioninit.SessionInitMessageStreamFactory
import com.bitato.ut.api.model.net.sessioninit.UtSessionInitMessageStreamFactory
import com.bitato.ut.api.model.net.socks.socks5.Socks5Factory
import com.bitato.ut.client.data.services.servers.ServerManager
import com.bitato.ut.client.data.services.servers.UtServerManager
import com.bitato.ut.client.data.services.session.SessionManager
import com.bitato.ut.client.data.services.session.SessionMetaProvider
import com.bitato.ut.client.data.services.session.UtSessionManager
import com.bitato.ut.client.data.services.session.UtSessionMetaProvider
import com.bitato.ut.client.net.authorization.AuthorizationServerConnector
import com.bitato.ut.client.net.authorization.UtAuthorizationServerConnector
import com.bitato.ut.client.net.connect.ServerSessionConnector
import com.bitato.ut.client.net.connect.UtServerSessionConnector
import com.bitato.ut.client.net.util.SocketManager
import com.bitato.ut.client.net.util.UtSocketManager
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.sbase.module.ServerBaseInjektModule
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import uy.kohesive.injekt.api.addPerKeyFactory

object ClientCoreInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {
        importModule(ServerBaseInjektModule)
        importModule(LogBaseInjektModule)

        addFactory<SessionMetaProvider> { UtSessionMetaProvider() }
        addPerKeyFactory<InputStreamThrottleController, Int>(::UtInputStreamThrottleController)
        addPerKeyFactory<OutputStreamThrottleController, Int>(::UtOutputStreamThrottleController)
        addFactory<HttpStreamFactory> { UtHttpStreamFactory }
        addFactory<Socks5Factory> { Socks5Factory }
        addFactory<SessionManager> { UtSessionManager }
        addFactory<ServerManager> { UtServerManager }
        addFactory<SocketManager> { UtSocketManager }
        addFactory<AuthorizationServerConnector> { UtAuthorizationServerConnector() }
        addFactory<ServerSessionConnector> { UtServerSessionConnector() }
        addFactory<SessionInitMessageStreamFactory> { UtSessionInitMessageStreamFactory }
        addFactory<InternalMessageStreamFactory> { UtInternalMessageStreamFactory }
    }
}