/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.net.connect

import java.util.*

interface ServerSessionConnector {

    fun connect(sessionUuid: String, remote: String, port: Int): Optional<ConnectionResult>

    fun reconnect(serverSessionUuid: String, remote: String, port: Int, raiseEvents: Boolean): Optional<ConnectionResult>

}