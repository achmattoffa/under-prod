/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.net.connect

import com.bitato.ut.api.model.net.internal.InternalConnectRequest
import com.bitato.ut.api.model.net.wrappers.ClientRequestType
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.timeout
import com.bitato.ut.client.configuration.UtNetworkConfiguration
import com.bitato.ut.client.data.events.EventBus
import com.github.salomonbrys.kotson.bool
import com.github.salomonbrys.kotson.int
import com.github.salomonbrys.kotson.string
import com.google.gson.JsonParser
import java.io.IOException
import java.net.ConnectException
import java.net.NoRouteToHostException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.*

class UtServerSessionConnector : ServerSessionConnector {

    override fun connect(sessionUuid: String, remote: String, port: Int): Optional<ConnectionResult> {

        val connectRequest = InternalConnectRequest(
          InternalConnectRequest.RequestCodes.CONNECT,
          sessionUuid
        )

        try {
            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_CONNECTING)
            val target = UtNetworkConfiguration.connectToTarget(remote, port)

            if (target.isConnected) {

                target.timeout(10000) {
                    val injectRequestPayload = UtNetworkConfiguration.injectRequestPayload(connectRequest.parse(), ClientRequestType.INTERNAL_REQUEST, remote, port, connectRequest.uuid)
                    outputStream < injectRequestPayload
                    outputStream.flush()
                }

                val js = UtNetworkConfiguration.getResponsePayload(target.getInputStream(), {
                    val jsonParser = JsonParser().parse(it.inputStream().reader(Charsets.ISO_8859_1))
                    if (jsonParser.isJsonObject) {
                        jsonParser.asJsonObject
                    } else {
                        throw IOException()
                    }
                })

                if (js["success"].bool) {
                    val uuid = js["uuid"].string
                    val throttle = js["throttle"].int

                    EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_CONNECTED, uuid)
                    return Optional.of(ConnectionResult(target, uuid, throttle))
                } else {
                    EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Target server denied attempt to connect")
                    target.close()
                }
            }
        } catch (Ex: ConnectException) {
            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Unable to connect to the remote service. Please try again later")
        } catch (ex: NoRouteToHostException) {
            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Unable to connect to the remote service. Please try again later")
        } catch (ex: SocketTimeoutException) {
            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Tunnel transmission timed out")
        } catch (ex: SocketException) {
            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "An error has occurred while connecting to the remote service")
        } catch (ex: IOException) {
            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "An error has occurred")
        }

        return Optional.empty()
    }

    override fun reconnect(serverSessionUuid: String, remote: String, port: Int, raiseEvents: Boolean): Optional<ConnectionResult> {

        val reconnectRequest = InternalConnectRequest(InternalConnectRequest.RequestCodes.RECONNECT, serverSessionUuid)

        try {
            val target = UtNetworkConfiguration.connectToTarget(remote, port)

            if (target.isConnected) {


                try {

                    target.timeout(10000) {
                        getOutputStream() < UtNetworkConfiguration.injectRequestPayload(reconnectRequest.parse(), ClientRequestType.INTERNAL_REQUEST, remote, port, reconnectRequest.uuid)
                        getOutputStream().flush()
                    }

                    val js = UtNetworkConfiguration.getResponsePayload(target.getInputStream(), {
                        val jsonParser = JsonParser().parse(it.inputStream().reader(Charsets.ISO_8859_1))
                        if (jsonParser.isJsonObject) {
                            jsonParser.asJsonObject
                        } else {
                            throw IOException()
                        }
                    })

                    if (js["success"].bool) {
                        val uuid = js["uuid"].string
                        val throttle = js["throttle"].int

                        if (raiseEvents) {
                            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_RECONNECTED, uuid)
                        }

                        return Optional.of(ConnectionResult(target, uuid, throttle))
                    } else {
                        if (raiseEvents) {
                            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Target server denied attempt to reconnect")
                        }
                        target.close()
                    }

                } catch (ex: SocketTimeoutException) {
                    if (raiseEvents) {
                        EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Tunnel transmission timed out")
                    }
                }
            }
        } catch (ex: ConnectException) {
            if (raiseEvents) {
                EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Tunnel transmission timed out")
            }
        } catch (ex: NoRouteToHostException) {
            if (raiseEvents) {
                EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Unable to connect to the remote service. Please try again later")
            }
        } catch (ex: SocketException) {
            if (raiseEvents) {
                EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "Unable to connect to the remote service. Please try again later")
            }
        } catch (ex: IOException) {
            if (raiseEvents) {
                EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, "An error has occurred. Please try again later")
            }
        }

        return Optional.empty()
    }
}