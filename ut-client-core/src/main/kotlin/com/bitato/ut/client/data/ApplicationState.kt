/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data

object ApplicationState {

    enum class States {
        INITIALIZE,
        DISCONNECTED,
        CONNECTING,
        RECONNECTING,
        CONNECTED,
        ABORTING,
        DISCONNECTING
    }

    private lateinit var currentState: States

    fun toggleState(newState: States) {

        if (currentState != newState) {
            currentState = newState
        }
    }

    fun getState(): States {
        return currentState
    }

    fun initialize() {
        currentState = States.INITIALIZE
    }
}

