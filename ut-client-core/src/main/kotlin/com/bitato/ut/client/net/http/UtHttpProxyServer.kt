
package com.bitato.ut.client.net.http

import com.bitato.ut.client.net.UtProxyServerBase

/*
 * Copyright (c) 2016. Under Tunnel
 */


class UtHttpProxyServer(port: Int) : UtProxyServerBase(port)