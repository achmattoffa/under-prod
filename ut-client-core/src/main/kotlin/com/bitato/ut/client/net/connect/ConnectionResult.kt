/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.net.connect

import java.net.Socket

data class ConnectionResult(val connection: Socket, val serverSessionUuid: String, val throttle: Int)