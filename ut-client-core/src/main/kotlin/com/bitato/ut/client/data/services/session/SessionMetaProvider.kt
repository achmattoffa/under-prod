/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.services.session

interface SessionMetaProvider {
    fun getMeta(): SessionMeta
}