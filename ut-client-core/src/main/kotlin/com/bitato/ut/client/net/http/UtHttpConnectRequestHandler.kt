/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.http

import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.HttpStatusCodes
import com.bitato.ut.api.model.net.wrappers.ClientRequestType
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.client.configuration.UtNetworkConfiguration
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.data.services.monitors.monitor
import com.bitato.ut.client.net.util.registerHandle
import java.io.IOException
import java.net.Socket

class UtHttpConnectRequestHandler(request: HttpRequest, client: Socket) : UtHttpRequestHandlerBase(request, client) {

    override fun run() {

        clientSock.use {
            val optionalRemote = connectToServer()

            if (!optionalRemote.isPresent) {
                EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_CONNECTION_FAILED)
            } else {

                val remoteSock = optionalRemote.get()

                remoteSock.use {
                    it.registerHandle()
                    EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_CONNECTION_ESTABLISHED)

                    handleRequest(it, request.getHost(), request.port)
                }
            }
        }
    }


    fun handleRequest(remoteSock: Socket, targetHost: String, targetPort: Int) {

        try {
            val remoteIn = remoteSock.inputStream.monitor()
            val remoteOut = remoteSock.outputStream.monitor()

            remoteOut < UtNetworkConfiguration.injectRequestPayload(request.parse(), ClientRequestType.HTTP_REQUEST, targetHost, targetPort, uuid)
            EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_REQUEST_SENT)

            val httpResponse = UtNetworkConfiguration.getResponsePayload(remoteIn, {
                httpStreamFactory.createHttpResponse(it.inputStream())
            })

            EventBus.notify(EventBus.EventIDs.PROXY_REMOTE_RESPONSE_RECEIVED, httpResponse.toString())

            clientOut < httpResponse
            EventBus.notify(EventBus.EventIDs.PROXY_CLIENT_RESPONSE_SENT)

            if (httpResponse.code == HttpStatusCodes.OK.toString()) {
                streamIndefinitely(remoteSock.getInputStream(), remoteSock.getOutputStream())
                EventBus.notify(EventBus.EventIDs.PROXY_STREAM_COMPLETED)
            }

        } catch (ex: IOException) {
            if (!isClientResponseSent) {
                EventBus.notify(EventBus.EventIDs.PROXY_ERROR_BEFORE_REMOTE_RESPONDED)
            } else {
                EventBus.notify(EventBus.EventIDs.PROXY_ERROR_AFTER_REMOTE_RESPONDED)
            }
        }
    }
}

