/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.net.vpn

import com.bitato.ut.api.util.protect
import com.bitato.ut.api.util.thread
import com.bitato.ut.client.data.ApplicationState
import com.bitato.ut.client.data.events.EventBus
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

object OpenVpnHelper {

    private lateinit var openVpnProcess: Process

    enum class States {
        STOPPED,
        CONNECTING,
        RECONNECTING,
        CONNECTED
    }

    private var processState = States.STOPPED
    private lateinit var vpnThread: Thread

    fun connect(remoteHost: String, remotePort: Int, localHost: String, localPort: Int) {

        vpnThread = thread {

            cleanUpVpnProcesses()

            if (!isTapDriverInstalled()) {

                //Install tap drivers before starting VPN

                try {
                    installTap()

                } catch (ex: IOException) {
                    EventBus.notify(EventBus.EventIDs.VPN_SERVICE_FAILED, "Cannot start the VPN service. Please ensure that OpenVPN is installed on the current machine.")

                    cleanUpVpnProcesses()
                    return@thread
                }
            }

            val path = createVpnConfig(remoteHost, remotePort, localHost, localPort)

            try {
                openVpnProcess = ProcessBuilder("bin/openvpn.exe", path).start()
            } catch (ex: IOException) {
                EventBus.notify(EventBus.EventIDs.VPN_SERVICE_FAILED, "Cannot start the VPN service. Please ensure that OpenVPN is installed on the current machine.")
                cleanUpVpnProcesses()
                return@thread
            }

            EventBus.notify(EventBus.EventIDs.VPN_SERVICE_STARTED)
            processState = States.CONNECTING

            val inputStream = openVpnProcess.inputStream.bufferedReader(Charsets.ISO_8859_1)

            var currentLine = inputStream.readLine()

            val logger = LoggerFactory.getLogger("com.bitato.ut.client.net.vpn")

            while (currentLine != null) {

                println(currentLine)
                logger.trace(currentLine)

                if (Thread.currentThread().isInterrupted) {
                    break
                }

                if (currentLine.contains("initialization sequence completed", true)) {

                    if (processState == States.RECONNECTING) {
                        EventBus.notify(EventBus.EventIDs.VPN_SERVICE_RECONNECTED)
                    } else {
                        EventBus.notify(EventBus.EventIDs.VPN_SERVICE_CONNECTED)
                    }

                    processState = States.CONNECTED
                }


                if (currentLine.contains("http proxy returned bad status", true)) {

                    if (processState == States.RECONNECTING) {
                        EventBus.notify(EventBus.EventIDs.VPN_SERVICE_RECONNECT_FAILED)
                    }

                    disconnect()
                }

                if (currentLine.contains("exiting", true) && processState !=  States.CONNECTED) {
                    EventBus.notify(EventBus.EventIDs.VPN_SERVICE_FAILED, "Cannot start the VPN service. Please ensure that OpenVPN is installed on the current machine and that all TAP interfaces are enabled.")

                    cleanUpVpnProcesses()
                    break
                }

                if (currentLine.contains("connection reset, restarting", true) && processState ==  States.CONNECTED) {
                    EventBus.notify(EventBus.EventIDs.VPN_SERVICE_RECONNECTING)
                    processState ==  States.RECONNECTING
                }

                if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.CONNECTED, ApplicationState.States.RECONNECTING)) {
                    disconnect()
                    break
                }

                currentLine = inputStream.readLine()
            }
        }

        vpnThread.name = "OpenVPN Service Worker"
    }

    private fun installTap() {
        EventBus.notify(EventBus.EventIDs.VPN_SERVICE_TAP_DRIVERS_INSTALL)

        ProcessBuilder("bin/tap${System.getProperty("sun.arch.data.model")}.exe",
          "install",
          "driver/win${System.getProperty("sun.arch.data.model")}/OemWin2k.inf",
          "tap0901")

          .start()
          .waitFor()
    }

    fun disconnect() {

        if (processState != States.STOPPED) {

            EventBus.notify(EventBus.EventIDs.VPN_SERVICE_DISCONNECTING)

            cleanUpVpnProcesses()

            EventBus.notify(EventBus.EventIDs.VPN_SERVICE_STOPPED)
        } else {
            cleanUpVpnProcesses()
        }
    }

    private fun cleanUpVpnProcesses() {

        try {
            openVpnProcess.destroyForcibly()
        } catch (ex: UninitializedPropertyAccessException) {
            //TODO
        }

        protect {
            val taskKill = Runtime.getRuntime().exec("cmd /c", arrayOf("taskkill /f /im openvpn.exe"))
            taskKill.waitFor()
        }

        processState = States.STOPPED
    }


    fun createVpnConfig(remoteHost: String, remotePort: Int, localHost: String, localPort: Int): String {

        val configLines = (ClassLoader.getSystemClassLoader().getResourceAsStream("vpn.template"))
          .bufferedReader(Charsets.ISO_8859_1)
          .lines()

        val file = File.createTempFile("vpn", "conf")
        file.deleteOnExit()

        val vpnOut = FileOutputStream(file)
          .bufferedWriter(Charsets.ISO_8859_1)

        configLines.forEach {
            vpnOut.write(
              it.replace("@RHOST@", remoteHost)
                .replace("@RPORT@", remotePort.toString())
                .replace("@LHOST@", localHost)
                .replace("@LPORT@", localPort.toString()).trim() + "\r\n"
            )
        }

        configLines.close()
        vpnOut.close()

        return file.absolutePath
    }


    private fun isTapDriverInstalled(): Boolean {

        return java.net.NetworkInterface.getNetworkInterfaces()
          .asSequence()
          .any {
              it.displayName.toLowerCase().matches("tap-windows adapter v[0-9]".toRegex())
          }
    }
}