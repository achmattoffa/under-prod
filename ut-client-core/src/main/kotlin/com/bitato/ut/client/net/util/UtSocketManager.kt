/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.util

import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

object UtSocketManager : SocketManager {

    private val sockets: MutableList<Socket>

    init {
        sockets = arrayListOf()
    }

    @Synchronized
    override fun add(socket: Socket) {
        sockets.add(socket)
    }

    @Synchronized
    override fun remove(socket: Socket): Unit {
        socket.use {
            sockets.remove(it)
        }
    }

    @Synchronized
    override fun close(): Unit {
        sockets.forEach(Socket::close)
        sockets.clear()
    }

    override fun count() = sockets.count()
}

internal fun Socket.registerHandle() = Injekt.get<SocketManager>().add(this)