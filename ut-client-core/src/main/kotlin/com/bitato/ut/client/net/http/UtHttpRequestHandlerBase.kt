/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.http

import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.HttpResponse
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.data.services.session.SessionCache
import com.bitato.ut.client.net.TunnelRequestHandlerBase
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket


abstract class UtHttpRequestHandlerBase(protected val request: HttpRequest, client: Socket)
    : TunnelRequestHandlerBase(client) {

    protected val httpStreamFactory: HttpStreamFactory = Injekt.get()
    protected val uuid = SessionCache.getSession().serverSessionUuid
    protected var isClientResponseSent: Boolean

    init {

        isClientResponseSent = false

        EventBus.subscribe(EventBus.EventIDs.PROXY_REMOTE_CONNECTION_FAILED, {

            val httpResponse = HttpResponse("HTTP/1.1 503 Connection Failed\r\n" + "Connection: Close\r\n" + "\r\n")

            io {
                clientOut < httpResponse
            }
        })

        EventBus.subscribe(EventBus.EventIDs.PROXY_ERROR_BEFORE_REMOTE_RESPONDED, {

            val httpResponse = HttpResponse("HTTP/1.1 500 Internal Error\r\n" + "Connection: Close\r\n" + "\r\n")

            io {
                clientOut < httpResponse
            }
        })

        EventBus.subscribe(EventBus.EventIDs.PROXY_CLIENT_RESPONSE_SENT, {

            isClientResponseSent = true
        })
    }
}
