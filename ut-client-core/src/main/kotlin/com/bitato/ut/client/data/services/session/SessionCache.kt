/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.services.session

import java.net.Socket

object SessionCache {

    val NONE = UserSession(false, "none", "none", -1, "none", "none", 0, Socket())
    private var session: UserSession = NONE

    fun registerSession(userSession: UserSession) {
        session = userSession
    }

    fun getSession(): UserSession {
        return session
    }

}