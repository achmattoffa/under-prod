/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.services.session

import com.bitato.ut.api.model.net.sessioninit.SessionInitRequest

interface SessionManager: AutoCloseable {

    fun initSession(request: SessionInitRequest)

    fun reInitSession(request: SessionInitRequest)
}