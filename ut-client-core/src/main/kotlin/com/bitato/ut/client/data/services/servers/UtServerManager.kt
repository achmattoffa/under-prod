/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.data.services.servers

import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.thread
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.sbase.net.server.AbstractServer

object UtServerManager : ServerManager {

    private val servers: MutableList<AbstractServer>

    init {
        servers = arrayListOf()
    }

    override fun add(server: AbstractServer) {
        servers.add(server)
    }

    override fun remove(server: AbstractServer) {
        server.close()
        servers.remove(server)
    }

    override fun start() {

        try {
            servers.forEach {
                thread {
                    it.start()
                }
            }

            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_STARTED)
        } catch (ex: Exception) {
            EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, ex.message!!)
        }
    }

    override fun close() {
        EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_STOPPING)

        servers.forEach {
            io {
                it.close()
            }
        }

        servers.clear()

        EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_STOPPED)
    }

}
