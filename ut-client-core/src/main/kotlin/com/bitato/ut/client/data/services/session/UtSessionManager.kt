/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.services.session

import com.bitato.ut.api.model.net.internal.InternalSessionRequest
import com.bitato.ut.api.model.net.sessioninit.SessionInitRequest
import com.bitato.ut.api.model.net.sessioninit.SessionInitResponse
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.client.data.ApplicationState
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.net.authorization.AuthorizationServerConnector
import com.bitato.ut.client.net.connect.ServerSessionConnector
import com.bitato.ut.client.net.internal.SessionHouseKeeper
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

object UtSessionManager : SessionManager {


    override fun reInitSession(request: SessionInitRequest) {

        val session = SessionCache.getSession()

        val serverConnector = Injekt.get<ServerSessionConnector>()
        val optional = serverConnector.reconnect(session.serverSessionUuid, session.remoteAddress, session.remotePort, true)

        if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
            throw InterruptedException()
        }

        if (optional.isPresent) {

            val connectionResult = optional.get()
            session.isActive = true
            session.internalSock = connectionResult.connection

            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                throw InterruptedException()
            }

            val sessionHouseKeeper = SessionHouseKeeper()

            val sessionWorker = Thread(sessionHouseKeeper)
            sessionWorker.name = "Internal Session Housekeeper"
            sessionWorker.isDaemon = true
            sessionWorker.start()


            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                throw InterruptedException()
            }

            EventBus.notify(EventBus.EventIDs.SERVICE_STARTED)
        } else {
            EventBus.notify(EventBus.EventIDs.SERVICE_FAILED)
        }
    }

    override fun initSession(request: SessionInitRequest) {

        val auth: AuthorizationServerConnector = Injekt.get()

        val response = auth.initializeSession(request)

        if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
            throw InterruptedException()
        }

        if (response.status == SessionInitResponse.ResponseCodes.ACCESS_GRANTED) {

            val serverConnector = Injekt.get<ServerSessionConnector>()
            val optional = serverConnector.connect(response.uuid, response.address, response.port)

            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                throw InterruptedException()
            }

            if (optional.isPresent) {

                val connectionResult = optional.get()

                SessionCache.registerSession(UserSession(response.status == SessionInitResponse.ResponseCodes.ACCESS_GRANTED,
                  request.username,
                  connectionResult.serverSessionUuid,
                  connectionResult.throttle,
                  request.region,
                  response.address,
                  response.port,
                  connectionResult.connection
                ))


                val sessionHouseKeeper = SessionHouseKeeper()
                if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                    throw InterruptedException()
                }

                val sessionWorker = Thread(sessionHouseKeeper)
                sessionWorker.name = "Internal Session Housekeeper"
                sessionWorker.isDaemon = true
                sessionWorker.start()

                EventBus.notify(EventBus.EventIDs.SERVICE_STARTED)
            } else {
                EventBus.notify(EventBus.EventIDs.SERVICE_FAILED)
            }
        }
    }

    override fun close() {

        val session = SessionCache.getSession()

        io {
            session.internalSock.getOutputStream() < InternalSessionRequest(session.serverSessionUuid, InternalSessionRequest.RequestCodes.DISCONNECT)
            session.internalSock.getOutputStream().flush()
        }

    }
}