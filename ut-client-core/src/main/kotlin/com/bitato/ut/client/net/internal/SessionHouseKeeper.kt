/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.internal

import com.bitato.ut.api.model.net.internal.InternalSessionRequest
import com.bitato.ut.api.model.net.internal.InternalSessionResponse
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.client.configuration.UtNetworkConfiguration
import com.bitato.ut.client.data.ApplicationState
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.data.services.servers.ServerManager
import com.bitato.ut.client.data.services.session.SessionCache
import com.bitato.ut.client.net.connect.ServerSessionConnector
import com.bitato.ut.client.net.util.SocketManager
import com.bitato.ut.client.net.vpn.OpenVpnHelper
import com.github.salomonbrys.kotson.byte
import com.github.salomonbrys.kotson.int
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.JsonParser
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.io.IOException
import java.net.Socket

class SessionHouseKeeper : Runnable {

    private val socketManager: SocketManager = Injekt.get()
    private val serverManager: ServerManager = Injekt.get()

    fun handleRequest(target: Socket) {

        target.getOutputStream() < InternalSessionRequest(SessionCache.getSession().serverSessionUuid, InternalSessionRequest.RequestCodes.STATUS)
        target.getOutputStream().flush()

        if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.CONNECTED, ApplicationState.States.RECONNECTING, ApplicationState.States.DISCONNECTING)) {
            throw InterruptedException()
        }

        val js = UtNetworkConfiguration.getResponsePayload(target.getInputStream(), {
            val jsonParser = JsonParser().parse(it.inputStream().reader(Charsets.ISO_8859_1))

            if (jsonParser.isJsonObject) {
                jsonParser.asJsonObject
            } else {
                //TODO
                throw IOException("Reconnect requested")
            }
        })

        if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.CONNECTED, ApplicationState.States.RECONNECTING, ApplicationState.States.DISCONNECTING)) {
            throw InterruptedException()
        }

        val messageType = js["type"].int
        if (messageType == InternalSessionResponse.MessageTypes.SESSION_STATUS) {
            when (js["code"].byte) {
                InternalSessionResponse.SessionStatusCodes.ACCOUNT_BANNED -> {
                    try {
                        EventBus.notify(EventBus.EventIDs.SESSION_ACCOUNT_BANNED)
                        SessionCache.getSession().isActive = false
                    } finally {
                        closeResources()
                        EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                    }
                }
                InternalSessionResponse.SessionStatusCodes.CAP_REACHED -> {
                    try {
                        EventBus.notify(EventBus.EventIDs.SESSION_CAP_REACHED)
                        SessionCache.getSession().isActive = false
                    } finally {
                        closeResources()
                        EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                    }
                }
                InternalSessionResponse.SessionStatusCodes.PACKAGE_EXPIRED -> {
                    try {
                        EventBus.notify(EventBus.EventIDs.SESSION_PACKAGE_EXPIRED)
                        SessionCache.getSession().isActive = false
                    } finally {
                        closeResources()
                        EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                    }
                }
                InternalSessionResponse.SessionStatusCodes.UPGRADE_REQUESTED -> {
                    try {
                        EventBus.notify(EventBus.EventIDs.SESSION_UPGRADE_REQUESTED)
                        SessionCache.getSession().isActive = false
                    } finally {
                        closeResources()
                        EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                    }
                }
                InternalSessionResponse.SessionStatusCodes.SERVER_ERROR -> try {
                    EventBus.notify(EventBus.EventIDs.SESSION_REMOTE_ERROR)
                    SessionCache.getSession().isActive = false
                } finally {
                    closeResources()
                    EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                }
                InternalSessionResponse.SessionStatusCodes.KEEP_ALIVE -> {
                }
                InternalSessionResponse.SessionStatusCodes.TERMINATE -> {

                    closeResources()
                    EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                }
                else -> throw IOException()
            }
        } else if (messageType == InternalSessionResponse.MessageTypes.SESSION_USAGE) {
            EventBus.notify(EventBus.EventIDs.SESSION_STATUS_UPDATE, jsonObject("data" to js["usage"].asInt))
        } else if (messageType == InternalSessionResponse.MessageTypes.ACCOUNT_INFO) {
            EventBus.notify(EventBus.EventIDs.ACCOUNT_INFORMATION, js)
        } else if (messageType == InternalSessionResponse.MessageTypes.INTERNAL) {
            //TODO
            EventBus.notify(EventBus.EventIDs.INTERNAL_COMMAND)
        }
    }

    override fun run() {
        runInternal()
    }

    private fun runInternal() {

        try {
            if (ApplicationState.getState() !in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.CONNECTED, ApplicationState.States.RECONNECTING, ApplicationState.States.DISCONNECTING)) {
                throw InterruptedException()
            }

            handleHouseKeeping()

            if (SessionCache.getSession().isActive) {

                if (ApplicationState.getState() == ApplicationState.States.CONNECTING || ApplicationState.getState() == ApplicationState.States.RECONNECTING) {
                    // Issue global reconnect
                    closeResources()
                    EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_PAUSED, "Connection to the remote server has been closed")
                    Thread.sleep(3000)
                    EventBus.notify(EventBus.EventIDs.SERVICE_RECONNECT_REQUESTED)
                } else {
                    EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STARTED, false)

                    var isReconnectSuccess = false
                    for (i in 1..5) {

                        if (SessionCache.getSession() == SessionCache.NONE) {
                            break
                        }

                        Thread.sleep(500)
                        EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_RECONNECTING, "Attempt $i of 5")
                        if (tryReconnect()) {
                            isReconnectSuccess = true
                            break
                        }
                    }

                    if (isReconnectSuccess) {
                        EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_RECONNECTED)
                        runInternal()
                    } else {
                        //                EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_DISCONNECTED)
                        //                EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                        closeResources()
                        //EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
                        EventBus.notify(EventBus.EventIDs.SERVICE_RECONNECT_REQUESTED)
                    }
                }
            }
        } catch (ex: InterruptedException) {
            //TODO
        }
    }

    private fun handleHouseKeeping() {
        SessionCache.getSession().internalSock.use { it ->
            io {
                val js = UtNetworkConfiguration.getResponsePayload(it.getInputStream(), {
                    val jsonParser = JsonParser().parse(it.inputStream().reader(Charsets.ISO_8859_1))

                    if (jsonParser.isJsonObject) {
                        jsonParser.asJsonObject
                    } else {
                        //TODO
                        throw IOException("Reconnect requested")
                    }
                })

                if (js["type"].int == InternalSessionResponse.MessageTypes.ACCOUNT_INFO) {
                    EventBus.notify(EventBus.EventIDs.ACCOUNT_INFORMATION, js)
                }

                while (SessionCache.getSession().isActive) {
                    handleRequest(it)

                    if (Thread.currentThread().isInterrupted) {
                        throw InterruptedException()
                    }
                }
            }
        }
    }


    fun tryReconnect(): Boolean {
        val userSession = SessionCache.getSession()

        if (userSession != SessionCache.NONE) {
            val host = userSession.remoteAddress
            val port = userSession.remotePort
            val uuid = userSession.serverSessionUuid

            val serverConnector = Injekt.get<ServerSessionConnector>()
            val connectionResult = serverConnector.reconnect(uuid, host, port, false)

            if (connectionResult.isPresent) {
                userSession.internalSock = connectionResult.get().connection
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }


    fun closeResources() {
        SessionCache.getSession().isActive = false
        SessionCache.getSession().updatedTime = System.currentTimeMillis()
        SessionCache.registerSession(SessionCache.NONE)

        OpenVpnHelper.disconnect()

        io { socketManager.close() }
        io { serverManager.close() }
    }
}