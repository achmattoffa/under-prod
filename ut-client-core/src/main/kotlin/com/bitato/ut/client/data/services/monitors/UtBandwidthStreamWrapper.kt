/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.data.services.monitors

import com.bitato.ut.api.io.counter.InputStreamCounter
import com.bitato.ut.api.io.counter.OutputStreamCounter
import com.bitato.ut.client.configuration.CounterConfig
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.io.InputStream
import java.io.OutputStream

class UtBandwidthStreamWrapper: BandwidthStreamWrapper {

    private val config: CounterConfig = Injekt.get()

    override fun wrap(inputStream: InputStream): InputStream {

        var stream: InputStream = inputStream

        if (config.enableBwPerSecondCounter()) {
            stream = InputStreamCounter(stream, BandwidthCounters.bwPerSecondIn)
        }

        if (config.enableSessionBwCounter()) {
            stream = InputStreamCounter(stream, BandwidthCounters.bwPerSessionIn)
        }

        return stream
    }

    override fun wrap(outputStream: OutputStream): OutputStream {

        var stream = outputStream

        if (config.enableBwPerSecondCounter()) {
            stream = OutputStreamCounter(stream, BandwidthCounters.bwPerSecondOut)
        }

        if (config.enableSessionBwCounter()) {
            stream = OutputStreamCounter(stream, BandwidthCounters.bwPerSessionOut)
        }

        return stream
    }
}

fun InputStream.monitor(): InputStream {
    val monitor = Injekt.get<BandwidthStreamWrapper>()
    return monitor.wrap(this)
}

fun OutputStream.monitor(): OutputStream {
    val monitor = Injekt.get<BandwidthStreamWrapper>()
    return monitor.wrap(this)
}