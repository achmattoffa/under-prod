/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.events.bandwidth

import com.bitato.ut.client.data.services.monitors.BandwidthInfo

interface BandwidthCounterEventListener {

    fun onCountersStarted()

    fun onCountersUpdated(counters: BandwidthInfo)

    fun onCountersStopped()
}