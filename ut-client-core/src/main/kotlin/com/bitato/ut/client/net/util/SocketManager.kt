/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.util

import com.bitato.ut.api.util.AutoCloseableController
import java.net.Socket


interface SocketManager : AutoCloseableController<Socket> {
    fun count(): Int
}

