/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net.http

import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.sbase.net.handler.RequestHandler
import com.bitato.ut.sbase.net.mapper.AbstractRequestMapper
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

class UtHttpRequestMapper : AbstractRequestMapper() {

    private val httpStreamFactory: HttpStreamFactory = Injekt.get()

    override fun mapRequest(incoming: Socket): RequestHandler {

        val request = httpStreamFactory.createHttpRequest(incoming.inputStream)

        return when (request.method) {
            "CONNECT" -> UtHttpConnectRequestHandler(request, incoming)
            "POST", "PUT" -> UtHttpPostRequestHandler(request, incoming)
            else -> UtHttpRequestHandler(request, incoming)
        }
    }

}