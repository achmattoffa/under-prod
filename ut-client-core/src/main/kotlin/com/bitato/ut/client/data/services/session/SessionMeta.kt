/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.services.session

interface SessionMeta {
    fun isActive(): Boolean
    fun getUuid(): String
}