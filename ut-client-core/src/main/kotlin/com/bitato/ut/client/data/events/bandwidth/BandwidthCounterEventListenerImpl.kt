/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.data.events.bandwidth

import com.bitato.ut.client.data.services.monitors.BandwidthInfo

open class BandwidthCounterEventListenerImpl : BandwidthCounterEventListener {

    override fun onCountersStarted() = Unit

    override fun onCountersUpdated(counters: BandwidthInfo) = Unit

    override fun onCountersStopped() = Unit
}