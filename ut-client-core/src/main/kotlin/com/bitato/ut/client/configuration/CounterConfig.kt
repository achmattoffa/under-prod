/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.configuration

interface CounterConfig {
    fun enableSessionBwCounter(): Boolean
    fun enableBwPerSecondCounter(): Boolean
}