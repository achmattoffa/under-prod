/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.data.services.monitors

interface BandwidthInfo {

    fun getBwPerSecondIn(): Long

    fun getBwPerSecondOut(): Long

    fun getBwPerSessionIn(): Long

    fun getBwPerSessionOut(): Long
}