/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.data.services.monitors

import com.bitato.ut.api.model.common.Counter
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

object BandwidthCounters {

    val bwPerSecondIn: Counter = Injekt.get()
    val bwPerSecondOut: Counter = Injekt.get()
    val bwPerSessionIn: Counter = Injekt.get()
    val bwPerSessionOut: Counter = Injekt.get()

}