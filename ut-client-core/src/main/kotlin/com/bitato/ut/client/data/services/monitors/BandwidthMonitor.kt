/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.data.services.monitors

import com.bitato.ut.api.events.UtAbstractEventDispatcher
import com.bitato.ut.api.util.protect
import com.bitato.ut.client.data.events.bandwidth.BandwidthCounterEventListenerImpl
import com.bitato.ut.client.data.services.session.SessionMeta
import com.bitato.ut.client.data.services.session.SessionMetaProvider
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class BandwidthMonitor : Runnable, UtAbstractEventDispatcher<BandwidthCounterEventListenerImpl>() {

    private val sessionMeta: SessionMeta = Injekt.get<SessionMetaProvider>().getMeta()

    override fun run() {

        event {
            it.onCountersStarted()
        }

        while (sessionMeta.isActive()) {
            protect {
                Thread.sleep(1000L)

                event {
                    it.onCountersUpdated(makeBwInfo())
                }

                BandwidthCounters.bwPerSessionIn.plusAssign(BandwidthCounters.bwPerSecondIn.value())
                BandwidthCounters.bwPerSecondIn.reset()

                BandwidthCounters.bwPerSessionOut.plusAssign(BandwidthCounters.bwPerSecondIn.value())
                BandwidthCounters.bwPerSecondOut.reset()
            }
        }

        event {
            it.onCountersStopped()
        }
    }

    private fun makeBwInfo() = object : BandwidthInfo {
        override fun getBwPerSecondIn() = BandwidthCounters.bwPerSecondIn.value()
        override fun getBwPerSecondOut() = BandwidthCounters.bwPerSecondOut.value()
        override fun getBwPerSessionIn() = BandwidthCounters.bwPerSessionIn.value()
        override fun getBwPerSessionOut() = BandwidthCounters.bwPerSessionOut.value()
    }
}