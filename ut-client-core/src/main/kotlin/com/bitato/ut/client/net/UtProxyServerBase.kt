/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.client.net

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.client.data.services.session.SessionMeta
import com.bitato.ut.client.data.services.session.SessionMetaProvider
import com.bitato.ut.client.net.http.UtHttpRequestMapper
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

abstract class UtProxyServerBase(port: Int) : AbstractServer(NetworkProtocol.TCP, port) {

    protected val requestMapper: RequestMapper = UtHttpRequestMapper()
    protected val sessionMeta: SessionMeta = Injekt.get<SessionMetaProvider>().getMeta()

    override fun handleClientConnection(client: Socket) {

        if (sessionMeta.isActive()) {
            log info "Mapping incoming request to request handler"
            val requestHandler = requestMapper.mapRequest(client)

            log info "Submitting request handler to thread pool"
            threadPool.submit(requestHandler)
        } else {
            close()
        }
    }

}