/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.module

import com.bitato.ut.persist.utility.DbProcedureCallBuilder
import com.bitato.ut.persist.utility.DbQueryBuilder
import com.bitato.ut.persist.utility.UtDbProcedureCallBuilder
import com.bitato.ut.persist.utility.UtDbQueryBuilder
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import java.sql.Connection

object PersistenceBaseInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {
        addFactory<DbQueryBuilder> { UtDbQueryBuilder() }
        addFactory<DbProcedureCallBuilder> { UtDbProcedureCallBuilder() }

        val hikariCfg = HikariConfig("hikari.properties")
        val hikariDataSource = HikariDataSource(hikariCfg)
        addFactory<Connection> {
            hikariDataSource.getConnection()
        }
    }
}