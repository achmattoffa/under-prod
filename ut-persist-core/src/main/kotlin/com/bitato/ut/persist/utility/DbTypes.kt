/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

import java.util.*

typealias MemoryRow = Map<String, Any>
val emptyMemoryRow = MemoryRowImpl()

typealias MemoryTable = List<MemoryRow>
val emptyMemoryTable = MemoryTableImpl()

typealias MemoryRowImpl = HashMap<String, Any>
typealias MemoryTableImpl = ArrayList<MemoryRow>