/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

enum class DbParameterType {
    IN, OUT, INOUT
}