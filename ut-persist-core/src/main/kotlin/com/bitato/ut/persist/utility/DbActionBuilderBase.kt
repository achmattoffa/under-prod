/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

import com.bitato.ut.api.util.protect
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

abstract class DbActionBuilderBase<T>: DbActionBuilder<T> {

    protected fun executeQueryAndCloseResources(st: PreparedStatement, isTransaction: Boolean = false): MemoryTable {

        val data = MemoryTableImpl()
        var rs: ResultSet? = null

        try {

            if (isTransaction) {
                st.connection.autoCommit = false
            }

            try {
                rs = st.executeQuery()
                val numColumns = rs.metaData.columnCount

                if (numColumns == 0) {
                    return data
                }

                while (rs.next()) {

                    val row = MemoryRowImpl()

                    for (i in 1..numColumns) {
                        val column = rs.metaData.getColumnName(i)
                        row[column] = rs.getObject(column) ?: Unit
                    }

                    data.add(row)
                }
            } catch (ex: SQLException) {

                if (isTransaction) {
                    st.connection.rollback()
                }

                throw ex;
            }

            return data
        } finally {
            protect { rs?.close() }
            protect { st.close() }
        }
    }

    protected fun executeUpdateAndCloseResources(st: PreparedStatement, isTransaction: Boolean = false): Int {
        try {

            if (isTransaction) {
                st.connection.autoCommit = false
            }

            try {

                return st.executeUpdate()
            } catch (ex: SQLException) {

                st.connection.rollback()
                throw ex
            }

        } finally {
            protect { st.close() }
        }
    }
}