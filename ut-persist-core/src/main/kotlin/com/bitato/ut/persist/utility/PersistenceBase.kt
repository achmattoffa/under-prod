/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.sql.SQLException


abstract class PersistenceBase {

    inline fun <reified T> sqlQuery(block: DbQueryBuilder.() -> Unit): T {

        try {
            val dbQueryBuilder: DbQueryBuilder = Injekt.get()
            dbQueryBuilder.registerConnection(Injekt.get())

            block.invoke(dbQueryBuilder)
            val data = dbQueryBuilder.executeQuery()

            return (data as? T ?: if (data.isNotEmpty()) data[0] as T else emptyMemoryRow as T)
        } catch(ex: Exception) {
            throw SQLException(ex)
        }
    }

    inline fun sqlUpdate(block: DbQueryBuilder.() -> Unit): Int {
        try {
            val dbQueryBuilder: DbQueryBuilder = Injekt.get()
            dbQueryBuilder.registerConnection(Injekt.get())
            block.invoke(dbQueryBuilder)

            return dbQueryBuilder.executeUpdate()
        } catch(ex: Exception) {
            throw SQLException(ex)
        }
    }

    inline fun <reified T> procQuery(block: DbProcedureCallBuilder.() -> Unit): T {
        try {
            val dbProcedureCallBuilder: DbProcedureCallBuilder = Injekt.get()
            dbProcedureCallBuilder.registerConnection(Injekt.get())

            block.invoke(dbProcedureCallBuilder)
            val data = dbProcedureCallBuilder.executeQuery()

            return (data as? T ?: if (data.isNotEmpty()) data[0] as T else emptyMemoryRow as T)
        } catch (ex: Exception) {
            throw SQLException(ex)
        }
    }

    inline fun procUpdate(block: DbProcedureCallBuilder.() -> Unit): Int {
        try {
            val dbProcedureCallBuilder: DbProcedureCallBuilder = Injekt.get()
            dbProcedureCallBuilder.registerConnection(Injekt.get())

            block.invoke(dbProcedureCallBuilder)

            return dbProcedureCallBuilder.executeUpdate()
        } catch (ex: Exception) {
            throw SQLException(ex)
        }
    }
}

