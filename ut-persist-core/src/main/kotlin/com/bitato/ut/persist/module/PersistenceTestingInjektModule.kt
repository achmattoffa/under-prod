/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.module

import com.bitato.ut.persist.utility.DbProcedureCallBuilder
import com.bitato.ut.persist.utility.DbQueryBuilder
import com.bitato.ut.persist.utility.UtDbProcedureCallBuilder
import com.bitato.ut.persist.utility.UtDbQueryBuilder
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import java.sql.Connection

open class PersistenceTestingInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {
        addFactory<DbQueryBuilder> { UtDbQueryBuilder() }
        addFactory<DbProcedureCallBuilder> { UtDbProcedureCallBuilder() }
        addFactory { getDatabaseConnection() }
    }

    open fun setDatabaseConfiguration(): HikariConfig {

        return HikariConfig("src/tests/resources/hikari.properties")
    }

    private fun getDatabaseConnection(): Connection {

        val config = setDatabaseConfiguration()
        val dataSource = HikariDataSource(config)

        return dataSource.connection
    }
}