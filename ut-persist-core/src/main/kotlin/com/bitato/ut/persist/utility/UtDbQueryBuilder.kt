/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.Types.*
import java.util.*

class UtDbQueryBuilder : DbQueryBuilder, DbActionBuilderBase<DbQueryBuilder>() {
    override fun transaction(isTransaction: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var paramCount: Int = 0
    private val parameters: ArrayList<Array<Any>> = ArrayList()
    private lateinit var connection: Connection
    private var sql: String = ""


    override fun registerConnection(connection: Connection) {
        this.connection = connection
    }

    override fun sql(query: String): DbQueryBuilder {
        this.sql = query
        return this
    }

    private fun withParameters(dbParameterType: DbParameterType, type: Int, parameter: Any) {
        paramCount++

        val arr = Array<Any>(4, { 0 })
        arr[0] = paramCount
        arr[1] = dbParameterType
        arr[2] = type
        arr[3] = parameter
        parameters.add(arr)
    }

    override fun parameters(vararg params: Pair<Any, DbParameterType>): DbQueryBuilder {

        params.forEach {
            withParameters(it.second,
              when (it.first) {
                  is String -> VARCHAR
                  is Int -> INTEGER
                  is Float,
                  is Double -> DOUBLE
                  else -> INTEGER
              }, it.first)
        }

        return this
    }

    override fun parameters(vararg params: DbParameter): DbQueryBuilder {

        params.forEach {
            withParameters(it.parameterType,
              when (it.parameterValue) {
                  is String -> VARCHAR
                  is Int -> INTEGER
                  is Float,
                  is Double -> DOUBLE
                  else -> INTEGER
              }, it.parameterValue)
        }

        return this
    }

    override fun executeQuery(): MemoryTable {
        try {
            return executeQueryAndCloseResources(createStatement())
        } finally {
            connection.close();
        }
    }

    override fun executeUpdate(): Int {
        try {
            return executeUpdateAndCloseResources(createStatement())
        } finally {
            connection.close();
        }
    }

    private fun createStatement(): PreparedStatement {

        val statement = connection.prepareCall(sql)

        parameters.forEach {

            val paramIndex = it[0] as Int
            val paramMode = it[1] as DbParameterType
            val dataType = it[2] as Int
            val parameter = it[3]

            if (paramMode == DbParameterType.IN || paramMode == DbParameterType.INOUT) {
                when (dataType) {
                    INTEGER -> statement.setInt(paramIndex, parameter as Int)
                    DECIMAL -> statement.setDouble(paramIndex, parameter as Double)
                    VARCHAR -> statement.setString(paramIndex, parameter as String)
                }
            }

            if (parameter == DbParameterType.OUT) {
                statement.registerOutParameter(paramIndex, dataType)
            }
        }

        return statement
    }
}
