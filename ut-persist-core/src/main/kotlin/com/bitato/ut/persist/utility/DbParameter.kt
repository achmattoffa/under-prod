/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

class DbParameter
private constructor(val parameterType: DbParameterType, val parameterValue: Any) {

    companion object {
        fun input(elem: Any) = DbParameter(DbParameterType.IN, elem)
        fun output(elem: Any) = DbParameter(DbParameterType.OUT, elem)
        fun inout(elem: Any) = DbParameter(DbParameterType.INOUT, elem)

    }
}