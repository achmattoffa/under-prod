/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

interface DbQueryBuilder: DbActionBuilder<DbQueryBuilder> {

    /**
     * Sql to execute
     */
    fun sql(query: String): DbQueryBuilder
}