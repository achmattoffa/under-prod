/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

import java.sql.Connection

interface DbActionBuilder<T> {

    /**
     * Register this connection to the call builder
     */
    fun registerConnection(connection: Connection)

    /**
     * With these parameters
     */
    fun parameters(vararg params: Pair<Any, DbParameterType>): T

    /**
     * With these parameters
     */
    fun parameters(vararg params: DbParameter): T

    /**
     * Execute a query
     */
    fun executeQuery(): MemoryTable

    /**
     * Execute an update
     */
    fun executeUpdate(): Int


    /**
     * Sets a value indicating whether the call or query should be wrapped inside a transacttion
     */
    fun transaction(isTransaction: Boolean)
}