/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.persist.utility

interface DbProcedureCallBuilder: DbActionBuilder<DbProcedureCallBuilder> {

    /**
     * On this stored procedure
     */
    fun procedure(procedureName: String): DbProcedureCallBuilder
}