--
-- Definition for table ut_user_server_session_terminated_reason
--
CREATE TABLE ut_user_server_session_terminated_reason (
  pki_user_server_session_terminated_reason_id int(11) NOT NULL AUTO_INCREMENT,
  fki_user_server_session_id int(11) NOT NULL,
  fki_server_session_terminated_reason_id int(11) NOT NULL,
  dt_created datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pki_user_server_session_terminated_reason_id),
  INDEX fki_server_session_terminated_reason_id_FOREIGN_idx (fki_server_session_terminated_reason_id),
  INDEX fki_user_server_session_id_FOREIGN_idx (fki_user_server_session_id),
  UNIQUE INDEX pki_user_server_session_terminated_reason_id_UNIQUE (pki_user_server_session_terminated_reason_id),
  CONSTRAINT fki_server_session_terminated_reason_id_FOREIGN FOREIGN KEY (fki_server_session_terminated_reason_id)
  REFERENCES ut_server_session_terminated_reason (pki_server_session_terminated_reason_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fki_user_server_session_id_FOREIGN FOREIGN KEY (fki_user_server_session_id)
  REFERENCES ut_user_server_session (pki_user_server_session_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;