--
-- Definition for table ut_user_session
--
CREATE TABLE ut_user_session (
  pki_user_session_id int(11) NOT NULL AUTO_INCREMENT,
  fki_user_package_id int(11) NOT NULL,
  sz_user_session_uuid varchar(128) DEFAULT NULL,
  bt_active bit(1) NOT NULL DEFAULT b'1',
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  dt_disposed datetime DEFAULT NULL,
  PRIMARY KEY (pki_user_session_id),
  INDEX fki_user_package_id_FOREIGN_idx (fki_user_package_id),
  UNIQUE INDEX pki_session_id_UNIQUE (pki_user_session_id),
  UNIQUE INDEX sz_session_uuid_UNIQUE (sz_user_session_uuid),
  CONSTRAINT ut_user_session_fki_user_package_id_FOREIGN FOREIGN KEY (fki_user_package_id)
  REFERENCES ut_user_package (pki_user_package_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '						';