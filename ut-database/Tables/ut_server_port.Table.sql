--
-- Definition for table ut_server_port
--
CREATE TABLE ut_server_port (
  pki_server_port_id int(11) NOT NULL AUTO_INCREMENT,
  fki_server_id int(11) NOT NULL,
  fki_port_id int(11) NOT NULL,
  PRIMARY KEY (pki_server_port_id),
  INDEX fki_port_id_FOREIGN_idx (fki_port_id),
  INDEX fki_server_id_FOREIGN_idx (fki_server_id),
  UNIQUE INDEX pki_server_port_id_UNIQUE (pki_server_port_id),
  CONSTRAINT ut_serverPort_fki_port_id_FOREIGN FOREIGN KEY (fki_port_id)
  REFERENCES gn_port (pki_port_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT ut_serverPort_fki_server_id_FOREIGN FOREIGN KEY (fki_server_id)
  REFERENCES ut_server (pki_server_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;