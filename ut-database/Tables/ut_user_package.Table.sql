--
-- Definition for table ut_user_package
--
CREATE TABLE ut_user_package (
  pki_user_package_id int(11) NOT NULL AUTO_INCREMENT,
  fki_user_id int(11) NOT NULL,
  fki_package_id int(11) NOT NULL,
  fki_package_duration_id int(11) NOT NULL,
  i_data_used int(11) NOT NULL DEFAULT 0,
  i_devices_connected int(11) NOT NULL DEFAULT 0,
  bt_active bit(1) NOT NULL DEFAULT b'1',
  bt_expires bit(1) NOT NULL DEFAULT b'1',
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  dt_expire datetime DEFAULT NULL,
  PRIMARY KEY (pki_user_package_id),
  INDEX fki_package_duration_id_FOREIGN_idx (fki_package_duration_id),
  INDEX fki_package_id_FOREIGN_idx (fki_package_id),
  INDEX fki_user_id_FOREIGN_idx (fki_user_id),
  UNIQUE INDEX pki_user_package_id_UNIQUE (pki_user_package_id),
  CONSTRAINT ut_user_package_fki_package_duration_id_FOREIGN FOREIGN KEY (fki_package_duration_id)
  REFERENCES ut_package_duration (pki_package_duration_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT ut_user_package_fki_package_id_FOREIGN FOREIGN KEY (fki_package_id)
  REFERENCES ut_package (pki_package_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT ut_user_package_fki_user_id_FOREIGN FOREIGN KEY (fki_user_id)
  REFERENCES gn_account (pki_account_id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;