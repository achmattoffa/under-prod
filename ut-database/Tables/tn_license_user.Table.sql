--
-- Definition for table tn_license_user
--
CREATE TABLE tn_license_user (
  pki_license_user_id int(11) NOT NULL AUTO_INCREMENT,
  fki_license_id int(11) NOT NULL,
  sz_user_name varchar(20) NOT NULL,
  bt_banned BIT NOT NULL DEFAULT 0,
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pki_license_user_id),
  INDEX fki_license_FOREIGN_idx (fki_license_id),
  UNIQUE INDEX pki_license_user_UNIQUE (pki_license_user_id),
  CONSTRAINT tn_license_user_fki_license_FOREIGN FOREIGN KEY (fki_license_id)
  REFERENCES tn_license (pki_license_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;