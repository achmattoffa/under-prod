--
-- Definition for table gn_protocol
--
CREATE TABLE gn_protocol (
  pki_protocol_id int(11) NOT NULL AUTO_INCREMENT,
  sz_protocol_name varchar(20) NOT NULL,
  PRIMARY KEY (pki_protocol_id),
  UNIQUE INDEX pki_protocol_id_UNIQUE (pki_protocol_id),
  UNIQUE INDEX sz_protocol_name_UNIQUE (sz_protocol_name)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;