--
-- Definition for table ut_server_session_terminated_reason
--
CREATE TABLE ut_server_session_terminated_reason (
  pki_server_session_terminated_reason_id int(11) NOT NULL AUTO_INCREMENT,
  i_reason_value int(11) NOT NULL,
  sz_reason_desc varchar(128) NOT NULL,
  PRIMARY KEY (pki_server_session_terminated_reason_id),
  UNIQUE INDEX iReasonValue_UNIQUE (i_reason_value),
  UNIQUE INDEX pki_server_session_terminated_reason_id_UNIQUE (pki_server_session_terminated_reason_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;