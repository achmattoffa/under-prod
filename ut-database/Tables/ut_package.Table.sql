--
-- Definition for table ut_package
--
CREATE TABLE ut_package (
  pki_package_id int(11) NOT NULL AUTO_INCREMENT,
  sz_package_code varchar(10) NOT NULL,
  sz_package_name varchar(45) NOT NULL,
  i_limit_speed int(11) NOT NULL,
  i_limit_connected_devices int(11) NOT NULL,
  i_limit_data int(11) NOT NULL,
  fl_price_monthly decimal(10, 0) NOT NULL,
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pki_package_id),
  UNIQUE INDEX pki_package_id_UNIQUE (pki_package_id),
  UNIQUE INDEX sz_package_code_UNIQUE (sz_package_code),
  UNIQUE INDEX sz_package_name_UNIQUE (sz_package_name)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;