--
-- Definition for table gn_port
--
CREATE TABLE gn_port (
  pki_port_id int(11) NOT NULL AUTO_INCREMENT,
  i_port smallint(6) NOT NULL,
  fki_protocol_id int(11) NOT NULL,
  PRIMARY KEY (pki_port_id),
  INDEX fki_protocol_id_FOREIGN_idx (fki_protocol_id),
  UNIQUE INDEX pki_port_id_UNIQUE (pki_port_id),
  CONSTRAINT fki_protocol_id_FOREIGN FOREIGN KEY (fki_protocol_id)
  REFERENCES gn_protocol (pki_protocol_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;