--
-- Definition for table ut_server_maintenance
--
CREATE TABLE ut_server_maintenance (
  pki_server_maintenance_id int(11) NOT NULL AUTO_INCREMENT,
  fki_server_id int(11) NOT NULL,
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  dt_ended datetime DEFAULT NULL,
  PRIMARY KEY (pki_server_maintenance_id),
  INDEX fki_server_address_FOREIGN_idx (fki_server_id),
  UNIQUE INDEX pki_server_maintenance_UNIQUE (pki_server_maintenance_id),
  CONSTRAINT ut_server_maintenance_fki_server_address_FOREIGN FOREIGN KEY (fki_server_id)
  REFERENCES ut_server (pki_server_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;