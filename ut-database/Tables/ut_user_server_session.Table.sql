--
-- Definition for table ut_user_server_session
--
CREATE TABLE ut_user_server_session (
  pki_user_server_session_id int(11) NOT NULL AUTO_INCREMENT,
  sz_user_server_session_uuid varchar(128) DEFAULT NULL,
  fki_user_session_id int(11) NOT NULL,
  fki_server_id int(11) NOT NULL,
  bt_active bit(1) NOT NULL DEFAULT b'1',
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  dt_disposed datetime DEFAULT NULL,
  PRIMARY KEY (pki_user_server_session_id),
  INDEX fki_server_id_FOREIGN_idx (fki_server_id),
  INDEX fki_user_session_id_FOREIGN_idx (fki_user_session_id),
  UNIQUE INDEX pki_user_server_session_id_UNIQUE (pki_user_server_session_id),
  UNIQUE INDEX sz_user_server_session_uuid_UNIQUE (sz_user_server_session_uuid),
  CONSTRAINT ut_user_server_session_fki_server_id_FOREIGN FOREIGN KEY (fki_server_id)
  REFERENCES ut_server (pki_server_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT ut_user_server_session_fki_user_session_id_FOREIGN FOREIGN KEY (fki_user_session_id)
  REFERENCES ut_user_session (pki_user_session_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;