--
-- Definition for table tn_license
--
CREATE TABLE tn_license (
  pki_license_id int(11) NOT NULL AUTO_INCREMENT,
  fki_account_id int(11) NOT NULL,
  sz_license_key varchar(128) DEFAULT NULL,
  sz_usage_key varchar(128) DEFAULT NULL,
  bt_active BIT NOT NULL DEFAULT 1,
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pki_license_id),
  INDEX fki_account_FOREIGN_idx (fki_account_id),
  UNIQUE INDEX pki_license_UNIQUE (pki_license_id),
  CONSTRAINT tn_license_fki_account_FOREIGN FOREIGN KEY (fki_account_id)
  REFERENCES gn_account (pki_account_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;
