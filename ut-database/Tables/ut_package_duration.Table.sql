--
-- Definition for table ut_package_duration
--
CREATE TABLE ut_package_duration (
  pki_package_duration_id int(11) NOT NULL AUTO_INCREMENT,
  sz_package_duration_code varchar(15) NOT NULL,
  sz_package_duration_description varchar(200) NOT NULL,
  PRIMARY KEY (pki_package_duration_id),
  UNIQUE INDEX pki_package_duration_id_UNIQUE (pki_package_duration_id),
  UNIQUE INDEX sz_package_duration_code_UNIQUE (sz_package_duration_code),
  UNIQUE INDEX sz_package_duration_description_UNIQUE (sz_package_duration_description)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;