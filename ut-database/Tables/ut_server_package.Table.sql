--
-- Definition for table ut_server_package
--
CREATE TABLE ut_server_package (
  pki_server_package_id int(11) NOT NULL AUTO_INCREMENT,
  fki_server_id int(11) NOT NULL,
  fki_package_id int(11) NOT NULL,
  PRIMARY KEY (pki_server_package_id),
  INDEX fki_package_id_FOREIGN_idx (fki_package_id),
  INDEX fki_server_id_FOREIGN_idx (fki_server_id),
  UNIQUE INDEX pki_server_package_id_UNIQUE (pki_server_package_id),
  CONSTRAINT ut_server_package_fki_package_id_FOREIGN FOREIGN KEY (fki_package_id)
  REFERENCES ut_package (pki_package_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT ut_server_package_fki_server_id_FOREIGN FOREIGN KEY (fki_server_id)
  REFERENCES ut_server (pki_server_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;