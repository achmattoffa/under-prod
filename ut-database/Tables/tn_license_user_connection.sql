--
-- Definition for table tn_license_user_connection
--
CREATE TABLE tn_license_user_connection (
  pki_license_user_connection_id int(11) NOT NULL AUTO_INCREMENT,
  fki_license_user_id int(11) NOT NULL,
  sz_ip_address varchar(30) NOT NULL,
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pki_license_user_connection_id),
  INDEX fki_license_user_FOREIGN_idx (fki_license_user_id),
  UNIQUE INDEX pki_license_user_connection_UNIQUE (pki_license_user_connection_id),
  CONSTRAINT tn_license_user_connection_fki_license_user_FOREIGN FOREIGN KEY (fki_license_user_id)
  REFERENCES tn_license_user (pki_license_user_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;