--
-- Definition for table ut_server
--
CREATE TABLE ut_server (
  pki_server_id int(11) NOT NULL AUTO_INCREMENT,
  sz_server_code varchar(10) NOT NULL,
  sz_server_name varchar(45) NOT NULL,
  fki_location_id int(11) NOT NULL,
  sz_hostname varchar(100) DEFAULT NULL,
  sz_ipv4_address varchar(20) DEFAULT NULL,
  sz_ipv6_address varchar(45) DEFAULT NULL,
  bt_online bit(1) NOT NULL DEFAULT b'1',
  bt_disabled bit(1) NOT NULL DEFAULT b'0',
  dt_created datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pki_server_id),
  INDEX fki_location_id_FOREIGN_idx (fki_location_id),
  UNIQUE INDEX pki_server_id_UNIQUE (pki_server_id),
  UNIQUE INDEX sz_hostname_UNIQUE (sz_hostname),
  UNIQUE INDEX sz_ipv4_address_UNIQUE (sz_ipv4_address),
  UNIQUE INDEX sz_ipv6_address_UNIQUE (sz_ipv6_address),
  UNIQUE INDEX sz_server_name_UNIQUE (sz_server_name),
  UNIQUE INDEX sz_server_uuid_UNIQUE (sz_server_code),
  CONSTRAINT ut_server_fki_location_id_FOREIGN FOREIGN KEY (fki_location_id)
  REFERENCES gn_location (pki_location_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;