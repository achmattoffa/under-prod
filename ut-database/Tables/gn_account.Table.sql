--
-- Definition for table gn_account
--
CREATE TABLE gn_account (
  pki_account_id int(11) NOT NULL AUTO_INCREMENT,
  sz_email varchar(100) NOT NULL,
  sz_username varchar(20) DEFAULT NULL,
  sz_password varchar(80) NOT NULL,
  sz_salt varchar(40) NOT NULL,
  bt_active bit(1) NOT NULL DEFAULT b'1',
  bt_banned bit(1) NOT NULL DEFAULT b'0',
  dt_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pki_account_id),
  UNIQUE INDEX pki_account_id_UNIQUE (pki_account_id),
  UNIQUE INDEX sz_email_UNIQUE (sz_email),
  UNIQUE INDEX sz_username_UNIQUE (sz_username)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;