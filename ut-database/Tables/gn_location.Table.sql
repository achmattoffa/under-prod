--
-- Definition for table gn_location
--
CREATE TABLE gn_location (
  pki_location_id int(11) NOT NULL AUTO_INCREMENT,
  sz_country varchar(45) DEFAULT NULL,
  PRIMARY KEY (pki_location_id),
  UNIQUE INDEX sz_country_UNIQUE (sz_country)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;