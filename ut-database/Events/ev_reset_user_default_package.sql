DELIMITER $$

CREATE EVENT `ev_reset_user_default_package_daily`
  ON SCHEDULE EVERY 1 DAY STARTS '2017-05-07 00:00:00'
  DO BEGIN
  
      UPDATE ut_user_package uup
        INNER JOIN ut_package_duration upd  ON uup.fki_package_duration_id = upd.pki_package_duration_id
          AND upd.sz_package_duration_code = 'DAY'
        INNER JOIN (SELECT uup1.fki_user_id, SUM(uup1.bt_active = 1) AS active_packages FROM ut_user_package uup1 GROUP BY uup1.fki_user_id) ap
          ON ap.fki_user_id = uup.fki_user_id
         SET uup.i_data_used = 0, 
             uup.bt_active = CASE WHEN ap.active_packages > 0 THEN 0 ELSE 1 END
        WHERE uup.bt_expires = 0;
  END;
$$
CREATE EVENT `ev_reset_user_default_package_weekly`
  ON SCHEDULE EVERY 1 WEEK STARTS '2017-05-07 00:00:00'
  DO BEGIN
  
      UPDATE ut_user_package uup
        INNER JOIN ut_package_duration upd  ON uup.fki_package_duration_id = upd.pki_package_duration_id
          AND upd.sz_package_duration_code = 'WEEK'
        INNER JOIN (SELECT uup1.fki_user_id, SUM(uup1.bt_active = 1) AS active_packages FROM ut_user_package uup1 GROUP BY uup1.fki_user_id) ap
          ON ap.fki_user_id = uup.fki_user_id
         SET uup.i_data_used = 0, 
             uup.bt_active = CASE WHEN ap.active_packages > 0 THEN uup.bt_active ELSE 1 END
        WHERE uup.bt_expires = 0;
  END;
$$
CREATE EVENT `ev_reset_user_default_package_monthly`
  ON SCHEDULE EVERY 1 MONTH STARTS '2017-05-07 00:00:00'
  DO BEGIN
  
      UPDATE ut_user_package uup
        INNER JOIN ut_package_duration upd  ON uup.fki_package_duration_id = upd.pki_package_duration_id
          AND upd.sz_package_duration_code = 'MONTH'
        INNER JOIN (SELECT uup1.fki_user_id, SUM(uup1.bt_active = 1) AS active_packages FROM ut_user_package uup1 GROUP BY uup1.fki_user_id) ap
          ON ap.fki_user_id = uup.fki_user_id
         SET uup.i_data_used = 0, 
             uup.bt_active = CASE WHEN ap.active_packages > 0 THEN uup.bt_active ELSE 1 END
        WHERE uup.bt_expires = 0;
  END
$$
CREATE EVENT `ev_reset_user_default_package_yearly`
  ON SCHEDULE EVERY 1 MONTH STARTS '2017-05-07 00:00:00'
  DO BEGIN
  
      UPDATE ut_user_package uup
        INNER JOIN ut_package_duration upd  ON uup.fki_package_duration_id = upd.pki_package_duration_id
          AND upd.sz_package_duration_code = 'YEAR'
        INNER JOIN (SELECT uup1.fki_user_id, SUM(uup1.bt_active = 1) AS active_packages FROM ut_user_package uup1 GROUP BY uup1.fki_user_id) ap
          ON ap.fki_user_id = uup.fki_user_id
         SET uup.i_data_used = 0, 
             uup.bt_active = CASE WHEN ap.active_packages > 0 THEN uup.bt_active ELSE 1 END
        WHERE uup.bt_expires = 0;
  END
$$
DELIMITER ;

