DELIMITER $$

--
-- Definition for trigger trg_gn_account_createDefaultuser_package
--
CREATE TRIGGER trg_gn_account_create_default_package
AFTER INSERT
ON gn_account
FOR EACH ROW
BEGIN
  INSERT INTO ut_user_package (fki_user_id, fki_package_id, fki_package_duration_id, bt_expires)
    VALUES (new.pki_account_id, 1, 2, 0);

END
$$
DELIMITER ;