DELIMITER $$

--
-- Definition for trigger trg_ut_user_server_session_setUuid
--
CREATE TRIGGER trg_ut_user_server_session_set_uuid
BEFORE INSERT
ON ut_user_server_session
FOR EACH ROW
BEGIN
  SET NEW.sz_user_server_session_uuid = UUID();
END
$$DELIMITER ;

