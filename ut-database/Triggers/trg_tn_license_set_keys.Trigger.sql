DELIMITER $$

--
-- Definition for trigger trg_tn_license_set_keys
--
CREATE TRIGGER trg_tn_license_set_keys
BEFORE INSERT
ON tn_license
FOR EACH ROW
BEGIN
  SET NEW.sz_license_key = UUID(),
    NEW.sz_usage_key = UUID();
END
$$DELIMITER ;

