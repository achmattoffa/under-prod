DELIMITER $$

--
-- Definition for trigger trg_ut_user_package_updatePackageExpirationDate
--
CREATE TRIGGER trg_ut_user_package_update_expiration
BEFORE INSERT
ON ut_user_package
FOR EACH ROW
BEGIN
  DECLARE package_duration_code varchar(10);

  IF (new.bt_expires = 1)
    THEN
    SELECT
      sz_package_duration_code INTO package_duration_code
    FROM ut_package_duration
    WHERE pki_package_duration_id = new.fki_package_duration_id;

    IF (package_duration_code = 'WEEK')
      THEN
      SET new.dt_expire = DATE_ADD(NOW(), INTERVAL 7 DAY);
    ELSEIF (package_duration_code = 'MONTH')
      THEN
      SET new.dt_expire = DATE_ADD(NOW(), INTERVAL 1 MONTH);
    ELSEIF (package_duration_code = 'DAY')
      THEN
      SET new.dt_expire = DATE_ADD(NOW(), INTERVAL 1 DAY);
    ELSEIF (package_duration_code = 'YEAR')
      THEN
      SET new.dt_expire = DATE_ADD(NOW(), INTERVAL 1 YEAR);
    ELSEIF (package_duration_code = 'TEST')
      THEN
      SET new.dt_expire = DATE_ADD(NOW(), INTERVAL 2 HOUR);
    END IF;

  END IF;

END
$$
DELIMITER ;