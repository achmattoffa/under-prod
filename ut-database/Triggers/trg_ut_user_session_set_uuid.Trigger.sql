DELIMITER $$

--
-- Definition for trigger trg_ut_user_session_SetUuid
--
CREATE TRIGGER trg_ut_user_session_set_uuid
BEFORE INSERT
ON ut_user_session
FOR EACH ROW
BEGIN
  SET NEW.sz_user_session_uuid = UUID();
END

$$DELIMITER ;

