DELIMITER $$

--
-- Definition for procedure usp_gn_account_get_user_salt
--
CREATE PROCEDURE usp_gn_account_get_user_salt(IN p_user_id INT)
  BEGIN

    SELECT ga.sz_salt
    FROM gn_account ga
    WHERE ga.pki_account_id = p_user_id;

  END
$$
DELIMITER ;