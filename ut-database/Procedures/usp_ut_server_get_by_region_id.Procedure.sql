DELIMITER $$

--
-- Definition for procedure usp_ut_server_get_by_region_id
--
CREATE PROCEDURE usp_ut_server_get_by_region_id(IN p_location_id INT)
  BEGIN

    IF (p_location_id > 0)
    THEN
      SELECT
        us.pki_server_id,
        us.sz_server_code,
        us.sz_server_name,
        us.fki_location_id,
        us.sz_hostname,
        us.sz_ipv4_address,
        us.sz_ipv6_address,
        us.bt_online,
        us.bt_disabled,
        us.dt_created,
        443                       AS 'i_port',
        gpr.sz_protocol_name,
        COUNT(uuss.fki_server_id) AS 'i_active_users'
      FROM ut_server us
        INNER JOIN ut_server_port usp
          ON usp.fki_server_id = us.pki_server_id
        INNER JOIN gn_port gpo
          ON usp.fki_port_id = gpo.pki_port_id
        INNER JOIN gn_protocol gpr
          ON gpo.fki_protocol_id = gpr.pki_protocol_id
        LEFT JOIN ut_user_server_session uuss
          ON uuss.fki_server_id = us.pki_server_id
             AND uuss.bt_active = 1
      WHERE us.bt_online = 1
            AND us.fki_location_id = p_location_id
            AND 'i_active_users' < 15
      GROUP BY usp.pki_server_port_id;

    END IF;
  END
$$
DELIMITER ;
