DELIMITER $$

--
-- Definition for procedure usp_tn_license_validate_init
--
CREATE PROCEDURE usp_tn_license_validate_init(IN p_sz_usage_key VARCHAR(128))
  BEGIN

    IF EXISTS(SELECT 1 FROM tn_license tl WHERE tl.sz_usage_key = p_sz_usage_key AND bt_active = 1)
    THEN
      SELECT 'SUCCESS' AS 'sz_status';
    ELSE
      SELECT 'INVALID USAGE KEY' AS 'sz_status';
    END IF;

  END
$$
DELIMITER ;