DELIMITER $$

CREATE PROCEDURE usp_create_user_server_session(IN p_user_id INT, IN p_server_id INT)
  BEGIN

    DECLARE v_user_package_id INT;
    DECLARE v_user_session_id INT;
    DECLARE v_user_server_session_id INT;
    DECLARE v_max_devices INT;
    DECLARE v_max_speed INT;
    DECLARE v_active_devices INT;

    SELECT pki_user_package_id
    INTO v_user_package_id
    FROM ut_user_package
    WHERE fki_user_id = p_user_id
          AND bt_active = 1;

    SELECT pki_user_session_id
    INTO v_user_session_id
    FROM ut_user_session
    WHERE fki_user_package_id = v_user_package_id
          AND bt_active = 1;

    IF (v_user_session_id IS NOT NULL)
    THEN

      SELECT
        if (i_devices_connected > 0, i_devices_connected, 0),
        i_limit_connected_devices,
        i_limit_speed
      INTO v_active_devices, v_max_devices, v_max_speed
      FROM ut_user_session
        INNER JOIN ut_user_package
          ON fki_user_package_id = pki_user_package_id
             AND pki_user_package_id = v_user_package_id
        INNER JOIN ut_package
          ON fki_package_id = pki_package_id;

      IF (v_max_devices > v_active_devices)
      THEN
        SET v_active_devices = v_active_devices + 1;

        INSERT INTO ut_user_server_session (fki_server_id, fki_user_session_id)
        VALUES (p_server_id, v_user_session_id);

        SELECT LAST_INSERT_ID()
        INTO v_user_server_session_id;

        UPDATE ut_user_package
        SET i_devices_connected = v_active_devices
        WHERE pki_user_package_id = v_user_package_id;


        SELECT
          p_user_id,
          v_user_server_session_id,
          sz_user_server_session_uuid,
          i_limit_speed
        FROM ut_user_server_session
          INNER JOIN ut_user_session
            ON fki_user_session_id = pki_user_session_id
          INNER JOIN ut_user_package
            ON fki_user_package_id = pki_user_package_id
          INNER JOIN ut_package
            ON fki_package_id = pki_package_id
        WHERE pki_user_server_session_id = v_user_server_session_id;


      END IF;
    ELSE

      INSERT INTO ut_user_session (fki_user_package_id)
      VALUES (v_user_package_id);

      INSERT INTO ut_user_server_session (fki_server_id, fki_user_session_id)
      VALUES (p_server_id, LAST_INSERT_ID());

      SELECT LAST_INSERT_ID()
      INTO v_user_server_session_id;

      UPDATE ut_user_package
      SET i_devices_connected = 1
      WHERE pki_user_package_id = v_user_package_id;


      SELECT
        p_user_id,
        v_user_server_session_id,
        sz_user_server_session_uuid,
        v_max_speed
      FROM ut_user_server_session
      WHERE pki_user_server_session_id = v_user_server_session_id;

    END IF;
  END
$$
DELIMITER ;

