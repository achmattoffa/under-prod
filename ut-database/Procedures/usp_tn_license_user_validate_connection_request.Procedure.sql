DELIMITER $$

--
-- Definition for procedure usp_tn_license_user_validate_connection_request
--
CREATE PROCEDURE usp_tn_license_user_validate_connection_request(IN p_sz_usage_key VARCHAR(128),
                                                                 IN p_sz_user_name VARCHAR(128),
                                                                 IN p_sz_ip        VARCHAR(128))
  BEGIN

    DECLARE v_license_id INT;
    DECLARE v_license_user_id INT;

    IF EXISTS(SELECT 1
              FROM tn_license tl
              WHERE tl.sz_usage_key = p_sz_usage_key AND bt_active = 1)
    THEN
      SELECT tl.pki_license_id
      INTO v_license_id
      FROM tn_license tl
      WHERE sz_usage_key = p_sz_usage_key;

      IF EXISTS(SELECT 1
                FROM tn_license_user tlu
                WHERE upper(tlu.sz_user_name) = upper(p_sz_user_name) AND tlu.fki_license_id = v_license_id)
      THEN
        SELECT tlu.pki_license_user_id
        INTO v_license_user_id
        FROM tn_license_user tlu
        WHERE upper(tlu.sz_user_name) = upper(p_sz_user_name)
              AND tlu.bt_banned = 0
              AND tlu.fki_license_id = v_license_id;


      ELSE
        INSERT INTO tn_license_user (fki_license_id, sz_user_name)
        VALUES (v_license_id, upper(p_sz_user_name));

        SELECT LAST_INSERT_ID()
        INTO v_license_user_id;

      END IF;

      IF ISNULL(v_license_user_id)
      THEN
        SELECT 'USER NOT VALID' AS 'sz_status';
      ELSE
        INSERT INTO tn_license_user_connection (fki_license_user_id, sz_ip_address)
        VALUES (v_license_user_id, p_sz_ip);

        SELECT 'SUCCESS' AS 'sz_status';
      END IF;
ELSE
SELECT 'INVALID USAGE KEY' AS 'sz_status';
END IF;

END
$$
DELIMITER ;