DELIMITER $$

--
-- Definition for procedure usp_gn_account_get_user_by_username
--
CREATE PROCEDURE usp_gn_account_get_user_by_username(IN p_sz_username VARCHAR(128))
  BEGIN
    SELECT
      ga.pki_account_id,
      ga.sz_email,
      ga.sz_username,
      ga.sz_salt,
      ga.bt_active,
      ga.bt_banned,
      ga.dt_created
    FROM gn_account ga
    WHERE upper(ga.sz_username) = upper(p_sz_username);
  END
$$
DELIMITER ;