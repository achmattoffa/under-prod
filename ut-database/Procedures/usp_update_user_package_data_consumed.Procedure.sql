DELIMITER $$

--
-- Definition for procedure usp_update_user_package_data_consumed
--
CREATE PROCEDURE usp_update_user_package_data_consumed(IN p_user_server_session_id INT,
                                                       IN p_data_in_bytes          INT)
  BEGIN

    DECLARE v_data_left INT;
    DECLARE v_user_package_id INT;
    DECLARE v_user_session_id INT;

    IF (p_user_server_session_id > 0 AND p_data_in_bytes > 0)
    THEN

      SELECT
        uus.fki_user_package_id,
        (i_limit_data - i_data_used),
        uuss.fki_user_session_id
      INTO v_user_package_id, v_data_left, v_user_session_id
      FROM ut_user_server_session uuss
        INNER JOIN ut_user_session uus
          ON uuss.fki_user_session_id = uus.pki_user_session_id
        INNER JOIN ut_user_package
          ON fki_user_package_id = pki_user_package_id
        INNER JOIN ut_package
          ON fki_package_id = pki_package_id
      WHERE uuss.pki_user_server_session_id = p_user_server_session_id;

      IF (v_user_package_id IS NOT NULL)
      THEN

        UPDATE ut_user_package
        SET i_data_used = i_data_used + p_data_in_bytes
        WHERE pki_user_package_id = v_user_package_id;

        IF (p_data_in_bytes >= v_data_left * 1024)
        THEN
          UPDATE ut_user_package
          SET bt_active         = 0,
            i_devices_connected = 0
          WHERE pki_user_package_id = v_user_package_id;

          -- dispose all server sessions for user
          UPDATE ut_user_server_session
          SET bt_active = 0,
            dt_disposed = CURRENT_TIMESTAMP()
          WHERE fki_user_session_id = v_user_session_id;

          -- dispose session
          UPDATE ut_user_session
          SET bt_active = 0
          WHERE pki_user_session_id = v_user_session_id;

        END IF;


      END IF;

    END IF;
  END
$$
DELIMITER ;