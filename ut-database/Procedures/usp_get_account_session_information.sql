DELIMITER $$

--
-- Definition for procedure usp_get_session_terminated_reason
--
CREATE PROCEDURE usp_get_account_session_information(IN p_user_server_session_id INT)
  BEGIN
    IF (p_user_server_session_id > 0)
    THEN
      SELECT
        ut_package.sz_package_name,
        ut_package.i_limit_data - ut_user_package.i_data_used AS 'i_data_remaining',
        IFNULL(ut_user_package.dt_expire, 'Never') AS 'dt_expire',
        ut_user_package.i_devices_connected
        FROM ut_user_server_session
        INNER JOIN ut_user_session
         ON ut_user_server_session.fki_user_session_id = ut_user_session.pki_user_session_id
        INNER JOIN ut_user_package
          ON ut_user_session.fki_user_package_id = ut_user_package.pki_user_package_id
        INNER JOIN ut_package
          ON ut_user_package.fki_package_id = ut_package.pki_package_id
        INNER JOIN ut_package_duration
          ON ut_user_package.fki_package_duration_id = ut_package_duration.pki_package_duration_id
      WHERE p_user_server_session_id = pki_user_server_session_id;
    END IF;
  END
$$ DELIMITER ;

