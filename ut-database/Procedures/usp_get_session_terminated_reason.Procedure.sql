DELIMITER $$

--
-- Definition for procedure usp_get_session_terminated_reason
--
CREATE PROCEDURE usp_get_session_terminated_reason(IN p_user_server_session_id INT)
  BEGIN
    IF (p_user_server_session_id > 0)
    THEN
      SELECT
        pki_user_server_session_terminated_reason_id,
        i_reason_value,
        sz_reason_desc,
        dt_created
      FROM ut_user_server_session_terminated_reason
        INNER JOIN ut_server_session_terminated_reason
          ON fki_server_session_terminated_reason_id = pki_server_session_terminated_reason_id
      WHERE fki_user_server_session_id = p_user_server_session_id;
    END IF;
  END
$$ DELIMITER ;

