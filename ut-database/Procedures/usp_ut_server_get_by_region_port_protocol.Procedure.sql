DELIMITER $$

--
-- Definition for procedure usp_ut_server_get_by_region_id
--

CREATE PROCEDURE usp_ut_server_get_by_region_port_protocol(IN p_region_code VARCHAR(128), IN p_port INT,
                                                           IN p_protocol    VARCHAR(128))
  BEGIN

    DECLARE v_region_id INT;
    DECLARE v_port_id INT;
    DECLARE v_protocol_id INT;
    DECLARE v_inputs_valid BIT DEFAULT 1;

    #   Validate input parameters
    IF (upper(p_protocol) NOT IN ('TCP', 'UDP', 'ANY'))
    THEN
      SET v_inputs_valid = 0;
    ELSE
      SELECT pki_protocol_id
      INTO v_protocol_id
      FROM gn_protocol
      WHERE gn_protocol.sz_protocol_name = UPPER(p_protocol);
    END IF;

    IF (p_port = 0 OR p_port < -1 OR p_port >= 65663) # -1 is a valid port
    THEN
      SET v_inputs_valid = 0;
    ELSE
      SELECT pki_port_id
      INTO v_port_id
      FROM gn_port
      WHERE i_port = p_port;
    END IF;

    IF (UPPER(p_region_code) <> 'ANY' AND NOT EXISTS(SELECT 1
                                                     FROM gn_location
                                                     WHERE sz_country = UPPER(p_region_code)))
    THEN
      SET v_inputs_valid = 0;
    ELSE
      SELECT pki_location_id
      INTO v_region_id
      FROM gn_location
      WHERE sz_country = upper(p_region_code);
    END IF;

    IF (v_inputs_valid = 1)
    THEN

      SELECT
        pki_server_id,
        sz_server_code,
        sz_server_name,
        sz_hostname,
        sz_country,
        sz_ipv4_address,
        sz_ipv6_address,
        i_port,
        sz_protocol_name,
        COUNT(pki_user_server_session_id) AS 'i_load'
      FROM ut_server
        INNER JOIN gn_location ON ut_server.fki_location_id = gn_location.pki_location_id AND
                                  fki_location_id = IFNULL(v_region_id, fki_location_id)
        INNER JOIN ut_server_port
          ON ut_server.pki_server_id = ut_server_port.fki_server_id AND fki_port_id = IFNULL(v_port_id, fki_port_id)
        INNER JOIN gn_port
          ON ut_server_port.fki_port_id = gn_port.pki_port_id AND i_port = IF(p_port = -1, i_port, p_port)
        INNER JOIN gn_protocol ON gn_port.fki_protocol_id = gn_protocol.pki_protocol_id AND
                                  fki_protocol_id = IFNULL(v_protocol_id, fki_protocol_id)
        LEFT JOIN ut_user_server_session ON ut_server.pki_server_id = ut_user_server_session.fki_server_id
      WHERE ut_server.bt_online = 1 AND ut_server.bt_disabled = 0
      GROUP BY
        pki_server_id,
        sz_server_code,
        sz_server_name,
        sz_hostname,
        sz_country,
        sz_ipv4_address,
        sz_ipv6_address,
        i_port,
        sz_protocol_name;

    END IF;

  END
$$
DELIMITER ;