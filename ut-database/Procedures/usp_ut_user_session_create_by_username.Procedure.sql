DELIMITER $$

--
-- Definition for procedure usp_ut_user_session_create_by_username
--
CREATE PROCEDURE usp_ut_user_session_create_by_username(IN p_username VARCHAR(128))
  BEGIN

    DECLARE v_user_package_id INT;
    DECLARE v_user_session_id INT;

    -- Get the users current package to use
    SELECT uup.pki_user_package_id
    INTO v_user_package_id
    FROM ut_user_package uup
      INNER JOIN gn_account ga
        ON UPPER(ga.sz_username) = UPPER(p_username)
           AND uup.fki_user_id = ga.pki_account_id
           AND ga.bt_banned = 0
           AND ga.bt_active = 1
    WHERE uup.bt_active = 1;

    IF (v_user_package_id IS NOT NULL)
    THEN
      -- Get the users current active session
      SELECT uus.pki_user_session_id
      INTO v_user_session_id
      FROM ut_user_session uus
      WHERE uus.fki_user_package_id = v_user_package_id
            AND uus.bt_active = 1;


      IF (v_user_session_id IS NULL)
      THEN

        INSERT INTO ut_user_session (fki_user_package_id)
        VALUES (v_user_package_id);

        SELECT LAST_INSERT_ID()
        INTO v_user_session_id;

        UPDATE ut_user_package uup
        SET uup.i_devices_connected = 0
        WHERE uup.pki_user_package_id = v_user_package_id;

      END IF;


      SELECT
        uus.pki_user_session_id,
        uus.fki_user_package_id,
        uus.sz_user_session_uuid,
        uus.bt_active,
        uus.dt_created,
        uus.dt_disposed
      FROM ut_user_session uus
      WHERE uus.pki_user_session_id = v_user_session_id;
    END IF;
  END;
$$ DELIMITER ;

