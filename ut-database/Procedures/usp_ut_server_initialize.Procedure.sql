DELIMITER $$

CREATE PROCEDURE usp_ut_server_initialize(IN p_server_code VARCHAR(255))
  BEGIN

    DECLARE v_server_id INT;
    DECLARE v_max_rows INT DEFAULT 0;
    DECLARE v_current_row INT DEFAULT 0;
    DECLARE v_current_uuid varchar(128);

    
    SELECT pki_server_id
    INTO v_server_id
    FROM ut_server
    WHERE UPPER(sz_server_code) = UPPER(p_server_code);

    IF (v_server_id IS NOT NULL)
    THEN

      CREATE TEMPORARY TABLE IF NOT EXISTS tmp_server_sessions AS (
        SELECT sz_user_server_session_uuid
        FROM ut_user_server_session
        INNER JOIN ut_user_session ON fki_user_session_id = pki_user_session_id
        WHERE fki_server_id = v_server_id AND ut_user_server_session.bt_active = 1
      );

      SELECT COUNT(*) FROM tmp_server_sessions INTO v_max_rows;

      SET v_current_row=0;
      WHILE v_current_row < v_max_rows DO 
        
        SELECT tmp_server_sessions.sz_user_server_session_uuid INTO v_current_uuid FROM tmp_server_sessions LIMIT v_current_row, 1;
        
        CALL usp_dispose_user_server_session(v_current_uuid);
        
        SET v_current_row = v_current_row + 1;
      
      END WHILE;
      
      DROP TEMPORARY TABLE IF EXISTS tmp_server_sessions;
    END IF;

  END
$$
DELIMITER ;