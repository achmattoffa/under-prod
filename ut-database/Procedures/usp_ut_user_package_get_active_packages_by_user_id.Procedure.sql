DELIMITER $$

--
-- Definition for procedure usp_ut_user_package_get_active_packages_by_user_id
--
CREATE PROCEDURE usp_ut_user_package_get_active_packages_by_user_id(IN p_user_id INT)
  BEGIN

    SELECT
      uup.pki_user_package_id,
      uup.fki_user_id,
      uup.fki_package_id,
      uup.fki_package_duration_id,
      uup.i_data_used,
      uup.i_devices_connected,
      uup.bt_active,
      uup.bt_expires,
      uup.dt_created,
      uup.dt_expire
    FROM ut_user_package uup
    WHERE uup.fki_user_id = p_user_id
          AND uup.bt_active = 1;

  END
$$
DELIMITER ;