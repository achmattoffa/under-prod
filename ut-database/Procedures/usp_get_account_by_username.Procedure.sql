DELIMITER $$

CREATE PROCEDURE usp_get_account_by_username(IN p_username VARCHAR(20))
  BEGIN
    IF p_username IS NOT NULL
    THEN
      SELECT
        pki_account_id,
        sz_email,
        sz_username,
        bt_active,
        bt_banned,
        dt_created
      FROM gn_account;
    END IF;
  END
$$
DELIMITER ;

