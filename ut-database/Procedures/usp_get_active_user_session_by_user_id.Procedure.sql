DELIMITER $$

CREATE PROCEDURE usp_get_active_user_session_by_user_id(IN p_user_id INT)
  BEGIN
    IF (p_user_id > 0)
    THEN
      SELECT
        a.pki_user_session_id,
        a.sz_user_session_uuid,
        a.dt_created,
        b.i_data_used,
        c.i_limit_data,
        b.i_devices_connected,
        c.i_limit_connected_devices
      FROM ut_user_session a
        INNER JOIN ut_user_package b
          ON a.fki_user_package_id = b.pki_user_package_id
             AND b.fki_user_id = p_user_id
        INNER JOIN ut_package c
          ON b.fki_package_id = c.pki_package_id
      WHERE a.bt_active = 1;
    END IF;
  END
$$
DELIMITER ;

