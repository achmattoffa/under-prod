DELIMITER $$

--
-- Definition for procedure usp_gn_account_get_by_username_and_password
--
CREATE PROCEDURE usp_gn_account_get_by_username_and_password(IN p_sz_username VARCHAR(128),
                                                             IN p_sz_password VARCHAR(128))
  BEGIN

    SELECT
      ga.pki_account_id,
      ga.sz_email,
      ga.sz_username,
      ga.sz_salt,
      ga.bt_active,
      ga.bt_banned,
      ga.dt_created
    FROM gn_account ga
    WHERE upper(ga.sz_username) = upper(p_sz_username)
          AND ga.sz_password = p_sz_password;

  END
$$
DELIMITER ;