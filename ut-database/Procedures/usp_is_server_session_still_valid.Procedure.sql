DELIMITER $$

CREATE PROCEDURE usp_is_server_session_still_valid(IN p_user_server_session_uuid VARCHAR(40))
  BEGIN
    SELECT 1
    FROM ut_user_server_session
    WHERE sz_user_server_session_uuid = p_user_server_session_uuid
          AND bt_active = 1;
  END
$$
DELIMITER ;

