DELIMITER $$

--
-- Definition for procedure usp_ut_server_get_by_region_id
--
CREATE PROCEDURE usp_ut_user_session_validate_session(IN p_user_server_session_id INT)
  BEGIN
    DECLARE v_user_session_id INT;
    DECLARE v_user_package_id INT;


    SELECT uuss.fki_user_session_id
    INTO v_user_session_id
    FROM ut_user_server_session uuss
    WHERE uuss.pki_user_server_session_id = p_user_server_session_id;

    SELECT uus.fki_user_package_id
    INTO v_user_package_id
    FROM ut_user_session uus
    WHERE uus.pki_user_session_id = v_user_session_id;

    IF EXISTS(SELECT 1
              FROM ut_user_package uup INNER JOIN ut_package up ON uup.fki_package_id = up.pki_package_id
              WHERE (uup.i_data_used >= up.i_limit_data) AND uup.pki_user_package_id = v_user_package_id)
    THEN

      UPDATE ut_user_package SET bt_active = 0, i_devices_connected = 0 WHERE pki_user_package_id = v_user_package_id;
      SELECT 1 AS 'i_status'; -- CAP REACHED
    ELSEIF EXISTS(SELECT 1
                  FROM ut_user_package uup
                  WHERE (uup.dt_expire <= CURRENT_TIMESTAMP()) AND uup.pki_user_package_id = v_user_package_id)
      THEN
        UPDATE ut_user_package SET bt_active = 0, i_devices_connected = 0 WHERE pki_user_package_id = v_user_package_id;
        SELECT 2 AS 'i_status'; -- PACKAGE EXPIRED
    ELSEIF EXISTS(SELECT 1
                  FROM ut_user_package uup INNER JOIN gn_account ga ON uup.fki_user_id = ga.pki_account_id
                  WHERE ga.bt_banned = 1 AND uup.pki_user_package_id = v_user_package_id)
      THEN
        UPDATE ut_user_package SET bt_active = 0, i_devices_connected = 0 WHERE pki_user_package_id = v_user_package_id;
        SELECT 4 AS 'i_status'; -- ACCOUNT BANNED
    ELSE
      SELECT 0 AS 'i_status'; -- OKAY
    END IF;

  END
$$
DELIMITER ;