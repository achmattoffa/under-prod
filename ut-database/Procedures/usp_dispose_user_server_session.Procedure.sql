DELIMITER $$

--
-- Definition for procedure usp_dispose_user_server_session
--
CREATE PROCEDURE usp_dispose_user_server_session(IN p_user_session_server_uuid VARCHAR(50))
  BEGIN

    DECLARE v_user_package_id INT;
    DECLARE v_user_session_id INT;

    SELECT
      a.fki_user_session_id,
      c.pki_user_package_id
    INTO v_user_session_id, v_user_package_id
    FROM ut_user_server_session a
      INNER JOIN ut_user_session b
        ON a.fki_user_session_id = b.pki_user_session_id
      INNER JOIN ut_user_package c
        ON b.fki_user_package_id = c.pki_user_package_id
    WHERE a.sz_user_server_session_uuid = p_user_session_server_uuid;

    UPDATE ut_user_server_session
    SET bt_active = 0,
      dt_disposed = CURRENT_TIMESTAMP
    WHERE sz_user_server_session_uuid = p_user_session_server_uuid;

    -- if no more active user server sessions, set user session to non active
    IF NOT EXISTS(SELECT 1
                  FROM ut_user_server_session
                  WHERE fki_user_session_id = v_user_session_id AND bt_active = 1)
    THEN
      UPDATE ut_user_session
      SET bt_active = 0,
        dt_disposed = CURRENT_TIMESTAMP
      WHERE pki_user_session_id = v_user_session_id;

      UPDATE ut_user_package
      SET i_devices_connected = 0
      WHERE pki_user_package_id = v_user_package_id;
    ELSE
      UPDATE ut_user_package
      SET i_devices_connected =  IF (i_devices_connected > 0, i_devices_connected - 1, 0)
      WHERE pki_user_package_id = v_user_package_id;
    END IF;

  END
$$ DELIMITER ;

