DELIMITER $$

--
-- Definition for procedure usp_ut_user_server_session_create_by_uuid_and_server_code
--
CREATE PROCEDURE usp_ut_user_server_session_create_by_uuid_and_server_code(IN p_session_uuid VARCHAR(128),
                                                                           IN p_server_code  VARCHAR(128))
  BEGIN

    DECLARE v_user_package_id INT;
    DECLARE v_user_session_id INT;
    DECLARE v_user_server_session_id INT;
    DECLARE v_max_devices INT;
    DECLARE v_max_speed INT;
    DECLARE v_active_devices INT;
    DECLARE v_server_id INT;

    SELECT
      uus.fki_user_package_id,
      uus.pki_user_session_id
    INTO v_user_package_id, v_user_session_id
    FROM ut_user_session uus
    WHERE uus.sz_user_session_uuid = p_session_uuid
          AND uus.bt_active = 1;

    IF (v_user_session_id IS NOT NULL)
    THEN

      SELECT us.pki_server_id
      INTO v_server_id
      FROM ut_server us
      WHERE us.sz_server_code = p_server_code;

      SELECT
        i_devices_connected,
        i_limit_connected_devices,
        i_limit_speed
      INTO v_active_devices, v_max_devices, v_max_speed
      FROM ut_user_session
        INNER JOIN ut_user_package
          ON fki_user_package_id = pki_user_package_id
             AND pki_user_package_id = v_user_package_id
        INNER JOIN ut_package
          ON fki_package_id = pki_package_id
      WHERE pki_user_session_id = v_user_session_id;

      IF (v_max_devices > v_active_devices)
      THEN
        SET v_active_devices = v_active_devices + 1;

        INSERT INTO ut_user_server_session (fki_server_id, fki_user_session_id)
        VALUES (v_server_id, v_user_session_id);

        SELECT LAST_INSERT_ID()
        INTO v_user_server_session_id;

        UPDATE ut_user_package
        SET i_devices_connected = v_active_devices
        WHERE pki_user_package_id = v_user_package_id;


        SELECT
          'okay' AS 'result',
          fki_user_id,
          v_user_server_session_id,
          sz_user_server_session_uuid,
          i_limit_speed
        FROM ut_user_server_session
          INNER JOIN ut_user_session
            ON fki_user_session_id = pki_user_session_id
          INNER JOIN ut_user_package
            ON fki_user_package_id = pki_user_package_id
          INNER JOIN ut_package
            ON fki_package_id = pki_package_id
        WHERE pki_user_server_session_id = v_user_server_session_id;
      ELSE
        SELECT
          'error'                   AS 'result',
          'connected devices limit' AS 'detail';
      END IF;
    ELSE
      SELECT
        'error'                  AS 'result',
        'no active user session' AS 'detail';
    END IF;
  END
$$ DELIMITER ;

