INSERT HIGH_PRIORITY INTO ut_package (pki_package_id, sz_package_code, i_limit_speed, sz_package_name, i_limit_connected_devices, i_limit_data, fl_price_monthly, dt_created)
  VALUES (1, 'FF1', 50, 'FREE FLEX 1', 1, 50000, 0, CURRENT_TIMESTAMP())
ON DUPLICATE KEY UPDATE
sz_package_code = 'FF1',
i_limit_speed = 50,
sz_package_name = 'FREE FLEX 1',
i_limit_connected_devices = 1,
i_limit_data = 50000, -- 100 MEGS A DAY
fl_price_monthly = 0,
dt_created = CURRENT_TIMESTAMP();