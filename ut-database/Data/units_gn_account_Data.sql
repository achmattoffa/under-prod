-- Non active user (2, userna, password)
INSERT INTO gn_account (pki_account_id, sz_email, sz_username, sz_password, sz_salt, bt_active, bt_banned, dt_created)
  VALUES (2, 'userna@under.com', 'userna', '62a1c768d3e58657f842a01dec8247426591ed1a8df541d286a1dd5a2a69b85a0378db4383a2d0', '62a1c768d3e58657f842a01dec8247426591ed1a', 0, 0, CURRENT_TIMESTAMP())
ON DUPLICATE KEY UPDATE
sz_email = 'userna@under.com',
sz_username = 'userna',
sz_password = '62a1c768d3e58657f842a01dec8247426591ed1a8df541d286a1dd5a2a69b85a0378db4383a2d0',
sz_salt = '62a1c768d3e58657f842a01dec8247426591ed1a',
bt_active = 0,
bt_banned = 0,
dt_created = CURRENT_TIMESTAMP();

-- Banned user (3, userba, password)
INSERT INTO gn_account (pki_account_id, sz_email, sz_username, sz_password, sz_salt, bt_active, bt_banned, dt_created)
  VALUES (3, 'userba@under.com', 'userba', '62a1c768d3e58657f842a01dec8247426591ed1a8df541d286a1dd5a2a69b85a0378db4383a2d0', '62a1c768d3e58657f842a01dec8247426591ed1a', 1, 1, CURRENT_TIMESTAMP())
ON DUPLICATE KEY UPDATE
sz_email = 'userba@under.com',
sz_username = 'userba',
sz_password = '62a1c768d3e58657f842a01dec8247426591ed1a8df541d286a1dd5a2a69b85a0378db4383a2d0',
sz_salt = '62a1c768d3e58657f842a01dec8247426591ed1a',
bt_active = 1,
bt_banned = 1,
dt_created = CURRENT_TIMESTAMP();

-- Banned + non active user (4, userbana, password)
INSERT INTO gn_account (pki_account_id, sz_email, sz_username, sz_password, sz_salt, bt_active, bt_banned, dt_created)
  VALUES (4, 'userbana@under.com', 'userbana', '62a1c768d3e58657f842a01dec8247426591ed1a8df541d286a1dd5a2a69b85a0378db4383a2d0', '62a1c768d3e58657f842a01dec8247426591ed1a', 0, 1, CURRENT_TIMESTAMP())
ON DUPLICATE KEY UPDATE
sz_email = 'userbana@under.com',
sz_username = 'userbana',
sz_password = '62a1c768d3e58657f842a01dec8247426591ed1a8df541d286a1dd5a2a69b85a0378db4383a2d0',
sz_salt = '62a1c768d3e58657f842a01dec8247426591ed1a',
bt_active = 0,
bt_banned = 1,
dt_created = CURRENT_TIMESTAMP();