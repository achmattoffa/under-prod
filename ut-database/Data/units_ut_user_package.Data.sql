-- Light Flex package for 1 month to account ('user')
INSERT INTO ut_user_package (pki_user_package_id, fki_user_id, fki_package_id, fki_package_duration_id, i_data_used, i_devices_connected, bt_active, bt_expires, dt_created)
  VALUES(1, 1, 1, 4, 0, 0, 1, 1, CURRENT_TIMESTAMP())
ON DUPLICATE KEY UPDATE
  fki_user_id = 1,
  fki_package_id = 2,
  fki_package_duration_id = 4,
  i_data_used = 0,
  i_devices_connected = 0, 
  bt_active = 1, 
  bt_expires = 1, 
  dt_created = CURRENT_TIMESTAMP();