INSERT INTO ut_package_duration (pki_package_duration_id, sz_package_duration_code, sz_package_duration_description)
  VALUES (1, 'NONE', 'NONE')
  ON DUPLICATE KEY UPDATE
  sz_package_duration_code = 'NONE',
  sz_package_duration_description = 'NONE';

INSERT INTO ut_package_duration (pki_package_duration_id, sz_package_duration_code, sz_package_duration_description)
  VALUES (2, 'DAY', 'DAY')
  ON DUPLICATE KEY UPDATE
  sz_package_duration_code = 'DAY',
  sz_package_duration_description = 'DAY';

INSERT INTO ut_package_duration (pki_package_duration_id, sz_package_duration_code, sz_package_duration_description)
  VALUES (3, 'WEEK', 'WEEK')
  ON DUPLICATE KEY UPDATE
  sz_package_duration_code = 'WEEK',
  sz_package_duration_description = 'WEEK';

INSERT INTO ut_package_duration (pki_package_duration_id, sz_package_duration_code, sz_package_duration_description)
  VALUES (4, 'MONTH', 'MONTH')
  ON DUPLICATE KEY UPDATE
  sz_package_duration_code = 'MONTH',
  sz_package_duration_description = 'MONTH';

INSERT INTO ut_package_duration (pki_package_duration_id, sz_package_duration_code, sz_package_duration_description)
  VALUES (5, 'YEAR', 'YEAR')
  ON DUPLICATE KEY UPDATE
  sz_package_duration_code = 'YEAR',
  sz_package_duration_description = 'YEAR';