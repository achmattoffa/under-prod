INSERT INTO gn_protocol (pki_protocol_id, sz_protocol_name)
  VALUES (1, 'TCP')
ON DUPLICATE KEY UPDATE
  sz_protocol_name = 'TCP';

INSERT INTO gn_protocol (pki_protocol_id, sz_protocol_name)
  VALUES (2, 'UDP')
ON DUPLICATE KEY UPDATE
  sz_protocol_name = 'UDP';