/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.webapi.api.endpoints

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.api.JsonApiBase
import com.bitato.ut.webapi.data.services.TunnelLookupService
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import spark.Route
import spark.Spark
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class TunnelServers : ApiEndpoint, JsonApiBase() {

    private val tunnelLookupService = Injekt.get<TunnelLookupService>()

    override fun configure() {

        Spark.path("/servers") {
            Spark.get("/lookup", Route { request, response ->

                val region = request.queryParams("region") ?: "ANY"
                val protocol = request.queryParams("protocol") ?: "ANY"
                val port = (request.queryParams("port") ?: "-1").toInt()

                val servers = tunnelLookupService.getTunnelsBySpecification(region, port, protocol)
                val jsonServers = jsonArray()

                servers.groupBy {
                    it["pki_server_id"] as Int
                }.forEach { _, sere ->

                    val serverRecord = sere.first()

                    jsonServers.add(jsonObject(
                      "code" to serverRecord["sz_server_code"],
                      "name" to serverRecord["sz_server_name"],
                      "region" to serverRecord["sz_country"],
                      "load" to serverRecord["i_load"],
                      "address" to jsonObject
                      (
                        "host" to serverRecord["sz_hostname"],
                        "ipv4" to serverRecord["sz_ipv4_address"],
                        "ipv6" to serverRecord["sz_ipv6_address"]
                      ),
                      "ports" to jsonArray(sere.map
                      {
                          jsonObject(
                            "port" to it["i_port"],
                            "protocol" to it["sz_protocol_name"]
                          )
                      })
                    ))
                }

                jsonObject("servers" to jsonServers)

            }, jsonResponseTransformer)
        }
    }
}

