/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.webapi.data.services

import com.bitato.ut.persist.utility.MemoryTable

interface TunnelLookupService {

    fun getTunnelsBySpecification(region: String = "", port: Int = -1, protocol: String = ""): MemoryTable

}