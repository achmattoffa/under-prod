/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.webapi.data.persistence.impl

import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.MemoryTable
import com.bitato.ut.persist.utility.PersistenceBase
import com.bitato.ut.webapi.data.persistence.TunnelServerDb

class UtTunnelServerDb: TunnelServerDb, PersistenceBase() {

    override fun getAvailableTunnelServersBySpec(region: String, port: Int, protocol: String): MemoryTable {
        return procQuery {
            procedure("usp_ut_server_get_by_region_port_protocol")
            parameters(region to DbParameterType.IN, port to DbParameterType.IN, protocol to DbParameterType.IN)
        }
    }

}