/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.webapi.data.persistence

import com.bitato.ut.persist.utility.MemoryTable

interface TunnelServerDb {

    fun getAvailableTunnelServersBySpec(region: String, port: Int, protocol: String): MemoryTable
}