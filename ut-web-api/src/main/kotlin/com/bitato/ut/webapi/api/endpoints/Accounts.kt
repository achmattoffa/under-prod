/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.webapi.api.endpoints

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.api.JsonApiBase
import com.bitato.ut.persist.utility.MemoryTable
import com.bitato.ut.persist.utility.PersistenceBase
import com.bitato.ut.security.services.PasswordEncryptionService
import com.bitato.ut.security.services.SaltGenerator
import spark.Route
import spark.Spark
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class Accounts : ApiEndpoint, JsonApiBase() {




    private fun validateAccuntCreationInputs(email: String, user: String, pass: String): Boolean {

        if (user.isBlank()) {
            return false
        }

        if (email.isBlank()) {
            return false
        }

        if (pass.isBlank()) {
            return false
        }

        return true
    }

    override fun configure() {

        val db =  object : PersistenceBase() {

            fun deleteUser(user: String) {
                sqlUpdate {
                    sql("DELETE FROM gn_account WHERE `sz_username` = '$user'")
                }
            }

            fun insertUser(email: String, user: String, password: String, salt: String) {
                sqlUpdate {
                    sql("INSERT gn_account(sz_email, sz_username, sz_password, sz_salt) VALUES ('$email', '$user', '$password', '$salt')")
                }
            }

            fun getUser(user: String, password: String): MemoryTable {
                return sqlQuery<MemoryTable> {
                    sql("SELECT * FROM gn_account WHERE sz_username = '${user}' AND sz_password = '${password}'")
                }
            }
        }

        spark.Spark.path("/accounts") {

            Spark.get("/create", Route { request, response ->

                val user = request.queryParams("username") ?: ""
                val pass = request.queryParams("password") ?: ""
                val email = request.queryParams("email") ?: ""

                if (!validateAccuntCreationInputs(user, pass, email)) {
                    "Invalid user inputs"
                    response.status(500)
                } else {

                    val salt = Injekt.get<SaltGenerator>().generateSalt()
                    val password = Injekt.get<PasswordEncryptionService>().encryptPassword(pass, salt)

                    try {
                        db.insertUser(email, user, password, salt)
                        response.status(200)
                        "Okay"
                    } catch (ex: Exception) {
                        response.status(500)
                        "Error"
                    }
                }

            }, jsonResponseTransformer)


            Spark.get("/delete", Route { request, response ->

                val user = request.queryParams("username") ?: throw Exception("Bad username")

                    try {
                        db.deleteUser(user)
                        response.status(200)
                        "Okay"
                    } catch (ex: Exception) {
                        response.status(500)
                        "Error"
                    }
            })
        }
    }
}