/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.webapi.data.services.impl

import com.bitato.ut.persist.utility.MemoryTable
import com.bitato.ut.webapi.data.persistence.TunnelServerDb
import com.bitato.ut.webapi.data.services.TunnelLookupService
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtTunnelLookupService : TunnelLookupService {

    private val tunnelServerDb: TunnelServerDb = Injekt.get()

    override fun getTunnelsBySpecification(region: String, port: Int, protocol: String): MemoryTable {
        return tunnelServerDb.getAvailableTunnelServersBySpec(region, port, protocol)
    }
}