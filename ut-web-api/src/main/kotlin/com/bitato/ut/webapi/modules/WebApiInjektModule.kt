/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.webapi.modules

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.api.model.net.sld.SldMessageStreamFactory
import com.bitato.ut.api.model.net.sld.UtSldMessageStreamFactory
import com.bitato.ut.webapi.api.endpoints.Accounts
import com.bitato.ut.webapi.api.endpoints.TunnelServers
import com.bitato.ut.webapi.data.persistence.TunnelServerDb
import com.bitato.ut.webapi.data.persistence.impl.UtTunnelServerDb
import com.bitato.ut.webapi.data.services.TunnelLookupService
import com.bitato.ut.webapi.data.services.impl.UtTunnelLookupService
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory

object WebApiInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<TunnelServerDb> { UtTunnelServerDb() }
        addFactory<TunnelLookupService> { UtTunnelLookupService() }
        addFactory<SldMessageStreamFactory> { UtSldMessageStreamFactory }
        addFactory<List<ApiEndpoint>> {
            listOf(TunnelServers(), Accounts())
        }
    }
}

