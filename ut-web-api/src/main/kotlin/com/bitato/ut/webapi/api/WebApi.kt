/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.webapi.api

import com.bitato.ut.api.ApiEndpoint
import com.bitato.ut.webapi.configuration.ServerConfig
import org.aeonbits.owner.ConfigFactory
import spark.Spark
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get


class WebApi {

    private val serverBindings: List<ApiEndpoint> = Injekt.get()

    fun initServer() {

        val serverConfig = ConfigFactory.create(ServerConfig::class.java)

        Spark.port(serverConfig.bindPort())
        Spark.threadPool(serverConfig.maxConnections())

        applyApiBindings()
    }

    private fun applyApiBindings() {

        serverBindings.forEach {
            it.configure()
        }
    }
}