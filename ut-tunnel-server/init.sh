#!/bin/sh

### BEGIN INIT INFO
# Provides:          undertunnel
# Required-Start:    $network $remote_fs $syslog
# Required-Stop:     $network $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: undertunnel
# Description:       undertunnel server daemon
### END INIT INFO

jar_exec=undertunnel.jar
jar_dir=/etc/under/tunnel

check_status() {

    touch ${jar_dir}/pidProcess
    cat /dev/null > ${jar_dir}/pidProcess
    ps -e -o pid,cmd > ${jar_dir}/pidProcess
    cmb_buffer=`cat ${jar_dir}/pidProcess | grep ${jar_exec}`
    cmb_buffer=`echo ${cmb_buffer} | cut -d' ' -f 1`

    echo ${cmb_buffer}
    if [ ${cmb_buffer} ] ; then
    return ${cmb_buffer}
    fi

    return 0
}

start() {

    check_status
    pid=$?

    if [ ${pid} -ne 0 ] ; then
    echo "The application is already started"
    exit 1
    fi

    echo -n "Starting application: "
    cd ${jar_dir}
    java -jar ${jar_exec} >> logfile 2>&1 &
    echo "OK"
}

stop() {

    check_status

    pid=$?

    if [ ${pid} -eq 0 ] ; then
    echo "Application is already stopped"
    exit 1
    fi
    
    echo -n "Stopping application: "
    kill -9 ${pid} &
    echo "OK"
}

status() {

    check_status

    if [ $? -ne 0 ] ; then
    echo "Application is started"
    else
    echo "Application is stopped"
    fi

}

case "$1" in
start)
start
;;
stop)
stop
;;
status)
status
;;
restart|reload)
stop
start
;;
*)
echo "Usage: $0 {start|stop|restart|reload|status}"
exit 1
esac

exit 0