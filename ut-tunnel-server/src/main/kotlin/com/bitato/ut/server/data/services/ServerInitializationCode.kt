/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.server.data.services

enum class ServerInitializationCode {
    SUCCESS, ERROR
}