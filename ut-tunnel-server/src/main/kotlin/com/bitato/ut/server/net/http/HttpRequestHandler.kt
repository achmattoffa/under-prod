/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.net.http

import HttpCommon
import com.bitato.ut.api.io.streamers.StreamerFactory
import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.HttpResponse
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.server.net.AbstractSessionisedTunnelRequestHandler
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket
import javax.net.SocketFactory


class HttpRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper)
: AbstractSessionisedTunnelRequestHandler(incoming, requestWrapper) {

    override fun run() {

        io {
            log info "HTTP request handler invoked"
            log info "Reading HTTP request from client"

            val httpRequest = readRequest {
                Injekt.get<HttpStreamFactory>().createHttpRequest(it)
            }

            log info "HTTP request: $httpRequest"

            if (httpRequest.isTunnelRequest()) {
                log info "Tunnel request detected. Handling HTTP Request tunnel"
                handleTunnelRequest(httpRequest)
            } else {
                log info "Handling HTTP Request request"
                handleHttpRequest(httpRequest)
            }
        }

        io { closeClient() }
    }

    private fun handleHttpRequest(httpRequest: HttpRequest) {

        val host = httpRequest.getHost()
        val port = httpRequest.port

        log info "Establishing a connection to $host, on port $port"
        val remote = Injekt.get<SocketFactory>().createSocket(host, port)
        log info "Connection to remote target established"

        io {
            log info "Writing HTTP request to remote target"
            remote.outputStream < httpRequest

            if (httpRequest.hasMessageBody()) {
                log info "HTTP request contains message body"
                val streamer = streamerFactory.create(clientIn, remote.outputStream, StreamerFactory.StreamerType.CONTENT_LENGTH)

                log info "Streaming message body to remote target"
                streamer.stream(httpRequest.getContentLength())
            }

            log info "Reading HTTP response from remote target"
            val httpResponse = Injekt.get<HttpStreamFactory>().createHttpResponse(remote.inputStream)
            log info "HTTP response: $httpResponse"

            log info "Writing response headers to client"
            clientOut < wrapServerPayload(httpResponse.parse())

            if (httpResponse.hasMessageBody()) {
                transferHttpContent(httpResponse, remote)
            } else {
                log info "This HTTP response has no associated message body"
            }
        }

        io {
            remote.close()
        }
    }

    private fun transferHttpContent(httpResponse: HttpResponse, remote: Socket) {
        log info "Writing response body to client"
        if (httpResponse.hasContentLength()) {
            log info "Content-Length HTTP header found"
            val streamer = streamerFactory.create(remote.inputStream, clientOut, StreamerFactory.StreamerType.CONTENT_LENGTH)

            log info "Streaming ${httpResponse.getContentLength()} bytes to client"
            streamer.stream(httpResponse.getContentLength())
        } else if (httpResponse.hasChunkedBody()) {
            log info "Chunked encoding detected"
            val streamer = streamerFactory.create(remote.inputStream, clientOut, StreamerFactory.StreamerType.CHUNK_ENCODING)

            log info "Streaming response data from remote target to client"
            streamer.stream()
        } else {
            val streamer = streamerFactory.create(remote.inputStream, clientOut, StreamerFactory.StreamerType.INDEFINITE)

            log info "Streaming response data from remote target to client"
            streamer.stream()
        }
    }

    private fun handleTunnelRequest(httpRequest: HttpRequest) {

        val host = httpRequest.getHost()
        val port = httpRequest.port

        log info "Establishing a connection to $host on port $port"
        val remote = Injekt.get<SocketFactory>().createSocket(host, port)
        log info "Connection established"

        io {

            log info "Sending intermediary HTTP response to client"
            clientOut < wrapServerPayload(HttpCommon.Responses.OKAY.parse())

            log info "Invoking bidirectional stream between client and target"
            streamIndefinitely(remote.inputStream, remote.outputStream)
        }

        io {
            remote.close()
        }
    }
}