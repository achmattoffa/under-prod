/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.exceptions

class UtAccountAuthenticationException : Exception()