/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.net

import com.bitato.ut.api.io.counter.InputStreamCounter
import com.bitato.ut.api.io.counter.OutputStreamCounter
import com.bitato.ut.api.io.throttle.ThrottledInputStream
import com.bitato.ut.api.io.throttle.ThrottledOutputStream
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.thread
import com.bitato.ut.api.util.waitWhile
import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.exceptions.UtSessionAuthenticationException
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket


abstract class AbstractSessionisedTunnelRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper)
    : AbstractTunnelRequestHandler(incoming, requestWrapper) {

    init {
        val currentSession = userServerSessionHandler[requestWrapper.uuid]

        if (!currentSession.isPresent) {
            throw UtSessionAuthenticationException("A session with the UUID specified does not exist")
        }

        //applyDataThrottle(currentSession)
        if (currentSession.isPresent) {
            attachDataCounter(currentSession.get())
            registerSessionStreams(currentSession.get())
        } else {
            TODO()
        }
    }

    private fun applyDataThrottle(session: Session) {
        clientIn = ThrottledInputStream(clientIn, session.inputStreamThrottleManager)
        clientOut = ThrottledOutputStream(clientOut, session.outputStreamThrottleController)
    }

    private fun attachDataCounter(session: Session) {
        clientIn = InputStreamCounter(clientIn, session.dataConsumed)
        clientOut = OutputStreamCounter(clientOut, session.dataConsumed)
    }

    private fun registerSessionStreams(session: Session) {
        session.streams.add(clientIn)
        session.streams.add(clientOut)
    }

    /**
     * Handles the relaying of data in cases where there is no certainty as to which way the data
     * will be travelling. Data can be sent from any end-point. The stream ends when one of the two
     * end-points closes their underlying socket.
     *
     * @param remoteIn      The <code>InputStream</code> associated with the remote server
     * @param remoteOut     The <code>OutputStream</code> associated with the remote server
     *
     * @throws java.io.IOException
     * @throws InterruptedException
     */
    protected fun streamIndefinitely(remoteIn: InputStream, remoteOut: OutputStream) {
        //TODO

//        val wrapperProvider = Injekt.get<WrapperProvider>()
//        val relayLock = AtomicBoolean()

        log info "Starting streams"
        val threads = listOf(
          //Client to Server
          thread {
              io {
                  remoteOut < clientIn
              }
          },

          //Server to Client
          thread {
                io {
                    clientOut < remoteIn
                }
          })

        waitWhile { threads.all { th -> th.isAlive } }
        log info "Bidirectional data relay complete"
    }

    protected fun closeClient() {
        val session = userServerSessionHandler[requestWrapper.uuid]

        if (session.isPresent) {
            session.get().run {
                disposeStream(clientIn)
                disposeStream(clientOut)
            }
        } else {
            throw UtSessionAuthenticationException("A session with the UUID specified does not exist")
        }
    }
}