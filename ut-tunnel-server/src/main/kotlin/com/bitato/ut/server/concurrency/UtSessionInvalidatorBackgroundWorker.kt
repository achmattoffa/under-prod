/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.concurrency

import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.api.model.net.internal.InternalSessionResponse
import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.services.ServerSessionService
import com.bitato.ut.server.data.services.SessionDisposalService
import com.github.salomonbrys.kotson.jsonObject
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtSessionInvalidatorBackgroundWorker : AbstractScheduledBackgroundWorker(10) {

    private val sessions: Map<String, Session> = Injekt.get()
    private val serverSessionService: ServerSessionService = Injekt.get()
    private val sessionDisposalService: SessionDisposalService = Injekt.get()

    override fun run() {
        sessions.values.forEach { validateSession(it) }
    }

    private fun validateSession(session: Session) {
        // 15 secs to reconnect with the current server session uuid
        if (session.isDisconnected && System.currentTimeMillis() > session.disconnectTimeMillis + 15000) {
            sessionDisposalService.disposeSession(session)
        } else if (!session.isDisposePending || !session.isDisposed) {

            val sessionState = serverSessionService.validateSession(session.serverSessionID)

            if (sessionState.toByte() != InternalSessionResponse.SessionStatusCodes.KEEP_ALIVE) {

                val sessionStatus = when (sessionState.toByte()) {
                    InternalSessionResponse.SessionStatusCodes.ACCOUNT_BANNED -> "account banned"
                    InternalSessionResponse.SessionStatusCodes.CAP_REACHED -> "cap reached"
                    InternalSessionResponse.SessionStatusCodes.PACKAGE_EXPIRED -> "package expired"
                    InternalSessionResponse.SessionStatusCodes.SERVER_ERROR -> "server error"
                    InternalSessionResponse.SessionStatusCodes.UPGRADE_REQUESTED -> "upgrade requested"
                    else -> "okay"
                }

                session.messageQueue.add(jsonObject(
                  "type" to InternalSessionResponse.MessageTypes.SESSION_STATUS,
                  "code" to sessionState.toByte(),
                  "detail" to sessionStatus
                ))

                sessionDisposalService.disposeSession(session)
            }
        } else {
            sessionDisposalService.disposeSession(session)
        }
    }
}