/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.dto

import com.bitato.ut.api.io.throttle.InputStreamThrottleController
import com.bitato.ut.api.io.throttle.OutputStreamThrottleController
import com.bitato.ut.api.model.common.AtomicCounter
import com.bitato.ut.api.model.common.Counter
import com.bitato.ut.api.util.io
import com.google.gson.JsonObject
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.io.Closeable
import java.net.Socket
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicLong

data class Session(val userID: Int,
                   val serverSessionID: Int,
                   val serverSessionUuid: String,
                   var limitThrottle: Int,
                   var dataConsumed: Counter = AtomicCounter(AtomicLong(0)),
                   var isDisconnected: Boolean = false,
                   var isDisposePending: Boolean = false,
                   var isDisposed: Boolean = false,
                   var disconnectTimeMillis: Long = 0,
                   var lastUpdatedTimeMillis: Long = 0,
                   val messageQueue: Queue<JsonObject> = ConcurrentLinkedQueue()) {

    lateinit var internalControl: Socket

    val inputStreamThrottleManager: InputStreamThrottleController = Injekt.get(limitThrottle * 1024)
    val outputStreamThrottleController: OutputStreamThrottleController = Injekt.get(limitThrottle * 1024)

    val streams: MutableList<Closeable> = Collections.synchronizedList(ArrayList())

    fun disposeStream(closeable: Closeable) {
        streams.remove(closeable)
        io { closeable.close() }
    }
}