/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.configuration

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.sbase.configuration.NetworkProtocolConverter
import com.bitato.ut.sbase.configuration.ServerBaseConfig
import org.aeonbits.owner.Config

@Config.Sources(
  "file:server.conf",
  "classpath:server.conf")
@Config.LoadPolicy(Config.LoadType.MERGE)
interface TunnelServerConfig : Config, ServerBaseConfig {

    @Config.Key("server.bindTo")
    @Config.DefaultValue("localhost")
    override fun bindTo(): String

    @Config.Key("server.port")
    @Config.DefaultValue("80")
    override fun port(): Int

    @Config.Key("server.protocol")
    @Config.DefaultValue("tcp")
    @Config.ConverterClass(NetworkProtocolConverter::class)
    override fun protocol(): NetworkProtocol

    @Config.Key("server.code")
    fun serverCode(): String
}



