/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.server.data.services.impl

import com.bitato.ut.server.configuration.TunnelServerConfig
import com.bitato.ut.server.data.persistence.ServerHouseKeeperDb
import com.bitato.ut.server.data.services.ServerHouseKeeper
import com.bitato.ut.server.data.services.ServerInitializationCode
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtServerHouseKeeper : ServerHouseKeeper {

    private val tunnelServerConfig: TunnelServerConfig = ConfigFactory.create(TunnelServerConfig::class.java)
    private val houseKeeperDb: ServerHouseKeeperDb = Injekt.get()

    override fun initializeServer(initializationCallback: (ServerInitializationCode) -> Unit) {

        val serverInitializationCode =
        try {
            houseKeeperDb.initializeServer(tunnelServerConfig.serverCode())
            ServerInitializationCode.SUCCESS
        } catch (ex: Exception) {
            ServerInitializationCode.ERROR
        }

        initializationCallback.invoke(serverInitializationCode)
    }
}