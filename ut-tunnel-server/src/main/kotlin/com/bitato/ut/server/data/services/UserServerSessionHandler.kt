/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services

import com.bitato.ut.server.data.dto.Session
import java.util.*

interface UserServerSessionHandler {

    fun register(session: Session)

    fun unregister(serverSessionUuid: String)

    operator fun get(serverSessionUuid: String): Optional<Session>

}