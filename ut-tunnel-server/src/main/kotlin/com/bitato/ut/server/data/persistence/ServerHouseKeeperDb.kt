/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.server.data.persistence

interface ServerHouseKeeperDb {
    fun initializeServer(serverCode: String)
}