/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.server.data.persistence

import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.PersistenceBase

class UtServerHouseKeeperDb: ServerHouseKeeperDb, PersistenceBase() {

    override fun initializeServer(serverCode: String) {
        procUpdate {
            procedure("usp_ut_server_initialize")
            parameters(serverCode to DbParameterType.IN)
        }
    }
}