/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.net

import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.sbase.net.handler.AbstractInjectableRequestHandler
import com.bitato.ut.server.data.services.UserServerSessionHandler
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

abstract class AbstractTunnelRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper)
: AbstractInjectableRequestHandler(incoming, requestWrapper) {

    val userServerSessionHandler: UserServerSessionHandler = Injekt.get()

}