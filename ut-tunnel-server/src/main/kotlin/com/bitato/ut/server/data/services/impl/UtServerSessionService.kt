/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services.impl

import com.bitato.ut.server.configuration.TunnelServerConfig
import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.persistence.UserSessionDb
import com.bitato.ut.server.data.services.ServerSessionService
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.util.*

class UtServerSessionService : ServerSessionService {

    private val userSessionDb: UserSessionDb = Injekt.get()
    private val tunnelServerConfig: TunnelServerConfig = ConfigFactory.create(TunnelServerConfig::class.java)

    override fun createServerSessionByUuid(sessionUuid: String): Optional<Session> {

        val dataRow = userSessionDb.createServerSessionByUserSessionUuidAndServerCode(sessionUuid, tunnelServerConfig.serverCode())

        return if (dataRow.isNotEmpty()) {
            if (dataRow["result"] == "okay") {
                val session = Session(dataRow["fki_user_id"] as Int, dataRow["v_user_server_session_id"] as Int, dataRow["sz_user_server_session_uuid"] as String, dataRow["i_limit_speed"] as Int)
                Optional.of(session)
            } else {
                Optional.empty()
            }

        } else {
            Optional.empty()
        }
    }

    override fun disposeSession(uuid: String) {
        userSessionDb.disposeSession(uuid)
    }

    override fun updateDataUsage(serverSessionID: Int, dataConsumed: Long) {
        userSessionDb.updateDataUsage(serverSessionID, dataConsumed)
    }

    override fun validateSession(serverSessionID: Int): Int {
        val responseCode = userSessionDb.validateSession(serverSessionID)
        return (responseCode["i_status"] as Long).toInt()
    }
}
