/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services.impl

import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.services.UserServerSessionHandler
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class UtUserServerSessionHandler : UserServerSessionHandler {

    companion object {
        val sessions = ConcurrentHashMap<String, Session>()
    }

    @Synchronized
    override fun get(serverSessionUuid: String): Optional<Session> {
        return if (sessions.containsKey(serverSessionUuid)) Optional.of(sessions[serverSessionUuid]!!) else Optional.empty()
    }

    @Synchronized
    override fun register(session: Session) {
        sessions.put(session.serverSessionUuid, session)
    }

    @Synchronized
    override fun unregister(serverSessionUuid: String) {
        sessions.remove(serverSessionUuid)
    }
}