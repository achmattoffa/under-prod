/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.server.configuration

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.sbase.configuration.NetworkProtocolConverter
import org.aeonbits.owner.Config

@Config.Sources(
  "file:server.conf",
  "classpath:server.conf")
@Config.LoadPolicy(Config.LoadType.MERGE)
interface BounceServerConfig : Config {

    @Config.Key("bouncer.enabled")
    @Config.DefaultValue("false")
    fun isEnabled(): Boolean

    @Config.Key("bouncer.bindTo")
    @Config.DefaultValue("localhost")
    fun bindTo(): String

    @Config.Key("bouncer.port")
    @Config.DefaultValue("80")
    fun port(): Int

    @Config.Key("bouncer.protocol")
    @Config.DefaultValue("tcp")
    @Config.ConverterClass(NetworkProtocolConverter::class)
    fun protocol(): NetworkProtocol
}