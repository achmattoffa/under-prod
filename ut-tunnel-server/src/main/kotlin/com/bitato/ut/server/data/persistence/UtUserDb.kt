/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.persistence

import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.MemoryRow
import com.bitato.ut.persist.utility.PersistenceBase

class UtUserDb : UserDb, PersistenceBase() {

    override fun getUser(username: String): MemoryRow {
        return procQuery {
            procedure("usp_gn_account_get_user_by_username")
            parameters(username to DbParameterType.IN)
        }
    }


    override fun getAccountSessionInformationByServerSessionID(serverSessionID: Int): MemoryRow {
        return procQuery {
            procedure("usp_get_account_session_information")
            parameters(serverSessionID to DbParameterType.IN)
        }
    }

}