/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.server.net.socks

import com.bitato.ut.api.model.net.socks.socks4.Socks4Request
import com.bitato.ut.api.model.net.socks.socks4.Socks4RequestFactory
import com.bitato.ut.api.model.net.socks.socks4.Socks4Response
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.server.net.AbstractSessionisedTunnelRequestHandler
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket
import javax.net.SocketFactory

class Socks4RequestHandler(clientSock: Socket, requestWrapper: UtRequestWrapper)
: AbstractSessionisedTunnelRequestHandler(clientSock, requestWrapper) {


    override fun run() {

        io {
            val request = readRequest {
                Socks4RequestFactory.createRequestHeader(it)
            }

            when (request.cmd) {
                Socks4Request.Companion.CONNECT -> handleSocksConnect(request)
                Socks4Request.Companion.BIND -> invokeSocksBind(request)
            }
        }

        io { closeClient() }
    }

    private fun handleSocksConnect(request: Socks4Request) {

        val host = request.getAddress()
        val port = request.getPort()

        val remote = Injekt.get<SocketFactory>().createSocket(host, port)

        io {
            log info "Connection established with target machine"
            sendResponseToClient(Socks4Response.REQUEST_GRANTED)

            log info "Invoking bidirectional stream between client and target"
            streamIndefinitely(remote.inputStream, remote.outputStream)
        }

        io {
            remote.close()
        }
    }

    private fun sendResponseToClient(statusCode: Byte) {

        val response = Socks4Response()
        response.status = statusCode

        clientOut < wrapServerPayload(response.parse())
    }



    private fun invokeSocksBind(request: Socks4Request) {
        //TODO
        sendResponseToClient(Socks4Response.REQUEST_REJECTED)
    }
}

