/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.concurrency

import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.api.model.net.internal.InternalSessionResponse
import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.services.ServerSessionService
import com.github.salomonbrys.kotson.jsonObject
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtSessionUsageBackgroundWorker : AbstractScheduledBackgroundWorker(5) {

    private val sessions: Map<String, Session> = Injekt.get()
    private val serverSessionService: ServerSessionService = Injekt.get()

    override fun run() {
        val currentTimeMillis = System.currentTimeMillis()

        sessions.values
            .filter { !it.isDisposePending && (currentTimeMillis >= it.lastUpdatedTimeMillis + sleepPeriod) }
            .forEach { updateDataUsage(it) }
    }


    private fun updateDataUsage(session: Session) {

        val lastBytesConsumed = session.dataConsumed.value()
        session.dataConsumed.reset()

        if (lastBytesConsumed > 0) {
            serverSessionService.updateDataUsage(session.serverSessionID, lastBytesConsumed / 1024)
            session.lastUpdatedTimeMillis = System.currentTimeMillis()

            session.messageQueue.add(jsonObject(
              "type" to InternalSessionResponse.MessageTypes.SESSION_USAGE,
              "usage" to lastBytesConsumed / 1024
            ))
        }
    }
}