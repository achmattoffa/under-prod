/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.server.data.services

interface ServerHouseKeeper {
    fun initializeServer(initializationCallback: (ServerInitializationCode) -> Unit)
}