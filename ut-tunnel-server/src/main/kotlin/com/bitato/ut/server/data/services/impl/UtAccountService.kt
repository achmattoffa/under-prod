/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services.impl

import com.bitato.ut.log.util.LoggerBase
import com.bitato.ut.persist.utility.MemoryRow
import com.bitato.ut.server.data.persistence.UserDb
import com.bitato.ut.server.data.services.AccountService
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtAccountService : AccountService, LoggerBase() {

    private val userDb: UserDb = Injekt.get()

    override fun getAccountByUsername(username: String): MemoryRow {
        return userDb.getUser(username)
    }

    override fun getAccountSessionInformationByServerSessionID(serverSessionID: Int): MemoryRow {
        return userDb.getAccountSessionInformationByServerSessionID(serverSessionID)
    }
}