/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.persistence

import com.bitato.ut.persist.utility.MemoryRow

interface UserSessionDb {

    fun createServerSessionByUserSessionUuidAndServerCode(uuid: String, serverCode: String): MemoryRow

    fun disposeSession(uuid: String)

    fun updateDataUsage(serverSessionID: Int, dataConsumed: Long)

    fun validateSession(serverSessionID: Int): MemoryRow
}