/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.net

import com.bitato.ut.api.model.net.UtInvalidRequestException
import com.bitato.ut.api.model.net.wrappers.ClientRequestType.*
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.model.net.wrappers.WrapperProvider
import com.bitato.ut.sbase.net.handler.RequestHandler
import com.bitato.ut.sbase.net.mapper.AbstractRequestMapper
import com.bitato.ut.server.data.exceptions.UtSessionAuthenticationException
import com.bitato.ut.server.data.services.UserServerSessionHandler
import com.bitato.ut.server.net.http.HttpRequestHandler
import com.bitato.ut.server.net.internal.InternalRequestHandler
import com.bitato.ut.server.net.socks.Socks4RequestHandler
import com.bitato.ut.server.net.socks.Socks5RequestHandler
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket
import java.net.SocketTimeoutException

class UtRequestMapper() : AbstractRequestMapper() {

    private val sessionHandler: UserServerSessionHandler = Injekt.get()

    override fun mapRequest(incoming: Socket): RequestHandler {

        try {
            val wrapperProvider = Injekt.get<WrapperProvider>()

            incoming.soTimeout = 10000
            val requestWrapper = wrapperProvider.provideRequestWrapper(incoming.inputStream)
            incoming.soTimeout = 0
            
            if (requestWrapper.requestType != INTERNAL_REQUEST) {
                authorizeClient(requestWrapper.uuid)
            }

            return getRequestHandler(incoming, requestWrapper)
        }
        catch (ex: SocketTimeoutException) {
            throw UtInvalidRequestException("Unable to determine request type. Socket timed out")
        }
    }

    private fun authorizeClient(uuid: String) {

        val session = sessionHandler[uuid]

        if (session.isPresent) {
            if (session.get().isDisposePending || session.get().isDisposed) {
                throw UtSessionAuthenticationException("Invalid session")
            }
        } else {
            throw UtSessionAuthenticationException("A tunnel session with the UUID specified could not be found")
        }
    }

    private fun getRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper): RequestHandler {
        return when (requestWrapper.requestType) {
            INTERNAL_REQUEST -> InternalRequestHandler(incoming, requestWrapper)
            HTTP_REQUEST -> HttpRequestHandler(incoming, requestWrapper)
            SOCKS4_REQUEST -> Socks4RequestHandler(incoming, requestWrapper)
            SOCKS5_REQUEST -> Socks5RequestHandler(incoming, requestWrapper)
            else -> throw UtInvalidRequestException("Unable to determine request type")
        }
    }
}