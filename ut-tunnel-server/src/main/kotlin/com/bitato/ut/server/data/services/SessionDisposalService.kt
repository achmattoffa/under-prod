/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services

import com.bitato.ut.server.data.dto.Session

interface SessionDisposalService {
    fun disposeSession(session: Session)
}