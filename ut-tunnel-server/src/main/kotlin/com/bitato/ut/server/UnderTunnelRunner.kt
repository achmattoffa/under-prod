/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server

import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.persist.module.PersistenceBaseInjektModule
import com.bitato.ut.sbase.ServerBaseMain
import com.bitato.ut.sbase.module.ServerBaseInjektModule
import com.bitato.ut.security.modules.SecurityUtilityInjektModule
import com.bitato.ut.server.configuration.BounceServerConfig
import com.bitato.ut.server.configuration.TunnelServerConfig
import com.bitato.ut.server.data.services.ServerHouseKeeper
import com.bitato.ut.server.data.services.ServerInitializationCode
import com.bitato.ut.server.module.TunnelServerInjektModule
import com.bitato.ut.server.net.HttpBounceServer
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.get

class UnderTunnelRunner : Runnable {

    override fun run() {

        val serverHousekeeper = Injekt.get<ServerHouseKeeper>()
        serverHousekeeper.initializeServer { initCode ->

            if (initCode == ServerInitializationCode.SUCCESS) {
                configureBackgroundServices()
                configureBounceServer()
                configureTunnelServers()
            } else {
                println("Unable to initialize tunnel server")
            }
        }
    }

    private fun configureBackgroundServices() {
        val services = Injekt.get<List<AbstractScheduledBackgroundWorker>>()
        services.forEach(AbstractScheduledBackgroundWorker::start)
    }

    private fun configureTunnelServers() {
        val config = ConfigFactory.create(TunnelServerConfig::class.java)
        startServer(config)
    }

    private fun configureBounceServer() {
        val bouncerConfig = ConfigFactory.create(BounceServerConfig::class.java)
        if (bouncerConfig.isEnabled()) {
            val server = HttpBounceServer(bouncerConfig.port())
            server.start()
        }
    }

    companion object UnderTunnelServer : ServerBaseMain() {

        override fun InjektRegistrar.registerInjectables() {
            importModule(PersistenceBaseInjektModule)
            importModule(ServerBaseInjektModule)
            importModule(SecurityUtilityInjektModule)
            importModule(TunnelServerInjektModule)
            importModule(LogBaseInjektModule)
        }

        @JvmStatic fun main(args: Array<String>) {
            UnderTunnelRunner().run()
        }
    }
}