/*
 * Copyright (c) 2016. Under Tunnel
 */
package com.bitato.ut.server.net.socks

import com.bitato.ut.api.model.net.socks.socks5.Socks5Factory
import com.bitato.ut.api.model.net.socks.socks5.Socks5Request
import com.bitato.ut.api.model.net.socks.socks5.Socks5Response
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.server.net.AbstractSessionisedTunnelRequestHandler
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket
import javax.net.SocketFactory

class Socks5RequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper)
: AbstractSessionisedTunnelRequestHandler(incoming, requestWrapper) {

    override fun run() {

        io {
            val request = readRequest {
                Socks5Factory.createRequestHeader(it)
            }

            when (request.cmd) {
                Socks5Request.Companion.CONNECT -> handleSocksConnect(request)
                Socks5Request.Companion.BIND -> handlSocksBind(request)
                Socks5Request.Companion.UDP_ASSOCIATE -> handleSocksAssociate(request)
            }
        }

        io { closeClient() }
    }

    private fun handleSocksConnect(request: Socks5Request) {

        val host = request.getAddressAsString()
        val port = request.port
        val remote = Injekt.get<SocketFactory>().createSocket(host, port.toInt())

        io {
            log info "Connection established with target machine"
            sendResponseToClient(Socks5Response.SUCCEEDED)

            log info "Invoking bidirectional stream between client and target"
            streamIndefinitely(remote.inputStream, remote.outputStream)
        }

        io {
            remote.close()
        }
    }


    private fun handleSocksAssociate(request: Socks5Request) {
        //TODO
        sendResponseToClient(Socks5Response.CMD_NOT_SUPPORTED)
    }

    private fun handlSocksBind(request: Socks5Request) {
        //TODO
        sendResponseToClient(Socks5Response.CMD_NOT_SUPPORTED)
    }

    private fun sendResponseToClient(statusCode: Byte) {

        val response = Socks5Response(statusCode)
        response.reply = statusCode

        clientOut < wrapServerPayload(response.parse())
    }
}