/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.persistence

import com.bitato.ut.persist.utility.MemoryRow

interface UserDb {

    fun getUser(username: String): MemoryRow


    fun getAccountSessionInformationByServerSessionID(serverSessionID: Int): MemoryRow
}