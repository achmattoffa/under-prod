/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services

import com.bitato.ut.server.data.dto.Session
import java.util.*

interface ServerSessionService {

    fun createServerSessionByUuid(sessionUuid: String): Optional<Session>

    fun disposeSession(uuid: String)

    fun updateDataUsage(serverSessionID: Int, dataConsumed: Long)

    fun validateSession(serverSessionID: Int): Int
}