/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.net.internal

import com.bitato.ut.api.model.net.internal.InternalConnectRequest.RequestCodes
import com.bitato.ut.api.model.net.internal.InternalMessageStreamFactory
import com.bitato.ut.api.model.net.internal.InternalSessionRequest
import com.bitato.ut.api.model.net.internal.InternalSessionResponse
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.services.AccountService
import com.bitato.ut.server.data.services.ServerSessionService
import com.bitato.ut.server.data.services.SessionDisposalService
import com.bitato.ut.server.net.AbstractTunnelRequestHandler
import com.github.salomonbrys.kotson.jsonObject
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket
import java.net.SocketTimeoutException

class InternalRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper)
    : AbstractTunnelRequestHandler(incoming, requestWrapper) {

    private val serverSessionService: ServerSessionService = Injekt.get()
    private val sessionDisposalService: SessionDisposalService = Injekt.get()
    private val accountService: AccountService = Injekt.get()

    override fun run() {

        io {

            val internalRequest = readRequest {
                val messageStreamer = Injekt.get<InternalMessageStreamFactory>()
                messageStreamer.createInternalConnectRequest(it)
            }

            when (internalRequest.command) {
                RequestCodes.CONNECT -> handleClientConnect(internalRequest.uuid)
                RequestCodes.RECONNECT -> handleClientReconnect(internalRequest.uuid)
            }
        }

        io { client.close() }
    }

    private fun handleClientConnect(uuid: String) {

        log info "handling client connect for $uuid"

        if (userServerSessionHandler[uuid].isPresent) {
            handleClientReconnect(uuid)
        } else {

            val optionalServerSession = serverSessionService.createServerSessionByUuid(uuid)

            if (optionalServerSession.isPresent) {

                val serverSession = optionalServerSession.get()

                try {
                    clientOut < wrapServerPayload(
                      jsonObject(
                        "success" to true,
                        "uuid" to serverSession.serverSessionUuid,
                        "throttle" to serverSession.limitThrottle
                      ).toString().toByteArray(Charsets.ISO_8859_1)
                    )
                } catch (ex: Exception) {
                    //TODO
                    sessionDisposalService.disposeSession(serverSession)
                    return
                }

                try {
                    userServerSessionHandler.register(serverSession)
                    serverSession.internalControl = client

                    handleSessionRequests(serverSession)
                } finally {

                    serverSession.isDisconnected = true
                    serverSession.disconnectTimeMillis = System.currentTimeMillis()
                }

            } else {
                //TODO
                clientOut < wrapServerPayload(
                  jsonObject(
                    "success" to false
                  ).toString().toByteArray(Charsets.ISO_8859_1)
                )
            }
        }
    }

    private fun handleClientReconnect(uuid: String) {
        log info "handling client reconnect for $uuid"
        val optionalServerSession = userServerSessionHandler[uuid]

        if (optionalServerSession.isPresent) {

            val serverSession = optionalServerSession.get()

            try {
                clientOut < wrapServerPayload(
                  jsonObject(
                    "success" to true,
                    "uuid" to serverSession.serverSessionUuid,
                    "throttle" to serverSession.limitThrottle
                  ).toString().toByteArray(Charsets.ISO_8859_1)
                )

                serverSession.internalControl = client
                serverSession.isDisconnected = false
                serverSession.disconnectTimeMillis = 0

                handleSessionRequests(serverSession)
            } finally {
                serverSession.isDisconnected = true
                serverSession.disconnectTimeMillis = System.currentTimeMillis()
            }

        } else {
            clientOut < wrapServerPayload(
              jsonObject(
                "success" to false
              ).toString().toByteArray(Charsets.ISO_8859_1)
            )
        }
    }

    private fun handleSessionRequests(session: Session) {

        io {

            //insert initial data into session json queue (account information)
            val data = accountService.getAccountSessionInformationByServerSessionID(session.serverSessionID)

            if (data.isNotEmpty()) {
                val js = jsonObject(
                  "type" to InternalSessionResponse.MessageTypes.ACCOUNT_INFO,
                  "packageName" to data["sz_package_name"].toString(),
                  "dataRemaining" to data["i_data_remaining"].toString().toLong(),
                  "dateExpires" to data["dt_expire"].toString(),
                  "activeSessions" to data["i_devices_connected"].toString().toInt()
                )

                clientOut < wrapServerPayload(js.toString().toByteArray(Charsets.ISO_8859_1))
            }

            val messageStreamer = Injekt.get<InternalMessageStreamFactory>()

            while (true) {

                client.soTimeout = 10000
                try {

                    val sessionRequest = messageStreamer.createInternalSessionRequest(clientIn)

                    if (sessionRequest.code == InternalSessionRequest.RequestCodes.DISCONNECT) {
                        handleSessionDisconnect(session)
                        break
                    }

                } catch (ex: SocketTimeoutException) {

                    handleSessionDisconnect(session)
                    break
                }

                client.soTimeout = 0


                val js = session.messageQueue.poll()

                if (js != null) {
                    clientOut < wrapServerPayload(js.toString().toByteArray(Charsets.ISO_8859_1))
                } else {
                    clientOut < wrapServerPayload(jsonObject(
                      "type" to InternalSessionResponse.MessageTypes.SESSION_STATUS,
                      "code" to InternalSessionResponse.SessionStatusCodes.KEEP_ALIVE
                    ).toString().toByteArray(Charsets.ISO_8859_1))
                }

                Thread.sleep(2000)
            }
        }
    }

    private fun handleSessionDisconnect(session: Session) {
        io {
            clientOut < wrapServerPayload(jsonObject(
              "type" to InternalSessionResponse.MessageTypes.SESSION_STATUS,
              "code" to InternalSessionResponse.SessionStatusCodes.TERMINATE
            ).toString().toByteArray(Charsets.ISO_8859_1))
        }

        sessionDisposalService.disposeSession(session)
    }
}
