/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.net

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.api.model.net.UtInvalidRequestException
import com.bitato.ut.api.util.io
import com.bitato.ut.log.util.ServiceDiagnostic
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import com.bitato.ut.server.data.exceptions.UtSessionAuthenticationException
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

class UtProxyServer(protocol: NetworkProtocol, port: Int) : AbstractServer(protocol, port) {

    val requestMapper: RequestMapper

    init {
        log info "Creating request mapper"
        requestMapper = Injekt.get()
    }

    override fun handleClientConnection(client: Socket) {
        try {

            log info "Mapping incoming request to request handler"
            val requestHandler = requestMapper.mapRequest(client)
            log info "Submitting request handler to thread pool"
            threadPool.submit(requestHandler)
        } catch (ex: UtSessionAuthenticationException) {
            ServiceDiagnostic.logException(ex)
            io { client.close() }
        } catch (ex: UtInvalidRequestException) {
            ServiceDiagnostic.logException(ex)
            io { client.close() }
        }
    }

}