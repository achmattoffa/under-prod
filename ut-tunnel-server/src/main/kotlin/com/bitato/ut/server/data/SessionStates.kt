/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data

object SessionStates {
    const val VALID: Byte = 0x01
    const val INVALID: Byte = 0x02
}

