/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services

import com.bitato.ut.persist.utility.MemoryRow

interface AccountService {

    fun getAccountByUsername(username: String): MemoryRow

    fun getAccountSessionInformationByServerSessionID(serverSessionID: Int): MemoryRow
}


