/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.server.net

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.util.io
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket

class HttpBounceServer(port: Int) : AbstractServer(NetworkProtocol.TCP, port) {

    val httpFactory: HttpStreamFactory = Injekt.get()
    val requestMapper: RequestMapper

    init {
        log info "Creating request mapper"
        requestMapper = Injekt.get()
    }

    override fun handleClientConnection(client: Socket) {
        io {
            log info "Bouncing HTTP request"
            val http = httpFactory.createHttpRequest(client.getInputStream())
            log info http
        }
    }
}