/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.services.impl

import com.bitato.ut.api.util.io
import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.services.ServerSessionService
import com.bitato.ut.server.data.services.SessionDisposalService
import com.bitato.ut.server.data.services.UserServerSessionHandler
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class UtSessionDisposalService : SessionDisposalService {

    private val userServerSessionHandler: UserServerSessionHandler = Injekt.get()
    private val serverSessionService: ServerSessionService = Injekt.get()

    @Synchronized
    override fun disposeSession(session: Session) {
        io {
            session.isDisposePending = false
            userServerSessionHandler.unregister(session.serverSessionUuid)
            serverSessionService.disposeSession(session.serverSessionUuid)

            session.streams.forEach { stream -> io { stream.close() } }
            session.isDisposed = true
        }
    }
}