/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.module

import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.api.io.throttle.InputStreamThrottleController
import com.bitato.ut.api.io.throttle.OutputStreamThrottleController
import com.bitato.ut.api.io.throttle.UtInputStreamThrottleController
import com.bitato.ut.api.io.throttle.UtOutputStreamThrottleController
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.model.net.internal.InternalMessageStreamFactory
import com.bitato.ut.api.model.net.internal.UtInternalMessageStreamFactory
import com.bitato.ut.api.model.net.wrappers.UtWrapperProvider
import com.bitato.ut.api.model.net.wrappers.WrapperProvider
import com.bitato.ut.sbase.configuration.ServerBaseConfig
import com.bitato.ut.sbase.net.mapper.AbstractRequestMapper
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import com.bitato.ut.server.concurrency.UtSessionInvalidatorBackgroundWorker
import com.bitato.ut.server.concurrency.UtSessionUsageBackgroundWorker
import com.bitato.ut.server.data.dto.Session
import com.bitato.ut.server.data.persistence.*
import com.bitato.ut.server.data.services.*
import com.bitato.ut.server.data.services.impl.*
import com.bitato.ut.server.net.UtProxyServer
import com.bitato.ut.server.net.UtRequestMapper
import com.bitato.ut.server.net.UtSocketFactory
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import uy.kohesive.injekt.api.addPerKeyFactory
import javax.net.SocketFactory

object TunnelServerInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<AccountService> { UtAccountService() }
        addFactory<ServerSessionService> { UtServerSessionService() }
        addFactory<InternalMessageStreamFactory> { UtInternalMessageStreamFactory }
        addFactory<SessionDisposalService> { UtSessionDisposalService() }
        addFactory<UserServerSessionHandler> { UtUserServerSessionHandler() }
        addFactory<UserDb> { UtUserDb() }
        addFactory<ServerHouseKeeperDb> { UtServerHouseKeeperDb() }
        addFactory<ServerHouseKeeper> { UtServerHouseKeeper() }
        addFactory<UserSessionDb> { UtUserSessionDb() }
        addPerKeyFactory<AbstractServer, ServerBaseConfig> { UtProxyServer(it.protocol(), it.port()) }
        addPerKeyFactory<InputStreamThrottleController, Int> { UtInputStreamThrottleController(it) }
        addPerKeyFactory<OutputStreamThrottleController, Int> { UtOutputStreamThrottleController(it) }
        addFactory<RequestMapper> { UtRequestMapper() }
        addFactory<AbstractRequestMapper> { UtRequestMapper() }
        addFactory<SocketFactory> { UtSocketFactory() }
        addFactory<Map<String, Session>> { UtUserServerSessionHandler.Companion.sessions }
        addFactory<WrapperProvider> { UtWrapperProvider() }
        addFactory<HttpStreamFactory> { UtHttpStreamFactory }
        addFactory<List<AbstractScheduledBackgroundWorker>> { listOf(UtSessionInvalidatorBackgroundWorker(), UtSessionUsageBackgroundWorker()) }
    }

}