/*
 * Copyright (c) 2016. Under Tunnel
 */

package com.bitato.ut.server.data.persistence

import com.bitato.ut.persist.utility.DbParameterType
import com.bitato.ut.persist.utility.MemoryRow
import com.bitato.ut.persist.utility.PersistenceBase

class UtUserSessionDb(): UserSessionDb, PersistenceBase() {

    override fun createServerSessionByUserSessionUuidAndServerCode(uuid: String, serverCode: String): MemoryRow {
        return procQuery {
            procedure("usp_ut_user_server_session_create_by_uuid_and_server_code")
            parameters(uuid to DbParameterType.IN, serverCode to DbParameterType.IN)
            transaction(true)
        }
    }

    override fun disposeSession(uuid: String) {
        procUpdate {
            procedure("usp_dispose_user_server_session")
            parameters(uuid to DbParameterType.IN)
        }
    }

    override fun updateDataUsage(serverSessionID: Int, dataConsumed: Long) {
        procUpdate {
            procedure("usp_update_user_package_data_consumed")
            parameters(serverSessionID to DbParameterType.IN, dataConsumed.toInt() to DbParameterType.IN)
        }
    }

    override fun validateSession(serverSessionID: Int): MemoryRow {
        return procQuery {
            procedure("usp_ut_user_session_validate_session")
            parameters(serverSessionID to DbParameterType.IN)
        }
    }
}