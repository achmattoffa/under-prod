/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.concurrency

import ProcessHelper
import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.client.data.events.EventBus

class PacketSnifferDetector : AbstractScheduledBackgroundWorker(5, 1) {

    override fun run() {

        if (ProcessHelper.isProcessRunning(".*(wireshark|tcpdump|netcat|ettercap|smartsniff|sysdig|ethereal|cloudshark|skapy|netmon|dumpcap).*")) {
            EventBus.notify(EventBus.EventIDs.GLOBAL_BLOCK, "UnderTunnel cannot run while packet sniffers are open. Please first close the all packet sniffers and then reopen Under Tunnel")
        }
    }
}