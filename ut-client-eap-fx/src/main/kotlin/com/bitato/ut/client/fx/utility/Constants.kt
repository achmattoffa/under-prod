/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.utility

object Constants {

    const val COMPANY: String = ".under"
    const val APP: String = "eap1"
    val SEPARATOR: String? = System.getProperty("file.separator")
    const val USER_APP_CONFIG: String = "configuration"

    val HOME = System.getProperty("user.home")
    val PROGRAM_DIR: String = "$HOME$SEPARATOR$COMPANY$SEPARATOR$APP$SEPARATOR"
    val CONFIG_DIR: String = "${PROGRAM_DIR}configuration$SEPARATOR"
    val APP_CONF_DIR: String = CONFIG_DIR
}