/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.net

import com.bitato.ut.api.model.net.sessioninit.SessionInitRequest
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.thread
import com.bitato.ut.client.configuration.UtNetworkConfiguration
import com.bitato.ut.client.data.ApplicationState
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.data.services.servers.ServerManager
import com.bitato.ut.client.data.services.session.SessionCache
import com.bitato.ut.client.data.services.session.SessionManager
import com.bitato.ut.client.fx.configuration.AccountCredentialsConfig
import com.bitato.ut.client.fx.configuration.ServicesConfiguration
import com.bitato.ut.client.net.http.UtHttpProxyServer
import com.bitato.ut.client.net.util.SocketManager
import com.bitato.ut.client.net.vpn.OpenVpnHelper
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

object ServiceConnector {

    private val sessionManager: SessionManager = Injekt.get()
    private val serverManager: ServerManager = Injekt.get()
    private val socketManager: SocketManager = Injekt.get()
    private lateinit var workerThread: Thread

    init {
        bindEvents()
    }

    fun bindEvents() {
        val credentials = ConfigFactory.create(AccountCredentialsConfig::class.java)
        EventBus.subscribe(EventBus.EventIDs.AUTHENTICATION_STARTED, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Authentication", "Starting the authentication service"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.AUTHENTICATION_CONNECTING, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Authentication", "Connecting to the authentication service"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.AUTHENTICATION_CONNECTED, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Authentication", "Successfully connected to the Authentication Server, authenticating the current user"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.AUTHENTICATION_SUCCEEDED, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Authentication", "${credentials.username()} successfully authenticated"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.AUTHENTICATION_FAILED, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                if (it is String) {
                    EventBus.notify(EventBus.EventIDs.SERVICE_FAILED, "$it")
                } else {
                    EventBus.notify(EventBus.EventIDs.SERVICE_FAILED, "The authentication process has failed for the user ${credentials.username()}")
                }
            }

            //EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
        })


        EventBus.subscribe(EventBus.EventIDs.REMOTE_TUNNEL_CONNECTING, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Remote service", "Connecting to the remote service"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.REMOTE_TUNNEL_CONNECTED, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Remote service", "Successfully connected to the remote service"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.REMOTE_TUNNEL_STOPPING, {
            if (ApplicationState.getState() == ApplicationState.States.DISCONNECTING) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Remote services", "Stopping the remote service"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.REMOTE_TUNNEL_RECONNECTING, {
            if (it is String) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Remote services", "Reconnecting to the remote service\r\n$it"))
            } else {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Remote services", "Reconnecting to the remote service"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.REMOTE_TUNNEL_RECONNECTED, {
            ApplicationState.toggleState(ApplicationState.States.CONNECTED)
            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_SUCCESS)
            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
        })

        EventBus.subscribe(EventBus.EventIDs.REMOTE_TUNNEL_FAILED, {
            if (ApplicationState.getState() == ApplicationState.States.CONNECTING) {
                ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_FAILED, it)
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
            }
        })

        EventBus.subscribe(EventBus.EventIDs.VPN_SERVICE_FAILED, {
            if (ApplicationState.getState() == ApplicationState.States.CONNECTING) {
                killConnections()
                ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_FAILED, it)
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
            }
        })

        EventBus.subscribe(EventBus.EventIDs.VPN_SERVICE_TAP_DRIVERS_INSTALL, {
            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("VPN Service", "Installing TAP drivers"))
        })

        EventBus.subscribe(EventBus.EventIDs.VPN_SERVICE_STARTED, {
            if (ApplicationState.getState() in listOf(ApplicationState.States.CONNECTING, ApplicationState.States.RECONNECTING)) {
                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("VPN Service", "Starting the VPN service"))
            }
        })

        EventBus.subscribe(EventBus.EventIDs.VPN_SERVICE_RECONNECT_FAILED, {
            killConnections()
            EventBus.notify(EventBus.EventIDs.SERVICE_STOPPED)
            EventBus.notify(EventBus.EventIDs.SERVICE_RECONNECT_REQUESTED)
        })

        EventBus.subscribe(EventBus.EventIDs.VPN_SERVICE_RECONNECTED, {
            ApplicationState.toggleState(ApplicationState.States.CONNECTED)
        })

        EventBus.subscribe(EventBus.EventIDs.VPN_SERVICE_CONNECTED, {
            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("VPN Service", "The VPN service has successfully started"))
            ApplicationState.toggleState(ApplicationState.States.CONNECTED)
            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_SUCCESS)
            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
        })
    }

    fun connectToService(useVpnService: Boolean, useTorrentServers: Boolean) {
        ApplicationState.toggleState(ApplicationState.States.CONNECTING)

        workerThread = thread {
            try {

                val credentials = ConfigFactory.create(AccountCredentialsConfig::class.java)
                val serviceConfig = ConfigFactory.create(ServicesConfiguration::class.java)

                val sessionRequest = SessionInitRequest("ANY", UtNetworkConfiguration.protocol(), UtNetworkConfiguration.port().first(), credentials.username(), credentials.password(), if (useTorrentServers) 1 else 0)

                EventBus.subscribe(EventBus.EventIDs.SERVICE_STARTED, {

                    val localPort = serviceConfig.localHttpProxyPort()
                    serverManager.add(UtHttpProxyServer(localPort))
                    serverManager.start()

                    OpenVpnHelper.connect(SessionCache.getSession().remoteAddress, SessionCache.getSession().remotePort, serviceConfig.localHttpProxyHost(), localPort)

                    EventBus.subscribe(EventBus.EventIDs.SERVICE_STARTED, {})
                })

                EventBus.subscribe(EventBus.EventIDs.SERVICE_FAILED, {
                    ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
                    Thread.sleep(3000)
                    EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_PAUSED, it)

                    Thread.sleep(3000)
                    connectToService(useVpnService, useTorrentServers)

                    EventBus.subscribe(EventBus.EventIDs.SERVICE_FAILED, {})
                })

                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STARTED, true)

                if (SessionCache.getSession() == SessionCache.NONE || !SessionCache.getSession().isReusable()) {
                    sessionManager.initSession(sessionRequest)
                } else {
                    sessionManager.reInitSession(sessionRequest)
                }
            } catch (ex: InterruptedException) {
            }
        }
    }

    fun reconnectToService(useTorrentServers: Boolean) {

        ApplicationState.toggleState(ApplicationState.States.RECONNECTING)

        workerThread = thread {
            try {

                val credentials = ConfigFactory.create(AccountCredentialsConfig::class.java)
                val serviceConfig = ConfigFactory.create(ServicesConfiguration::class.java)

                val sessionRequest = SessionInitRequest("ANY", UtNetworkConfiguration.protocol(), UtNetworkConfiguration.port().first(), credentials.username(), credentials.password(), if (useTorrentServers) 1 else 0)

                EventBus.subscribe(EventBus.EventIDs.SERVICE_STARTED, {

                    val localPort = serviceConfig.localHttpProxyPort()
                    serverManager.add(UtHttpProxyServer(localPort))
                    serverManager.start()

                    OpenVpnHelper.connect(SessionCache.getSession().remoteAddress, SessionCache.getSession().remotePort, serviceConfig.localHttpProxyHost(), localPort)

                    EventBus.subscribe(EventBus.EventIDs.SERVICE_STARTED, {})
                })

                EventBus.subscribe(EventBus.EventIDs.SERVICE_FAILED, {

                    ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
                    Thread.sleep(2000)
                    EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_PAUSED, it)

                    Thread.sleep(3000)
                    reconnectToService(useTorrentServers)

                    EventBus.subscribe(EventBus.EventIDs.SERVICE_FAILED, {})
                })

                EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STARTED, true)
                sessionManager.initSession(sessionRequest)
            } catch (ex: InterruptedException) {
            }
        }
    }

    fun abortConnectionToService() {
        ApplicationState.toggleState(ApplicationState.States.ABORTING)

        thread {

            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, Pair("Aborting", "Cancelling service initialization"))

            Thread.sleep(5000)
            killConnections()

            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_CANCELLED)
            EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_DONE)
        }

    }

    private fun killConnections() {
        io { serverManager.close() }
        io { socketManager.close() }
        io { sessionManager.close() }

        SessionCache.getSession().isActive = false
        SessionCache.getSession().updatedTime = System.currentTimeMillis()
        SessionCache.registerSession(SessionCache.NONE)

        io { OpenVpnHelper.disconnect() }
    }

    fun disconnectFromService() {
        ApplicationState.toggleState(ApplicationState.States.DISCONNECTING)
        EventBus.notify(EventBus.EventIDs.SERVICE_TOGGLE_STARTED, false)
        EventBus.notify(EventBus.EventIDs.REMOTE_TUNNEL_STOPPING)
        tryDisconnectServices()
    }

    private fun tryDisconnectServices() {
        io { sessionManager.close() }
    }
}