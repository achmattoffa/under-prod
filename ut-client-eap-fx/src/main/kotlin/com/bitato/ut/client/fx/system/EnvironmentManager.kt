/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.system

interface EnvironmentManager {

    fun isFirstStart(): Boolean

    fun prepareEnvironment(): Boolean
}