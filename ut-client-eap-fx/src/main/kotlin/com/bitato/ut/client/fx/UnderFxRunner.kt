/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx

import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.client.data.ApplicationState
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.fx.module.ClientFxInjektModule
import com.bitato.ut.client.fx.net.ServiceConnector
import com.bitato.ut.client.module.ClientCoreInjektModule
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.sbase.ServerBaseMain
import javafx.application.Application
import javafx.application.Platform
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.image.Image
import javafx.stage.Stage
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.get

class UnderFxRunner : Application() {

    override fun start(stage: Stage) {

        ApplicationState.initialize()

        val uiResource = javaClass.getResource("/views/UI.fxml")
        val uiLoader = FXMLLoader(uiResource)
        val uiParent = uiLoader.load<Parent>()

        uiParent.stylesheets.add("/stylesheets/main.css")

        stage.apply {
            scene = Scene(uiParent)
            title = "Under Tunnel"
            icons.add(Image(UnderFxRunner::class.java.getResourceAsStream("/images/satellite.png")))
            isResizable = false
        }

        stage.setOnCloseRequest {

            if (ApplicationState.getState() == ApplicationState.States.CONNECTED) {

                val result = Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to exit?", ButtonType.YES, ButtonType.CANCEL)
                  .apply {
                      title = "Confirmation required"
                      headerText = "Exit Under Tunnel?"
                      contentText = "Choosing to Exit Under Tunnel will disconnect you from the Service"
                      initOwner(stage.owner)
                  }
                  .showAndWait()

                if (result.isPresent && result.get() == ButtonType.YES) {

                    EventBus.subscribe(EventBus.EventIDs.SERVICE_STOPPED, {
                        Platform.runLater {
                            Platform.exit()
                            System.exit(0)
                        }
                    })

                    ServiceConnector.disconnectFromService()
                }
            } else {
                Platform.runLater {
                    Platform.exit()
                    System.exit(0)
                }
            }

            it.consume()
        }

        EventBus.subscribe(EventBus.EventIDs.GLOBAL_BLOCK, {

            if (it is String) {

                Platform.runLater {

                    Alert(Alert.AlertType.ERROR).apply {
                        contentText = it
                        headerText = "An error has occurred"
                        initOwner(stage.owner)

                    }
                      .showAndWait()

                    if (ApplicationState.getState() == ApplicationState.States.CONNECTED) {
                        EventBus.subscribe(EventBus.EventIDs.SERVICE_STOPPED, {
                            Platform.exit()
                            System.exit(0)
                        })

                        ServiceConnector.disconnectFromService()
                    } else {
                        Platform.exit()
                        System.exit(0)
                    }
                }

                EventBus.unsubscribe(EventBus.EventIDs.GLOBAL_BLOCK);
            }
        })

        configureBackgroundServices()

        ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
        stage.show()
    }

    private fun configureBackgroundServices() {
        val services = Injekt.get<List<AbstractScheduledBackgroundWorker>>()
        services.forEach(AbstractScheduledBackgroundWorker::start)
    }

    companion object : ServerBaseMain() {

        override fun InjektRegistrar.registerInjectables() {
            importModule(ClientCoreInjektModule)
            importModule(ClientFxInjektModule)
            importModule(LogBaseInjektModule)
        }

        @JvmStatic fun main(args: Array<String>) {
            launch(UnderFxRunner::class.java)
        }
    }
}
