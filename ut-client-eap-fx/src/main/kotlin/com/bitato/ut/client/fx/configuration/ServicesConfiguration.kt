/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.configuration

import org.aeonbits.owner.Config

interface ServicesConfiguration: Config {
    @Config.DefaultValue("8002")
    @Config.Key("http.local.port")
    fun localHttpProxyPort(): Int

    @Config.DefaultValue("localhost")
    @Config.Key("http.local.host")
    fun localHttpProxyHost(): String


}