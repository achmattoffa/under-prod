/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.configuration

import com.bitato.ut.client.fx.utility.Constants


object _o {
    //region File Constants
    const private val RESOURCE_ROOT = "file:src/main/resources/"
    const private val HOME_ROOT = "file:~/${Constants.COMPANY}/${Constants.APP}"
    const private val APP_CONFIG = "configuration"
    const val DEFAULT_INJECTION_CONF = "${RESOURCE_ROOT}${APP_CONFIG}/inject.xml"
    const val DEFAULT_SERVER_CONF = "${RESOURCE_ROOT}${APP_CONFIG}/server.xml"
    const val USER_INJECTION_CONF = "${HOME_ROOT}/${Constants.USER_APP_CONFIG}/inject.xml"
    const val USER_SERVER_CONF = "${HOME_ROOT}/${Constants.USER_APP_CONFIG}/server.xml"
    //endregion
}