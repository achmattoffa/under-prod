/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.module

import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.client.data.services.monitors.BandwidthStreamWrapper
import com.bitato.ut.client.fx.concurrency.PacketSnifferDetector
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import java.io.InputStream
import java.io.OutputStream

object ClientFxInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {
        addFactory<List<AbstractScheduledBackgroundWorker>> { listOf(PacketSnifferDetector()) }

        addFactory<BandwidthStreamWrapper> {
            object : BandwidthStreamWrapper {
                override fun wrap(inputStream: InputStream): InputStream {
                    return inputStream
                }

                override fun wrap(outputStream: OutputStream): OutputStream {
                    return outputStream
                }
            }
        }
    }

}