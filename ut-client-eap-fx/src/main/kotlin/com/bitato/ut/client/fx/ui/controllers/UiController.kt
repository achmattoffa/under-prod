/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.ui.controllers

import com.bitato.ut.client.configuration.UtNetworkConfiguration
import com.bitato.ut.client.data.ApplicationState
import com.bitato.ut.client.data.events.EventBus
import com.bitato.ut.client.fx.configuration.ACCOUNT_CREDENTIALS_KEYS_PASSWORD
import com.bitato.ut.client.fx.configuration.ACCOUNT_CREDENTIALS_KEYS_USERNAME
import com.bitato.ut.client.fx.configuration.ACCOUNT_CREDENTIALS_PATH
import com.bitato.ut.client.fx.configuration.AccountCredentialsConfig
import com.bitato.ut.client.fx.net.ServiceConnector
import com.github.salomonbrys.kotson.contains
import com.github.salomonbrys.kotson.int
import com.github.salomonbrys.kotson.long
import com.github.salomonbrys.kotson.string
import com.gluonhq.charm.glisten.control.ProgressIndicator
import com.google.gson.JsonObject
import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXPasswordField
import com.jfoenix.controls.JFXTextField
import com.jfoenix.controls.JFXToggleButton
import com.jfoenix.validation.RequiredFieldValidator
import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.scene.effect.ColorAdjust
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.DragEvent
import javafx.scene.input.MouseEvent
import javafx.scene.input.TransferMode
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.FileChooser
import org.aeonbits.owner.ConfigFactory
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timerTask


@Suppress("UNUSED_PARAMETER")
class UiController : Initializable {

    @FXML
    lateinit var uiMenuIconsPane: AnchorPane

    @FXML
    lateinit var uiConnectionMenuIcon: ImageView

    @FXML
    lateinit var uiAccountMenuIcon: ImageView

    @FXML
    lateinit var uiConfigurationMenuIcon: ImageView

    @FXML
    lateinit var uiPowerMenuIcon: ImageView

    @FXML
    lateinit var uiConnectionPane: AnchorPane

    @FXML
    lateinit var uiAccountCredentialsPane: AnchorPane

    @FXML
    lateinit var uiAccountUserTextField: JFXTextField

    @FXML
    lateinit var uiAccountPassTextField: JFXPasswordField

    @FXML
    lateinit var uiAccountSaveButton: JFXButton

    @FXML
    lateinit var uiAccountCancelButton: JFXButton

    @FXML
    lateinit var uiAccountInformationPane: AnchorPane

    @FXML
    lateinit var uiConfigurationPane: AnchorPane

    @FXML
    lateinit var uiConnectionProgressPane: AnchorPane

    @FXML
    lateinit var uiLoadInjectionFileButton: JFXButton

    @FXML
    lateinit var uiNoLoadedInjectionHBox: AnchorPane

    @FXML
    lateinit var uiLoadedInjectionVBox: VBox

    @FXML
    lateinit var uiInjectionFileVBox: AnchorPane

    @FXML
    lateinit var uiExportInjectionHyperLink: Hyperlink

    @FXML
    lateinit var uiInjectionNameLabel: Label

    @FXML
    lateinit var uiInjectionDescLabel: Label

    @FXML
    lateinit var uiInjectionLineLabel: Label

    @FXML
    lateinit var uiConnectingActionLabel: Label

    @FXML
    lateinit var uiConnectingActionDescriptionLabel: Label

    @FXML
    lateinit var uiNetworkConnectionIcon: ImageView

    @FXML
    lateinit var uiNetworkConnectionStatusLabel: Label

    @FXML
    lateinit var uiSelectServerToggleButton: JFXToggleButton

    @FXML
    lateinit var uiToggleServiceButton: JFXButton

    @FXML
    lateinit var uiCancelConnectButton: JFXButton

    @FXML
    lateinit var uiAccountInfoUserNameLabel: Label

    @FXML
    lateinit var uiAccountInfoPackageLabel: Label

    @FXML
    lateinit var uiAccountInfoDataLabel: Label

    @FXML
    lateinit var uiAccountInfoExpirationLabel: Label

    @FXML
    lateinit var uiAccountInfoSessionsLabel: Label

    @FXML
    lateinit var uiAccountInfoSessionUptime: Label

    @FXML
    lateinit var uiProgressIndicator: ProgressIndicator

    @FXML
    override fun initialize(location: URL?, resources: ResourceBundle?) {

        toggleViewForMenuIcon(uiConnectionMenuIcon)

        uiAccountUserTextField.validators.add(RequiredFieldValidator().apply {
            message = "No username has been specified"
        })

        uiAccountPassTextField.validators.add(RequiredFieldValidator().apply {
            message = "No password has been specified"
        })

        resetUserCredentials()
        checkLoadedInjection()

        bindUiEventBus()
    }

    fun bindUiEventBus() {

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_STARTED, {
            Platform.runLater {

                uiCancelConnectButton.isVisible = it !is Boolean || it

                toggleViewForMenuIcon(uiConnectionMenuIcon)

                uiConnectionPane.isVisible = false
                uiConnectionProgressPane.isVisible = true
                uiProgressIndicator.progress = ProgressIndicator.INDETERMINATE_PROGRESS
                uiMenuIconsPane.isDisable = true
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_PAUSED, {
            Platform.runLater {

                uiCancelConnectButton.isVisible = true

                toggleViewForMenuIcon(uiConnectionMenuIcon)
                uiProgressIndicator.progress = 50.0

                uiConnectionPane.isVisible = false
                uiConnectionProgressPane.isVisible = true

                uiConnectingActionLabel.text = "ERROR"
                if (it is String) {
                    uiConnectingActionDescriptionLabel.text = it + ". Retrying in 3 seconds"
                } else {
                    uiConnectingActionDescriptionLabel.text + "Retrying in 3 seconds"
                }

                uiMenuIconsPane.isDisable = true
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_STATUS_UPDATE, {
            Platform.runLater {

                if (it is Pair<*, *>) {
                    uiConnectingActionLabel.text = it.first.toString().toUpperCase()
                    uiConnectingActionDescriptionLabel.text = it.second.toString()
                }
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_PROGRESS_UPDATE, {
            Platform.runLater {
                if (it is String) {
                    uiNetworkConnectionStatusLabel.text = it
                }
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_SUCCESS, {

            Platform.runLater {

                val initialTime = System.currentTimeMillis()

                uiAccountInfoUserNameLabel.text = ConfigFactory.create(AccountCredentialsConfig::class.java).username()

                val uptimeProperty = SimpleStringProperty()
                uiAccountInfoSessionUptime.textProperty().bind(uptimeProperty)

                Timer().scheduleAtFixedRate(timerTask {
                    Platform.runLater {
                        val millis = System.currentTimeMillis() - initialTime
                        uptimeProperty.set(String.format("%d min, %d sec",
                          TimeUnit.MILLISECONDS.toMinutes(millis),
                          TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
                        ))
                    }
                }, 1000, 1000)

                toggleConnectionView()
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SESSION_STATUS_UPDATE, {

            if (it is JsonObject) {
                if (it.contains("data")) {
                    Platform.runLater {
                        val dataUsedKB = it["data"].asInt

                        val dataRemainingText = uiAccountInfoDataLabel.text.toUpperCase()
                        val dataRemainingKB = if (dataRemainingText.endsWith("MB")) {
                            dataRemainingText.removeSuffix("MB")
                              .trim()
                              .toDouble() * 1024
                        } else {
                            dataRemainingText.removeSuffix("KB")
                              .trim()
                              .toDouble()
                        }

                        val newDataRemaining = if (dataRemainingKB - dataUsedKB <= 0) 0.00 else dataRemainingKB - dataUsedKB

                        uiAccountInfoDataLabel.text = if (newDataRemaining > 1024.00) {
                            "${Math.round(newDataRemaining / 1024.00)} MB"
                        } else {
                            "$newDataRemaining KB"
                        }
                    }
                }
            }
        })

        EventBus.subscribe(EventBus.EventIDs.ACCOUNT_INFORMATION, {

            if (it is JsonObject) {
                Platform.runLater {
                    uiAccountInfoPackageLabel.text = it["packageName"].string

                    val dataUsedKB = it["dataRemaining"].long
                    uiAccountInfoDataLabel.text = if (dataUsedKB > 1024.00) {
                        "${Math.round(dataUsedKB / 1024.00)} MB"
                    } else {
                        "$dataUsedKB KB"
                    }

                    uiAccountInfoExpirationLabel.text = it["dateExpires"].string
                    uiAccountInfoSessionsLabel.text = "${it["activeSessions"].int}"
                }
            }
        })

//        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_FAILED, {
//            Platform.runLater {
//                    if (it is String) {
//
//                        Alert(Alert.AlertType.ERROR).apply {
//                            headerText = "Unable to start the Tunnel Service"
//                            contentText = it
//                            initOwner(uiConnectionPane.parent.scene.window)
//                        }.show()
//                    }
//            }
//        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_CANCELLED, {
            Platform.runLater {
                ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_DONE, {
            Platform.runLater {

                uiConnectingActionLabel.text = ""
                uiConnectingActionDescriptionLabel.text = ""

                uiConnectionProgressPane.isVisible = false
                uiConnectionPane.isVisible = true

                uiMenuIconsPane.isDisable = false

                toggleConnectionView()
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_STOPPED, {

            Platform.runLater {
                ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
                uiMenuIconsPane.isDisable = false

                toggleConnectionView()
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_TOGGLE_FAILED, {
            Platform.runLater {
                Alert(Alert.AlertType.ERROR).apply {
                    title = "Error"
                    contentText = it as String
                    initOwner(uiConnectionPane.parent.scene.window)
                }.show()
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SERVICE_RECONNECT_REQUESTED, {

            Platform.runLater {
                ServiceConnector.reconnectToService(true)
            }
        })

        EventBus.subscribe(EventBus.EventIDs.REMOTE_TUNNEL_DISCONNECTED, {
            ApplicationState.toggleState(ApplicationState.States.DISCONNECTED)
            Platform.runLater {
                Alert(Alert.AlertType.ERROR).apply {
                    title = "Info"
                    contentText = "The Tunnel Service has disconnected"
                    initOwner(uiConnectionPane.parent.scene.window)
                }.show()
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SESSION_ACCOUNT_BANNED, {
            Platform.runLater {
                Alert(Alert.AlertType.INFORMATION).apply {
                    title = "Info"
                    contentText = "Your account has been banned"
                    initOwner(uiConnectionPane.parent.scene.window)
                }.show()
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SESSION_CAP_REACHED, {
            Platform.runLater {
                Alert(Alert.AlertType.INFORMATION).apply {
                    title = "Info"
                    contentText = "Your bandwidth cap has been reached"
                    initOwner(uiConnectionPane.parent.scene.window)
                }.show()
            }
        })

        EventBus.subscribe(EventBus.EventIDs.SESSION_PACKAGE_EXPIRED, {
            Platform.runLater {
                Alert(Alert.AlertType.INFORMATION).apply {
                    title = "Info"
                    contentText = "Your current data package has expired"
                    initOwner(uiConnectionPane.parent.scene.window)
                }.show()
            }
        })

    }

    @FXML
    fun evtToggleServiceAction(event: ActionEvent) {

        if (ApplicationState.getState() == ApplicationState.States.CONNECTED) {

            val result = Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to disconnect?", ButtonType.YES, ButtonType.CANCEL)
              .apply {
                  title = "Confirmation required"
                  headerText = "Disconnect from the Tunnel Service?"
                  contentText = "Disconnecting from the service will temporarily disconnect you from the internet"
                  initOwner(uiConnectionPane.parent.scene.window)
              }
              .showAndWait()

            if (result.isPresent && result.get() == ButtonType.YES) {
                ServiceConnector.disconnectFromService()
            }
        } else {

            if (validateServiceInputsForStart()) {
                ServiceConnector.connectToService(true, true)
            }

        }
    }

    fun validateServiceInputsForStart(): Boolean {

        if (!isInjectionLoaded()) {
            Alert(Alert.AlertType.ERROR, "Please first load an injection file", ButtonType.OK).apply {
                headerText = "No injection file loaded"
                contentText = "First load an injection file, and then try again"
                title = "Unable to connect"
                initOwner(uiConnectionPane.parent.scene.window)
            }.showAndWait()

            return false
        }

        val credentials = ConfigFactory.create(AccountCredentialsConfig::class.java)

        if (credentials.username().isBlank() || credentials.password().isBlank()) {

            Alert(Alert.AlertType.ERROR, "Invalid account credentials", ButtonType.OK).apply {
                headerText = "Invalid account credentials"
                contentText = "Please correct your account information and then try again"
                title = "Unable to connect"
                initOwner(uiConnectionPane.parent.scene.window)
            }.showAndWait()

            return false
        }

        return true
    }

    @FXML
    fun evtCancelConnectionAction(event: ActionEvent) {
        if (ApplicationState.getState() != ApplicationState.States.DISCONNECTED) {
            val result = Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.CANCEL)
              .apply {
                  title = "Confirmation required"
                  headerText = "Cancel Service connection?"
                  contentText = "Are you sure you want to cancel the Service connection process?"
                  initOwner(uiConnectionPane.parent.scene.window)
              }
              .showAndWait()

            if (result.isPresent && result.get() == ButtonType.YES) {
                ServiceConnector.abortConnectionToService()
            }
        } else {
            ServiceConnector.abortConnectionToService()
        }
    }

    private fun toggleConnectionView() {

        if (ApplicationState.getState() == ApplicationState.States.CONNECTED) {

            val effect = uiNetworkConnectionIcon.effect as ColorAdjust
            effect.hue = 0.7

//            val servicesConfig = ConfigFactory.create(ServicesConfiguration::class.java)

            uiNetworkConnectionStatusLabel.text = "Connected to the VPN service"

            (uiNetworkConnectionStatusLabel.childrenUnmodifiable.first() as ImageView)
              .image = Image("images/connected.png")

            uiToggleServiceButton.text = "Stop service"

        } else {

            val effect = uiNetworkConnectionIcon.effect as ColorAdjust
            effect.hue = 0.0

            uiNetworkConnectionStatusLabel.text = "You are currently disconnected"
            (uiNetworkConnectionStatusLabel.childrenUnmodifiable.first() as ImageView)
              .image = Image("images/disconnected.png")

            uiToggleServiceButton.text = "Start service"


            toggleViewForMenuIcon(uiConnectionMenuIcon)
        }
    }

    @FXML
    fun evtMenuIconMousePressed(event: MouseEvent) {
        toggleViewForMenuIcon(event.target as ImageView)
    }

    private fun toggleViewForMenuIcon(menuIcon: ImageView) {

        if (menuIcon != uiPowerMenuIcon) {
            uiConnectionPane.isVisible = false
            uiConnectionProgressPane.isVisible = false
            uiAccountCredentialsPane.isVisible = false
            uiAccountInformationPane.isVisible = false
            uiConfigurationPane.isVisible = false

            (uiMenuIconsPane.children[0] as HBox).children.forEach {
                it.styleClass.remove("selected")
            }
            menuIcon.parent.styleClass.add("selected")
        }

        when (menuIcon) {
            uiConnectionMenuIcon -> {
                uiConnectionPane.isVisible = true
            }
            uiAccountMenuIcon -> {
                if (ApplicationState.getState() != ApplicationState.States.CONNECTED) {
                    uiAccountCredentialsPane.isVisible = true
                } else {
                    uiAccountInformationPane.isVisible = true
                }
            }
            uiConfigurationMenuIcon -> {
                uiConfigurationPane.isVisible = true
            }
            uiPowerMenuIcon -> {

                if (ApplicationState.getState() != ApplicationState.States.CONNECTED) {
                    Platform.exit()
                    System.exit(0)
                } else {

                    val result = Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to exit?", ButtonType.YES, ButtonType.CANCEL)
                      .apply {
                          title = "Confirmation required"
                          headerText = "Exit Under Tunnel?"
                          contentText = "Choosing to Exit Under Tunnel will disconnect you from the Service"
                          initOwner(uiConnectionPane.parent.scene.window)
                      }
                      .showAndWait()

                    if (result.isPresent && result.get() == ButtonType.YES) {

                        //disconnect and exit from service TODO

                        EventBus.subscribe(EventBus.EventIDs.SERVICE_STOPPED, {
                            Platform.runLater {
                                Platform.exit()
                                System.exit(0)
                            }
                        })

                        ServiceConnector.disconnectFromService()
                    }

                }
            }
        }
    }

    @FXML
    fun evtAccountCredentialsSaveAction(event: ActionEvent) {

        var isInputsValid = true

        if (!uiAccountUserTextField.validate()) {
            isInputsValid = false
        }

        if (!uiAccountPassTextField.validate()) {
            isInputsValid = false
        }

        if (!isInputsValid) {
            return
        }

        storeAccountCredentials()
        toggleViewForMenuIcon(uiConnectionMenuIcon)
    }

    private fun storeAccountCredentials() {

        val credentials = ConfigFactory.create(AccountCredentialsConfig::class.java)

        credentials.setProperty(ACCOUNT_CREDENTIALS_KEYS_USERNAME, uiAccountUserTextField.text)
        credentials.setProperty(ACCOUNT_CREDENTIALS_KEYS_PASSWORD, uiAccountPassTextField.text)

        FileOutputStream(ACCOUNT_CREDENTIALS_PATH).use {
            credentials.store(it, "Account Credentials")
        }
    }

    @FXML
    fun evtAccountCredentialsCancelAction(event: ActionEvent) {
        resetUserCredentials()

        uiAccountUserTextField.resetValidation()
        uiAccountPassTextField.resetValidation()
    }

    private fun resetUserCredentials() {
        val credentials = ConfigFactory.create(AccountCredentialsConfig::class.java)
        uiAccountUserTextField.text = credentials.username()
        uiAccountPassTextField.text = credentials.password()
    }

    private fun isInjectionLoaded(): Boolean {
        val injectionFile = File("injection")
        return injectionFile.exists()
    }

    private fun checkLoadedInjection() {
        if (!isInjectionLoaded()) {
            uiNoLoadedInjectionHBox.isVisible = true
            uiLoadedInjectionVBox.isVisible = false
        } else {

            val injectionLineEncoded = FileInputStream("injection").bufferedReader(Charsets.ISO_8859_1).readText()
            UtNetworkConfiguration.reset()

            uiInjectionNameLabel.text = UtNetworkConfiguration.name()
            uiInjectionDescLabel.text = UtNetworkConfiguration.description()
            uiInjectionLineLabel.text = injectionLineEncoded

            uiNoLoadedInjectionHBox.isVisible = false
            uiLoadedInjectionVBox.isVisible = true
        }
    }

    @FXML
    fun evtLoadInjectionFile(event: ActionEvent) {
        val fileChooser = FileChooser()

        fileChooser.title = "Open Injection File"
        val file = fileChooser.showOpenDialog(uiConfigurationPane.scene.window)

        if (file != null) {
            loadInjectionFile(file)
        }
    }

    private fun loadInjectionFile(file: File) {
        try {
            val f = File("injection")

            if (f.exists()) {
                f.delete()
            }

            file.copyTo(f, true)
        } catch (ex: Exception) {
        }

        checkLoadedInjection()
    }

    @FXML
    fun evtExportInjectionAction(event: ActionEvent) {
        val fileSaver = FileChooser()

        fileSaver.title = "Open Injection File"
        val file = fileSaver.showSaveDialog(uiConfigurationPane.scene.window)

        if (file != null) {
            File("injection").copyTo(file, true)
        }
    }


    @FXML
    fun evtToggleButtonAction(event: ActionEvent) {
        val toggleButton = event.source

        if (toggleButton is JFXToggleButton) {
            toggleButton.text = if (toggleButton.isSelected) "ON" else "OFF"
        }
    }

    @FXML
    fun evtConfigurationPaneDragEntered(event: DragEvent) {

        uiLoadInjectionFileButton.apply {
            text = "Drop"
            styleClass.add("accept-drop")
        }

        uiInjectionFileVBox.styleClass.add("accept-drop")
        event.consume()
    }

    @FXML
    fun evtConfigurationPaneDragExited(event: DragEvent) {

        uiLoadInjectionFileButton.apply {
            text = "Load"
            styleClass.remove("accept-drop")
        }

        uiInjectionFileVBox.styleClass.remove("accept-drop")

        event.consume()
    }

    @FXML
    fun evtConfigurationPaneDragOver(event: DragEvent) {
        if (event.dragboard.hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY)
        }

    }

    @FXML
    fun evtConfigurationPaneDragDropped(event: DragEvent) {

        val file = event.dragboard.files.first()
        loadInjectionFile(file)

        event.isDropCompleted = true
        event.consume()
    }
}