/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.system

import com.bitato.ut.client.fx.utility.Constants
import java.io.File
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path

object UtEnvironmentManager : EnvironmentManager {

    /**
     * Returns true if this is the first time Rocket has been executed
     */
    override fun isFirstStart(): Boolean {
        val workingDirectory = File(Constants.PROGRAM_DIR)
        return !workingDirectory.exists()
    }

    /**
     * Prepare the user's environment for use with Rocket. After this method has completed
     * successfully, the user should be able to start Rocket.
     */
    override fun prepareEnvironment(): Boolean {
        return prepare()
    }

    private fun prepare(): Boolean {
        File(Constants.APP_CONF_DIR).mkdirs()
        Files.copy(getConfigurationFile("tunnel.conf"), getConfigurationPath("tunnel.conf"))
        return true
    }

    private fun getConfigurationPath(name: String): Path {
        return File("${Constants.APP_CONF_DIR}$name").toPath()
    }

    private fun getConfigurationFile(name: String): InputStream {
        return javaClass.getResourceAsStream("/application/$name")
    }

    private fun makeDirectory(source: File, currentDirectory: String) {
        val directory = File("$currentDirectory${source.name}")
        directory.mkdir()

        copyFiles(source, "$currentDirectory${directory.name}${Constants.SEPARATOR}")
    }

    private fun copyFiles(source: File, directory: String) {
        source.listFiles().forEach {
            if (it.isDirectory) {
                makeDirectory(it, directory)
            } else {
                it.copyTo(File("$directory${it.name}"))
            }
        }
    }
}