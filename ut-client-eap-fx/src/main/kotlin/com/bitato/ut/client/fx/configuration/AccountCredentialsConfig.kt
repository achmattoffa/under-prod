/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.ut.client.fx.configuration

import org.aeonbits.owner.Accessible
import org.aeonbits.owner.Config
import org.aeonbits.owner.Mutable


const val ACCOUNT_CREDENTIALS_PATH = "account.txt"
const val ACCOUNT_CREDENTIALS_KEYS_USERNAME = "username"
const val ACCOUNT_CREDENTIALS_KEYS_PASSWORD = "password"

@Config.Sources("file:$ACCOUNT_CREDENTIALS_PATH")
interface AccountCredentialsConfig : Accessible, Mutable {

    @Config.Key(ACCOUNT_CREDENTIALS_KEYS_USERNAME)
    @Config.DefaultValue("")
    fun username(): String

    @Config.Key(ACCOUNT_CREDENTIALS_KEYS_PASSWORD)
    @Config.DefaultValue("")
    fun password(): String
}