/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.module

import com.bitato.tc.server.concurrency.TcAccessKeyBackgroundWorker
import com.bitato.tc.server.data.SessionCache
import com.bitato.tc.server.data.services.AccessKeyLoader
import com.bitato.tc.server.data.services.AccessKeyValidator
import com.bitato.tc.server.data.services.impl.TcAccessKeyLoader
import com.bitato.tc.server.data.services.impl.TcAccessKeyValidator
import com.bitato.tc.server.net.TcProxyServer
import com.bitato.tc.server.net.TcRequestMapper
import com.bitato.tc.server.net.TcSocketFactory
import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.api.io.throttle.InputStreamThrottleController
import com.bitato.ut.api.io.throttle.OutputStreamThrottleController
import com.bitato.ut.api.io.throttle.UtInputStreamThrottleController
import com.bitato.ut.api.io.throttle.UtOutputStreamThrottleController
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.model.net.internal.InternalMessageStreamFactory
import com.bitato.ut.api.model.net.internal.UtInternalMessageStreamFactory
import com.bitato.ut.api.model.net.wrappers.UtWrapperProvider
import com.bitato.ut.api.model.net.wrappers.WrapperProvider
import com.bitato.ut.sbase.configuration.ServerBaseConfig
import com.bitato.ut.sbase.net.mapper.AbstractRequestMapper
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addFactory
import uy.kohesive.injekt.api.addPerKeyFactory
import javax.net.SocketFactory

object TunnelServerInjektModule : InjektModule {

    override fun InjektRegistrar.registerInjectables() {

        addFactory<AccessKeyLoader> { TcAccessKeyLoader() }
        addFactory<AccessKeyValidator> { TcAccessKeyValidator() }

        addFactory<InternalMessageStreamFactory> { UtInternalMessageStreamFactory }
        addPerKeyFactory<AbstractServer, ServerBaseConfig> { TcProxyServer(it.protocol(), it.port()) }
        addPerKeyFactory<InputStreamThrottleController, Int> { UtInputStreamThrottleController(it) }
        addPerKeyFactory<OutputStreamThrottleController, Int> { UtOutputStreamThrottleController(it) }
        addFactory<RequestMapper> { TcRequestMapper() }
        addFactory<AbstractRequestMapper> { TcRequestMapper() }
        addFactory<SocketFactory> { TcSocketFactory() }
        addFactory<WrapperProvider> { UtWrapperProvider() }
        addFactory<HttpStreamFactory> { UtHttpStreamFactory }
        addFactory<List<AbstractScheduledBackgroundWorker>> { listOf(TcAccessKeyBackgroundWorker()) }

        addFactory<SessionCache> { SessionCache }
    }
}