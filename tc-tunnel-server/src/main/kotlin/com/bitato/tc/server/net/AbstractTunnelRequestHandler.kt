/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.net

import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.thread
import com.bitato.ut.api.util.waitWhile
import com.bitato.ut.sbase.net.handler.AbstractInjectableRequestHandler
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket


abstract class AbstractTunnelRequestHandler(client: Socket, requestWrapper: UtRequestWrapper)
    : AbstractInjectableRequestHandler(client, requestWrapper) {

    /**
     * Handles the relaying of data in cases where there is no certainty as to which way the data
     * will be travelling. Data can be sent from any end-point. The stream ends when one of the two
     * end-points closes their underlying socket.
     *
     * @param remoteIn      The <code>InputStream</code> associated with the remote server
     * @param remoteOut     The <code>OutputStream</code> associated with the remote server
     *
     * @throws java.io.IOException
     * @throws InterruptedException
     */
    protected fun streamIndefinitely(remoteIn: InputStream, remoteOut: OutputStream) {

        log info "Starting streams"
        val threads = listOf(
          //Client to Server
          thread {
              io {
                  remoteOut < clientIn
              }
          },

          //Server to Client
          thread {
              io {
                  clientOut < remoteIn
              }
          })

        waitWhile { threads.all { th -> th.isAlive } }
        log info "Bidirectional data relay complete"
    }

    protected fun closeClient() {
        io { client.close() }
    }
}