/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.net

import com.bitato.ut.api.model.net.NetworkProtocol
import com.bitato.ut.api.model.net.http.HttpStreamFactory
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.thread
import com.bitato.ut.sbase.net.mapper.RequestMapper
import com.bitato.ut.sbase.net.server.AbstractServer
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.Socket
import java.net.SocketTimeoutException

class HttpBouncer(port: Int) : AbstractServer(NetworkProtocol.TCP, port) {

    val httpFactory: HttpStreamFactory = Injekt.get()
    val requestMapper: RequestMapper

    init {
        log info "Creating request mapper"
        requestMapper = Injekt.get()
    }

    override fun handleClientConnection(client: Socket) {

        thread {
            io {
                client.apply {
                    soTimeout = 0
                    keepAlive = true
                }

                try {
                    while (true) {
                        httpFactory.createHttpRequest(client.getInputStream())
                    }
                } catch (ex: SocketTimeoutException) {
                    log info "Connection timed out"
                }
            }
        }
    }
}