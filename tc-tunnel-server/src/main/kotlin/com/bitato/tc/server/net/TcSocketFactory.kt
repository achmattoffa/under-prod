/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.net

import java.net.InetAddress
import java.net.Socket
import javax.net.SocketFactory

class TcSocketFactory : SocketFactory() {
    override fun createSocket(p0: String?, p1: Int): Socket {
        return Socket(p0, p1)
    }

    override fun createSocket(p0: String?, p1: Int, p2: InetAddress?, p3: Int): Socket {
        return Socket(p0, p1, p2, p3)
    }

    override fun createSocket(p0: InetAddress?, p1: Int): Socket {
        return Socket(p0, p1)
    }

    override fun createSocket(p0: InetAddress?, p1: Int, p2: InetAddress?, p3: Int): Socket {
        return Socket(p0, p1, p2, p3)
    }
}