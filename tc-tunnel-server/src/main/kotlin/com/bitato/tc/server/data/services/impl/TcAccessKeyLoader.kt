/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.data.services.impl

import com.bitato.tc.server.data.services.AccessKeyLoader
import java.io.File
import java.util.*

class TcAccessKeyLoader: AccessKeyLoader {
    override fun loadAccessKey(): String {
        return Scanner(File("accesskey.txt")).useDelimiter("\\Z").next().trim()
    }
}