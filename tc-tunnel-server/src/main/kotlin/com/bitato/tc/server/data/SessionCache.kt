/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.data

import java.util.concurrent.ConcurrentHashMap

object SessionCache : ConcurrentHashMap<String, Long>()