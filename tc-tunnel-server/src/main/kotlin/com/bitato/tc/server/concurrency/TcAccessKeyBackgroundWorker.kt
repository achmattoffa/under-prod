/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.concurrency

import com.bitato.tc.server.data.services.AccessKeyValidator
import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

class TcAccessKeyBackgroundWorker : AbstractScheduledBackgroundWorker(60 * 30) { //30 minutes

    private val accessKeyValidator: AccessKeyValidator = Injekt.get()

    override fun run() {
        if (!accessKeyValidator.validateAccessKey()) {
            println("Tunnel access key is no longer valid")
            System.exit(-1)
        }
    }
}