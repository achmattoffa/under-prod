/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.data.services

interface AccessKeyValidator {
    fun validateAccessKey(): Boolean
}


