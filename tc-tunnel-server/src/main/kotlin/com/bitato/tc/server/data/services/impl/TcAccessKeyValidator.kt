/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.data.services.impl

import com.bitato.tc.server.configuration.EnterpriseServerConfig
import com.bitato.tc.server.data.services.AccessKeyLoader
import com.bitato.tc.server.data.services.AccessKeyValidator
import com.bitato.ut.log.util.LoggerBase
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.URL

class TcAccessKeyValidator() : AccessKeyValidator, LoggerBase() {

    private val enterpriseServerConfig = ConfigFactory.create(EnterpriseServerConfig::class.java)
    private val accessKeyLoader: AccessKeyLoader = Injekt.get()

    override fun validateAccessKey(): Boolean {

        log info "Validating tunnel access key"

        try {
            val accessKey = accessKeyLoader.loadAccessKey()

            val url = URL("http://${enterpriseServerConfig.host()}:${enterpriseServerConfig.port()}/$accessKey/validate")
            log info "Connecting to authentication service"
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"

            log info "Authentication process started"
            val responseCode = connection.responseCode
            connection.disconnect()

            if (responseCode == 200) {
                log info "Authentication successful"
                return true
            } else {
                return false
            }
        } catch (ex: ConnectException) {
            log error "Unable to connect to authentication service"
            return false
        } catch (ex: Exception) {
            log error "An unknown error has occurred while verifying tunnel access credentials"
            return false
        }
    }

}