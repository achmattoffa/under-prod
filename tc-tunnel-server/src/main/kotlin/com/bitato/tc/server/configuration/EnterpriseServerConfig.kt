/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.configuration

import org.aeonbits.owner.Config

interface EnterpriseServerConfig : Config {

    @Config.DefaultValue("tsvreg.duckdns.org")
    fun host(): String

    @Config.DefaultValue("80")
     fun port(): Int

}