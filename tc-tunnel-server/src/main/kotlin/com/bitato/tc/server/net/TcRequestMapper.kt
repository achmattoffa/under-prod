/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server.net

import com.bitato.tc.server.configuration.EnterpriseServerConfig
import com.bitato.tc.server.data.SessionCache
import com.bitato.tc.server.data.services.AccessKeyLoader
import com.bitato.tc.server.net.http.TcHttpRequestHandler
import com.bitato.ut.api.model.net.UtInvalidRequestException
import com.bitato.ut.api.model.net.wrappers.ClientRequestType.HTTP_REQUEST
import com.bitato.ut.api.model.net.wrappers.ClientRequestType.INTERNAL_REQUEST
import com.bitato.ut.api.model.net.wrappers.UtRequestWrapper
import com.bitato.ut.api.model.net.wrappers.WrapperProvider
import com.bitato.ut.sbase.net.handler.RequestHandler
import com.bitato.ut.sbase.net.mapper.AbstractRequestMapper
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.net.*

class TcRequestMapper : AbstractRequestMapper() {

    private val sessions: SessionCache = Injekt.get()
    private val enterpriseServerConfig = ConfigFactory.create(EnterpriseServerConfig::class.java)


    override fun mapRequest(incoming: Socket): RequestHandler {

        try {
            val wrapperProvider = Injekt.get<WrapperProvider>()

            incoming.soTimeout = 10000
            val requestWrapper = wrapperProvider.provideRequestWrapper(incoming.inputStream)
            incoming.soTimeout = 0
            
            if (requestWrapper.requestType != INTERNAL_REQUEST) {
                authorizeClient(incoming, requestWrapper.uuid.toLowerCase())
            }

            return getRequestHandler(incoming, requestWrapper)
        }
        catch (ex: SocketTimeoutException) {
            throw UtInvalidRequestException("Unable to determine request type. Socket timed out")
        }
    }

    private fun authorizeClient(incoming:Socket, userName: String) {

        val timer = System.currentTimeMillis() - 1000 * 60 * 30

        if (!sessions.containsKey(userName) || timer >= sessions[userName]!!) {
            if (sessions.contains(userName)) {
                sessions[userName] = System.currentTimeMillis()
            } else {
                sessions.put(userName, System.currentTimeMillis())
            }

            val accessKeyLoader: AccessKeyLoader = Injekt.get()
            val accessKey = accessKeyLoader.loadAccessKey()

            try {
                val clientIP = incoming.remoteSocketAddress as InetSocketAddress

                val url = URL("http://${enterpriseServerConfig.host()}:${enterpriseServerConfig.port()}/$accessKey/connect/$userName?ip=${clientIP.address.hostAddress}")

                val connection = url.openConnection() as HttpURLConnection
                connection.requestMethod = "GET"
                val code = connection.responseCode

                if (code != 200) {
                    throw SecurityException()
                }

                connection.disconnect()

            } catch (ex: ConnectException) {
                log error "Unable to connect to authentication server"
            }
        }
    }

    private fun getRequestHandler(incoming: Socket, requestWrapper: UtRequestWrapper): RequestHandler {
        return when (requestWrapper.requestType) {
            HTTP_REQUEST -> TcHttpRequestHandler(incoming, requestWrapper)
            else -> throw UtInvalidRequestException("Unable to determine request type")
        }
    }
}