/*
 * Copyright (c) 2017. Under Tunnel
 */

package com.bitato.tc.server

import com.bitato.tc.server.configuration.TunnelServerConfig
import com.bitato.tc.server.data.services.AccessKeyValidator
import com.bitato.tc.server.module.TunnelServerInjektModule
import com.bitato.tc.server.net.HttpBouncer
import com.bitato.ut.api.concurrency.AbstractScheduledBackgroundWorker
import com.bitato.ut.log.module.LogBaseInjektModule
import com.bitato.ut.sbase.ServerBaseMain
import com.bitato.ut.sbase.module.ServerBaseInjektModule
import org.aeonbits.owner.ConfigFactory
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.get

class EnterpriseTunnelRunner : Runnable {

    val accessKeyValidator: AccessKeyValidator = Injekt.get()

    override fun run() {

        if (accessKeyValidator.validateAccessKey()) {
            val services = Injekt.get<List<AbstractScheduledBackgroundWorker>>()
            services.forEach(AbstractScheduledBackgroundWorker::start)

            val config = ConfigFactory.create(TunnelServerConfig::class.java)

            if (config.useBouncer()) {
                println("Starting HTTP Bouncer")

                val port = config.bouncerPort()
                val bouncer = HttpBouncer(port)
                bouncer.start()
            }

            startServer(config)


        } else {
            println("Unable to verify tunnel access key")
        }
    }

    companion object EnterpriseTunnelServer : ServerBaseMain() {

        override fun InjektRegistrar.registerInjectables() {
            importModule(ServerBaseInjektModule)
            importModule(TunnelServerInjektModule)
            importModule(LogBaseInjektModule)
        }

        @JvmStatic fun main(args: Array<String>) {
            EnterpriseTunnelRunner().run()
        }
    }
}